<?php

if(!isset($_GET['execute'])) {
	exit("ERRO");
}

date_default_timezone_set('America/Sao_Paulo');
require_once 'config.php';
require_once 'Core/App.php';
require_once 'Core/DB.php';

$DB = new DB();
$DB->CnPrincipal();



$QUERY = "SELECT * from pri_contratos ";//WHERE CONT_ID IN ($CONT_ID) ";
$DB->ExecQuery($QUERY);


$RES = $DB->result_array();



$CONTS = array();
$BASES = array();
foreach ($RES as $DBCLIENTE) {
	$DBNAME = $DBCLIENTE['CONT_DBNAME'];
	if(!in_array($DBNAME, $CONTS)) {
		array_push($CONTS,$DBNAME);
		$BASES[] = $DBCLIENTE;
	}
	
	//echo $FILE . "<br/>";
	//exec("mysqldump -h ".<HOST>." -u ".<usuario>." -p".<senha>." mysql > mysql.sql");
}
$BKS = "";

$dir = "BKS_AUTOMATICOS/";
$ano = date("Y");
$mes = date("m");

if(!is_dir($dir.$ano)) {
	mkdir($dir.$ano);
}

if(!is_dir($dir.$ano."/".$mes)) {
	mkdir($dir.$ano."/".$mes);
}

$dir = $dir.$ano."/".$mes."/";




foreach ($BASES as $BASE) {
	$NN   = strtoupper($BASE['CONT_DBNAME'])."_".date("Ymd_Gi").".sql";
	$FILE = $dir . strtoupper($BASE['CONT_DBNAME'])."_".date("Ymd_Gi").".sql";
	//echo "<br/>";
	$BKS .=  " => " . $NN . "<br/>";
/*	*/
	$pass = "";
	if($BASE['CONT_DBPASS'] != '') {
		$pass = " -p".$BASE['CONT_DBPASS']." ";
	}
	$cmd = "mysqldump -h ".$BASE['CONT_BDHOST']."  -u ".$BASE['CONT_DBUSER']." $pass  --databases ".$BASE['CONT_DBNAME']." > ".$FILE;
	exec($cmd);
/**/
}



$message = "Em: ".date('Y-m-d G:i:s')." O Backup dos bancos:<br/> $BKS <br> estão prontos... ";
//$message = wordwrap($message, 70);


// Inclui o arquivo class.phpmailer.php localizado na pasta class
require_once("vendor/PHPMailer_5.2.0/class.phpmailer.php");
 
// Inicia a classe PHPMailer
$mail = new PHPMailer(true);
 
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
/*
try {
     $mail->Host = 'smtp.sigauto.club'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
     $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
     $mail->Port       = 587; //  Usar 587 porta SMTP
     $mail->Username = 'aviso@sigauto.club'; // Usuário do servidor SMTP (endereço de email)
     $mail->Password = 'sigauto@123'; // Senha do servidor SMTP (senha do email usado)
 
     //Define o remetente
     // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
     $mail->SetFrom('aviso@sigauto.club', 'Sigauto'); //Seu e-mail
     $mail->AddReplyTo('aviso@sigauto.club', 'Sigauto'); //Seu e-mail
     $mail->Subject = 'SigAuto: Backup';//Assunto do e-mail
 
 
     //Define os destinatário(s)
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     $mail->AddAddress('andrestolf@hotmail.com', 'André');
     $mail->AddAddress('emerson@valesys.com.br', 'Emerson');
     $mail->AddAddress('emerson_valesys@hotmail.com', 'Emerson');
     
     
 
     //Campos abaixo são opcionais 
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
     //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
     //$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
 
 
     //Define o corpo do email
     $mail->MsgHTML($message); 
 
     ////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
     //$mail->MsgHTML(file_get_contents('arquivo.html'));
 
     $mail->Send();
     echo "Mensagem enviada com sucesso</p>\n";
 
    //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
      echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
}
*/


/*
$headers  = "MIME-Version: 1.1" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=UTF-8" . PHP_EOL;
$headers .= "From: aviso@alvorada.sigauto.club\n";
$headers .= "From: Sigauto <aviso@alvorada.sigauto.club>\n";
$headers .= "Return-Path: aviso@alvorada.sigauto.club" . PHP_EOL;
$envio = mail("andrestolf@gmail.com", "SigAuto: Backup", $message, $headers);

//$headers .= "From: aviso@alvorada.sigauto.club" . PHP_EOL;

if($envio)
 echo "Mensagem enviada com sucesso!!!";
else
 echo "A mensagem não pode ser enviada";
// Send
/*
if(mail('andrestolf@hotmail.com', 'SigAuto: Backup', $message)){

} else {
	echo "Falha";
}
*/
