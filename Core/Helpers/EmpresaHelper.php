<?php

class EmpresaHelper {
    
    public function ValidaCodigo($EMP_COD) {
        $DB = new DB();
        $DB->CnPrincipal();
    
        //print_r($DB);
        //$XX = "SELECT * FROM pri_contratos WHERE  CONT_CODIGO = '$EMP_COD' AND CONT_BLOQUEADO IS NULL AND (CONT_VALIDADE >= current_timestamp() OR CONT_VALIDADE IS NULL) ";
        $XX = "SELECT * FROM pri_contratos WHERE  CONT_CODIGO = '$EMP_COD' ";
        $empresa = $DB->ExecQuery($XX);
        
        if($empresa->num_rows != 1) {
            return false;
        } 
        
        $EMPRESA = $empresa->fetch_object();
        if(!defined("CLI_DBHOST")) {
            
            $_SESSION['CLI_HOST']      = $EMPRESA->CONT_BDHOST;
            $_SESSION['CLI_DBPORT']    = $EMPRESA->CONT_DBPORT;
            $_SESSION['CLI_DBNAME']    = $EMPRESA->CONT_DBNAME;
            $_SESSION['CLI_DBUSER']    = $EMPRESA->CONT_DBUSER;
            $_SESSION['CLI_DBPASS']    = $EMPRESA->CONT_DBPASS;
            $_SESSION['CLI_CODCFC']    = $EMPRESA->CONT_CODIGOCFC;
            
            
            
            
            $_SESSION['CLI_DBCHARSET'] = "utf8";            
        }
        
        $_SESSION['APP_LOCALNOME']          = $EMPRESA->CONT_LOCALNOME;
        $_SESSION['APP_LOCALFANTASIA']      = $EMPRESA->CONT_NOMEFANTASIA;
        $_SESSION['APP_LOCALLOGRADOURO']    = $EMPRESA->CONT_LOGRADOURO;
        $_SESSION['APP_LOCALCOMPLEMENTO']   = $EMPRESA->CONT_COMPLEMENTO;
        $_SESSION['APP_LOCALBAIRRO']        = $EMPRESA->CONT_BAIRRO;
        $_SESSION['APP_LOCALCIDADE']        = $EMPRESA->CONT_CIDADE;
        $_SESSION['APP_LOCALESTADO']        = $EMPRESA->CONT_ESTADO;
        $_SESSION['APP_LOCALCEP']           = $EMPRESA->CONT_CEP;
        $_SESSION['APP_LOCALCPFCNPJ']       = $EMPRESA->CONT_CPFCNPJ;
        $_SESSION['APP_LOCALRGIE']          = $EMPRESA->CONT_RGIE;
        $_SESSION['APP_LOCALFONE']          = $EMPRESA->CONT_FONE;
        $_SESSION['APP_LOCALCELULAR']       = $EMPRESA->CONT_CELULAR;
        $_SESSION['APP_LOCALCODIGO']        = $EMPRESA->CONT_CODIGO;

        @$_SESSION['APP_LOCALAGENCIA']       = $EMPRESA->CONT_AGENCIA;
        @$_SESSION['APP_LOCALCONTA']         = $EMPRESA->CONT_CONTA;

        $_SESSION['APP_LOCALIMPCONTRATO']   = $EMPRESA->CONT_IMPCONTRATO;
        $_SESSION['APP_PATHGERAL']          = $EMPRESA->CONT_PATHGERAL;
        $_SESSION['APP_PATHCORE']           = $EMPRESA->CONT_PATHCORE;
        $_SESSION['APP_PATHAPP']            = $EMPRESA->CONT_PATHAPP;
        
        return $EMPRESA;
    }

    public function __destruct() {
        
    }
    
}