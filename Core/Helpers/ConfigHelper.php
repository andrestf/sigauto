<?php

class ConfigHelper {

	private $DB, $LOCALID;

	public function __construct($loadDB = true) {
        
		
		if($loadDB) {
			$DB = new DB();
	       	$DB->CnCliente();
	    	

	        $this->DB = $DB;		
		}

		if(!isset($_SESSION['APP_LOCALID'])) {
			exit("ERRO: sessao nao iniciada!");
		} else {
			$this->LOCALID = $_SESSION['APP_LOCALID'];
		}

	}


	public function Autoload() {
		$XX = "SELECT cf_value FROM _configs WHERE cf_name = '__autoload' AND cf_localid = '$this->LOCALID' ";
		$res = $this->DB->ExecQuery($XX);
		
		if($res->num_rows == 1) {
			$res = $this->DB->result_array();
			$res = $res[0]['cf_value'];
			eval($res);
		}
 
	}

	public function Get($key="",$execute = FALSE) {

		$XX = "SELECT cf_value FROM _configs WHERE cf_key = '$key' AND cf_localid = '$this->LOCALID' ";
		$res = $this->DB->ExecQuery($XX);
		
		if($res->num_rows == 1) {
			
			$res = $this->DB->result_array();
			$res = $res[0]['cf_value'];
			
			if($execute) {
				//$res = str_replace('"', "'", $res);
				//$res = str_replace("'", "\'", $res);
				
				eval($res);
			} else {
				return $res;
			}
		} else  {
			return "[$key nao encontrada]";
		}
       
	}

	public function UpdateMatricula($adiciona = 1) {
		$update = "UPDATE _configs SET cf_value = cf_value+$adiciona WHERE cf_key = 'nMatricula' and cf_codigo = 'GERAL' AND cf_localid = '$this->LOCALID'";
		$res = $this->DB->ExecNonQuery($update);
	}



	public function GetVars($local = '', $cod = 'GERAL', $exec = FALSE) {
		
		if($local == "" or $local = null) {
			$local = $this->LOCALID;
		}
		
		if($cod == "" or $cod == NULL) {
			$cod = "GERAL";
		}
		
		if(isset($_SESSION['APP_VARS'][$local."_".$cod])) {
			//return $_SESSION['APP_VARS'][$local."_".$cod];
		}
		/**/

		#####################################################################################################
		$ret = "";
		$queryGeral = "SELECT var_content as VARS FROM sis_vars WHERE var_localid = '0' and var_cod = 'GERAL' ";
		$resGeral = $this->DB->ExecQuery($queryGeral);
		$resGeral = $this->DB->result_array();
		$ret .= $resGeral[0]['VARS'];
		#####################################################################################################



		$query = "SELECT var_content as VARS FROM sis_vars WHERE var_localid = '$local' and var_cod = '$cod' ";
		$res = $this->DB->ExecQuery($query);

		if($res->num_rows == 1) {
			$res = $this->DB->result_array();
			$ret .= $res[0]['VARS'];
		}

		$_SESSION['APP_VARS'][$local."_".$cod] = $ret;

		if($exec) {
			eval($ret);	
		} else {
			return $ret;
		}
	}

    
    
}