<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RedirectHelper
 *
 * @author andrestolf
 */
class RedirectHelper extends App {
    
    public $controller_redir_to, $action_redir_to;
    
    
    public function Redirecionar() {
        $link = $this->Link($this->controller_redir_to,  $this->action_redir_to);
        header("location: $link");
    }
    
    public function Redir($c,$a= "",$gets = array()) {
        
        
        $params = "";
        if($gets) {
            foreach ($gets as $param => $valor) {
                $params .= "$param=$valor&";
            }
            
            $params = substr($params,0,-1);
        }
        

        
        if(USE_REWRITEMOD) {
            $r = "/$c/$a/?$params";
        } else {
            $r = "/index.php?route=$c/$a/&$params";
        }
        
        header("location: $r");
    }    
}
