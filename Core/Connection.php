<?php

@$pri_con = new mysqli(PRIN_DBHOST,PRIN_DBUSER,PRIN_DBPASS,PRIN_DBNAME);

if(mysqli_connect_errno()) {
    //echo mysqli_connect_error();
    echo "<h1>Erro ao conectar no banco de dados!</h1>";
    exit();    
} else {
    $pri_con->autocommit(TRUE);
    $pri_con->set_charset(PRIN_DBCHARSET);
}