<?php


class Reports extends Controllers {


	public $DB;
	private $report;

	#arquivo do report;
	private $reportFile;

	#dados a passar para a select ($_POST)
	private $reportDados;

	#model responsavel pela select
	private $modelFile;

	#query executada
	private $query; 

	# Conteudo  memo do banco( tabela sis_reports)
	private $reportMemo;

	# conteudo  do report (resultado da select)
	private $reportContent;

	#configurações de impressão
	private $printConf;
	private $pdf;
	private $marginR,$marginL,$marginT,$marginB, $pageSize;

	##
	private $header, $footer;


	public function __construct()
	{
		parent::__construct();
		
		
		$this->DB = new DB;

		
		$this->DB->CnCliente();

		$this->ID_LOCAL = $_SESSION['APP_LOCALID'];

		$this->pdf = false;
		$this->pageSize = "A4";
		$this->marginR = 5;
		$this->marginL = 5;
		$this->marginT = 5;
		$this->marginB = 5;
		$this->fontSize = "12px";

		eval($this->vars);

		$this->header = $reportHeader;
		
		$this->footer = $reportFooter . '
		<script>
			$( function( ){
				$(".main-footer").remove();	
			});
		</script>
		';		

	}
	
	public function g($localNome) {
		$query = "SELECT * FROM sis_reports WHERE (rep_localid = '$this->ID_LOCAL' OR rep_localid = '0') and rep_local = '$localNome' ORDER BY rep_ordem DESC, rep_nome";
		$x = $this->DB->ExecQuery($query);

		if($x->num_rows >= 1) {
			$retorno = $this->DB->result_array();
			return $retorno;        	
       	}

       	return false;
	
	}


	public function SetReport($valor) {
    	if($valor == "" || $valor == NULL) {
    		$valor = "'public/noreport'";
    	}

    	$query = "SELECT * FROM sis_reports WHERE rep_id = $valor AND (rep_localid = '0' OR rep_localid = '".$_SESSION['APP_LOCALID']."')  ";
    	$c = $this->DB->ExecQuery($query);


    	$extras  = "";

    	$extras .= '$campos = "*"; ';
    	$extras .= '$orderBy = NULL;';
    	$extras .= '$groupBy = NULL;';
    	$extras .= '$condicaoExtra = NULL;';
    	$extras .= '$join = NULL;';

    	if($c->num_rows <= 0) {
    		$this->reportFile    = $valor;
    	} else {
    		$c = $this->DB->result_array();
    		$this->reportFile    = $c[0]['rep_arquivo'];
   		
    		//GET EXTRAS
    		$extras .= $c[0]['rep_memo'];
    		$this->printConf = $c[0]['rep_impconf'];	
    	} 	




    	$this->reportMemo = $extras;

        
    }

    public function GetReport() {
        return $this->reportFile;
    }    

    public function SetPrintConfs() {
    	if($this->printConf != '') {
	    		eval($this->printConf);

    			( isset($pdf) ) 	  ? $this->pdf      = $pdf      : "";
				( isset($pageSize) )  ? $this->pageSize = "'$pageSize'" : "";
				( isset($marginR) )   ? $this->marginR  = $marginR  : "";
				( isset($marginL) )   ? $this->marginL  = $marginL  : "";
				( isset($marginT) )   ? $this->marginT  = $marginT  : "";
				( isset($marginB) )   ? $this->marginB  = $marginB  : "";
				( isset($fontSize) )  ? $this->fontSize  = "'$fontSize'" : "";

    	}
    }


	public function SetModel($valor) {
        $this->modelFile = $valor;
    }

    public function GetDados() {
    	return $this->reportDados;
    }

	public function SetDados($valor) {
        $this->reportDados = $valor;
    }


    public function GetQuery() {
    	return $this->query;
    }

    public function GerarQuery() {
    	$r = new $this->modelFile();
    	$query = $r->GerarQuery($this->reportDados,$this->reportMemo);

    	$this->query = $query;
    }


    public function Gerar() {

		
    	$dados = $this->GetDados();
    	if($dados == "" || $dados == NULL) {
    		return "ERRO -> DADOS NÃO GERADA"; }

		$this->GerarQuery();
    	$query = $this->GetQuery();
    	if($query == "" || $query == NULL) {
    		return "ERRO -> QUERY NÃO GERADA"; }


    	$c = $this->DB->ExecQuery($query);
    	if($c->num_rows >= 1) {
    		$res = $this->DB->result_array();
    		$this->reportContent = $res;
    		return $res;
    	}

    	$this->reportContent = false;
    	return false;
    }	

    public function Render() {	

    	$datas = array();
    	$datas['dados'] = $this->reportContent;

    	$datas['header'] = $this->header;
    	$datas['footer'] = $this->footer;
		


		if($datas['dados'] == "") {
			$datas['dados'] = array();
		}

		$this->SetPrintConfs();
		


		if($this->pdf) {
			/* */
            ob_start();
            $txt = $this->RenderView($this->GetReport(), $datas, "public/header", false, false);	
            $txt = ob_get_contents();
            
            ob_clean();
            
            $mpdf = new mPDF("BLANK",$this->pageSize,$this->fontSize,"",$this->marginB,$this->marginB,$this->marginB,$this->marginB);
            $mpdf->WriteHTML($txt);
            $mpdf->Output();       


            
            
		} else {
			//$this->RenderView($this->GetReport(), $dados, "public/header", "public/footer", false);
			$this->RenderView($this->GetReport(), $datas, "public/header", "public/footer", false);	

			return;
		}
    	
    }
}



