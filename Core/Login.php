<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author andrestolf
 */
class Login {
    public function index() {
        echo "po";
    }

    public function ValidaLogin() {
        global $url;

        $redir = new RedirectHelper();

        if(
          !isset($_SESSION['APP_KEY'])      ||
          !isset($_SESSION['APP_USUKEY'])   ||
          !isset($_SESSION['APP_USUID'])    ||
          !isset($_SESSION['APP_USUNOME'])  ||
          !isset($_SESSION['APP_USUNOMEC'])
        ){

            if(strtolower($url[0]) != 'login') {
                $redir->controller_redir_to = 'login';
                $redir->Redirecionar();
            }
        } else {
            $this->ValidaUsuarioLogado();
        }

    }


    public function ValidaUsuarioLogado() {
        $DB = new DB();
        $DB->CnCliente();
        
        //AQUI VALIDA O HASH DO LOGIN
        $Q = "SELECT * FROM sis_usuarios WHERE usu_id = '".$_SESSION['APP_USUID']."' AND usu_hash = '".$_SESSION['APP_USUKEY']."' AND usu_localid = '".$_SESSION['APP_LOCALID']."' AND usu_sitacesso = 'A' ";

        //AQUI NAO VALIDA O HASH DO LOGIN
        //$Q = "SELECT * FROM sis_usuarios WHERE usu_id = '".$_SESSION['APP_USUID']."' AND usu_localid = '".$_SESSION['APP_LOCALID']."' ";

        $Q = $DB->ExecQuery($Q);

        if($Q->num_rows != '1'){
            session_destroy();
            $redir = new RedirectHelper();
            $redir->controller_redir_to = "login";
            $redir->Redirecionar();
        } else {
            $uri =  "$_SERVER[REQUEST_URI]";
            $usuario = $Q->fetch_object();
            $ID = $usuario->usu_id;
            $DB->ExecNonQuery("UPDATE sis_usuarios SET usu_lastlogincheck = current_timestamp(), usu_lasturi = '$uri' WHERE usu_id = '$ID' ");
        }
    }


    public function LogarUsuario($usuario,$senha,$localid) {
        $DB = new DB();
        $DB->CnCliente();

        $usuario = $DB->Prepare($usuario);
        $senha = $DB->Prepare($senha);
        $Q = "SELECT * FROM sis_usuarios WHERE usu_login = '$usuario' AND usu_senha = '$senha' AND  usu_localid = '$localid' ";

        $usuario = $DB->ExecQuery($Q);

        if($usuario->num_rows != '1') {

            $controller = new Controllers();

            $datas = array(
                "sidebar"     => false,
                "header" => false,
                "footer" => false,
                "viewDatas" => array( 
                    "id" => 1,
                    "retorno" => '<div class="alert alert-info">Usuário ou senha inválidos!</div>'

                )
            );

            $controller->RenderView("login",$datas,false,false,false);

        } else {
            $usuario = $usuario->fetch_object();
            $ID = $usuario->usu_id;
            $ID_PERMGRP = $usuario->usu_permgrupo;
            $hash = md5(sha1(time()));
            $DB->ExecNonQuery("UPDATE sis_usuarios SET usu_hash ='$hash', usu_lastlogin = current_timestamp() WHERE usu_id = '$ID' ");

            if($usuario->usu_sitacesso == "I") {
                $controller = new Controllers();

                $datas = array(
                        "sidebar"   => false,
                        "header"    => false,
                        "footer"    => false,
                        "viewDatas" => array( 
                        "id"        => 1,
                        "retorno"   => '<div class="alert alert-warning">Usuário Inativo!</div>'
                        
                        )
                );

                $controller->RenderView("login",$datas,false,false,false);
                return false;
            }


            $_SESSION['APP_KEY'] = sha1(md5(time()*round(5,10)));
            $_SESSION['APP_LOCALID'] = $localid;

            # $_SESSION['APP_LOCALNOME'] = Empresas Helper
            $_SESSION['APP_LOCALNOME'] = $_SESSION['APP_LOCALNOME'];
            $_SESSION['APP_USUKEY'] = $hash;
            $_SESSION['APP_USUID'] = $ID;



            $_SESSION['APP_USUNOMEC'] = $usuario->usu_nomecompleto;

            $_SESSION['APP_USUNOME'] = substr($usuario->usu_nomecompleto,0,strpos($usuario->usu_nomecompleto," "));

            $permissoes = new Permissoes();
            $perms_usua = $permissoes->GetPermissoes($ID_PERMGRP,$ID); // pegar permissoes do usuário

            $redir = new RedirectHelper();
            $redir->controller_redir_to = "alunos";
            $redir->Redirecionar();

        }






    }


}
