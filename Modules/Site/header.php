<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $view_PageTitle;?></title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- <link rel="stylesheet" href="/Public/css/bootstrap.css"> -->
  <link rel="stylesheet" href="/Public/plugins/bootstrap/css/bootstrap.min.css">
  


  <link rel="stylesheet" href="/Public/css/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/Public/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/Public/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" href="/Public/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="/Public/plugins/datatables/jquery.dataTables.min.css">
  
  <link rel="stylesheet" href="/Public/plugins/jQueryUI/jquery-ui.min.css">

  <link rel="stylesheet" href="/Public/css/style.css"> 
  <link rel="stylesheet" href="/Public/plugins/alertify/css/alertify.min.css"> 
  <link rel="stylesheet" href="/Public/plugins/alertify/css/themes/bootstrap.css"> 
  
  <link rel="stylesheet" href="/Public/css/asec-print.css" media="print"> 
  
 
  <script src="/Public/plugins/jQuery/jquery-2.2.3.min.js"></script>
  
  <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script> 
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-purple-light layout-boxed">

<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header  ">

    <!-- Logo -->
    <a href="<?php echo $this->Link("index")?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Sig</b>Auto</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SigAuto</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="#" class="user-image hide" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo $_SESSION['APP_USUNOME']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="#" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['APP_USUNOMEC']; ?>
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            &nbsp;
          </li>
        </ul>
      </div>
    </nav>
  </header>
  