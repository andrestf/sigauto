<div class='box-header'>
            
                <div style="font-size: 24px; width: 100%; text-align: center;">
                    <b>
                         CONTRATO DE PRESTAÇÃO DE SERVIÇOS
                    </b>
                </div>
            
        </div>

        <div class='box-body' style="line-height: 14px; padding: 0px; text-align: justify">
                <p> 
                    &nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo $_SESSION['APP_LOCALNOME'] ?></b>, sob o nome Fantasia <b><?php echo $_SESSION['APP_LOCALFANTASIA'] ?></b>, CNPJ: <b><?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?></b>, com sede na <b><?php echo $_SESSION['APP_LOCALLOGRADOURO'] ?></b> – <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?></b>/<b><?php echo $_SESSION['APP_LOCALESTADO'] ?></b>, aqui denominada Contratada, tem entre si justo e contratado o que se segue:
                </p>


                <p>
                <center><b><u>OBJETO DO CONTRATO</u></b></center>
                         Cláusula 1 – É objeto do presente contrato a prestação de serviço de Aulas Práticas de Direção nas Categorias A,B,C,D ou E, o uso dos veículos para Exames Práticos, Recolhimento das taxas previstas na Legislação, Marcação dos Exames Práticos e Teóricos junto a 144ª Ciretran de <?php echo $_SESSION['APP_LOCALCIDADE'] ?>/SP. Em anexo, ficará uma Nota Promissória emitida pelo aluno(a) no valor da matrícula, a qual fará parte do processo até a sua integral conclusão. O valor total do serviço contratado, foi apresentado ao aluno, o qual será pago nas seguintes condições: <br>
                         Valor Total:_____________, Entrada:_____________, Parcelamento:_____________, (   ) Cartão  (   ) Cheque  (   ) Promissórias.
                </p>


                <p>
                    <center><b><u>OBRIGAÇÕES DO CONTRATANTE</u></b></center>
                         Cláusula 2 – O Contratante deverá fornecer a Contratada, para abertura do processo de habilitação junto ao Poupatempo/ 144ª Ciretran de <?php echo $_SESSION['APP_LOCALCIDADE'] ?>/SP, a cópia dos seus documentos pessoais (Rg, Cpf ou Rg Atualizado), Telefones para contato, bem como um Comprovante de Residência (recente), não sendo exigido que esteja em seu nome, porém responsabilizando-se civil e criminalmente pelo endereço declarado junto ao órgão de trânsito (DETRAN/SP)
                </p>

            
                <p>
                    <center><b><u>OBRIGAÇÕES DA CONTRATADA</u></b></center>
                         Cláusula 3 – A contratada deverá fornecer ao contratante os recibos de pagamentos, bem como a cópia do presente contrato, se este for solicitado pelo contratante no ato da matrícula.<br>
                         Cláusula 4 – A contratada se compromete a cumprir todas etapas que lhe cabem e ministrar os cursos dentro das normas estabelecidas de Código de Trânsito Brasileiro.
                </p>

            
                <p>
                    <center><b><u>DOS CURSOS DE CARRO, MOTO, ADIÇÃO E MUDANÇA DE CATEGORIAS</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 5 – Os cursos mencionados no caput desta cláusula terão validade de 12 (doze) meses a contar da data do exame médico, sendo que o aluno NÃO será notificado da validade do vencimento do processo pela Auto Escola e terá todos os seus exames, cursos, taxas cancelados automaticamente pelo Detran-SP após o prazo deste. Ficando o Contratante obrigado a quitar os débitos pendentes com a Contratada e resgatar a Nota Promissória emitida no inicio do curso.<br>
                         Parágrafo 1 – No caso de “desistência” ou “transferência” do curso contratado, o valor será devolvido pela contratada, deduzido o valor integral da matrícula que é de R$ 280,00 (duzentos e oitenta reais), as aulas práticas de direção realizadas pelo contratante, bem como as taxas recolhidas para a marcação dos exames.<br>
                         Parágrafo 2 – No caso do aluno estar matriculado nos cursos A/B, e por ventura solicitar a desistência de uma das duas categorias, será deduzido o valor referente a meia matricula, ou seja R$ 140,00 (cento e quarenta reais) e as aulas práticas que o aluno tenha assistido.<br>
                         Cláusula 6 – A marcação de aulas será de acordo com a disponibilidade da agenda da Auto Escola, sendo possível agendar no máximo 3 (três) aulas por dia, até atingir o número de aulas exigidos pelo Detran-SP, após serão cobradas as aulas extras no valor de R$ 60,00 (sessenta Reais) cada aula, e o cancelamento das aulas, só será possível salvo se o aluno comunicar a Contratada com o prazo de 3 dias (72 horas) antes da aula marcada.<br>
                         Parágrafo 1 – O não comparecimento no horário agendado, acarretará para o aluno o custo da “remarcação” da aula que é de R$ 60,00 (setenta reais) e a aula só será efetivada após a realização da mesma através do sistema Biométrico do Detran-SP. O que possibilitará desta forma a Contratada emitir o Certificado de conclusão das aulas práticas e desta forma agendar o Exame Prático do aluno..<br>
                         Cláusula 7 – Em relação às aulas práticas de Moto, mesmo nos dias Chuvosos, o aluno deverá comparecer as aulas, sendo que a contratada disponibiliza aos seus alunos Capa de Chuva para a realização da mesma, o que ocorre também no dia do Exame prático, possibilitando desta forma o exame ser realizado normalmente.<br>
                         Cláusula 8 – Nos casos de reprovação nos exames o Contratante deverá agendar um novo exame dentro dos prazos estabelecidos pelo Detran-SP, sendo que o Exame Teórico o Contratante fará o agendamento diretamente junto a Contratada a um custo de R$ 70,00 (setenta Reais), já em relação ao Exame Prático deverá ser agendado junto a Contratada a um custo de R$ 180,00 (cento e oitenta reais) com o prazo mínimo de 10 (Dez) dias de Antecedência em relação ao novo exame.<br>
                         Cláusula 9 – Para agendar um novo Exame Prático, este só será agendado desde que o pagamento das parcelas junto a contratada esteja plenamente em dia.<br>
                         Cláusula 10 – Ao final dos cursos, para os planos feitos através de “Promissórias", o contratante concorda que para retirar a CNH, terá que acertar o débito restante mediante o pagamento total à vista em Dinheiro, ou Cartão de Crédito ou Cheque Pré datados podendo ser pago nas mesmas condições acordadas anteriormente.<br>
                         Cláusula 11 – Sem mais, as partes assinam e de comum acordo elegem o Foro de Botucatu, para dirimir possíveis dúvidas oriundas deste, renunciando a qualquer outro por mais privilegiado que seja.
            </p>

        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?>, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row" style="text-align: center;">
            <div class="col-xs-6" style="border-top: solid 1px; width: 45%; float: left;">
                <center>
                    CONTRATANTE<br>
                    <?php echo $Aluno->usu_nomecompleto; ?><br>
                    <?php echo $Aluno->usu_cpf; ?>
                </center>
            </div>
            
            <div class="col-xs-6" style="border-top: solid 1px; width: 45%; float: right;">
                <center>
                    CONTRATADA <br>
                    <?php echo $_SESSION['APP_LOCALNOME'] ?> - <?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?>
                </center>
            </div>
        </div>