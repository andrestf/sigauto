<div class="content" style="padding: 0px;">
    <div class='box box-primary' style="border: none; padding: 0px;" >
        <div class='box-header'>
            <center>
                <div style="border: solid 2px #000; font-size: 24px;">
                    <b>
                    <u> AUTO ESCOLA JÓIA DA FÊNIX </u>
                    </b>
                </div>
            </center>
        </div>

        <div class='box-body' style="line-height: 16px; padding: 0px; text-align: justify">
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pelo &nbsp; presente &nbsp; instrumento &nbsp; particular &nbsp; de &nbsp; contrato, &nbsp; entre &nbsp; as &nbsp; partes &nbsp; de &nbsp; um &nbsp; lado &nbsp; denominado <b>CONTRATADO</b>, 
                    <br> &nbsp; esta &nbsp; empresa &nbsp; <b><?php echo $_SESSION['APP_LOCALFANTASIA'] ?></b>, telefone: <?php echo $_SESSION['APP_LOCALFONE'] ?>, &nbsp; &nbsp; situada á &nbsp; <b><?php echo $_SESSION['APP_LOCALLOGRADOURO'] ?></b>, 
                    <br> &nbsp; portador do CNPJ: &nbsp; <b><?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?></b>, e de outro lado, denominado <b>CONTRATANTE</b> está o Sr.(a):
                    <br> &nbsp; <b> <?php echo $Aluno->usu_nomecompleto; ?>, RG: &nbsp; <?php echo $Aluno->usu_rg; ?>, CPF: &nbsp; <?php echo $Aluno->usu_cpf; ?> </b>
                    <br> &nbsp; residente e domiciliado á &nbsp; <?php echo $Aluno->usu_logradouro; ?>, Nr. &nbsp; <?php echo $Aluno->usu_logranumero; ?>, no bairro: &nbsp; <?php echo $Aluno->usu_lograbairro; ?>
                    <br> &nbsp; telefone: &nbsp; <?php echo $Aluno->usu_telcelular?> &nbsp; combinado o presente termo nas condições abaixo expostas:
                    <br>
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Primeira: </b> &nbsp; obriga-se o contratado, a ministrar as aulas práticas ao contratante de acordo com o plano 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                constante nesse contrato;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Segunda: </b> &nbsp; caberá ao contratado, a distribuição de documentação prevista e exigida para inicio dos 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                processos de habilitação, não incluso no preço total desta;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Terceira: </b> &nbsp; o contratante terá um prazo de 45 (Quarenta e cinco) dias para fornecer á Auto-Escola documentos 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                e exames necessários tais como: &nbsp; Exame de vista, Psicotécnico, fotos, comprovantes de 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                residência atual e outros que não fazem parte integrante deste;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Quarta: </b> &nbsp;&nbsp; o contratante realizará o curso teórico/técnico/simulador/aulas práticas previsto conforme tabela:
                <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Valor Total: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="10" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Data: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="10" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Forma de Pgto: &nbsp; <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="20"/>
                <br>
                </p>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 6ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 7ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 8ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 9ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 10ª Parcela: R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br> <br>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Quinta: </b> &nbsp; o contratante receberá sua carteira de habilitação somente com todos os <u>DÉBITOS</u> liquidados, ou
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                em caso de parcelamento, se as parcelas estiverem com pagamento em dia.
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Sexta: </b> &nbsp; o presente sofrerá majoração nos seguintes casos: se houver determinação dos órgãos componentes 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                (CNP e taxas estaduais), inclusive aulas práticas e ou, acréscimo de aulas e outros serviços, caso 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                atraso o pagamento será cobrado de acordo com a tabela constante na Auto-Escola;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Sétima: </b> &nbsp; pagar em dia as despesas extras contratuais como: Aulas adicional, taxas de inscrição, expedição e 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                reenclusão serviços e reenclusão de exames, complemento de curso, aluguel do veículo para exame,
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                aulas de simulador caso ocorrer decorrentes e necessárias ao andamento normal do processo de
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                habilitação sob pena de não ser cumprida a obrigação pelo contratante no prazo de 30(Trinta)
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                dias a contar do fato que se originou a despesa extra, ficando inadimplente, bem como;
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                perdendo os valores já pagos e direitos adquiridos até a presente data;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Oitava: </b> &nbsp; as partes derem causa de rescisão contratual perderão valores já pagos e direitos adquiridos, bem
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                como, também indenizará a outra 30% (Trina por cento) do valor deste, e comissão de vendas, 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                bancos, cartórios, taxas estaduais, curso teórico, exame de vista e psicotécnico. &nbsp;&nbsp; Em caso
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                de desistência e transferência por parte do aluno, será cobrado a matricula no valor de 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                R$ 250,00 (Duzentos e cinquenta reais);
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Nona: </b> &nbsp; as partes de comum elegem o fórum de BOTUCATU para esclarecer qualquer dúvida oriunda deste, 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                renunciando a qualquer outro privilégio que seja;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Décima: </b> &nbsp; o aluno que não comparecer no horário marcado ou não avisar 24hs com antecedência no CFC
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Jóia da Fênix, está ciente que pagará o valor de R$ 60,00 na categoria B e R$ 60,00 na categoria A, 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                para o reagendamento da aula perdida.
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E, por estarem justo contratado, assinam o presente termo em 2(duas) vias de igual teor,na presença de testemunha
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                que a tudo viram e assistiram.
                </p>

        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b>BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row">
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    <b> <?php echo $_SESSION['APP_LOCALNOME'] ?> </b>
                </center>
            </div>
            <div class="col-xs-2"></div>
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    <b> <?php echo $Aluno->usu_nomecompleto; ?> </b>
                </center>
            </div>
        </div>
    </div>
</div>
        


<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>