<div class="content" style="padding: 0px;">
    <div class='box box-primary' style="border: none; padding: 0px;" >
        <div class='box-header'>
            <center>
                <div style="border: solid 2px #000; font-size: 24px;">
                    <b>
                    <u> AUTO ESCOLA JÓIA DE FÊNIX </u>
                    </b>
                </div>
            </center>
        </div>

        <div class='box-body' style="line-height: 16px; padding: 0px; text-align: justify">
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pelo &nbsp; presente &nbsp; instrumento &nbsp; particular &nbsp; de &nbsp; contrato, &nbsp; entre &nbsp; as &nbsp; partes &nbsp; de &nbsp; um &nbsp; lado &nbsp; denominado <b>CONTRATADO</b>, 
                    <br> &nbsp; esta &nbsp; empresa &nbsp; <b><?php echo $_SESSION['APP_LOCALFANTASIA'] ?></b>, telefone: <?php echo $_SESSION['APP_LOCALFONE'] ?>, &nbsp; &nbsp; situada á &nbsp; <b><?php echo $_SESSION['APP_LOCALLOGRADOURO'] ?></b>, 
                    <br> &nbsp; portador do CNPJ: &nbsp; <b><?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?></b>, e de outro lado, denominado <b>CONTRATANTE</b> está o Sr.(a):
                    <br> &nbsp; <b> <?php echo $Aluno->usu_nomecompleto; ?>, RG: &nbsp; <?php echo $Aluno->usu_rg; ?>, CPF: &nbsp; <?php echo $Aluno->usu_cpf; ?> </b>
                    <br> &nbsp; residente e domiciliado á &nbsp; <?php echo $Aluno->usu_logradouro; ?>, Nr. &nbsp; <?php echo $Aluno->usu_logranumero; ?>, no bairro: &nbsp; <?php echo $Aluno->usu_lograbairro; ?>
                    <br> &nbsp; telefone: &nbsp; <?php echo $Aluno->usu_telcelular?> &nbsp; combinado o presente termo nas condições abaixo expostas:
                    <br>
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Primeira: </b> &nbsp; obriga-se o contratado, a ministrar curso teórico-técnico, aulas de simulador e aulas práticas 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                ao contratante de acordo com a portaria 101 DETRANSP;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Segunda: </b> &nbsp; caberá ao contratado, a distribuição de documentação prevista e exigida para inicio dos 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                processos de habilitação, não incluso no preço total desta;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Terceira: </b> &nbsp; o contratante terá um prazo de 45 (Quarenta e cinco) dias para fornecer á Auto-Escola documentos 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                e exames necessários tais como: &nbsp; CHN, RG, CPF, Comprovantes de residência, Foto 3X4,  
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Certidão de casamento, Exame Toxicológico, de vista e Psicotécnico ;
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Quarta: </b> &nbsp;&nbsp; o contratante realizará o curso teórico/técnico/simulador/aulas práticas previsto conforme tabela:
                <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Valor Total: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="10" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Data: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="10" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Forma de Pgto: &nbsp; <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="20"/>
                <br>
                </p>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 7ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 8ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 9ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 10ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 11ª Parcela: R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6ª Parcela: &nbsp; R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp; 12ª Parcela: R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="" size="8" onkeyup="formataValor(this)"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Venc: &nbsp; <input type="text" style="border: 0px; text-align: right" placeholder="" size="7" class="datepicker"/>
                <br> <br>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Quinta: </b> &nbsp; o contratante receberá sua carteira de habilitação somente com todos os <u>DÉBITOS</u> liquidados, ou
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                em caso de parcelamento, se as parcelas estiverem com pagamento em dia.
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Sexta: </b> &nbsp; o presente sofrerá majoração nos seguintes casos: se houver determinação dos órgãos componentes 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                (taxas estaduais, etc), inclusive aulas teóricas, simulador, práticas e ou, acréscimo de aulas e outros 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                serviços caso atraso o pagamento será cobrado de acordo com a tabela constante na Auto-Escola;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Sétima: </b> &nbsp; pagar em dia as despesas extras contratuais como: Aulas adicional, taxas de inscrição, expedição e 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                reenclusão serviços e reenclusão de exames, complemento de curso, aluguel do veículo para exame,
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                aulas de simulador caso ocorrer decorrentes e necessárias ao andamento normal do processo de
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                habilitação sob pena de não ser cumprida a obrigação das mesmas ficará inadimplente, sabendo
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                que o contratante tem prazo de 12 meses para conclusão da CNH a partir da data do exame de vista;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp; <b> * Cláusula Oitava: </b> &nbsp; caso contratante decida pela rescisão contratual perderão valores já pagos, direitos adquiridos,
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                custas processuais bem como, também indenizará o contratado em 30% (Trina por cento) do valor 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                deste, comissão de vendas, bancos, cartórios, taxas estaduais, exame de vista, psicotécnico, teórico,
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                simulador, aulas e exames práticos. Em caso de desistência e ou transferência por parte do 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                contratante, será cobrado a matricula no valor de R$ 350,00 (Trezentos e cinquenta reais);
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Nona: </b> &nbsp; as partes de comum elegem o fórum de <?php echo $_SESSION['APP_LOCALCIDADE'] ?> para esclarecer qualquer dúvida oriunda, 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                renunciando a qualquer outro privilégio que seja;
                </p>

                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> * Cláusula Décima: </b> &nbsp; o aluno que não comparecer no horário marcado ou não avisar 24hs com antecedência no CFC
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Jóia da Fênix, está ciente que pagará o valor de R$ 70,00 na categoria B e R$ 60,00 na categoria A, 
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                para o reagendamento da aula perdida.
                </p>

                <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E, por estarem justo contratado, assinam o presente termo em 2(duas) vias de igual teor,na presença de testemunha
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                que a tudo viram e assistiram.
                </p>

        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?>, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row">
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    <b> <?php echo $_SESSION['APP_LOCALNOME'] ?> </b>
                </center>
            </div>
            <div class="col-xs-2"></div>
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    <b> <?php echo $Aluno->usu_nomecompleto; ?> </b>
                </center>
            </div>
        </div>
    </div>
</div>
        


<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>