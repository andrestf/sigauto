<?php
$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
#$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->CondicaoExtra = " AND amf_dataprocesso = '$data'";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<style>
    @media print{
        table{ page-break-inside:avoid; }
    }
</style>
<section class="content" >
    <?php if ($dados->num_rows >= 1) {?>
        <?php while ($report = $dados->fetch_assoc()) { ?>
        <table class="" style="page-break-before: always; width: 100%; border: none; margin-top: -40px; ">
            <tr class="text-center">
                <td><img src="/Public/img/logo_detransp.jpg" width="100"></td>
                <td width="" class="text-bold"><h3>&nbsp;&nbsp;&nbsp;BOLETO DE AVALIAÇÃO DE EXAME PRÁTICO DE <br /> &nbsp;&nbsp;&nbsp;DIREÇÃO VEICULAR Categorias “A” &nbsp;&nbsp;&nbsp;</h3></td>
                <td><img src="/Public/img/NumeroBO.jpg" width="100"></td>
            </tr>
        </table>
        
    <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: 0px; font-size: 9px;  ">
        <tr>
            <td colspan="3" class="text-center bg-gray"> <b> DADOS DO CANDIDATO </b> </td>
            <td colspan="3" class="text-center bg-gray"> <b> DADOS DO EXAME </b> </td>
        </tr>
        
        <tr>
            <td width="150" class="text-center bg-gray"> <b> CPF DO CANDIDATO </b> </td>
            <td width="500" class="text-center bg-gray"> <b> NOME DO CANDIDATO </b> </td>
            <td width="150" class="text-center bg-gray"> <b> RENACH </b> </td>
            <td colspan="3" class="text-center bg-gray"> <b> Nº PROTOCOLO DO EXAME </b> </td>
        </tr>
        
        <tr>
            <td style="font-size: 15px" class="text-center"> &nbsp;&nbsp;&nbsp; <?php echo $report['usu_cpf']; ?> </td>
            <td style="font-size: 10px" class="text-center"> &nbsp;&nbsp;&nbsp; <?php echo strtoupper($report['usu_nomecompleto']); ?> </td>
            <td style="font-size: 15px" class="text-center"> &nbsp;&nbsp;&nbsp; <?php echo $report['usu_renach']; ?> </td>
            <td width="100" class="text-center"> <b> DATA </b> <br> <?php echo $_POST['data_fase']; ?>  </td>
            <td width="100" class="text-center" valign="top"> <b> HORA </b> </td>
            <td width="100" class="text-center"> <b> CATEGORIA </b> <br> <?php echo substr($report['serviten_descricao'],-1,1); ?> </td>
        </tr>
        
    </table>
    
    <table class="tablex table-condensed" style="margin-top: 0px; width: 100%; font-size: 9px;" border="1">
        <tr>
            <td width="200" class="text-center bg-gray"> <b> CÓDIGO CFC </b> </td>
            <td width="500" class="text-center bg-gray"> <b> NOME DO CFC </b> </td>
            <td width="150" class="text-center bg-gray"> <b> EDITAL </b> </td>
            <td width="500" class="text-center bg-gray"> <b> MUNICÍPIO DO EDITAL </b> </td>
        </tr>
        
        <tr>
            <td style="font-size: 18px"><?php echo $_SESSION['CLI_CODCFC']; ?></td>
            <td style="font-size: 18px"><?php echo $_SESSION['APP_LOCALFANTASIA']; ?></td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <p style="font-size: 11px; margin-top: 0px; margin-bottom: 0px;  ">
        &nbsp;Para a avaliação, assinalar de acordo com as faltas cometidas pelo candidato durante o exame prático de direção veicular. &nbsp; Conforme a Resolução nº 168/04 do Contran, o candidato que cometer falta eliminatória ou cuja soma dos pontos negativos ultrapassar a 3 (três) será considerado reprovado.        
    </p>
    
    <table style="margin-top: 5px; width: 100%;  font-size: 11px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> I - Faltas Eliminatórias: </b> </td>
        </tr>
        <tr>
            <td width="30"></td>
            <td>&nbsp;&nbsp; <b> Iniciar a prova sem estar com o capacete devidamente ajustado à cabeça ou sem viseira ou óculos de proteção </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Descumprir o percurso pre-estabelecido </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Abalroar um ou mais cones de balizamento </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Cair do veículo, durante a prova </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Não manter equilíbrio na prancha, saindo lateralmente dela </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Avançar sobre o meio fio ou parada obrigatória </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Colocar o(s) pé(s) no chão, com o veículo em movimento </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Provocar acidente durante a realização do exame </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Cometer qualquer outra infração de trânsito de natureza gravíssima </b> </td>
        </tr>
    </table>

    <table style="margin-top: 0px; width: 100%;  font-size: 11px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> II - Faltas Graves (3 pontos): </b> </td>
            <td colspan="4">&nbsp;&nbsp;<b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Deixar de colocar um pé no chão e o outro no freio ao parar o veículo</b> </td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Invadir qualquer faixa durante o percurso </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Fazer incorretamente a sinalização ou deixar de fazê-la </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Fazer o percurso com o farol apagado </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Cometer qualquer outra infração de trânsito de natureza grave </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="margin-top: 0px; width: 100%;  font-size: 11px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> III - Faltas Médias (2 pontos): </b> </td>
            <td colspan="4">&nbsp;&nbsp; <b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Utilizar incorretamente os equipamentos </b> </td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Engrenar ou utilizar marchas inadequadas durante o percurso </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Não recolher o pedal de partida ou o suporte do veículo, antes de iniciar o percurso </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Interromper o funcionamento do motor sem justa razão, após o início da prova </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Conduzir o veículo durante o exame sem segurar o guidom com ambas as mãos, salvo
eventualmente para indicação de manobras </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Cometer qualquer outra infração de trânsito de natureza média </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="margin-top: 0px; width: 100%;  font-size: 11px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> IV - Faltas Leves (1 ponto): </b> </td>
            <td colspan="4">&nbsp;&nbsp; <b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Colocar o motor em funcionamento, quando já engrenado </b> </td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Conduzir o veículo provocando movimento irregular no mesmo sem motivo justificado </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Regular os espelhos retrovisores durante o percurso do exame </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;&nbsp; <b> Cometer qualquer outra infração de trânsito de natureza leve </b> </td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="width: 100%; margin-top: 0px;">
        <tr>
            <td width="48%">

                <table style="width: 100%; font-size: 9px;     margin-top: -13px;" class="text-center" border="1">
                    <tr> <td colspan="4" class="text-center text-bold">&nbsp</td></tr>
                    <tr> <td colspan="4" class="text-center text-bold"> <b> Não cometeu nenhuma falta </b></td></tr>
                    <tr  style="font-size: 9px; " class="bg-gray"> <td colspan="4" class="text-center">
                      <b>  DADOS DO EXAMINADOR </b> ( Carimbar e assinar )
                    </td>
                    </tr>
                    <tr>    
                        <td>CPF</td>
                        <td>NOME</td>
                        <td>DATA</td>
                        <td>ASSINATURA</td>
                    </tr>
                    <tr>    
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>             


            </td>
            <td width="48%">
                <table style="width: 100%; font-size: 9px;" class="text-center" border="1">
                    <tr> <td colspan="4" class="text-center text-bold"> <b> RESULTADO </b> </td></tr>
                    <tr class="bg-gray">
                        <td>APROVADO</td>
                        <td>REPROVADO</td>
                        <td colspan="2">AUSENTE</td>
                    </tr>
                    <tr class="">
                        <td>&nbsp;</td>
                        <td></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr  style="font-size: 9px; " class="bg-gray"> <td colspan="4" class="text-center"> <b> DADOS DO PRESIDENTE DA BANCA </b> ( Carimbar e assinar )</td>
                    </tr>
                    <tr>    
                        <td with="100">CPF</td>
                        <td>NOME</td>
                        <td>DATA</td>
                        <td>ASSINATURA</td>
                    </tr>
                    <tr>    
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
    <br><br><br><br><br><br><br><br><br>
    <div style="margin-top: 60px; border-top: solid 2px; width: 100%; padding-top: 2px;" class="text-center">
        Departamento Estadual de Trânsito de São Paulo - Diretoria de Habilitação
    </div>

        <?php }//while ?>
    <?php } //if ?>
</section>
<!--
<tr class="hide">
            <td>
                <table style="width: 100%">
                    <td><img src="/Public/img/RodapeBO.jpg" width="1200"></td>
                </table>
            </td>
        </tr>-->
