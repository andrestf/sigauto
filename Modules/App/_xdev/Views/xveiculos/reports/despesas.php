<?php echo $header; ?>

<?php
	#variaveis gerais
	$valorTotal = 0;

	$dataIni = $_POST['dataini'];
	$dataFim = $_POST['datafim'];

	$wDataIni = "";
	if($dataIni != '') {
		$wDataIni = " AND veicxdesp_data >= '" . DataDB($dataIni). "'";
	}

	$wDataFim = "";
	if($dataFim != '') {
		$wDataFim = " AND veicxdesp_data <= '" . DataDB($dataFim). "'";
	}

?>

<?php foreach ($dados as $veic) {
	#variaveis por veiculo
	$valorTotalViculo = 0; 
	?>


	<?php $query = "SELECT * FROM veic_veicxdespesas LEFT OUTER JOIN veic_despesas ON veicdesp_cod = veicxdesp_despcod 
					WHERE veicxdesp_veicid = '".$veic['veic_id']."' 
					$wDataIni $wDataFim
					"; ?>
	

	<?php $this->DB->ExecQuery($query); $desps = $this->DB->result_array(); ?>
	<?php if($desps) { ?>
			<hr/>
			<span style="font-weight: bold; ">Veiculo: </span><?php echo $veic['veicmarc_descri']; ?> - <?php echo $veic['veic_modelo']; ?>
			<br/>
			<?php foreach ($desps as $desp) { ?>
				<?php echo $desp['veicdesp_descri']; ?> Valor: R$ <?php echo $desp['veicxdesp_valor']; ?> <br/>

			<?php 
				$valorTotalViculo += $desp['veicxdesp_valor'];
				$valorTotal += $desp['veicxdesp_valor'];
			?>

			<?php }//foreach despesas ?>
			
			<br/>
			<span class="text-red">Valor Total do Veiculo: R$ <?php echo number_format($valorTotalViculo,2,",","."); ?></span>
	<?php }// if se tem despesas ?>
<?php }//foreach veiculos ?>

<hr />
<div class="text-center text-red">
Valor Total Despesas: <?php echo number_format($valorTotal,2,",","."); ?>
</div>