<?php if(!$dados) { ?>
	nenhum resultado
<?php } ?>


<?php foreach ($dados as $veic) { ?>
	<?php echo $header; ?>
	<div class="">
		<div class="col-sx-12 text-center"><h4><center> Ficha para vistoria </center></h4></div>		
		<div class="row" style="font-size: 12px;">
			<div class="col-xs-4"><span style="font-weight: bold;">Veículo:</span> <?php echo $veic['veicmarc_descri']; ?> - <?php echo $veic['veic_modelo']; ?></div>
			<div class="col-xs-4"><span style="font-weight: bold;">Placa:</span> <?php echo $veic['veic_placa']; ?></div>
		</div>

		<div class="row" style="font-size: 12px;">
			<div class="col-xs-4"><span style="font-weight: bold;">Ano Mod / Ano Fáb.</span> <?php echo $veic['veic_anomod']; ?> / <?php echo $veic['veic_anofab']; ?></div>
			<div class="col-xs-4"><span style="font-weight: bold;">Combustível:</span><?php echo $veic['veiccomb_descri']; ?></div>
		</div>
		
		<div class="row" style="font-size: 12px; background: #E1E1E1; margin: 5px;">
			<div class="col-xs-12">
				<span style="font-weight: bold;">Legenda:</span>
				B = BOM &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				R = RUIM &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				N = NÃO POSSUI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>

		

		<table class="table table-borderedx table-condensed table-ficha" style="border: solid 1px #454545;">
			<thead>
			<tr>
				<th>Item</th>
				<th width="40" > <center> B </center> </th>
				<th width="40" > <center> R </center> </th>
				<th width="40" > <center> N </center> </th>
			</tr>
			</thead>

			<tbody>
			
				<tr>
					<td>Farol Dianteiro Esquerdo</td>
					<td></td><td></td><td></td>
				</tr>

				<tr>
					<td>Farol Dianteiro Direito</td>
					<td></td><td></td><td></td>
				</tr>

				<tr>
					<td>Farol Traseiro Esquerdo</td>
					<td></td><td></td><td></td>
				</tr>

				<tr>
					<td>Farol Traseiro Esquerdo</td>
					<td></td><td></td><td></td>
				</tr>

				<tr>
					<td>Limpador Dianteiro</td>
					<td></td><td></td><td></td>
				</tr>


				<tr>
					<th>LATARIA</th>
					<th width="40" > <center> B </center> </th>
					<th width="40" > <center> R </center> </th>
					<th width="40" > <center> N </center> </th>
				</tr>				

			</tbody>
		</table>
	</div>

	<pagebreak page-break-type="clonebycss" />
<?php } ?>
	



<style>
	.table-ficha {
		font-size: 12px;
	}
	.table-ficha tr th  {
		border:  solid 1px;
	}
	.table-ficha tbody tr td {
		border: solid 1px #bbb;
	}

	.table-ficha tbody tr td:first-child {
		border: solid 1px #e1e1e1;
		border-right: solid 1px #bbb;
	}
</style>

<?php echo $footer; ?>