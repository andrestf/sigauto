<?php if(!$dados) { ?>
	nenhum resultado
<?php } ?>

<section class="content" >

	<?php echo $header; ?>
	
	<center>
		Listagem de veículos
	</center>
	
	<hr/>

	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Cód</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Placa</th>
				<th>Descrição</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $veic) { ?>
			<tr>
				<td><?php echo $veic['veic_id']; ?></td>
				<td><?php echo $veic['veicmarc_descri']; ?></td>
				<td><?php echo $veic['veic_modelo']; ?></td>
				<td><?php echo $veic['veic_placa']; ?></td>
				<td><?php echo $veic['veicmarc_descri']; ?></td>
			</tr>

			<?php } ?>
		</tbody>
	</table>
</section>


<?php echo $footer; ?>