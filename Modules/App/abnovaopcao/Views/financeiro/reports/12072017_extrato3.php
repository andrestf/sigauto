
<?php
$report = new FinanceiroRelatoriosModel();
$report->Campos = " finmov_descricao, finmov_tipo, sum(finmov_valorbaixa) as TOTAL  ";
$report->CondicaoExtra = " and finmov_tprecpag='CAIXA' ";
$report->OrderBy = " order by f.finmov_databaixareal,f.finmov_tpdocumbx";
$report->GroupBy = " group by f.finmov_databaixareal,f.finmov_tpdocumbx";

$report->Debug = FALSE;

$dados = $report->Gerar();

$report = $dados->fetch_array();

$Funcoes = new UsuariosHelper();

if ($dados->num_rows < 1) {
    echo "sem resultado";
    return;
}


if($_POST['databxreal_ini'] == "") {
    exit("ERRO: data inicial nao informada!");
}

if($_POST['databxreal_fim'] == "") {
    exit("ERRO: data final nao informada!");
}
// Pegando o dia Inical
// Se nao informado o dia sera sempre 1
if (isset($_POST['databxreal_ini'])) {
    $dia_inicial = substr($_POST['databxreal_ini'], 0, 2);
} else {
    $dia_inicial = '01';
    $_POST['databxreal_ini'] = date("01-m-Y");
}

// Pegando o dia Final
// Se nao informado o dia sera sempre o dia atual
if (isset($_POST['databxreal_fim'])) {
    $dia_fim = substr($_POST['databxreal_fim'], 0, 2);
} else {
    $dia_fim = date("d");
    $_POST['databxreal_fim'] = date("d-m-Y");
}

##############################################
// Definindo data Inicial
    $data_ini = $_POST['databxreal_ini'];
    $ano_inicial = substr($data_ini, 6, 4);
    $mes_inicial = substr($data_ini, 3, 2);
    $dia_inicial = $dia_inicial;
    $data_ini = $ano_inicial . "/" . $mes_inicial . "/" . $dia_inicial;

##############################################
// Definindo data Final
    $data_fim = $_POST['databxreal_fim'];
    $ano_fim = substr($data_fim, 6, 4);
    $mes_fim = substr($data_fim, 3, 2);
    $dia_fim = $dia_fim;
    $data_fim = $ano_fim . "/" . $mes_fim . "/" . $dia_fim;


$date_range = date_range($data_ini, $data_fim);


$primeraData = strtotime(DataDB($date_range['0']));
$ultimaData = strtotime(DataDB($date_range[count($date_range) - 1]));

?>

<div class='content'>
    <div class='formx'>
        <h3>
            Extrato de Sintético - Quebra por Baixa + Tp Documento
        </h3>
        <div class="row">
            <div class="col-sm-12">
                <b>Lçto Inicial: </b><?php echo $_POST['datamov_ini'] ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Lçto Final: </b><?php echo $_POST['datamov_fim'] ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <b>Baixa Inicial: </b><?php echo $_POST['databxmov_ini'] ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Baixa Final: </b><?php echo $_POST['databxmov_fim'] ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Caixa Bx.Inicial: </b><?php echo $_POST['databxreal_ini'] ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Caixa Bx.Final: </b><?php echo $_POST['databxreal_fim'] ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                <b>Vcto Inicial: </b><?php echo $_POST['venc_ini'] ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Vcto Final: </b><?php echo $_POST['venc_fim'] ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                <b>Usuário:</b> <?php
                if (isset($_POST['usubaixa'])) {
                    foreach ($_POST['usubaixa'] as $usuXX) {
                        echo $Funcoes->fRetCampo('sis_usuarios', 'usu_apelido', "usu_id = '$usuXX'") . ", ";
                    };
                } ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Documento:</b> <?php echo $Funcoes->fRetCampo('sis_tpdocum', 'tpd_descricao', "tpd_cd = '" . $_POST['tpdoc'] . "'") ?> 
            </div>
        </div>

        <table class="table table-condensed">
            <?php foreach ($date_range as $dataBR) { $dataUS = dataDB($dataBR); ?>

                <tr style="height: 45px; background-color: #E1E1E1;">
                    <td colspan="3" style="padding-top: 10px !important; "> <b> Data: <?php echo $dataBR; ?> </b></td>
                </tr>
                
                <tr>
                    <th width="130">Tp.Documento</th>
                    <th width="1"></th>
                    <th width='770'>Valor Recebido</th>
                </tr>
                
                <?php $valor = 0; $valorDia = 0;  foreach ($dados as $value) { $valor = $valor+$value['TOTAL'] ?>
                        <?php if($value['finmov_databaixareal'] == $dataUS) {
                            $valorDia = $valorDia+$value['TOTAL']
                            ?>
                        <tr>
                            <td><?php echo $value['tpd_descricao']; ?></td>
                            <td><?php echo '                       ';?></td>
                            <td>R$ <?php echo number_format($value['TOTAL'],2,",",".");?></td>
                        </tr>
                        <?php }//if ?>
                    
                <?php }//while ?>
                        <tr style="    background-color: #fff; border-bottom: solid 2px #ffabab;" class="text-blue">
                            <td colspan=""><b>Recebido no dia:</b></td>
                            <td></td>
                            <td><b>R$ <?php echo number_format($valorDia,2,",",".");?></b> </td>
                        </tr>                        
            <?php }//foreach ?>
	 	<!--tfoot-->
	 		<tr>
	 			<td colspan="6">
	 				<b>
	 				VALOR TOTAL MOVIMENTADO: R$ <?php echo number_format($valor,2,",","."); ?>
					</b>
	 			</td>
	 		</tr>
	 	<!--/tfoot-->                        
        </table>
    </div>
</div>	