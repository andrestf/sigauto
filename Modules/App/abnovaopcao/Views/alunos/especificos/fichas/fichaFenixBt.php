<div class='box-header'>
    <div style="font-size: 24px; width: 100%; text-align: center;">
        <b>
        <?php echo $aluno->usu_nomecompleto; ?>
        </b>
        <br/>
        FICHA DO ALUNO
        
        <div style="border-bottom: solid 1px; width: 80%; margin: 0 auto">
            <b><?php echo $_SESSION['APP_LOCALNOME'] ?>,</b> CNPJ Nº <?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?><br/>
            <?php echo $_SESSION['APP_LOCALCIDADE'] ?>,<br/><br/>
        </div>
        <div style="border-bottom: solid 0px; width: 80%; margin: 0 auto;">
            <center>
                <?php echo strtoupper($aluno->usu_nomecompleto); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;CPF: <?php echo strtoupper($aluno->usu_cpf); ?><br>
                <?php echo $aluno->usu_logradouro;?>,
                Nº <?php echo $aluno->usu_logranumero; ?> - 
                <?php echo $aluno->usu_lograbairro; ?><br>
                <?php echo $aluno->usu_logramunicipio; ?> / 
                <?php echo $aluno->usu_lograuf; ?> - 
                <?php echo $aluno->usu_cep; ?></b> 
            </center>
        </div>        
        
    </div>
</div>



<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>