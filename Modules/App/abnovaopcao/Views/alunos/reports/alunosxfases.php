<?php

$report = new AlunosRelatoriosModel();
$report->CondicaoExtra = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL  ";
$report->OrderBy = "serv_descricao";
$report->GroupBy = "usu_id";
$dados  = $report->Gerar();
?>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>Aluno</th>
			<th>Matricula</th>
			<th>Processo</th>
			<th>Fase</th>
                        <th>Resultado</th>
		</tr>
	</thead>
	<tbody>
		<?php if($dados->num_rows >= 1) { ?>
                    <?php while($report = $dados->fetch_assoc() ) {?>
                    <tr>
                            <td><?php echo $report['usu_nomecompleto']?> <br/></td>
                            <td><?php echo $report['tpmat_descricao']?> <br/></td>
                            <td><?php echo $report['serv_descricao']?> <br/></td>
                            <td><?php echo $report['serviten_descricao']?> <br/></td>
                            <td><?php echo $report['amf_resultado']?> <br/></td>
                    </tr>
                    <?php }//while ?>
                <?php } else {//if ?>
                    <tr>
                        <td  colspan="5" style="font-size: 28px">
                            <center>
                            Sem resultados
                            </center>
                        </td>
                    </tr>
                    
                <?php } ?>
	</tbody>
</table>

