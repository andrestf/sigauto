
<?php

$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_faltoso is NULL OR amf_faltoso='') ";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();

$config = new ConfigHelper();
$ValorMarcacao = $config->Get('nValorTaxaMarcacao');

?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-titlez" style="width: 100%">

                <center>
                <u><?php echo strtoupper($_SESSION['APP_LOCALNOME']);?></u><br/><br/>
                </center>
                <b> EXAME : TEÓRICO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp DATA:  <?php echo $_POST['data_fase']; ?> </b>

                <center>
                    <h1>
                        <hr>
                            <u>
                            TAXA DE MARCAÇÃO
                            </u>
                    </h1>
                </center>
            </h3>
        </div>
        <div class="box-body">
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th with="50">Nº.</th>                        
                        <th with="500">NOME DO ALUNO</th>
                        <th width="210">C P F</th>
                        <th width="120">VALOR</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($dados->num_rows >= 1) {
                        $i = 1;
                        ?>
                        <?php
                           $subTotal = 0;
                           while ($report = $dados->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $i; ?> <br/></td>
                                <td><?php echo $report['usu_nomecompleto'] ?> <br/></td>
                                <td> <b> <?php echo $report['usu_cpf'] ?> </b> <br/></td>
                                <td><?php if($report['amat_incluitaxa'] == '*') {
                                                $subTotal += $ValorMarcacao;
                                                echo "R$ $ValorMarcacao";
                                          } else {
                                                $subTotal += $ValorMarcacao;
                                                echo "R$ $ValorMarcacao";
                                          }?> <br/>
                                </td>
                            </tr>
                        <?php $i++;}//while ?>
                            <tr>
                                <td colspan="4" class="text-right"></td>
                            </tr>

                            <tr class="text-bold">
                                <td>Total</td>
                                <td colspan="3" class="text-right">R$ <?php echo $subTotal; ?></td>                                
                            </tr>
                    <?php } else {//if ?>
                        <tr>
                            <td  colspan="7" style="font-size: 28px">
                    <center>
                        Sem resultados
                    </center>
                    </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
