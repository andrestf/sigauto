<?php
/**
 * Controller: ProcessosController
 * Action: detalhes
 * 
 * Lista fases da matricula selecionada 
 */

?>


    <div class="content asec-box" id="ficha_novo_aluno">
        <h3 class="page-title">
            Matrícula - <small><?php echo $aluno->usu_nomecompleto; ?></small>
        </h3>
        
        <h4 class="ficha_title"><i class="fa fa-list-alt"></i> Fases </h4>
        <div class="form" style="padding: 10px 15px;">
            
            <table class="table table-bordered table-condensed table-hover table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Fase</th>
                        <th width="120">Status/Data</th>
                        <th width="120">Início</th>
                        <th width="120">Concluído</th>
                        <th width="95"></th>
                    </tr>
                </thead>
                <tbody id="listaFases">
                        <tr><td colspan="4"><center><img src="/Public/img/ld03.gif" /></center></td></tr>
                </tbody>
                    
            </table>
        </div>
    </div>


<!-- Modal RESULTADO -->
<div class="modal fade" id="resultado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Informações do processo</h4>
      </div>
      <div class="modal-body">
          <form id="resultado-fase">
            <input type="hidden" name="fase-id" value="" id="fase-id"/>
            <input type="hidden" name="mat-id"  value="" id="mat-id"/>
            <div class="row">
              <div class="col-md-6 hide">
                <label>Data</label>
                <input type="text" class="form-control datepicker" name="fase-data" id="fase-data" value="<?php echo date('d/m/Y')?>"/>
              </div>                
            </div>
              <br/>
            <div class="row">
              <div class="col-md-6">
                <label>Data do Exame</label>
                <input type="text" class="form-control datepicker" name="fase-data-exame" id="fase-data-exame"/>
              </div>
            
              <div class="col-md-6">
                <label>Resultado</label>
                <select name="fase-resultado" class="form-control" id='fase-resultado'>
                    <option value=""> </option>
                    <option value="AGENDADO"> Agendado </option>
                    <option value="APROVADO"> Aprovado </option>
                    <option value="REPROVADO"> Reprovado </option>
                    <option value="FALTOSO"> Faltoso </option>
                </select>
              </div>
            </div>
            
            <br/>
            <div class="row">
                <div class="col-md-6 ">
                  <div class="dt-concluii" style='display: none'>
                      <label>Concluído</label> 
                      <input type name="fase-conclui" id="fase-conclui" class="form-control datepicker" />

                  </div>
                </div>
                <div class="col-md-6 marcacaoexame" style='display: none'>
                        <label>Marcação/Emissão</label> 
                        <select name="marcacaoexame" class="form-control" id='marcacaoexame'>
                            <option value=" "></option>
                            <option value="M" selected="selected"> Marcação </option>
                            <option value="E"> Marcação + Emissão </option>
                        </select>                    

                </div>
            </div>
          </form>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary btnAlteraDados">Salvar</button>
      </div>
    </div>
  </div>
</div>

<script>
    /*
     * Lista fases do processo
     */
    function ListaFases() {     
        $("#listaFases").html('<tr><td colspan="5"><center><img src="/Public/img/ld03.gif" /></center></td></tr>');
        
        $.ajax({
           url : 'index.php?route=processos/ListaProcessosIniciados/&aluno=<?php echo $_GET['aluno']?>&processo=<?php echo $_GET['processo']?>',
           success: function(e) {
              $("#listaFases").html(e);
           }
        });
    };


    $("#fase-resultado").change( function() {
      if($("#fase-resultado").val() == 'APROVADO') {
          $("#fase-conclui").val( $("#fase-data-exame").val() );          
        $(".dt-concluii").show();
      } else {
        $(".dt-concluii").hide();
        //$("#fase-conclui").val('');
        $('#fase-conclui').datepicker('setDate', null);
      }
    });
    /*
     * Inicia processo de conclusão de fase
     * o ID do form tem que ser igual ao campo que requisita os dados (_campos)
     * */
    function ConcluirFase(fase,id) {
        var campos = $("#chk-"+id).data('campos');
        if(campos != "") {
            var campo = campos.split(',');


            if(campo == "resultado") {
                CarregaFaseProcesso(fase,id,"resultado")
                return false;
            }
            
            if(campo == "resultado_taxa") {
                CarregaFaseProcesso(fase,id,"resultado_taxa")
                return false;

            }

            if(campo == "resultopcional") {
                CarregaFaseProcesso(fase,id,"resultado"); 
              /*
                alertify.confirm("Atenção","Informar resultado? <br/> O mesmo não podera ser alterado!",
                  function() {
                    alertify.confirm().close(); 
                    CarregaFaseProcesso(fase,id,"resultado");
                    return false;
                  },

                  function() {
                      
                      XX = confirm('Continuar processo sem data?');
                      if(XX) {
                          ContinuaConcluirFase(fase,id);
                      }
                  }
                );
            */
            }
        } else {
            ContinuaConcluirFase(fase,id);
        }
    }
    
    /**
    * Carrega dados da fase dentro da modal
    **/
    var resultado_taxa;

    function CarregaFaseProcesso(fase,id,tipo) {
        $(".marcacaoexame").hide();
        $("#fase-conclui").val("");
        $("#resultado-fase")[0].reset();
        $.ajax({
            dataType : 'json',
            url   : 'index.php?route=processos/GetDadosFase/&aluno=<?php echo $_GET['aluno']?>&processo=<?php echo $_GET['processo']?>&fase='+fase,
            success : function(e) {
              console.log(e);
                $("#fase-id").val(e.amf_id);
                $("#mat-id").val(e.amf_matid);
                $("#fase-resultado").val(e.amf_resultado);
                $("#fase-data-exame").val(e.data_processo);
                $("#resultado").modal('show');
                $(".dt-concluii").hide();
                $('#fase-conclui').datepicker('setDate', null);
                $('#fase-data-exame').datepicker('setDate', null);


                $("[name=marcacaoexame]").val(null);


                if(tipo == "resultado_taxa") {

                  $(".marcacaoexame").show();
                  
                  if(e.amf_examepratico !== null ) {
                    $("[name=marcacaoexame]").val(e.amf_examepratico);
                  }
                  
                  
                }
                //

            }
        })
    }

    /*
     * Atualiza dados da fase atual se modal de dados for exibida
     */
    $(".btnAlteraDados").click( function() {
      fase = $("#fase-id").val();
      id   = $("#mat-id").val();
      SalvaFaseProcesso(fase,id)
    });
    function SalvaFaseProcesso(fase,id) {
      if($("#fase-resultado").val() == 'APROVADO') {
          if( $("#fase-conclui").val() == '' ) {
            alertify.alert('Atenção','Informe a data de conclusão!');
            return false;
          }
      }

      if($("#fase-data-exame").val() == '') {
        alertify.alert('Atenção','Informe a data do exame');
        return false;
      }

      if($("#fase-resultado").val() == '') {
        alertify.alert('Atenção','Informe o Resultado do exame');
        return false;
      }

      //se data de concluido preenchido, pede confirmação!
      if( $("#fase-conclui").val() != '' ) {
        SalvaFaseProcesso2(fase,id);
        /*
        alertify.confirm("Concluir Processo?","Ao concluir esse processo, o mesmo não poderá ser alterado!",
            function() {
              SalvaFaseProcesso2(fase,id);
            },

            function() {
              
            }
          );
          */
      } else {
        //se nao tiver data concluir preenchida, vai direto para fase2
        SalvaFaseProcesso2(fase,id);
      }
    }

    function SalvaFaseProcesso2(fase,id) {
      $.ajax({
        type : 'post',
        dataType : 'json',
        data : $("#resultado-fase").serializeArray(),
        url : '/index.php?route=alunos/AtualizaFase/&fid='+fase+'&mid='+id+'&aid=<?php echo $_GET['aluno']?>',
        success : function(e) {
          if(e.erro == '') {
            if(e.conclui == true) {
              ContinuaConcluirFase(fase,id);
            }

            $("#resultado").modal('hide');
            ListaFases();
          }
        }
      })
    }

    /*
     * Continua processo de conclusao de fase após informar dados obrigatórios se houver
     * dados obrigatorios são chamados na função Concluir fase, no campo da tabela _campos
     */
    function ContinuaConcluirFase(fase,id) {
  
        matricula = "<?php echo (isset($_GET['processo'])) ? (int) $_GET['processo'] : '0' ?>";
        aluno = "<?php echo (isset($_GET['aluno'])) ? (int) $_GET['aluno'] : '0' ?>";

        if(aluno == 0 || aluno == "") {
            return false;
        }
        data = null;
        $.ajax({
           dataType: 'json',
           url : "/index.php?route=processos/ConcluirFase/&aluno="+aluno+"&fase="+fase+"&matricula="+matricula+"&data="+data,
           success: function(e) {
               if(e.erro != '') {
                   alertify.error(e.mensagem);
                   return false;
               }

               alertify.success(e.mensagem);
               ListaFases();
            }
        });
    }
    function VoltaProc(id)   { alertify.error("SEM PERMISSAO");}
    function CancelaProc(id) { alertify.error("SEM PERMISSAO");}    
    
    <?php if($this->ValidaNivel2(40)) { ?>
    
        var matricula = "<?php echo (isset($_GET['processo'])) ? (int) $_GET['processo'] : '0' ?>";
        var aluno = "<?php echo (isset($_GET['aluno'])) ? (int) $_GET['aluno'] : '0' ?>";

        
        function VoltaProc(fase) {

            alertify.confirm("Atenção!!!","Voltar fase do aluno?",
              function() {
                $.ajax({
                    dataType : 'json',
                    url : "/index.php?route=processos/VoltaFase/&aluno="+aluno+"&fase="+fase+"&matricula="+matricula+"&data=",
                    success : function(e) {
                        if(e.erro) {
                            alertify.error(e.mensagem);
                            return false;
                        }
                        alertify.success(e.mensagem);
                        ListaFases();
                    },
                    error: function(e,x,s) {
                        alertify.error(s + "<br/> ERRO: ProcessosDetalhes E45 ");
                    }
                });
              },

              function() {}
            );

        }


        function CancelaProc(fase) {
          alertify.confirm("Atenção !!!","Deseja cancelar a fase?", 
            function() { 
              //SIM
              $.ajax({
                  dataType : 'json',
                  url : "/index.php?route=processos/CancelaFase/&aluno="+aluno+"&fase="+fase+"&matricula="+matricula+"&data=",
                  success : function(e) {
                      if(e.erro) {
                          alertify.error(e.mensagem);
                          return false;
                      }
                      alertify.success(e.mensagem);
                      ListaFases();
                  },
                  error: function(e,x,s) {
                      alertify.error(s + "<br/> ERRO: ProcessosDetalhes E45 ");
                  }
              });
            }, 
            function() {
              //nao
            }
          );

        }
    <?php } ?>
    $(function() {
        ListaFases();
    });
</script>