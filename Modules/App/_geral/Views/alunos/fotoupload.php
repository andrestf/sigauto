<div class="content asec-box" id="foto_aluno">
    <h4 class="ficha_title">Foto do Aluno</h4>
    <div class="form">
        <?php if(isset($view_retorno)) {
            echo "<div style='padding: 12px;'>";
            echo $view_retorno;
            echo "</div>";
        }?>
        
        <?php if(isset($view_foto)) {?>
        <div style='padding: 12px;'>
            <div class="alert alert-success">Foto enviada com sucesso!</div>
            <center><img src="/alunos/GetFoto/?codigo=<?php echo $view_codigo?>'>" class="aluno-foto"/>
            <br/><br/>
            Você pode re-enviar a foto, ou <a href="/alunos/editar/?codigo=<?php echo $view_codigo; ?>">clique aqui</a> para voltar.
            </center>
        </div>
        <?php } ?>
        
        <form enctype="multipart/form-data" action="<?php echo $this->Link("alunos","FotoUpload","codigo=$view_codigo")?>" method="post">
          <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
          <input type="hidden" name="action" value="send"/>
          <div class="col-md-6">
              <input name="imagem" type="file" class="form-control"/>
          </div>
          <div class="col-md-6">
              
              <a href="<?php echo $this->Link("alunos","editar","codigo=$view_codigo")?>" class="btn btn-primary">Voltar </a>
            <input type="submit" value="Enviar" class="btn btn-success"/>
          </div>
      </form>
         <div class="clearfix"></div>
    </div>
</div>