<div class='box box-primary' style="border: solid 2px; padding: 2px;" >
    <div class='box-header'>
        
        <div style="font-size: 16px; border-bottom: solid 1px; width: 100%; margin: 0 auto;">
            <center><b>
                FICHA &nbsp;&nbsp; DE &nbsp;&nbsp; MATRÍCULA
            </center></b>

            <b> 1-) De acordo com a Resolução de nº. 358 do CONTRAN, os documentos que não forem entregues no primeiro dia de aula, deverão ser apresentados na secretaria até a data de ____ / ____ / 2018, sob pena de desligamento do curso.</b><br>

            <b> 2-) A Botutrânsito se isenta de qualquer responsabilidade pela NÃO entrega dos documentos, consequentemente, desligamento do curso.</b><br>

            <b> 3-) Conforme Comunicado DET nº 002/2017: "No caso de falta ou atraso do aluno, constatado pelo instrutor no momento da chamada em sala de aula, que deve ocorrer no inicio da instrução, deverá o instruendo ser delisgado imediatamente."</b><br><br>

            CURSO: &nbsp; ______________________________________________________________________________
            <br>

            NOME: &nbsp; <?php echo strtoupper($aluno->usu_nomecompleto); ?>
            <br>

            <b> Celular: &nbsp; </b> <?php echo $aluno->usu_telcelular; ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> Telefone: &nbsp;</b> <?php echo $aluno->usu_telresid; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b> E-mail: &nbsp;&nbsp; : </b> <?php echo $aluno->usu_email; ?> 
            <br>

            <b> CPF: &nbsp; </b> <?php echo $aluno->usu_cpf; ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> RG: &nbsp; </b> <?php echo $aluno->usu_rg; ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> CNH: &nbsp; </b> <?php echo $aluno->usu_numdoc; ?> &nbsp;&nbsp;
            <b> CATEGORIA: &nbsp; </b> <?php echo $aluno->usu_catatual; ?>
            <br>

            <b> DATA NASC: &nbsp;&nbsp; : </b> <?php echo DataBR($aluno->usu_nascimento); ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> LOCAL NASC: &nbsp;&nbsp; </b> <?php echo $aluno->usu_naturalidade; ?>  <br>
            <b> ESTADO: &nbsp;&nbsp; </b>  <?php echo $aluno->usu_uf; ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> ESTADO CIVIL: &nbsp;&nbsp; </b> <?php echo $aluno->usu_estadocivil; ?>
            <br>

            <b> PAI: &nbsp;&nbsp; </b> <?php echo $aluno->usu_nomepai; ?> &nbsp;&nbsp;&nbsp;&nbsp;
            <b> MÃE: &nbsp;&nbsp; </b> <?php echo $aluno->usu_nomemae; ?> 
            <br>        

            <b> ENDEREÇO: </b> &nbsp; <?php echo strtoupper($aluno->usu_logradouro); ?>
            <b>       Nº: </b> &nbsp; <?php echo $aluno->usu_logranumero; ?>
            <b> COMPLEM.: </b> &nbsp; <?php echo $aluno->usu_logracomplemento; ?> <br>
            <b> BAIRRO: &nbsp;&nbsp; </b> <?php echo strtoupper($aluno->usu_lograbairro); ?> &nbsp;&nbsp; 
            <b> CIDADE: </b> <?php echo strtoupper($aluno->usu_logramunicipio); ?> / 
            &nbsp; <?php echo strtoupper($aluno->usu_lograuf); ?> -  &nbsp;&nbsp; 
            <b> CEP: </b> &nbsp; <?php echo $aluno->usu_cep; ?><br>

            <b> FORMA DE PAGAMENTO: &nbsp;&nbsp; </b> ______________________________________________________________ <br> <br>

        </div>
            <br>
            <div style="width: 100%; margin: 0 auto; text-align: left;">
                <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?>, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b> &nbsp;&nbsp; ASSINATURA: &nbsp; </b> _____________________________________________________
            </div>  

    </div>
</div>

<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>