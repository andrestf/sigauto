<div class='box-header'>
<div class='box-header'>
            
                <div style="font-size: 24px; width: 100%; text-align: center;">
                    <b><u>
                    CONTRATO DE PRESTAÇÃO DE SERVIÇOS
                    </b></u>
                </div>
            
        </div>

        <div class='box-body' style="font-size: 16px; line-height: 20px; padding: 0px; text-align: justify">
                <p> 
                    &nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo $_SESSION['APP_LOCALNOME'] ?></b>, com o nome Fantasia <b><?php echo $_SESSION['APP_LOCALFANTASIA'] ?></b>, CNPJ: <b><?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?></b>, com sede na <b><?php echo $_SESSION['APP_LOCALLOGRADOURO'] ?></b> – <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?></b>/<b><?php echo $_SESSION['APP_LOCALESTADO'] ?></b>, aqui denominada Contratada, tem entre si justo e contratado o que se segue:
                </p>

                <br>
                <p>
                <center><b><u>OBJETO DO CONTRATO</u></b></center>
                    Cláusula 1 – È objeto do presente contrato a prestação de serviço de: Cursos de formação, especializados, atualizações e outros.
                </p>

                <br>
                <p>
                    <center><b><u>OBRIGAÇÕES DO CONTRATANTE</u></b></center>
                    Clàusula 2 – O Contratante deverá fornecer a contratada documentação exigida pelo órgão competente, seus documentos pessoais e telefone para cadastro.
                </p>

                <br>
                <p>
                    <center><b><u>OBRIGAÇÕES DA CONTRATADA</u></b></center>
                    Cláusula 3 – Contratada deverá fornecer ao contratante os recibos de pagamentos, bem como a cópia do presente contrato, se for solicitada e ao término do curso após registro na EPT, o certificado do curso.
                </p>

                <br>
                <p>
                    <center><b><u>DOS CURSOS</u></b></center>
                    Cláusula 4 – O curso é 100% presencial, se houver atraso ou o não comparecimento do aluno por qualquer motivo, este será desligado e perderá todo o valor pago.</br><br>

                    Cláusula 5 - Só haverá devolução de valores pagos em casos de desistência, se o candidato comunicar com 10 dias de antecedência ao curso que se matriculou, deduzido o valor da matrícula que é de R$ 100,00 ( Cem Reais ) para todos os cursos, exceto o curso de instrutor que é de R$ 200,00 ( Duzentos Reais ), caso comunique com menos de 10 dias, se optar fazer um curso em outra data, terá que pagar uma nova matrícula.<br><br>

                    Cláusula 6 – Se o Contratante comunicar com mais de 10, poderá trocar a data do curso sem o pagamento de nova matrícula .<br><br>

                    Cláusula 7 – O valor do curso ___________________________________________________ é de R$________________________________________________________________________________________________ sendo R$_____________________ matricula e R$_____________________ referente ao curso.<br><br>

                    Cláusula 8 – Ao final e sem mais, as partes de comum acordo elegem o Foro de Botucatu para dirimir possíveis dúvidas oriundas deste, renunciando a qualquer outro por mais privilegiado que seja.<br>
                </p>
        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?>, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row" style="text-align: center;">
            <div class="col-xs-6" style="border-top: solid 1px; width: 45%; float: left;">
                <center>
                    CONTRATADA <br>
                    <?php echo $_SESSION['APP_LOCALNOME'] ?> - <?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?>
                </center>
            </div>
            
            <div class="col-xs-6" style="border-top: solid 1px; width: 45%; float: right;">
                <center>
                    CONTRATANTE <br>
                    <?php echo $Aluno->usu_nomecompleto; ?><br>
                    <?php echo $Aluno->usu_cpf; ?>
                </center>
            </div>
        </div>