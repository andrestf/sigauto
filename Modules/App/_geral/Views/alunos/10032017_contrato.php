<div class="content" style="padding: 0px;">
    <div class='box box-primary' style="border: none; padding: 0px;" >
        <div class='box-header'>
            <center>
                <div style="font-size: 24px;">
                    <b>
                    CONTRATO DE PRESTAÇÃO DE SERVIÇOS
                    </b>
                </div>
            </center>
        </div>

        <div class='box-body' style="line-height: 16px; padding: 0px; text-align: justify">
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo $_SESSION['APP_LOCALNOME'] ?></b>, com o nome Fantasia <b><?php echo $_SESSION['APP_NOMEFANTASIA'] ?></b>, CNPJ: <b><?php echo $_SESSION['APP_CPFCNPJ'] ?></b>, com sede na <b><?php echo $_SESSION['APP_LOGRADOURO'] ?></b> – <b><?php echo $_SESSION['APP_CIDADE'] ?></b>/<b><?php echo $_SESSION['APP_ESTADO'] ?></b>, aqui denominada Contratante, tem entre si justo e contratado o que se segue:
                </p>


                <p>
                <center><b><u>OBJETO DO CONTRATO</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 1 – È objeto do presente contrato a prestação de serviço de: Aulas Práticas de Direção em automóvel, motocicleta, microônibus, carreta e o uso de veículos para exames e marcações de exames junto a 6ª Ciretran de Botucatu/SP. Em anexo o formulário dos valores dos cursos e uma nota promissória no valor da matrícula, emitida pelo aluno(a).
                </p>


                <p>
                    <center><b><u>OBRIGAÇÕES DO CONTRATANTE</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Clàusula 2 – O Contratante deverá fornecer a contratada comprovante de residência recente, seus documentos pessoais e telefone para contato para abertura do processo de habilitação junto ao Poupatempo/6ª Ciretran de Botucatu/SP.
                </p>

            
                <p>
                    <center><b><u>OBRIGAÇÕES DA CONTRATADA</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 3 – A contratada deverá fornecer ao contratante os recibos de pagamentos, bem como a cópia do presente contrato, se for solicitada.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 4 – A contratada se compromete a ministrar os cursos dentro das normas estabelecidas de Código de Trânsito Brasileiro.
                </p>

            
                <p>
                    <center><b><u>DOS CURSOS DE CARRO, MOTO, ADIÇÃO E MUDANÇA DE CATEGORIAS</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 5 – Os cursos mencionados no caput desta cláusula terão validade de 12 (doze) meses a contar da data do exame médico, sendo que o aluno não será notificado da validade do vencimento do processo pela Auto Escola e terá todos os seus exames, cursos, taxas cancelados automaticamente pelo Detran/SP após o prazo deste. Ficando o Contratante obrigado a quitar os débitos pendentes com a Contratada e resgatar a Nota Promissória emitida no inicio do curso.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Parágrafo 1 – Havendo desistência ou transferência do contratante durante os cursos, os valores serão devolvidos pela contratada, deduzidos o equivalente ao valor da matrícula integral que é de R$ 350,00 (Trezentos e Cinquenta Reais) e as aulas práticas de direção que por ventura o aluno tenha assistido, acrescidos das taxas recolhidas para os exames.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Parágrafo 2 – No caso do aluno estar matriculado nas categorias A/B e por ventura vir a desistir de uma das duas categorias, será deduzido o valor referente a meia matricula, ou seja R$ 175,00 (Cento e Setenta e Cinco Reais) e as aulas práticas que o aluno tenha assistido.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 6 – A marcação de aulas será de acordo com a disponibilidade das agendas da Auto Escola, sendo no máximo de 3 (três) aulas por dia, até atingir o número de aulas exigidos pelo Detran/SP, após serão cobradas as aulas extras no valor de R$ 60,00 (Sessenta Reais) cada aula, e o cancelamento das aulas só será possível salvo se o aluno comunicar a Contratada com o prazo de 72 horas (Setenta e duas Horas) antes da aula marcada. O não comparecimento do aluno no horário marcado, acarretará no custo da remarcação da aula que é de R$ 60,00 (Sessenta reais) e a aula só será efetivada após a realização da mesma através do sistema Biométrico do Detran/SP.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; O que possibilitará desta forma a Contratada emitir o Certificado de conclusão das aulas que deverá conter a assinatura do contratante para então agendar o Exame Prático de Direção.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 7 – Mesmo em dias Chuvosos o aluno deverá comparecer as aulas, sendo que a contratada disponibiliza aos seus alunos Capa de Chuva para a realização da mesma, e ocorre também no dia do Exame prático, possibilitando desta forma ter exame normalmente.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 8 – Em casos em que haja reprovação nos exames o Contratante deverá agendar um novo exame dentro dos prazos estabelecidos pelo Detran/SP, sendo que em se tratando do Exame teórico o Contratante fará o agendamento diretamente junto a Ciretran/Poupatempo e em relação ao Exame Prático deverá ser agendado junto a Contratada a um custo de R$ 150,00 (Cento e Cinquenta Reais) com o prazo de 10 (Dez) dias de Antecedência em relação ao novo exame.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 9 – O aluno só poderá agendar o exame Prático desde que suas parcelas junto a contratada esteja plenamente em dia.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 10 – Ao final dos cursos, o Contratante, para retirar a C.N.H, terá que acertar o débito restante mediante o pagamento em Dinheiro, Cartão de Crédito ou Cheque Pré datados.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 11 – Ao Final e sem mais, as partes de comum acordo elegem o Foro de Botucatu, para dirimir possíveis dúvidas oriundas deste, renunciando a qualquer outro por mais privilegiado que seja.
                </p>

        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b>BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row">
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    CONTRATANTE <br>
                    <?php echo $Aluno->usu_nomecompleto; ?><br>
                    <?php echo $Aluno->usu_cpf; ?>
                </center>
            </div>
            <div class="col-xs-2"></div>
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    CONTRATADA <br>
                    <?php echo $_SESSION['APP_LOCALNOME'] ?> - 05.677.097/0001-95
                </center>
            </div>
        </div>
    </div>
</div>
        


<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>