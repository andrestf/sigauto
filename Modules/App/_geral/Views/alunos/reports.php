<form method="post" action="<?php echo Permalink('alunos/RelatorioGerar/')?>" onsubmit="return true;"  id="newReportZ">
<div class="content">
    <div class='box box-primary' style="width: 550px; margin:0 auto; ">
        <div class='box-header with-border'>
            <h3 class='box-title'>Filtro</h3>
        </div>
        
        <div class='box-body'>
            <div class='row'>
                <div class='col-sm-6'>
                    <label>Data Inicial</label>
                    <input type="text" name="data_ini" id="data_ini" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Data de Final</label>
                    <input type="text" name="data_fim" id="data_fim" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Tipo de Matrícula</label>
                    <select name="matricula" class="select2 form-control">
                        <option value="">&nbsp;</option>
                        <?php foreach ($matriculas as $mastricula) {?>
                        <option value="<?php echo $mastricula['tpmat_id']?>" > <?php echo $mastricula['tpmat_descricao']?> </option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Processos</label>
                    <select name="processo[]" id="processo" class="select2 form-control" multiple="multiple">
                        <option value="0">&nbsp;</option>
                        <?php foreach ($procs as $proc) {?>
                        <option value="<?php echo $proc['serv_id']?>" > <?php echo $proc['serv_descricao']?> </option>
                        <?php } ?>
                    </select>
                </div>
                
  
                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Fases</label>
                    <select name="fase[]" id="fase" class="select2 form-control" multiple="multiple">
                        <option value="0">&nbsp;</option>
                        <?php foreach ($itenserv as $iten) {?>
                        <option value="<?php echo $iten['serviten_id']?>" > <?php echo $iten['serviten_descricao']?> </option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class='col-sm-3' style="margin-top: 5px;">
                    <label>Início Fase</label>
                    <input type="text" name="data_fase" id="data_fase" class="form-control datepicker" />
                </div>              
                
                <div class='col-sm-3' style="margin-top: 5px;">
                    <label>Fim Fase</label>
                    <input type="text" name="data_fasefin" id="data_fasefin" class="form-control datepicker" />
                </div>              
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Resultado</label>
                    <select name="resultado" class="select2">
                        <option></option>
                        <option value="AGENDADO">Agendado</option>
                        <option value="APROVADO">Aprovado</option>
                        <option value="REPROVADO">Reprovado</option>
                        <option value="FALTOSO">Faltoso</option>
                    </select>
                </div>

                <div class='col-sm-6'  style="margin-top: 5px;">
                    <label>Vencimento Inicial</label>
                    <input type="text" name="data_venc" id="data_venc" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Vencimento Final</label>
                    <input type="text" name="data_vencfin" id="data_vencfin" class="form-control datepicker" />
                </div>
                
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Relatório</label>
                    <select name="report" class="select2">
                        <option></option>

                        <?php if($_SESSION['APP_LOCALID'] == "7") { ?>
                            <?php ## REPORTS AUTO ESCOLA MATRIZ LENCOIS PAULISTA?>
                            <option value="teoricomarcacaoaula">Teórico - Marcação de Aulas</option>
                            <option value="teoricomarcacaoaulamoto">Marcação de Aulas - Moto</option>
                            <option value="alunos_boletimavaliacaocarro">Boleto de Avaliação de Exame Prático Carro</option>
                            <option value="alunos_boletimavaliacaomoto">Boleto de Avaliação de Exame Prático Moto</option>
                            <option value="alunosexapraticocertificado">Exames Prático - Certificado</option>
                            <option value="alunosexapraticoresutmatriz">Exames Prático Resultado Poupa Tempo</option>
                            <option value="alunosexpraticoamarcacao">Exames Prático - Taxa de Marcação</option>
                            <option value="alunosexpraticoamarcacaoemissao">Exames Prático - Taxa de Marcação + Emissão</option>
                            <option value="taxaRegistrolistagem">Lista de Taxa de Registro</option>
                            <option value="alunospraticoTaxaemissao">Lista de Taxa de Emissão CNH</option>
                            <option value="alunosexateoricoresutmatriz" selected="selected">Exames Teórico Resultado Poupa Tempo</option>
                            <option value="examesagendados">Relatório de Exames Agendados</option>
                            <option value="alunosxfases">Relatório de Fases</option>
                            <option value="vencimentoprocesso">Relatório Vencimento Processo</option>
                            <option value="alunosexateoricomarcacao">Exames Teórico - Taxa de Marcação</option>
                            <option value="alunos_exportaTaxamarcacao">Exportação Prático Marcação - Banco Rendimento</option>
                            <option value="alunos_exportaTaxamarcacaoemissao">Exportação Prático Marcação / Emissão - Banco Rendimento</option>
                            <option value="alunos_exportaTaxateorico">Exportação Teórico Marcação - Banco Rendimento</option>
                            <option value="alunos_expTxregistronovo">Exportação Taxa de Registro - Banco Rendimento</option>
                            <option value="alunos_exportaTaxaemissao">Exportação Taxa de Emissão - Banco Rendimento</option>

                            <?php # FIM REPORTS MATRIZ LENCOIS PAULISTA ######################################################################### ?>

                        <?php } elseif($_SESSION['APP_LOCALID'] == "1" OR $_SESSION['APP_LOCALID'] == "2" OR $_SESSION['APP_LOCALID'] == "3" OR $_SESSION['APP_LOCALID'] == "4" OR $_SESSION['APP_LOCALID'] == "5" OR $_SESSION['APP_LOCALID'] == "6" OR $_SESSION['APP_LOCALID'] == "7") { ?>

                            <?php ## REPORTS AUTO ESCOLA ALVORADA ?>
                            <option value="teoricomarcacaoaula">Teórico - Marcação de Aulas</option>
                            <option value="teoricomarcacaoaulamoto">Marcação de Aulas - Moto</option>
                            <option value="alunos_boletimavaliacaocarro">Boleto de Avaliação de Exame Prático Carro</option>
                            <option value="alunos_boletimavaliacaomoto">Boleto de Avaliação de Exame Prático Moto</option>
                            <option value="alunosexapraticocertificado">Exames Prático - Certificado</option>
                            <option value="alunosexapraticolista">Exames Prático Lista Poupa Tempo</option>
                            <option value="alunosexpraticoamarcacao">Exames Prático - Taxa de Marcação</option>
                            <option value="alunosexpraticoamarcacaoemissao">Exames Prático - Taxa de Marcação + Emissão</option>
                            <option value="taxaRegistrolistagem">Lista de Taxa de Registro</option>
                            <option value="alunospraticoTaxaemissao">Lista de Taxa de Emissão CNH</option>
                            <option value="alunosexateoricoassina">Exames Teórico Assinatura</option>
                            <option value="alunosexateoricolista" selected="selected">Exames Teórico Lista Poupa Tempo</option>
                            <option value="alunosexateoricoresut">Exames Teórico Resultado</option>
                            <option value="alunosexateoricoresutcad">Exames Teórico Resultado Caderno</option>
                            <option value="alunosexateoricomarcacao">Exames Teórico - Taxa de Marcação</option>
                            <option value="alunosexateoricolistaCidadao">Exames Teórico Casa do Cidadão</option>
                            <option value="alunos_exportaTaxamarcacao">Exportação Prático Marcação - Banco Rendimento</option>
                            <option value="alunos_exportaTaxamarcacaoemissao">Exportação Prático Marcação / Emissão - Banco Rendimento</option>
                            <option value="alunos_exportaTaxateorico">Exportação Teórico Marcação - Banco Rendimento</option>
                            <option value="alunos_expTxregistronovo">Exportação Taxa de Registro - Banco Rendimento</option>
                            <option value="alunos_exportaTaxaemissao">Exportação Taxa de Emissão - Banco Rendimento</option>

                            <?php if($this->ValidaNivel2(1)) { ?>
                            <option value="examesagendados">Relatório de Exames Agendados</option> 
                            <option value="alunosxfases">Relatório de Fases</option>
                            <option value="vencimentoprocesso">Relatório de Vencimento Processo</option>
                            <?php } ?>
                            
                            <?php # FIM REPORTS ALVORADA ######################################################################### ?>

                        <?php } elseif($_SESSION['APP_LOCALID'] == "11") { ?>

                            <?php ## REPORTS AUTO ESCOLA FENIX BOTUCATU ?>
                            <option value="teoricomarcacaoaula">Teórico - Marcação de Aulas</option>
                            <option value="teoricomarcacaoaulamoto">Marcação de Aulas - Moto</option>
                            <option value="alunos_boletimavaliacaocarro">Boleto de Avaliação de Exame Prático Carro</option>
                            <option value="alunos_boletimavaliacaomoto">Boleto de Avaliação de Exame Prático Moto</option>
                            <option value="alunosexapraticocertificado">Exames Prático - Certificado</option>                            
                            <option value="alunosexapraticolista">Exames Prático Lista Poupa Tempo Botucatu</option>
                            <option value="alunosexpraticoamarcacao">Exames Prático - Taxa de Marcação</option>
                            <option value="alunosexpraticoamarcacaoemissao">Exames Prático - Taxa de Marcação + Emissão</option>
                            <option value="taxaRegistrolistagem">Lista de Taxa de Registro</option>
                            <option value="alunospraticoTaxaemissao">Lista de Taxa de Emissão CNH</option>
                            <option value="alunosexateoricoassina">Exames Teórico Assinatura</option>
                            <option value="alunosexateoricolista" selected="selected">Exames Teórico Lista Poupa Tempo Botucatu</option>
                            <option value="alunosexateoricoresut">Exames Teórico Resultado</option>
                            <option value="alunosexateoricoresutcad">Exames Teórico Resultado Caderno</option>
                            <option value="alunosexateoricomarcacao">Exames Teórico - Taxa de Marcação</option>
                            <option value="alunos_exportaTaxamarcacao">Exportação Prático Marcação - Banco Rendimento</option>
                            <option value="alunos_exportaTaxamarcacaoemissao">Exportação Prático Marcação / Emissão - Banco Rendimento</option>
                            <option value="alunos_exportaTaxateorico">Exportação Teórico Marcação - Banco Rendimento</option>
                            <option value="alunos_expTxregistronovo">Exportação Taxa de Registro - Banco Rendimento</option>
                            <option value="alunos_exportaTaxaemissao">Exportação Taxa de Emissão - Banco Rendimento</option>

                            <?php if($this->ValidaNivel2(1)) { ?>
                            <option value="examesagendados">Relatório de Exames Agendados</option> 
                            <option value="alunosxfases">Relatório de Fases</option>
                            <option value="vencimentoprocesso">Relatório de Vencimento Processo</option>
                            <?php } ?>
                            <?php # FIM REPORTS FENIX BOTUCATU  ######################################################################### ?>

                        <?php } elseif($_SESSION['APP_LOCALID'] == "12") { ?>

                            <?php ## REPORTS AUTO ESCOLA FENIX LENCOIS PAULISTA ?>
                            <option value="teoricomarcacaoaula">Teórico - Marcação de Aulas</option>
                            <option value="teoricomarcacaoaulamoto">Marcação de Aulas - Moto</option>
                            <option value="alunos_boletimavaliacaocarro">Boleto de Avaliação de Exame Prático Carro</option>
                            <option value="alunos_boletimavaliacaomoto">Boleto de Avaliação de Exame Prático Moto</option>
                            <option value="alunosexapraticocertificado">Exames Prático - Certificado</option>                            
                            <option value="alunosexapraticoresutmatriz">Exames Prático Resultado Poupa Tempo</option>
                            <option value="alunosexpraticoamarcacao">Exames Prático - Taxa de Marcação</option>
                            <option value="alunosexpraticoamarcacaoemissao">Exames Prático - Taxa de Marcação + Emissão</option>
                            <option value="alunosexpraticoaxaregistro">Lista de Taxa de Registro</option>
                            <option value="alunospraticoTaxaemissao">Lista de Taxa de Emissão CNH</option>
                            <option value="alunosexateoricoassina">Exames Teórico Assinatura</option>
                            <option value="alunosexateoricoresutmatriz" selected="selected">Exames Teórico Resultado Poupa Tempo</option>
                            <option value="alunosexateoricoresut">Exames Teórico Resultado</option>
                            <option value="alunosexateoricoresutcad">Exames Teórico Resultado Caderno</option>
                            <option value="alunosexateoricomarcacao">Exames Teórico - Taxa de Marcação</option>
                            <option value="alunos_exportaTaxamarcacao">Exportação Prático Marcação - Banco Rendimento</option>
                            <option value="alunos_exportaTaxamarcacaoemissao">Exportação Prático Marcação / Emissão - Banco Rendimento</option>
                            <option value="alunos_exportaTaxateorico">Exportação Teórico Marcação - Banco Rendimento</option>
                            <option value="alunos_expTxregistronovo">Exportação Taxa de Registro - Banco Rendimento</option>
                            <option value="alunos_exportaTaxaemissao">Exportação Taxa de Emissão - Banco Rendimento</option>

                            <?php if($this->ValidaNivel2(1)) { ?>
                            <option value="examesagendados">Relatório de Exames Agendados</option> 
                            <option value="alunosxfases">Relatório de Fases</option>
                            <option value="vencimentoprocesso">Relatório de Vencimento Processo</option>                                    
                            <?php } ?>

                            <?php # FIM REPORTS AUTO ESCOLA FENIX LENCOIS PAULISTA ######################################################################### ?>

                        <?php } elseif($_SESSION['APP_LOCALID'] == "13") { ?>

                            <?php ## REPORTS BOTUTRANSITO ?>
                            <option value="alunos_fichadematriculaTCPF">Ficha de Matricula - Curso Especializado</option>                                    
                            <option value="alunos_fichadematriculaTCPF2">Ficha de Notificação</option>

                            <?php # FIM REPORTS BOTUTRANSITO ######################################################################### ?>

                        <?php } else { ?>

                            <?php ## REPORTS DAS OUTRAS AUTO ESCOLAS ?>                            
                            <option value="teoricomarcacaoaula">Teórico - Marcação de Aulas</option>
                            <option value="teoricomarcacaoaulamoto">Marcação de Aulas - Moto</option>
                            <option value="alunos_boletimavaliacaocarro">Boleto de Avaliação de Exame Prático Carro</option>
                            <option value="alunos_boletimavaliacaomoto">Boleto de Avaliação de Exame Prático Moto</option>
                            <option value="alunosexapraticocertificado">Exames Prático - Certificado</option>
                            <option value="alunosexapraticolista">Exames Prático Lista Poupa Tempo</option>
                            <option value="alunosexpraticoamarcacao">Exames Prático - Taxa de Marcação</option>
                            <option value="alunosexpraticoamarcacaoemissao">Exames Prático - Taxa de Marcação + Emissão</option>
                            <option value="taxaRegistrolistagem">Lista de Taxa de Registro</option>
                            <option value="alunospraticoTaxaemissao">Lista de Taxa de Emissão CNH</option>
                            <option value="alunosexateoricoassina">Exames Teórico Assinatura</option>
                            <option value="alunosexateoricolista" selected="selected">Exames Teórico Lista Poupa Tempo</option>
                            <option value="alunosexateoricoresut">Exames Teórico Resultado</option>
                            <option value="alunosexateoricoresutcad">Exames Teórico Resultado Caderno</option>
                            <option value="alunosexateoricomarcacao">Exames Teórico - Taxa de Marcação</option>
                            <option value="alunos_exportaTaxamarcacao">Exportação Prático Marcação - Banco Rendimento</option>
                            <option value="alunos_exportaTaxamarcacaoemissao">Exportação Prático Marcação / Emissão - Banco Rendimento</option>
                            <option value="alunos_exportaTaxateorico">Exportação Teórico Marcação - Banco Rendimento</option>
                            <option value="alunos_expTxregistronovo">Exportação Taxa de Registro - Banco Rendimento</option>
                            <option value="alunos_exportaTaxaemissao">Exportação Taxa de Emissão - Banco Rendimento</option>

                            <?php if($this->ValidaNivel2(1)) { ?>
                            <option value="examesagendados">Relatório de Exames Agendados</option> 
                            <option value="alunosxfases">Relatório de Fases</option>
                            <option value="vencimentoprocesso">Relatório de Vencimento Processo</option>
                            <option value="vencimentoprocesso">Relatório de Vencimento Processo</option>
                            <?php } ?>
                            
                            <?php # FIM REPORTS DAS OUTRAS AUTO ESCOLAS ######################################################################### ?>

                        <?php } ?>
                    </select>
                </div>
            </div>
            
            <div class='box-footer'>
                <br/>
                <span class='pull-right'>
                    <input type="submit" value="Gerar" class='btn btn-primary'/>
                </span>
            </div>
        </div>
    </div>
    
</div>
</form>


<script>
    $("#newReport").on("submit",function(){
        var datas = $("#newReport").serialize();
        window.open("<?php echo Permalink('alunos/RelatorioGerar2/')?>&"+datas,"_blank","width=400, height: 550");
    })
</script>