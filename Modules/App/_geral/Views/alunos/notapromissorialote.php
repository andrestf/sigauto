<script src="/Public/js/valorPorExtenso.js"></script>
<?php if($parcelas) {?>
<br/><br/>
<?php foreach($parcelas as $parcela) { ?>
<div class="content" style="margin-bottom: -20px;">
    <div class='box box-primary' style="border: solid 2px #000" >
        <div class='box-header'>
            <h3 style="margin-top: -5px; height: 15px;">
                  NOTA PROMISSÓRIA
            </h3>
        </div>
        
        <div class='box-body' >  
            <div style="z-index: 1; position: relative;">
                <div class="text-right" style="margin-top: -40px;">
                    Vencimento <input type="text" style="border: 0px; text-align: right" placeholder="" size="20" class="datepicker" value="<?php echo DataBR($parcela['finmov_dtvenc']);?>"/>
                </div>
                <div class="text-right" style="margin-top: 0px;">
                    R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="Digite o valor" value="<?php echo $parcela['finmov_valor'];?>" size="20" onkeyup="formataValor(this)"/>
                </div>
            </div>
                
                <div style="width: 100%; font-size: 18px;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Pagarei  por esta única via de nota promissória à
                        <b><?php echo $_SESSION['APP_LOCALNOME'] ?>,</b> CNPJ Nº <?php echo $_SESSION['APP_LOCALCPFCNPJ'] ?>, a quantia de: <br/>
                        ( <b> <input value="<?php echo escreverValorMoeda($parcela['finmov_valor'])?>" type="text" style="border: 0px; width: 95%;" placeholder="Valor por extenso" class="valorext"/> )
                </div>
                
                
                <br/>

                    
                <div style="border-bottom: solid 1px; width: 80%; margin: 0 auto">
                    <?php echo $_SESSION['APP_LOCALCIDADE'] ?>,<br/><br/>
                </div>
                <div style="border-bottom: solid 0px; width: 80%; margin: 0 auto;">
                    <center>
                        <?php echo strtoupper($Aluno->usu_nomecompleto); ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;CPF: <?php echo strtoupper($Aluno->usu_cpf); ?><br>
                        <?php echo $Aluno->usu_logradouro;?>,
                        Nº <?php echo $Aluno->usu_logranumero; ?> - 
                        <?php echo $Aluno->usu_lograbairro; ?><br>
                        <?php echo $Aluno->usu_logramunicipio; ?> / 
                        <?php echo $Aluno->usu_lograuf; ?> - 
                        <?php echo $Aluno->usu_cep; ?></b> 
                    </center>
                </div>
        </div>
    </div>
</div>
<br/><br/>

<?php } ?>
<div style="border: solid 2px; padding: 10px; margin: -15px 15px;">
    <div class="text-justify">
        Eu <b><?php echo strtoupper($Aluno->usu_nomecompleto); ?></b>, estou ciente e concordo que em virtude do meu plano de pagamento parcelado com promissórias, só receberei a CNH (carta) após a quitação das parcelas.
        
    </div>
    <br>
    
        <div style="width: 90%; margin: 0 auto; text-align: center">
            <b><?php echo $_SESSION['APP_LOCALCIDADE'] ?>, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>
    <br>
    <br>
        <div style="border-top: solid 1px; width: 40%; margin: 0 auto;">
            <center>
            <?php echo strtoupper($Aluno->usu_nomecompleto); ?><br>
            </center>
        </div>
</div>
<?php } else { ?>
<div class="content" style="margin-bottom: -20px;">
    Nenhuma nota promissória para esse registro foi encontrada!
</div>

<?php } //if $parcelas?>
<script>
    
    $(function() {
        $(".main-footer").hide();
        
        $(".valor").keyup( function() {
            $(".valor").val( $(".valor").val() );
            //$(".valorext").val( $(".valor").val().extenso() );
        });        
        
        $(".valorext").keyup( function() {
            $(".valorext").val( $(".valorext").val() );
        });        
        
        /* ##############################################################
        var valor = prompt("Informe o Valor", "");
        if (valor != null) {
            $(".valor").val(valor);
        }
        ############################################################## */
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
        /**/


        String.prototype.extenso = function(c){
            var ex = [
                ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
                ["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
                ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
                ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
            ];
            var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
            for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
                j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
                if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
                for(a = -1, l = v.length; ++a < l; t = ""){
                    if(!(i = v[a] * 1)) continue;
                    i % 100 < 20 && (t += ex[0][i % 100]) ||
                    i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
                    s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
                    ((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
                }
                a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
                a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
            }
            return r.join(e);
        }
        
        
        /**/


    });
    

    
</script>



