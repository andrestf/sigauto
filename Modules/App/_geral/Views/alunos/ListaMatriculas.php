<?php
    $funcs = new FuncoesHelper(); 
?>
<div class="content">
    <h3 class="page-title">Matrícula - <small><?php echo $aluno->usu_nomecompleto; ?></small></h3>
    <br/>
  
        <a href="<?php echo Permalink("alunos","")?>" class="btn btn-primary"> <i class="fa fa-list-ul"></i> Alunos </a>
        <a href="<?php echo Permalink("alunos","editar","codigo=".(int) $_GET['aluno'])?>" class="btn btn-default "> <i class="fa fa-user"></i> Ficha do Aluno </a>
        <a href="<?php echo Permalink("AlunosFinanceiro","index","codigo=".(int) $_GET['aluno'])?>" class="btn btn-default "> <i class="fa fa-money"></i> Financeiro </a>
        <div class="pull-right">
            
            <a href="<?php echo Permalink("processos","incluir","aluno=".(int) $_GET['aluno'])?>" class="btn btn-success "> <i class="fa fa-plus-circle"></i> Novo Processo </a>
        </div>
  
    <br/><br/>
    
    <div class="form" style="padding: 10px 15px;">
        <h4 class="ficha_title"><i class="fa fa-list-alt"></i> Matrículas Cadastradas </h4>
        <hr/>
        <table class="table table-bordered table-condensed table-hover table-responsive table-striped">
            <thead>
                <tr>
                    <th width="">Tipo</th>
                    <th width="120">Situação</th>
                    <th width="40">Categoria</th>
                    <th width="180">Fase</th>
                    <th width="120">Data de Registro</th>
                    <th width="100"></th>
                </tr>
            </thead>
            <tbody>
                <?php if($matriculas->num_rows == 0) { 
                    echo "<tr><td colspan='5'>Nenhuma matrícula cadastrada para o aluno selecionado!</td></tr>";
                } else { ?>
                    <?php while( $mat = $matriculas->fetch_object() ) { 
                        $style = '';
                        
                        if($mat->amCan_data != '') {
                            $style .= "    background: #ffdddd;";
                        }
                        $categorias = explode(",",$mat->amat_catcnh);
                        
                        
                        $cat_descri = "";
                        $Categorias = new CategoriasModel();
                        foreach ($categorias as $cat) {
                            $cat_descri .= $Categorias->retcampo("catc_categoria",$cat) . "/";
                        }
                        
                        $cat_descri = substr($cat_descri, 0,-1);
                        ?>
                <tr  class="cursor" style="<?php echo $style;?>" data-toogle="popover" title="<?php echo  $mat->amCan_motivo?>">
                    <td <?php if($mat->amCan_data == '') {?> onclick='AbreProcesso(<?php echo $mat->amat_id?>)' <?php } ?> class="" ><?php echo $funcs->fRetCampo('sis_servicos','serv_descricao'," serv_id = $mat->amat_servico ") ."<br> <small>". $mat->tpmat_descricao."</small>"; ?></td>
                    <td <?php if($mat->amCan_data == '') {?> onclick='AbreProcesso(<?php echo $mat->amat_id?>)' <?php } ?>  class="" ><?php echo $mat->amat_situacao?></td>
                    <td <?php if($mat->amCan_data == '') {?> onclick='AbreProcesso(<?php echo $mat->amat_id?>)' <?php } ?> class="" ><?php echo $cat_descri;?></td>
                    <td <?php if($mat->amCan_data == '') {?> onclick='AbreProcesso(<?php echo $mat->amat_id?>)' <?php } ?> class="" ><?php echo $this->SelecionaFaseAtual($aluno_id,$mat->amat_id);?></td>
                    <td <?php if($mat->amCan_data == '') {?> onclick='AbreProcesso(<?php echo $mat->amat_id?>)' <?php } ?> class="" ><?php echo DataBR($mat->amat_situacaodata)?></td>
                    <td>
                        <center>
                        <?php if($mat->amCan_data == '') {?>
                        
                        <?php
                              if($mat->amat_exporta == '') {
                                echo "<a href='".Permalink('AlunosFinanceiro','IniciaLancaParcelas','codigo='.$aluno->usu_id.'&matricula='.$mat->amat_id)."'><i class='btn btn-default btn-xs fa fa-money text-red' data-toggle='tooltip' title='Parcelas não lancadas, clique para lançar'></i></a>";
                              } else {
                                echo "<i class='  btn btn-default btn-xs fa fa-money text-green' data-toggle='tooltip' title='Parcelas Lançadas'></i>";
                              } ?>
                              &nbsp;
                              <?php if($usuario->usu_nivel >= 40) {?>
                              <a href="javascript: CancelarProcesso(<?php echo $mat->amat_id; ?>,<?php echo $_GET['aluno']; ?>)">
                                    <i class=" btn btn-default btn-xs fa fa-ban text-orange" data-toggle="tooltip" title="Cancelar Processo"></i>
                                </a>
                              <?php } ?>
                        
                        <?php } else {?>
                              <i class="fa fa-info-circle" ></i>
                        <?php } ?>
                        </center>
                    </td>
                </tr>
                    <?php }//while ?>
                <?php }//else ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCancelamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cancelamento de Processo</h4>
      </div>
      <div class="modal-body">
          <b class="text-red">"Cancelar processo? Ação não poderá ser revertida!!!"</b>
          <br/>
          Motivo:
          <input type="text" id="motivoCancelamento" class="form-control" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" onclick="confirmaCancelamento();" class="btn btn-primary">Confirmar</button>
      </div>
    </div>
  </div>
</div>


<script>
    function AbreProcesso(ID) {
        window.location = "<?php echo Permalink("processos","detalhes","aluno=".(int)$_GET['aluno']."&processo=")?>"+ID;
    }
    
    var idCancelamento, aID;
    function CancelarProcesso(id,a) {
        idCancelamento = id;
        aID = a;
        
        $("#ModalCancelamento").modal('show');
        return;
        alertify.confirm(
                "Atenção",
                "Cancelar processo? <br /> Ação não poderá ser revertida!!!",
                function(){
                    window.location = '<?php echo Permalink("processos","cancelamento")?>/&aluno='+a+'&processo='+id;
                },
                
                function(){
                }
                
                );
    }
    
    function confirmaCancelamento() {
    
        motivo = $("#motivoCancelamento").val();
        
        if(motivo == '') {
            alertify.error("Informe o motivo de cancelamento!");
            return;
        }
        
        window.location = '<?php echo Permalink("processos","cancelamento")?>/&aluno='+aID+'&processo='+idCancelamento+"&motivo="+motivo;
    }
</script>