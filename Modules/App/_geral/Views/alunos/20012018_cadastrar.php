<form class="" method="post" name="frm_aluno" id="frm_aluno" action="" onsubmit="salvar(); return false;">
    <div class="content asec-box" id="ficha_novo_aluno">
        <h3 class="page-title">
            <?php echo $view_PageTitle ?>
        </h3>

        <BR/>&nbsp;

        <div class="pull-right grupo-btn">
            <button type="reset" onclick="cancelar();" class="btn btn-link"> <i class="fa fa-arrow-left"></i>  Cancelar </button> &nbsp;
            <span class="grupo-btn-01">
                <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Salvar</button>
            </span>

            <span class="grupo-btn-02" style="display: none">

                <span class="btn btn-info btnEditar" > <i class="fa fa-save "></i> Salvar</span>

                <div class="btn-group hide">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-print"></i> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu ">
                    <li><a href="#" class="btnContrato">Contrato</a></li>
                    <li><a href="#" class="btnFicha">Ficha</a></li>
                    <li><a href="#" class="btnNP">Promissória</a></li>
                  </ul>
                </div> 

                <div class="btn-group hide">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cogs"></i> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#" class="btnExporta">PréCadastro eCNH</a></li>
                    <li><a href="#" class="btnProcessos">Processos</a></li>
                  </ul>
                </div>                 

                <span class="btn btn-dropbox btnFicha" > <i class="fa fa-user"></i> Ficha</span>
                <span class="btn btn-primary btnContrato" > <i class="fa fa-newspaper-o"></i> Contrato</span>
                <span class="btn btn-linkedin btnNP" > <i class="fa fa-money"></i> Promissória</span>

                <span class="btn btn-danger btnExporta"> <i class="fa fa-bars "></i> PréCadastro eCNH</span>
                <span class="btn btn-warning btnProcessos"> <i class="fa fa-cogs "></i> Processos</span>
                <?php if($matriculas) { ?>
                <a data-toggle="tooltip" title="Financeiro" href="<?php echo Permalink('AlunosFinanceiro/index','','codigo='.$view_Aluno->usu_id)?>" class="btn btn-success btnFinanceiro"> <i class="fa fa-money"></i> Financeiro</a>
                <?php } ?>



            </span>
        </div>
        
        <?php if(isset($matriculas)) { ?>
        <?php if(!$matriculas) { ?>
            <BR/><BR/>
            <div class="alert alert-danger text-center"><h3>Aluno sem matricula</h3></div>
            
        <?php } else { ?>

        
            <section id="infos" style="margin-top: 45px;">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-aqua">
                                &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;
                                Matrículas 
                                &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp 
                                | 
                                &nbsp;&nbsp;&nbsp;&nbsp 
                                Vencimento Processo
                            </div>
                            <div class="box-body text-center" style="    min-height: 60px;">
                                <?php if($matriculas) {
                                        echo count($matriculas); } else { echo "Aluno sem matricula."; } ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp | &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if(is_null(@$view_Aluno->usu_procvenc)) { 
                                        echo "Sem Renach"; } else { echo DataBR(@$view_Aluno->usu_procvenc); } ?> 
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-fuchsia">
                                Fase Atual
                            </div>
                            <div class="box-body text-center" style="    min-height: 60px;">
                                <?php if($fases) { ?>
                                    <?php foreach ($fases as $fasex) { ?>
                                        <?php echo $fasex['serviten_descricao']; ?> <br/>
                                    <?php } ?>
                                <?php } else {  ?>

                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-orange">
                                Financeiro
                            </div>
                            <div class="box-body text-center">
                                <div class="row">
                                    <div class="col-md-4 text-red">Em aberto<br/> R$ <?php echo number_format($valorDevido,2,",","."); ?></div>
                                    <div class="col-md-4 text-blue">Pago<br/> R$ <?php echo number_format($valorPago,2,",","."); ?></div>
                                    <div class="col-md-4 text-green">Crédito <br/> R$ <?php echo number_format($creditosDisponivel,2,",","."); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        <?php } //if !$matriculas ?>
        <?php } //isset matricula ?>
        <section id="dados_pessoais">
            <h4 class="ficha_title">Dados Pessoais</h4>
            <div class="form">
                <input type="hidden" id="aluno_id" name="id" value="<?php echo @$view_Aluno->usu_id?>" />
                <input type="hidden" id="aluno_acao" name="acao" value="incluir" />
                <input type="hidden" id="aluno_acesso" name="acesso" value="" />
                <input type="hidden" id="aluno_chave" name="chave" value="" />
                
                <div class="form-group">
                    <div class="col-md-2">
                        <label>Foto</label>
                        <div class="div-foto">
                            <img src="http://econdutorcfc.com/alunos/semfoto.jpg" class="foto-aluno hide" />
                            
                            <img src="/alunos/GetFoto/?codigo=<?php echo $view_Aluno->usu_id?>" class="foto-aluno"  />
                            <div class="foto-edit">
                                <i class="fa fa-pencil cursor" onclick="FotoUpload();"></i>
                                &nbsp;&nbsp;&nbsp;
                                <i class="fa fa-trash cursor" onclick="FotoRemove();"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <div class="col-sm-9 bottom10">
                                <label for="nomecompleto">Nome completo <span class="obg">*</span></label>
                                <input id="nomecompleto" name="nomecompleto" placeholder="Nome completo do aluno" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_nomecompleto?>" requiredx="">
                            </div>

                            <div class="col-sm-3 bottom10">
                                <label for="situacao">Situação </label>
                                <input id="situacao" name="situacao" placeholder="Situacao do aluno" maxlength="30" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_situacao?>" requiredx="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 bottom10">
                                <label for="nomepai">Nome do pai <span class="obg">*</span></label>
                                <input id="nomepai" name="nomepai" placeholder="Nome do pai" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)"  value="<?php echo @$view_Aluno->usu_nomepai; ?>"  requiredx="">
                            </div>
                            <div class="col-sm-6 bottom10">
                                <label for="nomemae">Nome da mãe <span class="obg">*</span></label>
                                <input id="nomemae" name="nomemae" placeholder="Nome da mãe" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_nomemae; ?>" requiredx="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 bottom10">							
                                <label for="nascimento">Data nascimento <span class="obg">*</span></label>

                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar"></span></span>
                                    <input id="nascimento" name="nascimento" maxlength="10" placeholder="dd/mm/aaaa" type="text" class="form-control datepicker" value="<?php echo DataBR(@$view_Aluno->usu_nascimento); ?>" requiredx="" style=" z-index: 1 !important;">
                                </div>
                            </div>

                            <div class="col-sm-6 bottom10">
                                <input type="hidden"  name="sexo"/>
                                <label for="sexo">Sexo <span class="obg">*</span></label><br>
                                <input id="sf" type="radio"  name="sexo" value="F" <?php echo (@$view_Aluno->usu_sexo == "F") ? "checked='checked'" : '';  ?> > Feminino &nbsp;&nbsp;
                                <input id="sm" type="radio" name="sexo" value="M"  <?php echo (@$view_Aluno->usu_sexo == "M") ? "checked='checked'" : '';  ?>> Masculino
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-12 col-md-10 col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="nacionalidade">Nacionalidade  <span class="obg">*</span></label><br>
                                <select name="nacionalidade" id="nacionalidade" class="form-control ">
                                    <option value="1" <?php echo (@$view_Aluno->usu_nacionalidade == "1") ? "selected='selected'" : '';  ?> >Brasileiro</option>
                                    <option value="2" <?php echo (@$view_Aluno->usu_nacionalidade == "2") ? "selected='selected'" : '';  ?>>Brasileiro Naturalizado</option>
                                    <option value="3" <?php echo (@$view_Aluno->usu_nacionalidade == "3") ? "selected='selected'" : '';  ?>>Estrangeiro</option>
                                    <option value="4" <?php echo (@$view_Aluno->usu_nacionalidade == "4") ? "selected='selected'" : '';  ?>>Brasileiro Nascido no Exterior</option>
                                </select>
                            </div>
                            <div class="col-sm-3" id="ufpessoadiv">
                                <label for="uf">UF <span class="obg">*</span></label><br>
                                <select name="uf" class="form-control select2" id="ufpessoa">
                                    <option value=""> Selecione </option>
                                        <?php while($uf = $ListaUF->fetch_object()) { ?>
                                            <option value="<?php echo $uf->uf; ?>" data-id="<?php echo $uf->codigo; ?>" <?php if($uf->uf == @$view_Aluno->usu_uf) { echo 'selected="selected"';}?> > <?php echo $uf->uf; ?>  </option>
                                        <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-5">
                                <label for="naturalidade"> Naturalidade <span class="obg">*</span></label><br>
                                <select name="naturalidade" class="form-control select2x" id="naturalidade">  </select>
                            </div>
                        </div>                          
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <section id="documentos">
            <h4 class="ficha_title">Documentos</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="cpf">CPF <span class="obg">*</span></label>
                        <input id="cpf" name="cpf" placeholder="000.000.000-00" onkeyup="formataCPF(this)" maxlength="14" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_cpf; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rg">RG <span class="obg">*</span></label>
                        <input id="rg" name="rg" placeholder="000000000" maxlength="20" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_rg; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rgemissor">Orgão Emissor <span class="obg">*</span></label>
                        <input id="rgemissor" name="rgemissor" placeholder="Ex.: SSP" maxlength="25" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_rgemissor; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rguf">UF <span class="obg">*</span></label>
                        <select id="rguf" name="rguf" class="form-control select2 " requiredx="">
                            <option value=""> Selecione </option>
                                <?php while($uf2 = $ListaUF2->fetch_object()) { ?>
                                    <option value="<?php echo $uf2->uf; ?>"  <?php if($uf2->uf == @$view_Aluno->usu_rguf) { echo 'selected="selected"'; }?> > <?php echo $uf2->uf; ?> </option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>

        <section id="endereco">
            <h4 class="ficha_title">Endereço</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="cep">CEP <span class="obg">*</span></label>
                        <input id="cep" name="cep" placeholder="CEP" maxlength="9" class="form-control cep" type="text" value="<?php echo @$view_Aluno->usu_cep; ?>" requiredx="" onblur="buscacep();">
                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank" class="fa fa-search btn btn-default" style="  float: right; margin-top: -34px;  height: 34px;"></a>
                    </div>
                    <div class="col-sm-6 bottom10">
                        <label for="logradouro">Logradouro <span class="obg">*</span></label>
                        <input id="logradouro" name="logradouro" onkeyup="filtraCampo(this)" placeholder="Logradouro" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logradouro; ?>" requiredx="">
                    </div>                
                    <div class="col-sm-3 bottom10">
                        <label for="logranumero">Numero <span class="obg">*</span></label>
                        <input id="logranumero" name="logranumero" onkeyup="formataNumerico(this)" placeholder="Número" maxlength="9" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logranumero; ?>" requiredx="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="lograbairro">Bairro <span class="obg">*</span></label>
                        <input id="lograbairro" name="lograbairro" onkeyup="filtraCampo(this)" placeholder="Bairro" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_lograbairro; ?>" requiredx="">
                    </div>
                    <div class="col-sm-3 bottom10">
                        <label for="logramunicipio">Município <span class="obg">*</span></label>
                        <input id="logramunicipio" name="logramunicipio" onkeyup="filtraCampo(this)" placeholder="Município" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logramunicipio; ?>" requiredx="">
                    </div>              
                    <div class="col-sm-3 bottom10">
                        <label for="lograuf">Complemento </label>
                        <?php if(@$view_Aluno->usu_logracomplemento == '') {
                            $complemento = "CASA";
                        } else {
                            $complemento = @$view_Aluno->usu_logracomplemento;
                        } ?>
                        <input id="logracomplemento" name="logracomplemento" placeholder="Complemento" maxlength="30" class="form-control" type="text" value="<?php echo $complemento; ?>" requiredx="">
                    </div>                    
                    <div class="col-sm-3 bottom10">
                        <label for="lograuf">UF <span class="obg">*</span></label>
                        <input id="lograuf" name="lograuf" placeholder="UF" maxlength="2" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_lograuf; ?>" requiredx="">
                    </div>
                    

                </div>
                <div class="clearfix"></div>

            </div>
        </section>


        <section id="dadosCondutor">
            <h4><b>Dados do Condutor</b></h4>

            <div class="form">

                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="renach">Renach</label>
                        <input id="renach" name="renach" placeholder="000000000" onkeyup="formataNumerico(this)"  maxlength="9" pattern="[0-9]{9}$" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_renach; ?>">
                    </div>

                    <div class="col-sm-3">
                        <label for="tipodoc">Tipo de Documento</label>
                        <select id="tipodoc" name="tipodoc" class="form-control">
                            <option value="0" selected="">SELECIONE</option>
                            <option value="1" <?php echo (@$view_Aluno->usu_tipodoc == "1") ? "selected='selected'" : '';  ?> >PGU (nº CNH sem foto)</option>
                            <option value="2" <?php echo (@$view_Aluno->usu_tipodoc == "2") ? "selected='selected'" : '';  ?> >Registro (nº CNH com foto)</option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <label for="numdoc">Nº Registro/PGU</label>
                        <input id="numdoc" name="numdoc" id="numdoc" placeholder="00000000000" onkeyup="formataNumerico(this)"  maxlength="11" pattern="[0-9]{11}$" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_numdoc; ?>">
                    </div>

                    <div class="col-sm-3">
                        <label for="catatual">Categoria Atual</label>
                        <select id="catatual" name="catatual" class="form-control">
                            <option value="" selected="">SELECIONE</option>
                            <option value="A" <?php echo (@$view_Aluno->usu_catatual == "A") ? "selected='selected'" : '';  ?> >A - Moto</option>
                            <option value="B" <?php echo (@$view_Aluno->usu_catatual == "B") ? "selected='selected'" : '';  ?> >B - Automóvel</option>
                            <option value="C" <?php echo (@$view_Aluno->usu_catatual == "C") ? "selected='selected'" : '';  ?> >C - Caminhão</option>
                            <option value="D" <?php echo (@$view_Aluno->usu_catatual == "D") ? "selected='selected'" : '';  ?> >D - Ônibus</option>
                            <option value="E" <?php echo (@$view_Aluno->usu_catatual == "E") ? "selected='selected'" : '';  ?> >E - Veículo Acoplado</option>
                            <option value="AB" <?php echo (@$view_Aluno->usu_catatual == "AB") ? "selected='selected'" : '';  ?> >AB</option>
                            <option value="AC" <?php echo (@$view_Aluno->usu_catatual == "AC") ? "selected='selected'" : '';  ?> >AC</option>
                            <option value="AD" <?php echo (@$view_Aluno->usu_catatual == "AD") ? "selected='selected'" : '';  ?> >AD</option>
                            <option value="AE" <?php echo (@$view_Aluno->usu_catatual == "AE") ? "selected='selected'" : '';  ?> >AE</option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <label for="faseecnh">Fase ECNH</label>
                        <select id="faseecnh" name="faseecnh" class="form-control">
                            <option value="" selected="">SELECIONE</option>
                            <option value="PRIHAB" <?php echo (@$view_Aluno->usu_faseecnh == "PRIHAB") ? "selected='selected'" : '';  ?> >Primeira Habilitação </option>
                            <option value="RENOVA" <?php echo (@$view_Aluno->usu_faseecnh == "RENOVA") ? "selected='selected'" : '';  ?> >Renovação </option>
                            <option value="ADIMUD" <?php echo (@$view_Aluno->usu_faseecnh == "ADIMUD") ? "selected='selected'" : '';  ?> >Adição / Mudança </option>
                            <option value="INIREA" <?php echo (@$view_Aluno->usu_faseecnh == "INIREA") ? "selected='selected'" : '';  ?> >Iniciar Reabilitação </option>
                            <option value="REACRI" <?php echo (@$view_Aluno->usu_faseecnh == "REACRI") ? "selected='selected'" : '';  ?> >Reabilitação-Cassação/Crime </option>

                        </select>
                    </div>

                </div>
            
                <div class="clearfix"></div>
            </div>

        </section>
        
        
        <section id="contatos" style="display: block;">
            <h4><b>Telefones</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label>Celular</label>
                        <input type="text" class="form-control telefone" name="telcelular" id="telcelular" value="<?php echo @$view_Aluno->usu_telcelular; ?>"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Residencial</label>
                        <input type="text" class="form-control telefone" name="telresid" id="telresid" value="<?php echo @$view_Aluno->usu_telresid; ?>"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Comercial</label>
                        <input type="text" class="form-control telefone" name="telcomercial" value="<?php echo @$view_Aluno->usu_telcomercial; ?>" />
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Contato</label>
                        <input type="text" class="form-control telefone" name="telrecado" value="<?php echo @$view_Aluno->usu_telrecado; ?>"/>
                    </div>
                </div>
                
                <div class="clearfix"></div>
             </div>
            
        </section>
        
        <section id="acesso">
            <h4><b>Acesso</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" value="<?php echo @$view_Aluno->usu_email; ?>"/>
                    </div>

                    <div class="col-sm-4">
                        <label>Acesso</label>
                        <select class="form-control" name="sitacesso">
                            <option value="A" <?php echo (@$view_Aluno->usu_sitacesso == "A") ? "selected='selected'" : '';  ?> >Ativo</option>
                            <option value="I" <?php echo (@$view_Aluno->usu_sitacesso == "I") ? "selected='selected'" : '';  ?> >Inativo</option>
                        </select>
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Matricula</label>
                        <input type="text" class="form-control matricula" name="matricula" value="<?php echo @$view_Aluno->usu_matricula; ?>"/>
                    </div>

                </div>
                
                <div class="clearfix"></div>
             </div>            
            
        </section>
        
        <section id="observacao">
            <h4><b>Observações</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Observacao</label><br/>
                        <input type="text" multiple="multiple" name="observa" class="form-control obs" value="<?php echo @$view_Aluno->usu_observa; ?>" />
                    </div>

                </div>

                <div class="clearfix"></div>
             </div>

        </section>

        <hr/>
        <div class="pull-right grupo-btn">
            <button type="reset" onclick="cancelar();" class="btn btn-link"> <i class="fa fa-arrow-left"></i>  Cancelar </button> &nbsp;
            <span class="grupo-btn-01">
                <button type="submit" class="btn btn-info"> <i class="fa fa-save"></i> Salvar</button>
            </span>
            
            <span class="grupo-btn-02" style="display: none">
                
                <span class="btn btn-info btnEditar" > <i class="fa fa-save "></i> Salvar</span>
                <?php if(@$matriculas) { ?>
                
                    <span class="btn btn-dropbox btnFicha" > <i class="fa fa-user"></i> Ficha</span>
                    <span class="btn btn-primary btnContrato" > <i class="fa fa-newspaper-o"></i> Contrato</span>
                    <span class="btn btn-linkedin btnNP" > <i class="fa fa-money"></i> Promissória</span>
                    <span class="btn btn-danger btnExporta"> <i class="fa fa-bars "></i> PréCadastro eCNH</span>             
                    <span class="btn btn-warning btnProcessos"> <i class="fa fa-cogs "></i> Processos</span>
                    <a  href="<?php echo Permalink('AlunosFinanceiro/index','','codigo='.$view_Aluno->usu_id)?>" class="btn btn-success btnFinanceiro"> <i class="fa fa-money"></i> Financeiro</a>
                <?php } ?>
            </span>
        </div>
    </div>
</form>

<script>
    var codigo = "";
    <?php if(isset($view_Aluno->usu_id)) { ?>
        codigo = '<?php echo $view_Aluno->usu_id; ?>';
        Ficha2();
        
    <?php } ?>
    
    
    $(function(){
        alertify.set('notifier','position', 'top-right');
    
        $('body').on('keydown', 'input, select, textarea', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
        })
    });

    $(".btnFicha").click(function() {
        window.location = "<?php echo Permalink("alunos","impFicha")?>/&aluno="+codigo; 
    });
    
   $(".btnContrato").click(function() {
       window.location = "<?php echo Permalink("alunos","contrato")?>/&aluno="+codigo;
   });
   
   $(".btnNP").click(function() {
       window.location = "<?php echo Permalink("alunos","notapromissoria")?>/&aluno="+codigo;
   });
    
   $("#nacionalidade").change(function() {
       var nac = $("#nacionalidade option:selected").val();
       
       
       if(nac == 3 || nac == 4){
           $("#ufpessoadiv").hide();
           CarregaListaPaises();
       } else {
           $("#ufpessoadiv").show();
       }
   })
   function CarregaListaPaises(callback) {
        $("#naturalidade").html('<option value="0"> Carregando </option>');
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaListaPaises/',
            success: function(e) {
                $("#naturalidade").html(e);
//                callback();
            },
            erro: function(){
                alertify.error("Erro ao carregar Naturalidade!");
            }
        });        
    }
   
    $(".btnEditar").click( function() {
        var dados = $("#frm_aluno").serializeArray();
        $.ajax({
           data : dados,
           dataType : "json",
           type : 'post',
           url : '/index.php?route=alunos/SalvaFicha',
           success : function(e) {
               if(e.erro != "") {
                   alertify.error(e.mensagem);
                   return false;
               }
               
               alertify.success(e.mensagem);
           }
        });
        
    });
   
   
    $(".btnProcessos").click( function() {
        if(codigo == "") {
            alertify.error("Concluir cadastro antes de iniciar processos!");
            return false;
        }
        
        window.location = "<?php echo Permalink("alunos","ListaMatriculas")?>/&aluno="+codigo;
    })
    
    
    function cancelar() {
        window.location = "<?php echo Permalink("alunos")?>"
    }
    
    function Ficha2(){
        $(".grupo-btn-01").hide();
        $(".grupo-btn-02").show();
    }
    
    function Ficha1(){
        $(".grupo-btn-01").show();
        $(".grupo-btn-02").hide();
    }
    function salvar() {

        if($("#aluno_id").val() !== "" ) {
          
            return false;
        }
        
        var dados = $("#frm_aluno").serializeArray();
        
        $.ajax({
            data : dados,
            dataType: "json",
            type : "post",
            url  : '/index.php?route=alunos/incluir',
            success: function(e) {
                
                if(e.erro != "") {
                    alertify.error(e.mensagem);
                    Ficha1();
                    return false;
                    
                }
                
                codigo = e.id;
                $("#aluno_id").val(e.id);
                $("#aluno_acao").val('editar');
                alertify.success('Aluno cadastrado com sucesso!');
                Ficha2();
                location.href="index.php?route=alunos/editar/&codigo="+codigo;
                
            },
            error : function(e,x,s) {
                console.log(e +"\n"+ x +"\n"+ s)
            },
            
            complete: function() {}
        });
    }
    

    
    
    
    $("#ufpessoa").change(function() {
        CarregaNaturalidade(function(){void(0)});
    });
    
    function CarregaNaturalidade(callback) {
        var CODIGO_ESTADO = $("#ufpessoa option:selected").attr("data-id");
        
        $("#naturalidade").html('<option value="0"> Carregando </option>');
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaNaturalidade/&codigo='+CODIGO_ESTADO,
            success: function(e) {
                $("#naturalidade").html(e);
                callback();
            },
            erro: function(){
                alertify.error("Erro ao carregar Naturalidade!");
            }
        });        
    }
    
    
    $(function() {
        var CODIGO_ESTADO = $("#ufpessoa option:selected").val();
        if(CODIGO_ESTADO !== '' || CODIGO_ESTADO !== '0') {
            
            $("#naturalidade").html('<option value="<?php echo @$view_Aluno->usu_naturalidade?>"><?php echo @$view_Aluno->usu_naturalidade?></option>');
            //CarregaNaturalidade(function(){
            //   $("#naturalidade").val('<?php echo @$view_Aluno->usu_naturalidade?>');
            //});
        }
    });
    
    
    $(".btnExporta").click( function() {
        ExportaECNH();
    });
    var CodigoMunicipio, metodo;
    function ExportaECNH() {
        CodigoMunicipio = "";
        xTemp = $("#logramunicipio").val();
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaCodMunicipioDeTranSP/&municipio='+xTemp,
            success: function(e) {
                CodigoMunicipio = e;
                //alert(CodigoMunicipio);
                
            },
            erro: function(){
                //alertify.error("Erro ao carregar Naturalidade!");
            },
            complete: function() {
                ExportaECNH2();
            }
        });
    }
    
    
    
    function ExportaECNH2() {

        
        metodo = $("#faseecnh option:selected").val();
        
        if(metodo == "PRIHAB") {
            metodo = 'iniciarPrimeiraHabilitacao';
        }else if(metodo == "INIREA") {
            metodo = 'iniciarReabilitacao';        
        }else if(metodo == "RENOVA") {
            metodo = 'iniciarRenovacao';
        }else if(metodo == "ADIMUD") {
            metodo = 'iniciarMudancaCategoria';
        }else if(metodo == "REACRI") {
            metodo = 'iniciarCassacaoReabilitacao';
        }else {
            alert("Informe a fase do ECNH");
           return false;
        }
        
        if( $("#sf:checked").val() ) {
            sexo = "F";
        }
        
        if( $("#sm:checked").val() ) {
            sexo = "M";
        }
        
        var url;
        rg = $("#rg").val();
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        
        url = ""+
              "&nome="+$("#nomecompleto").val()+
              "&nomePai="+$("#nomepai").val()+
              "&nomeMae="+$("#nomemae").val()+
              "&dataNascimento="+$("#nascimento").val()+
              "&sexo="+sexo+
              "&cpf="+$("#cpf").val()+
              "&numDocumento="+rg+
              "&orgaoDocumento="+$("#rgemissor").val()+
              "&ufDocumento="+$("#rguf").val()+
              "&cep="+$("#cep").val()+
              "&logradouro="+$("#logradouro").val()+
              "&numero="+$("#logranumero").val()+
              "&complemento="+$("#logracomplemento").val()+
              "&bairro="+$("#lograbairro").val()+
              "&municipio="+$("#logramunicipio").val()+
              "&email="+$("#email").val()+
              "&telefone="+$("#telresid").val().replace(" ","").replace("-","")+
              "&celular="+$("#telcelular").val().replace(" ","").replace("-","")+
              "&nacionalidade="+$("#nacionalidade").val()+
              "&confirmEmail="+$("#email").val()+
              "&idMunicipio="+CodigoMunicipio+
              "&numRegPgu="+$("#numdoc").val()+
              "&categoriaAtual="+
              "&captchaResponse="+
              "&autorizaSMS=S"+
              "&autorizaEmail="+
              
              "";

//              "&tipoRegPgu="+$("#rgemissor").val()+
//              "&numRegPgu="+$("#rgemissor").val()+
//              "&categoriaAtual="+$("#rgemissor").val()+
//              "&autorizaSMS="+$("#rgemissor").val()+
//              "&autorizaEmail="+$("#rgemissor").val()+
//              "&captchaResponse="+$("#rgemissor").val()+
//              "&idMunicipio="+$("#rgemissor").val()+

        window.open("https://www.e-cnhsp.sp.gov.br/gefor/GFR/candidato/cadastro/preCadastro.do?method="+metodo+""+url,"_blank");
                          
                          
//                            &nomePai=Cadastro%20Teste
//                            &nomeMae=Cadastro%20Teste
//                            &dataNascimento=01/01/1980
//                            &sexo=F
//                            &cpf=385.738.207-48
//                            &numDocumento=00000000
//                            &orgaoDocumento=ssp
//                            &ufDocumento=SP
//                            &tipoRegPgu=0
//                            &numRegPgu=
//                            &categoriaAtual=
//                            &cep=18600-010
//                            &logradouro=R%20DOUTOR%20COSTA%20LEITE
//                            &numero=150
//                            &complemento=
//                            &bairro=CENTRO
//                            &municipio=BOTUCATU
//                            &telefone=
//                            &email=
//                            &celular=
//                            &confirmEmail=
//                            &autorizaSMS=S
//                            &autorizaEmail=S
//                            &captchaResponse=
//                            &idMunicipio=6249

    
    
    }
    
</script>
