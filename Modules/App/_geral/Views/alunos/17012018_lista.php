<section class="content asec-box" id="aluno">
    <h3 class="page-title">Alunos</h3> <br/>

    <div class="box">
        <div class="box-header">
            <a href="<?php echo $this->Link('alunos',"relatorio");?>" ><span class="btn btn-info btnRel"> <i class="fa fa-list-alt"></i> Relatórios</span> </a>
            <a href="<?php echo $this->Link('alunos',"cadastrar");?>" ><span class="btn btn-primary btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
            <a href="<?php echo $this->Link('alunos',"processosgerais");?>" ><span class="btn btn-danger btnProcGerais"> <i class="fa fa-cogs"></i> Processos Gerais</span> </a>
        </div>
    </div>
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-graduation-cap"></i>
                Cadastrados</h3>
            <div class="box-tools pull-right hide">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body" id="">
            <form id="frm_alunos_busca">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control" />
                    </div>

                    <div class="col-sm-4">
                        <label>CPF</label>
                        <input type="text" name="cpf" class="form-control"/>
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Situação</label>
                        <select class="form-control" name="situacao" >
                            <option value="">Todos</option>
                            <option value="1">Ativos</option>
                            <option value="2">Inativos</option>
                        </select>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-sm-4">
                        <label>Fase</label>
                        <?php $faseDB = new DB(); $faseDB->CnCliente();?>
                        <?php $faseDB = $faseDB->ExecQuery("SELECT * FROM sis_servicositens WHERE serviten_ativo = 's' order by serviten_descricao ASC")?>
                        <select class="form-control" name="fase" >
                            <option value=""></option>
                            <?php while($fase = $faseDB->fetch_object()) {?>
                                <option value="<?php echo $fase->serviten_id?>"><?php echo $fase->serviten_descricao?></option>
                            <?php } ?>
                        </select>
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Limite</label>
                        <select class="form-control" name="limite" >
                            <option value="10" selected="selected">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                            <option value="*">Sem Limite</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                            
                        </span>
                    </div>
                </div>
                    
                
            </form>
            <hr/>
            <div id="lista_alunos"></div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->


<script>

    $(function(){
        ListaAlunos();
    });
    
    $("#frm_alunos_busca").submit( function(e){
        e.preventDefault();
        ListaAlunos();
    });
    
    
    function ListaAlunos() {
        $("#lista_alunos").html( $("#ld").html() );
        $datas = $("#frm_alunos_busca").serialize();
        $.ajax({
            type: 'get',
            url : "/index.php?route=alunos/listagem/?&"+$datas,
            success: function(res) {
                $("#lista_alunos").html(res);
            }
            
        })
    };
    
    function EditarAluno(id) {
        linkx = "<?php echo $this->Link("alunos","editar")?>";
        window.location = linkx + "/&codigo="+id;
    }

</script>
    

