<div class="content" style="padding: 0px;">
    <div class='box box-primary' style="border: solid 2px; padding: 2px;" >
        <div class='box-header'>
            
            <center>
                <div>
                    <h3 style="margin-top: -5px; height: 30px;">
                        <b><?php echo $_SESSION['APP_LOCALNOME'] ?></b><br>
                          Rua Prefeito Tonico de Barros, 20 - Centro - Botucatu/SP<br>
                          Fone: (14) 3882-4970<br>
                    </h3>
                </div>
            </center>            
            <br>
            <br>
            <br>
            <center>
                <div style="margin-left: 40px; font-size: 30px;">
                    <b>
                       <u>
                          R E C I B O  
                       </u>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         R$ <?php echo $movimento['finmov_valorbaixa']?>
                    </b>
                </div>
            </center>
        </div>
        <br>
        <div style="margin-left: 48px; font-size: 20px;">
            Recebemos de <b><?php echo $aluno->usu_nomecompleto?></b><br/>
            A importância de (<?php echo escreverValorMoeda( $movimento['finmov_valorbaixa'])?>)<br>
            <b>PARA MAIOR CLAREZA, FIRMO O PRESENTE RECIBO:</b></b>
        </div>
        <br>
        <div style="width: 98%; margin: 0 auto; text-align: right; font-size: 20px">
            <?php

            $ANO = substr($movimento['finmov_databaixa'],0,4);
            $MES = substr($movimento['finmov_databaixa'],5,2);
            $DIA = substr($movimento['finmov_databaixa'],8,2); ?>
            BOTUCATU, <?php echo $DIA; ?> DE <?php echo strtoupper(MesExtenso($MES))?> DE <?php echo $ANO?>.<br><br>
        </div>
        <div class="row">
            <div class="col-xs-5" style="border-top: solid 0px;">
            </div>
            <div class="col-xs-2"></div>
            <div class="col-xs-4" style="border-top: solid 1px;">
                <center>
                    <?php echo $_SESSION['APP_LOCALNOME'] ?>
                </center>
            </div>
        </div>
    </div>
</div>	



<script>
    
    $(function() {
        $(".main-footer").hide();
        $(".main-header").hide()  
    });
    
</script>