<script src="/Public/js/valorPorExtenso.js"></script>

<?php #for($i=0;$i<=0;$i++) { ?>
<div class="content" style="margin-bottom: -20px;">
    <div class='box box-primary' style="border: solid 2px #000" >
        <div class='box-header'>
            <h3 style="margin-top: -5px; height: 15px;">
                  R E C I B O
            </h3>
        </div>
        
        <div class='box-body' >  
            <div style="z-index: 1; position: relative;">
                <div class="text-right" style="margin-top: -40px;">
                    Pago em: <input type="text" style="border: 0px; text-align: right" placeholder="" size="20" class="datepicker"/>
                </div>
                <div class="text-right" style="margin-top: 0px;">
                    R$ <input type="text" class="valor" style="border: 0px; text-align: right" placeholder="Digite o valor" size="20" onkeyup="formataValor(this)"/>
                </div>
            </div>
                
                <br/>
                <div style="width: 100%; font-size: 18px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                    Recebemos de: <?php echo strtoupper($Aluno->usu_nomecompleto); ?> <br>
                    a importância de: R$ <br>
                    referente à <br>
                    PARA MAIOR CLAREZA, FIRMO O PRESENTE RECIBO
                       ( <b> <input type="text" style="border: 0px; width: 95%;" placeholder="Valor por extenso" class="valorext"/> )
                </div>
                
                
                <br/>

                    
                <div style="border-bottom: solid 1px; width: 50%; margin: 0 auto">
                    <?php echo $_SESSION['APP_LOCALCIDADE'] ?>,<br/><br/>
                </div>
                <div style="border-bottom: solid 0px; width: 50%; margin: 0 auto;">
                    <?php echo strtoupper($Aluno->usu_nomecompleto); ?><br>
                    <?php echo $Aluno->usu_logradouro;?>,
                    Nº <?php echo $Aluno->usu_logranumero; ?> - 
                    <?php echo $Aluno->usu_lograbairro; ?><br>
                    <?php echo $Aluno->usu_logramunicipio; ?> / 
                    <?php echo $Aluno->usu_lograuf; ?> - 
                    <?php echo $Aluno->usu_cep; ?></b> 
                </div>
        </div>
    </div>
</div>    
<?php# } ?>
<script>
    
    $(function() {
        $(".main-footer").hide();
        
        $(".valor").keyup( function() {
            $(".valor").val( $(".valor").val() );
            //$(".valorext").val( $(".valor").val().extenso() );
        });        
        
        $(".valorext").keyup( function() {
            $(".valorext").val( $(".valorext").val() );
        });        
        
        /* ##############################################################
        var valor = prompt("Informe o Valor", "");
        if (valor != null) {
            $(".valor").val(valor);
        }
        ############################################################## */
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
        /**/


        String.prototype.extenso = function(c){
            var ex = [
                ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
                ["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
                ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
                ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
            ];
            var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
            for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
                j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
                if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
                for(a = -1, l = v.length; ++a < l; t = ""){
                    if(!(i = v[a] * 1)) continue;
                    i % 100 < 20 && (t += ex[0][i % 100]) ||
                    i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
                    s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
                    ((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
                }
                a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
                a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
            }
            return r.join(e);
        }
        
        
        /**/


    });
    

    
</script>



