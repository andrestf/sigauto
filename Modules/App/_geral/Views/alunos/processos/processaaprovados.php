<?php 
	if (!isset($_GET['data']) && !isset($_GET['processo']) ) {
		$lContinua = false;
	} else {
		$lContinua = true;
	}

?>




<?php

if($lContinua) {
$report = new AlunosRelatoriosModel();


$data = DataDB($_GET['data']);
$fases = $_GET['fases'];

$report->Campos = " amf_id, amf_matid ";
$report->CondicaoExtra = 'AND amf_resultado = "AGENDADO" and  amf_concluido IS NULL and amf_dataprocesso = "'.$data.'" and amf_servitenid in ('.$fases.') ';
$report->OrderBy = " usu_nomecompleto ";

$dados  = $report->Gerar();

?>

<section class="content asec-box" id="ProcessosGerais">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-cogs"></i>
                Processar [ Aprovados | Reprovados | Faltosos ]</h3>

        </div>
        <div class="box-body" id="">
        	<b><span>Legenda</span> <br/></b>
        	<small class="text-red">
        		* AP = Aprovado <br/>
        		* RP = Reprovado </br>
        		* FT = Faltoso </br>
                </small>
        	<br/>
        	<form action="" id="AvancaProcesso" >
				<table class="table table-bordered table-striped table-hover" id="TbAvancaProcesso">
					<thead>
						<tr>
							<th>Aluno</th>
							<th>Matricula</th>
							<th>Processo</th>
							<th>Fase</th>
	                        <th width="40" title="">AP</th>
	                        <th width="40" title="">RP</th>
	                        <th width="40" title="">FT</th>
                                                </tr>
					</thead>
					<tbody>
						<?php if($dados->num_rows >= 1) { ?>
		                    <?php while($report = $dados->fetch_assoc() ) {?>
		                    <tr>
	                            <td><?php echo $report['usu_nomecompleto']?> <br/></td>
	                            <td><?php echo $report['tpmat_descricao']?> <br/></td>
	                            <td><?php echo $report['serv_descricao']?> <br/></td>
	                            <td><?php echo $report['serviten_descricao']?> <br/></td>
	                            
	                            <td onclick="MarcaChkAprov(<?php echo $report['usu_id']; ?>,'AP')" class="cursor"> <input type="checkbox" class="chkStatus AP ln-<?php echo $report['usu_id']; ?>" value="<?php echo $report['usu_id'].",".$report['amf_id'].",".$report['amf_matid']?>,1" name="aprovados[]" checked="checked" /> </td>
	                            <td onclick="MarcaChkAprov(<?php echo $report['usu_id']; ?>,'RP')" class="cursor"> <input type="checkbox" class="chkStatus RP ln-<?php echo $report['usu_id']; ?>" value="<?php echo $report['usu_id'].",".$report['amf_id'].",".$report['amf_matid']?>,2" name="aprovados[]" /> </td>
	                            <td onclick="MarcaChkAprov(<?php echo $report['usu_id']; ?>,'FT')" class="cursor"> <input type="checkbox" class="chkStatus FT ln-<?php echo $report['usu_id']; ?>" value="<?php echo $report['usu_id'].",".$report['amf_id'].",".$report['amf_matid']?>,0" name="aprovados[]" /> </td>
                                    </tr>
		                    <?php }//while ?>
		                <?php } else {//if ?>
		                    <tr>
		                        <td  colspan="5" style="font-size: 28px">
		                            <center>
		                            Sem resultados
		                            </center>
		                        </td>
		                    </tr>
		                <?php } ?>
					</tbody>
				</table>
				<script>
					function MarcaChkAprov(id,s) {
					    $('.AP.ln-'+id).prop('checked', false); 
					    $('.RP.ln-'+id).prop('checked', false); 
					    $('.FT.ln-'+id).prop('checked', false); 
					    $('.'+s+'.ln-'+id).prop('checked', true); 
					}; 


				</script>
				<hr/>
				<a href="javascript: history.back(-1)" class="btn btn-default"> Voltar </a>
				<?php if($dados->num_rows >= 1) { ?>
					<input type="submit" class="btn btn-primary pull-right" value="Continuar" />
				<?php }?>
			</form>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="Modal12">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmar Processo</h4>
      </div>
      
      <div class="modal-body">
        Selecione a data de aprovação para continuar.
        <br/><br/>
			<div class="row">
				<div class="col-xs-6">
					<input type="text" class="form-control datepicker" id="dataProc"  value="<?php echo $_GET['data'];?>" />
				</div>
			</div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="Continuar1()">Confirmar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$("#AvancaProcesso").on("submit",function(e){
	e.preventDefault();
	$("#Modal12").modal('show');
});

function Continuar1() { 
	Continuar2();
}

function Continuar2() {
	var datas = $("#AvancaProcesso").serializeArray();


	if($("#dataProc").val() == "") {
		alertify.error("Informe uma data!");
		return;
	}

	dataProc = $("#dataProc").val();
	datas.push({name:'data', value: dataProc });

	$.ajax({
		type : "post",
		dataType : "json",
		data : datas,
		url : "<?php echo $this->Link('alunos/ProcessaAprovados/')?>",
		success: function(e) {
			console.log(e);
			if(e.erro == "") {
				alertify.success("Processo concluído!");
				$("#Modal12").modal('hide');
				window.location = "<?php echo $this->Link('alunos/ProcessaAprovados/&ret=1')?>";
			} else {
				alertify.error(e.erro);
			}
		}
	})
}


</script>
<?php } else { //else $lContinua?>

<section class="content asec-box" id="ProcessosGerais">

	<?php echo $msg; ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-cogs"></i>
                Processa Aprovados</h3>

        </div>
        <div class="box-body row" id="">
        	<div class="col-xs-12">
				<h4>Seleciona a data e o processo para continuar.</h4>
			</div>
			

			<form id="formProc" method="get">
				<div class="col-sm-6 form-group">
					<label>Data</label>
					<input type="text" class="datepicker form-control" name="data" id="data"/>
				</div>

				<div class="col-sm-6 form-group">
					<label>Fase</label>
					<select name="fases[]" class="select2 form-control" multiple id="fases">
						<option value="7"> Exame Teórico </option>
						<option value="9"> Exame Prático B </option>
						<option value="11"> Exame Prático A </option>
						<option value="15"> Exame Prático C </option>
						<option value="17"> Exame Prático D </option>
						<option value="19"> Exame Prático E </option>
					</select>
				</div>

				<div class="col-sm-12 form-group">
					<a href="javascript: history.back(-1)" class="btn btn-default"> Voltar </a>
					<input type="submit" value="Continuar" class="btn btn-primary pull-right" />
				</div>
			</form>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->
<script>
	$("#formProc").on("submit", function(e) {
		e.preventDefault();

		var data = $("#data").val();
		var fases = $("#fases").val();

		if(data === null || data == "" || fases === null || fases == "") {
			alertify.error("Data e Fase são de preenchimento obrigatório!");
			return false;
		}

		window.location = "<?php echo $this->Link('alunos/ProcessaAprovados/')?>&data="+data+"&fases="+fases;
		
	})
</script>
<?php } //else if$lContinua?>
