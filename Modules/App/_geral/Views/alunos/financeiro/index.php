<div class="content hide " style="min-height: auto;">
    <div class="form">
        <div class="btn-atalhos">
            <a href="<?php echo Permalink("alunos", "") ?>" class="btn btn-primary"> <i class="fa fa-list-ul"></i> Alunos </a>
            <a href="<?php echo Permalink("alunos", "editar", "codigo=" . (int) $_GET['codigo']) ?>" class="btn btn-primary "> <i class="fa fa-user"></i> Ficha do Aluno </a>

            <div class="pull-right">
                <a href="<?php echo Permalink("alunos", "notapromissoria", "lote&aluno=" . (int) $_GET['codigo']) ?>" class="btn btn-warning "> <i class="glyphicon glyphicon-warning-sign"></i> Nota Promissória </a>
                <a href="<?php echo Permalink("processos", "incluir", "aluno=" . (int) $_GET['codigo']) ?>" class="btn btn-success "> <i class="fa fa-plus-circle"></i> Novo Processo </a>
            </div>
        </div>
    </div>	
</div>
<?php #if($nParcelasLancadas == 0 && $nMatriculas > 0 ) { ?>
<?php #if($nParcelasLancadas == 0 && $nMatriculas > 0 && false) { ?>
<section class="content asec-boxx" id="novo_finan" style="min-height: 100px;">
    <h3 class="page-title">
        Financeiro
    </h3>
    <br/>
    <?php if ($nMatriculas == 0) { ?>
        <div class="alert alert-info text-black xhide">
            O aluno selecionado não possui processos, <a class="text-black" href="<?php echo Permalink('processos', 'incluir', 'aluno=' . $_GET['codigo']) ?>">clique aqui</a> para iniciar.
        </div>
        
    <?php } else {//$nMatriculas == 0?>
        <div class="alert alert-warning text-black hide">
            O aluno não possui nenhuma parcela lançada até o momento. 
        </div>
        <div class="form">
            <label>Selecione o processo para lancar parcelas</label>
            <hr/>
            <select class='form-control' style='width: 300px; float: left; margin-right: 15px;' id="matricula">
                <option value="">  </option>
                <?php foreach ($MatriculasSemFinanceiro as $mat) { ?>
                    <option value="<?php echo $mat['amat_id']; ?>"> <?php echo $mat['serv_descricao']; ?> </option>
                <?php } ?>
            </select>
            <a href="javascript: Continuar();" class='btn btn-success'> Continuar </a>
            <a href="<?php echo Permalink('alunos', 'editar', 'codigo=' . $ID_ALUNO) ?>" class='btn btn-default'> Cancelar </a>
        </div>
    <?php } ?>
</section>
<?php #} //nParcelasLancadas ==0?>
<?php
//if($Parcelas) { //Se o aluno tiver parcelas lancadas
if (true) {
    ?>

    <div class="content asec-box" id="parcelas_aluno">
        <div class="row">
            <div class="form-group">
                <div class="col-sm-8 bottom10">
                    <h2>    
    <?php echo $Funcao->fRetCampo("sis_usuarios", "usu_nomecompleto", "usu_id = " . $ID_ALUNO); ?>
    <?php echo $Funcao->fRetCampo("sis_usuarios", "usu_observa", "usu_id = " . $ID_ALUNO); ?>
                    </h2>
                </div>

                <div class="col-sm-4 bottom10 text-blue blink">
                    <h2 class="text-right">    
    <?php echo $Funcao->fRetCampo("sis_usuarios", "usu_situacao", "usu_id = " . $ID_ALUNO); ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="form">
            <a href="<?php echo Permalink("alunos", "editar", "codigo=" . (int) $_GET['codigo']) ?>" class="btn btn-primary "> <i class="fa fa-user"></i> Ficha do Aluno </a>
            <a href="<?php echo Permalink("alunos", "ListaMatriculas", "aluno=" . (int) $_GET['codigo']) ?>" class="btn btn-info btnProcessos "> <i class="fa fa-cogs"></i> Processos </a>
            <a href="<?php echo Permalink("alunos", "notapromissoria", "lote&aluno=" . (int) $_GET['codigo']) ?>" class="btn btn-warning "> <i class="glyphicon glyphicon-warning-sign"></i> Nota Promissória </a>
            <a href="<?php echo Permalink("alunos", "notapromissoria", "lote&total&aluno=" . (int) $_GET['codigo']) ?>" class="btn btn-danger "> <i class="glyphicon glyphicon-warning-sign"></i> Nota Promissória Total</a> 
            <a href="<?php echo Permalink("alunos", "GeraReciboAvulso", "codigo=" . (int) $_GET['codigo']) ?>" class="btn btn-default "> <i class="fa fa-money"></i> Recibo Avulso </a>
            <span class="pull-right">
                <a href="<?php echo Permalink("AlunosFinanceiro", "LancaValores", "codigo=" . (int) $_GET['codigo']) ?>&act=debito" class="btn btn-outline-primary"> <i class="fa fa-money"></i> Lançar Parcela </a>
                <a href="<?php echo Permalink("AlunosFinanceiro", "LancaValores", "codigo=" . (int) $_GET['codigo']) ?>&act=credito" class="btn btn-success"> <i class="fa fa-gift"></i> Lança Crédito </a>
            </span>
        </div>
        <br/><br/>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_p" data-toggle="tab">Parcelas</a></li>
                <li class=""><a href="#tab_c" data-toggle="tab">Créditos</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_p">
    <?php require_once 'tab_parcelas.php'; ?>
                </div>
                <div class="tab-pane activex" id="tab_c">
    <?php require_once 'tab_creditos.php'; ?>
                </div>                
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $(".paga").css('background-color', "#fffad0;");
            $(".vencida").css('background-color', "#ffd0d0;");
            $(".cancelada").css('background-color',"#ffbbbb");
        })
    </script>
<?php }//Parcelas lancadas  ?>

<!-- Modal -->
<div class="modal fade" id="form_bxparcela" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Baixa da parcela</h4>
            </div>

            <div class="modal-body">
                <form id="baixa-parcela">
                    <input type="hidden" name="parc-id" value="" id="parc-id"/>
                    <input type="hidden" name="mat-id"  value="" id="mat-id"/>
                    <input type="hidden" name="alu-id"  value="<?php echo $ID_ALUNO?>" id="alu-id"/>

                    <?php if ($CreditosDisponivel > 0 ) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info">
                                    <input type="checkbox" name="chkUtilizaCreditos" value="1" id="chkUtilizaCreditos" class="form-check-input"> <label for="chkUtilizaCreditos">Utilizar créditos ?</label>
                                </div>
                            </div>                                                
                        </div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Data Baixa</label>
                            <input type="text" class="form-control datepicker" name="baixa-data" id="fase-data" value="<?php echo date('d/m/Y') ?>"/>
                        </div>                

                        <div class="col-md-4">
                            <label>Valor Baixa</label>
                            <input type="text" class="form-control" onkeyup="formataValorNovo(this)" name="baixa-valor" id="baixa-valor"/>
                        </div>

                        <div class="col-md-4">
                            <label>Tipo Documento</label>

                                <?php #var_dump($TpDocumentos)   ?>
                            <select name="tipo-documento" class="form-control" id='tipo-documento'>
                                    <option value="" selected="selected"></option>
                                <?php foreach ($TpDocumentos as $TipoDoc) { ?>
                                    <option value="<?php echo $TipoDoc['tpd_cd'] ?>"> <?php echo $TipoDoc['tpd_descricao'] ?> </option>
                                <?php } ?>
                                <script>
                                    $("#tipo-documento option[value='NOTP']").remove();
                                </script>
                            </select>
                        </div>
                    </div>

                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Observações</label> 
                            <input type="text" name="observacoes" id="" class="form-control" />

                        </div>
                    </div>
                </form>

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btnBxParc">Baixar Parcela</button>
            </div>
        </div>
    </div>
</div>

<script>

    var ValorOri, ValorFinal;
    var ValorDes = <?php echo $CreditosDisponivel; ?>;
    var TpDocsOri = $("#tipo-documento").html();
    var TpDocDesc = "<option value='CREDITOA'>Crédito</option>"
    //ValorDes = ValorDes.toFixed(2);

        $('#chkUtilizaCreditos').change(function() {
        if($(this).is(":checked")) {

            console.log("ORI = " + ValorOri + "\n Des = " + ValorDes);

            if(ValorDes >= ValorOri) {
                console.log("a");
                ValorDes = ValorOri
            }

            

            ValorFinal = ValorOri-ValorDes;
            ValorFinal = ValorFinal.toFixed(2);
            ValorDes = ValorDes.toFixed(2);

            $("#baixa-valor").val(ValorDes);
            $("#baixa-valor").attr("readonly","readonly");
            $("#baixa-valor").keyup();
            $("#tipo-documento").html(TpDocDesc);

            return;
        }
        $("#tipo-documento").html(TpDocsOri);
        $("#baixa-valor").val(ValorOri);
        $("#baixa-valor").removeAttr("readonly");
        $("#baixa-valor").keyup();
        
        
    });
        

    function Continuar() {
        ID_MATRICULA = $("#matricula").val();
        if (ID_MATRICULA == '') {
            alertify.error('Informe um processo para continuar');
            return false;
        }
        window.location = "<?php echo Permalink('AlunosFinanceiro', 'IniciaLancaParcelas', 'codigo=' . $ID_ALUNO) ?>&matricula=" + ID_MATRICULA;
    }

    function AbreParcela(id_parc, tp_docum, valor) {

        $("#baixa-parcela")[0].reset();
        $("#parc-id").val(id_parc);
        $("#baixa-valor").val(valor);
        ValorOri = valor;
        $("#baixa-valor").keyup();
        //$("#tipo-documento").val(tp_docum);
        $('#form_bxparcela').modal('show');
    }

    $(".btnBxParc").click(function () {

        tpDocum = $("#tipo-documento").val();
        if(tpDocum == '') {
            alertify.error("Informe um tipo de documento!");
            return;    
        }
        
        
        var dados = $("#baixa-parcela").serializeArray();
        $.ajax({
            type: 'post',
            //dataType : 'json',
            data: dados,
            url: "index.php?route=financeiro/BaixaParcela/&codigo=<?php echo $_GET['codigo'] ?>",
            success: function (e) {
                if (e == "") {
                    alertify.success("Parcela baixada com sucesso!")
                    $('#form_bxparcela').modal('hide');
                    setTimeout(function () {
                        location.reload();
                    }, 100);
                } else {
                    alertify.error(e)
                }

            }
        })
    })
// $('#btn_abrebxparcela').click(function(){
//   $('#form_bxparcela').modal('show');
// })
</script>