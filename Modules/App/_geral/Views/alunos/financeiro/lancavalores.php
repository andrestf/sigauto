<?php if($_GET['act'] == 'debito') {
    $Titulo = "Débito/Parcela";
} else {
    $Titulo = "Crédito";
}
?>
<section id="lancamentos">
    <form id="frmLancaValores">
        <div class="content asec-box" id="ficha_novo_aluno">
            <h3 class="page-title">
                Lançamento <?php echo $Titulo; ?>
            </h3>
            <br/>
            <div class="form">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Valor</label>
                        <input type="text" class="form-control" name="valorec" id="valorec" value="" onkeyup="formataValor(this)"/>
                    </div>

                    <div class="col-sm-4">
                        <label>Vencimento</label>
                        <input type="text" class="form-control datepicker" id="vencimento" name="vencimento" value="<?php echo date('d/m/Y') ?>"/>
                    </div>

                    <div class="col-sm-4">
                        <label>Tipo de Documento</label>
                        <select class="form-control" name="tpdoc" id="tpdoc">
                            <?php if(count($TpDocumento) > 1) {?>
                            <option value=""></option>
                            <?php } ?>
                            <?php foreach ($TpDocumento as $Documento) { ?>
                                <option value="<?php echo $Documento['tpd_cd'] ?>"> <?php echo $Documento['tpd_descricao'] ?> </option>
                            <?php } ?>
                        </select>
                    </div>

                </div>

                <br/>
                <div class="row">
                    <div class="col-sm-8">
                        <label>Descrição</label>
                        <input type="text" class="form-control" name="descricao" id="descricao" value="" />
                    </div>
                    <div class="col-sm-4">
                    <?php if($_GET['act'] == 'debito') {?>
                        
                        <label>Dividir em</label>
                        <select id="nParcelas" name="nParcelas" class="form-control">
                            <option value="1">01 Parcela </option>
                            <option value="2">02 Parcelas </option>
                            <option value="3">03 Parcelas</option>
                            <option value="4">04 Parcelas</option>
                            <option value="5">05 Parcelas</option>
                            <option value="6">06 Parcelas</option>
                            <option value="7">07 Parcelas</option>
                            <option value="8">08 Parcelas</option>
                            <option value="9">09 Parcelas</option>
                            <option value="10">10 Parcelas</option>
                            <option value="11">11 Parcelas</option>
                            <option value="12">12 Parcelas</option>
                        </select>
                    <?php } else { ?>
                        <input type="hidden" class="form-control" value="1" name="nParcelas" id="nParcelas" />
                    <?php } ?>
                    </div>                    
                 </div>                

                <br/>
                <div class="row">
                    <div class="col-sm-4" id="">
                        <label>Tipo</label>
                        <?php if($_GET['act'] == 'debito') {
                            $Tempcc = "Débito/Parcela";
                        } else {
                            $Tempcc = "Crédito";
                        }
                        ?>
                        <input type="text" class="form-control" value="<?php echo $Tempcc?>" name="tplanca" id="tplanca" value="" readonly/>
                        
                    </div>
                    <div class="col-sm-4" id="colDocumento">
                        <label>Documento</label>
                        <input type="text" class="form-control" name="documento" id="documento" maxlength="25" />
                    </div>
                    <div class="col-sm-4" id="colPredata">
                        <label>Pré Data</label>
                        <input type="text" class="form-control datepicker" name="predata" id="predata" maxlength="8" />
                    </div>
                    

                </div>
                <br/>

                <div class="row">
                    <div class="col-sm-12" style="direction: rtl">
                        <span class='btn btn-success pull-rightx' onClick="SalvarParcelas();">Lançar</span>&nbsp;
                    </div>
                </div>


            </div>
        </div>

    </form>
</section>
<script>
        $("#colDocumento").hide();
        $("#colPredata").hide();

    $(function () {
        $("#valorec").keyup();
        $("#valorec").focus();
    })


    $("#tpdoc").change(function () {
        $("#colDocumento").hide();
        $("#colPredata").hide();

        tpdoc = $("#tpdoc").val();
        if (
                tpdoc == 'CHEQ' ||
                tpdoc == 'CHPR' ||
                tpdoc == 'NOTP') {

                $("#colPredata").show(); 
                //if(tpdoc == "CHPR") {
                    $("#colDocumento").show();
                //}
        } else {
            
        }
    });


    

    function SalvarParcelas() {


            descricao = $("#descricao").val();
            valor = $("#valorec").val();
            vencimento = $("#vencimento").val();
            tpdoc = $("#tpdoc option:selected").val();


            if (valor <= 0 || valor == "00,00" || valor == "") {
                erro = true;
                alertify.error("Valor informado não é válido!");
                return false;
            }

            if (tpdoc == '') {
                erro = true;
                alertify.error("Informe o tipo de documento");
                return false;
            }


            if (vencimento == '') {
                erro = true;
                alertify.error("Informe a data de vencimento");
                return false;
            }
                        if (descricao == '') {
                erro = true;
                alertify.error("Informe uma descrição!");
                return false;
            }

            valor = valor.replace(".", "").replace(",", ".");
            valor = parseFloat(valor);

            


        alertify.confirm('Atenção', 'Confirmar lançamento?',
                function () {
                    confirmaLancamento();
                },
                function () {

                })





    }

    function confirmaLancamento() {
        $("#lancamentos").fadeOut();
        datas = $("#frmLancaValores").serializeArray();
        $.ajax({
            type: 'post',
            data: datas,
            url: "/index.php?route=AlunosFinanceiro/SalvaParcelas/&codigo=<?php echo $_GET['codigo'] ?>&parcelas=0&act=<?php echo $_GET['act'];?>",
            success: function (e) {
                if (e == '') {
                    alertify.success("Lancamento realizado!");
                    setTimeout(function () {
                        window.location = 'index.php?route=AlunosFinanceiro/index/&codigo=<?php echo $_GET["codigo"] ?>';
                    }, 150)
                } else {
                    alertify.error(e);
                    $("#lancamentos").fadeIn();
                }
            },
            error: function() {
                alertify.error("Houve um erro no servidor, por favor, tente em alguns instantes");
                $("#lancamentos").fadeIn();
            }
        })
    }


</script>