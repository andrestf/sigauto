    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title text-blue">
                <center> Parcelas </center>
            </h3>
        </div>
    </div>
    <br/>
    <div class="form" style="font-size: 18px;">
        <span class="text-blue">
            Total Pago: R$ <span class="totPago"></span>
        </span>
        
        <span class="text-red pull-right">
            Total Devido: R$ <span class="totDevido"></span>
        </span>
    </div>
    
    <br/><br/>

    <div class="form" style="font-size: 18px;">
        <a href="javascript: void(0)" onclick="IniciaCancLote()" class="btn btn-default btnIniciaCancLote" title="Cancelamento em Lote"><i class="fa fa-trash"></i></a>

        <a href="javascript: void(0)" onclick="CancLote()" class="btn btn-default btnCancLote" title="Cancelar Parcelas"><i class="fa fa-check"></i></a>

        <a href="javascript: void(0)" onclick="CancelaCancLote()" class="btn btn-default btnCancelaCancLote" title=" Desmarcar Cancelamento em Lote"><i class="fa fa-history"></i></a>
		
		<span class="pull-right" style="font-size: 12px;">
        	<input type="checkbox" name="cancelado" id="chkMostraCancelado" class="x-control" /> Mostra Cancelado
        </span>
    </div>

    <br/><br/>
	<div class="form">
            <?php if(!$Parcelas) {
                echo '<div class="text-danger"> Aluno não possui parcelas lançadas! </div>';
            } else {?>
		<table class="table table-condensed table-hover table-striped table-grid">
			<thead>
				<tr>
					<th widht="25">#</th>
					<th>Tipo</th>
					<th>Descrição</th>
					<th>Vencimento</th>
					<th>Valor</th>
					<th>Valor Pago</th>
					<th>Dt. Paga</th>
                    <th width="40">Baixado</th>
					<th width="50">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php 
                                
                                $totValorPago = 0;
                                $totValorDevido = 0;
                                
                                foreach ($Parcelas as $parcela) {
                                	$styleCancelado = '';
                                    
                                        if($parcela['parcela']['finmov_databaixa'] == '') {
                                            $classPaga = ''; 

                                            if($parcela['parcela']['can_data'] == '')
                                                $totValorDevido = $totValorDevido+$parcela['parcela']['finmov_valor'];
                                        } else {
                                            $classPaga = ' paga ';
                                            $totValorPago = $totValorPago + $parcela['parcela']['finmov_valorbaixa'];
                                        }
                                        $classCancelado = "";
                                        if($parcela['parcela']['can_data'] != '') {
                                            #$styleCancelado = 'background: #ffd0d0 !important';
                                            $classCancelado = " cancelada ";
                                        }
                                        
					#($parcela['parcela']['finmov_databaixa'] == '')       ? $classPaga = '';  : $classPaga = ' paga ';
					if ($parcela['parcela']['finmov_dtvenc'] < date('Y-m-d') && $parcela['parcela']['finmov_databaixa'] == '') {
					 	$classVenc = 'vencida';
					} else {
						$classVenc = ' ';
					}
				 ?>
					<tr class=' <?php echo $classPaga . $classVenc . $classCancelado ?> ' id="parc-<?php echo $parcela['parcela']['finmov_id']; ?>" style="<?php echo $styleCancelado ?>">
						<td><?php echo $parcela['parcela']['finmov_id']; ?></td>
						<td><?php echo $parcela['parcela']['finmov_tipo']; ?></td>
						<td><?php echo $parcela['parcela']['finmov_descricao']; ?></td>
						<td><?php echo DataBR($parcela['parcela']['finmov_dtvenc']); ?></td>
						<td>R$ <?php echo number_format($parcela['parcela']['finmov_valor'],"2",",","."); ?></td>
						<td><?php echo ($parcela['parcela']['finmov_valorbaixa'] == '') ? "" : 'R$ '.number_format($parcela['parcela']['finmov_valorbaixa'],"2",",","."); ?></td>
						<td><?php echo DataBR($parcela['parcela']['finmov_databaixa']); ?></td>
						<td>
							<?php if($parcela['parcela']['finmov_databaixa'] != '') {?>
								<?php echo $Funcao->fRetCampo("sis_usuarios","usu_apelido","usu_id = ".$parcela['parcela']['finmov_usubaixa']); ?>
							<?php } ?>
						</td>



						<td class="no-print">

							

							<?php if(isset($parcela['log'])) { ?>
                                <span class="pull-right">
                                    &nbsp;

                                    <?php 
                                    $log = "";
                                    foreach ($parcela['log'] as $key => $value) {
                                        $log .= DataBRHora($value['finlog_caddata']) . " => " .$value['finlog_alteracao'] . "<br/>";
                                    } ?>

                                    <div id="modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog">  
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                	<div style="">
                                                    <?php echo $log;?>
                                                	</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <i class="fa fa-bars" title="Histórico do Movimento" data-target="#modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" data-toggle="modal"></i>
                                </span>
                            <?php } ?>


							<?php if($parcela['parcela']['can_data'] == '' && $parcela['parcela']['finmov_deonde'] != 'CREDITO') { ?>
								<?php if($parcela['parcela']['finmov_databaixa'] == '') {?>
									<input type="checkbox" value="<?php echo $parcela['parcela']['finmov_id']; ?>" name="cancLote[]" class="chkCancLote" > &nbsp;
									<i class='fa fa-download cursor' title="Baixar Parcela" id="btn_abrebxparcela" onclick="AbreParcela(<?php echo $parcela['parcela']['finmov_id']; ?>,'<?php echo $parcela['parcela']['finmov_tpdocum']; ?>','<?php echo $parcela['parcela']['finmov_valor']; ?>')" data-toggle="tooltip"></i> &nbsp;
									<i class="fa fa-bars hide " title="Histórico do Movimento" data-target="#modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" data-toggle="modal"></i>
									<i class="fa fa-edit" title="Editar Movimento" onClick="EditarParcela(<?php echo $parcela['parcela']['finmov_id']; ?>)"></i>
								<?php } else { ?>
								<i class='fa fa-undo cursor hide' title="Estornar Parcela" data-toggle="tooltip"></i> &nbsp;
	                            <a href="<?php echo Permalink('alunos','GeraRecibo','movimento='.$parcela['parcela']["finmov_id"].'&codigo='.$ID_ALUNO)?>">
								 <i class='fa fa-print cursor' title="Imprimir Comprovante" data-toggle="tooltip"></i>
	                            </a>
	                            <i class="fa fa-bars hide" title="Histórico do Movimento" data-target="#modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" data-toggle="modal"></i>
								<?php } ?>
							<?php } ?>
						</td>
					</tr>
				<?php }?>
                                        
                        <script>
                            $(".totPago").html("<?php echo number_format($totValorPago,"2",",",".")?>");
                            $(".totDevido").html("<?php echo number_format($totValorDevido,"2",",",".")?>");
                        </script>        
			</tbody>
		</table>
		<div class="row col-md-12">
			<hr />
			
		</div>
        <?php } //if Parcelas?>



	</div>

<div class="modal fade" id="mdEdtParc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="">Editar parcela</h4>
      </div>

      <div class="modal-body">
          <form id="edtparc_frm" method="">
            <div id="edtparc_dados">
            </div>
          </form>
          
      </div>
        
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar Edição</button>
        <button type="button" class="btn btn-primary" onclick='confirmaEditParcela();' >Salvar Edição </button>
      </div>
    </div>
  </div>
</div>

<style>
	.vencida {
		background: #ffd0d0 !important;
	}
	.cancelada {
		background: #ffb900 !important;
	}
</style>

<script>
	var tmp_IDAtualParc;
    function EditarParcela(id_parc) {

        tmp_IDAtualParc = id_parc;


        $.ajax({
            url : "<?php echo permalink('Financeiro/CarregaDadosParcela/')?>&from=tabAlunos&n="+id_parc,
            success:  function(res) {
                $("#edtparc_dados").html(res);
                $("#mdEdtParc").modal('show');
                $(".vlr").keyup();
                $(".hasDatepicker").datepicker();
            },
            error: function() {
                tmp_IDAtualParc = "0";
                return false;
            }
        })

    }

    function confirmaEditParcela() {
        alertify.confirm("Atenção","Confirmar edição de parcela?",
            function() {
                EditParcela(tmp_IDAtualParc);
            },
            function() {}
        );
    }

    function EditParcela(tmp_IDAtualParc) {
        var datas = "";
        datas = $("#edtparc_frm").serializeArray();
        $.ajax({
            dataType: 'json',
            type : 'post',
            data : datas,
            url : "<?php echo permalink('Financeiro/EditarParcela/')?>&from=tabAlunos&n="+tmp_IDAtualParc,
                    success: function(res) {
                        tmp_IDAtualParc = "0";
                        if(typeof res.erro !== "undefined") {
                            alertify.error(res.mensagem);
                            return;
                        }
                        else {
                            alertify.success(res.mensagem);
                            location.reload();
                        }
                    },
                    error: function(e,x,s) {
                        tmp_IDAtualParc = "0";
                        alertify.error("ERRO DE SERVIDOR! <br/> Falhar na edição de parcela! <br/>" + s)
                    }
        });
    }    

	$(".cancelada").hide();
	$("#chkMostraCancelado").change(function() {
		if(this.checked) {
			$(".cancelada").show();
		} else {
			$(".cancelada").hide();
		}
	})

	$(".chkCancLote").hide();
		$(".btnIniciaCancLote").show();
		$(".btnCancLote").hide();
		$(".btnCancelaCancLote").hide();
	var bIniciaCancLote = false;
	
	function IniciaCancLote() {

		$(".btnIniciaCancLote").hide();
		$(".btnCancLote").show();
		$(".btnCancelaCancLote").show();


		if(bIniciaCancLote == false) {
			$(".chkCancLote").show();
			bIniciaCancLote = true;	
		} else {
			CancelaCancLote();
		}
				
	}

	function CancelaCancLote() {
		$('.chkCancLote').prop('checked', false);
		$(".chkCancLote").hide();
		bIniciaCancLote = false;		

		$(".btnIniciaCancLote").show();
		$(".btnCancLote").hide();
		$(".btnCancelaCancLote").hide();
	}

	function CancLote() {
		ids = "";
		$( $(".chkCancLote") ).each(function( index ) {
			if( $(this).prop("checked") == 1 ) {
				val = $(this).val();
				ids = ids + val + ",";
			}
		});

		if(ids == "") {
			alertify.error("Selecione ao menos uma parcela");
			return false;
		}

		alertify.confirm("Atenção","Confirmar Cancelamento de Parcelas",
			function(){
				///////////////////////////////////////////////////////////////
				$.ajax({
					type: 'post',
					data : {n : ids},
					url: "/index.php?route=Financeiro/CancelarParcelalote/",
					success: function(res) {
						if(res == 1) {
							alertify.success("Cancelamento Realizado");
						}

						if(res == 0) {
							alertify.error("Falha ao processar!");
						}

						setTimeout(function(){ location.reload(); },300)

						return;
					}
				})
				///////////////////////////////////////////////////////////////
			},
			function(){})




	}


</script>