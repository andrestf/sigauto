<section id="lancamentos">

    <form id="frmLancaParcelas">
        <div class="content asec-box" id="ficha_novo_aluno">
            <h3 class="page-title">
                Lançamento
            </h3>
            <br/>

            <div class="form">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Valor Original</label>
                        <input type="text" class="form-control" name="vlrorig" id="vlrorig" value="<?php echo $Matricula['amat_vlr'] ?>" onkeyup="formataValor(this)" readonly/>
                    </div>

                    <div class="col-sm-4">
                        <label>Data</label>
                        <input type="text" class="form-control" value="<?php echo date('d/m/Y') ?>"readonly/>
                    </div>

                    <div class="col-sm-4">
                        <label>Desconto</label>
                        <input type="text" class="form-control" name="desconto" id="desconto" value="00,00" onkeyup="formataValor(this)" />
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="col-sm-4 hide">
                        <label>Valor a Receber</label>
                        <input type="text" class="form-control" name="valorrec" id="valorec" value="<?php echo $Matricula['amat_vlr'] ?>" onkeyup="formataValor(this)"/>
                    </div>
                    <div class="col-sm-4">
                        <label>Dia do Vencimento</label>
                        <select class="form-control" name="diavenc" id="diavenc">
                            <?php for ($i = 1; $i <= 31; $i++) { ?>
                                <option value="<?php echo $i ?>" <?php echo (date('d') == $i ) ? "selected" : ""; ?> > <?php echo $i ?> </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label>Tipo de Documento</label>
                        <select class="form-control" name="tpdoc" id="tpdoc">
                            <option value=""></option>
                            <?php foreach ($TpDocumento as $Documento) { ?>
                                <option value="<?php echo $Documento['tpd_cd'] ?>"> <?php echo $Documento['tpd_descricao'] ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label>Forma de Pagamento</label>
                        <select class="form-control formapaga" name="formapaga" id="formapaga" required>
                            <option data-parcelas="0" value="0"></option>
                            <?php foreach ($FormaPagamentos as $Forma) { ?>
                                <option value="<?php echo $Forma['fpg_id'] ?>" data-parcelas="<?php echo $Forma['fpg_qtdparc'] ?>" data-entrada="<?php echo $Forma['fpg_entrada'] ?>" data-jur="<?php echo $Forma['fpg_juros'] ?>"> <?php echo $Forma['fpg_descricao'] ?> </option>
                            <?php } ?>
                        </select>
                    </div>

                </div>

                <br/>
                <div class="row">

                    <div class="col-sm-4 hide" id="colDocumento">
                        <label>Documento</label>
                        <input type="text" class="form-control" name="documento" id="documento" maxlength="25" />
                    </div>
                    <div class="col-sm-4" id="colPredata">
                        <label>Pré Data</label>
                        <input type="text" class="form-control datepicker" name="predata" id="predata" maxlength="8" />
                    </div>
                </div>
                <br/>

                <div class="row" id="rowParcelas">
                    <div class="col-xs-12">
                        <table class="table table-bordered" id="sort">
                            <thead>
                                <tr>
                                    <th width="40">Nº</th>
                                    <th width="150">Descrição</th>
                                    <th>Tp Doc.</th>
                                    <th width="150">Vencimento.</th>
                                    <th width="150">Valor</th>
                                    <th width="20"></th>
                                </tr>
                            </thead>
                            <tbody id="tableParcelasAd"></tbody>
                            <tbody id="tableParcelas"></tbody>
                            <tfoot>
                                <tr style='text-align: center;'>
                                    <td colspan='6'>Valor Total: R$ <span class='valorFinal'></span></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <br/>

                <?php //if ($VlrCreditoDisponivel > 0) {
                if (false) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-info">
                                <div class="text-danger"> Créditos disponíveis</div>
                                O aluno possui crédito no valor de R$ <span class="saldoCredito"><?php echo number_format($VlrCreditoDisponivel, 2, ",", "."); ?></span>
                                <br/>
                                <br/>
                                <a href="javascript:;" onclick="UtilizaCreditos()" class="btn btn-warning btn-sm alert-link">Utilizar Créditos</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>                

                <div class="row">
                    <div class="col-sm-12" style="direction: rtl">
                        <span class='btn btn-success pull-rightx' onClick="SalvarParcelas();">Lançar</span>&nbsp;
                        <span class='btn btn-warning pull-rightx' onclick="LancaParcelaAdicional()">Parcela +</span>

                    </div>
                </div>


            </div>
        </div>

    </form>
</section>


<?php if ($VlrCreditoDisponivel > 0) { ?>
    <div class="modal" id="modalLancaCredito" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">

            <div class="modal-content">
                <div class="modal-header">Lançamento de Crédito</div>
                <div class="modal-body">
                    <label>Informe o valor do crédito a ser lançado</label>
                    <input type="text" id="vlrCredito" name="vlrCredito" onkeyup="formataValor(this)" class="form-control" />
                </div>
                <div class="modal-footer">
                    <button class="pull-left btn btn-info btn-flat" data-dismiss="modal">Cancelar</button>
                    <button class="pull-right btn btn-success btn-flat" onclick="ConfirmaLancaCredito();" >Utilizar</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    $(function () {
        $("#valorec").keyup();
        $("#vlrorig").keyup();

        $("#colDocumento").hide();
        $("#colPredata").hide();

        $("#sort tbody tr").sortable();
        $("#sort tbody tr").disableSelection();

    })

    $("#valorec").focusout(function () {

        MontaParcelas();
    })

    var i, nParcelas = 0, nEntrada = 0;
    var jur;
    var valorParcelas, valorTotal, valorJur, valor, ValorOrig;
    var tpdoc, documento, diaVenc, ano;
    $("#formapaga").change(function () {

        if (jur > 0) {
            //alertify.alert('Atenção','Formar de pagamento contem juros de ' + jur + "%")
        }
        
        
        
        MontaParcelas()

    });

    $("#diavenc").change(function () {
        MontaParcelas()

    });

    $("#tpdoc").change(function () {
        $("#colDocumento").hide();
        $("#colPredata").hide();

        tpdoc = $("#tpdoc").val();
        if (
                tpdoc == 'CHEQ' ||
                tpdoc == 'DINH' ||
                tpdoc == 'CDEB') {

            $("#formapaga").val('1');
            $("#formapaga").attr('disabled', 'disabled');
            MontaParcelas();
        } else {
            $("#formapaga").removeAttr('disabled');
            $("#formapaga").val('0');
        }
        MontaParcelas()
    });


    function MontaParcelas() {
        VlrCreditoDisponivel = "<?php echo number_format($VlrCreditoDisponivel, 2); ?>";
        nParcelas = $("#formapaga option:selected").attr('data-parcelas');
        
        jur = $("#formapaga option:selected").attr('data-jur');

        $("#tableParcelas").html('');
        $("#tableParcelasAd").html('');
        //nParcelas = $("#formapaga option:selected").attr('data-parcelas');
        element = "";
        valor = parseFloat($("#valorec").val().replace('.', '').replace(',', '.'));
        valorOrig = valor;
        valorJur = ((jur / 100) * valor) + valor;

        //MEnos desconto
        valorJur = valorJur - $("#desconto").val().replace(".", "").replace(",", ".");


        valorParcelas = (valorJur / nParcelas).toFixed(2);
        valorTotal = (valorParcelas * nParcelas).toFixed(2);
        diaVenc = $("#diavenc").val();
        entrada = $("#formapaga option:selected").attr('data-entrada');
        ano = moment().format('YYYY');


        anoVenc = parseInt(ano);
        if (entrada == "0") {
            //alert('com entrda')
            mesVenc = moment().format('M')
            //passando valor 1 lancamento de parcelas (onde 1 mostra que tem 1 entrada)
            nEntrada = 1

        } else {
            //alert('nao tem')
            //passando valor 0 lancamento de parcelas (onde 0 mostra que nao teve)
            nEntrada = 0
            
            mes = moment().format('M');
            d = moment().format('YYYY-M-D')
            //alert(entrada)
            if (mes == 12) {
                anoVenc = anoVenc + 1;
            }
            mesVenc = moment().add(entrada, 'month').format('M') - 1;

        }



        for (i = 1; i <= nParcelas; i++) {

            if (entrada > 0 || i > 1)
                mesVenc = parseInt(mesVenc) + 1;

            if (mesVenc > 12) {
                mesVenc = 1
                anoVenc = anoVenc + 1;
            }

            if (mesVenc < 10)
                mesVenc = "0" + mesVenc

            diaVenc = parseInt(diaVenc);
            if (diaVenc < 10)
                diaVenc = "0" + diaVenc
            iLinha = i;
            if (iLinha < 10) {
                iLinha = "0" + iLinha;
            }
            element = element + "<tr id='Parc_" + i + "' class='trParcelas ParcelasOrigi'>";
            element = element + "<td class='ParcN' >" + iLinha + "</td>";
            element = element + "<td class='ParcDescri' ><input type='text' class='ParcDescriTxt' value='Parc.: " + iLinha + "/" + nParcelas + "' class='form-control' style='border: 0px;  box-shadow: none;' /></td>";
            element = element + "<td class='ParcTpDoc'><select class='form-control tpdocs' style='border: none;'></select></td>";
            element = element + "<td class='ParcVenc'><input type='text' class='form-control datepicker cursor' title='Clique para alterar o vencimento' data-toggle='tooltip' value='" + diaVenc + "/" + mesVenc + "/" + anoVenc + "' style='border: 0; box-shadow: none;' ></td>";
            element = element + "<td class='ParcValor'> R$ <input type='text' id='txtParcVlr_" + i + "'  onkeyup='formataValorNovo(this); CalculaValor();' onfocusout='RecalculaParcelas(this)' class='cursor valorparcelas txtParcVlr' value='" + valorParcelas + "' style='border: 0; box-shadow: none; width: 100px;'/></td>"
            element = element + "<td></td>";
            //element = element + "<td></td>";
            element = element + "</tr>";




        }
        element = element + ""
        $("#tableParcelas").html(element);
        DatePicker('.datepicker');
        $(".tpdocs").html($("#tpdoc").html());
        $(".tpdocs").val($("#tpdoc").val());
        $('.valorparcelas').keyup();
        AcertaValor();
        ContaParcelas();

    }

<?php if ($VlrCreditoDisponivel > 0) { ?>
        var VlrCreditoOri = "<?php echo $VlrCreditoDisponivel; ?>";
            VlrCreditoOri = parseFloat(VlrCreditoOri).toFixed(2);

        var VlrCreditoDisponivel = "<?php echo number_format($VlrCreditoDisponivel, 2); ?>";
        var VlrCreditoDisponivelTemp;


        function UtilizaCreditos() {
            CreditosUtilizados = SomaDescontoDeCreditos();
            
            

            //VlrCreditoDisponivel = VlrCreditoDisponivel.toFixed(2);
            VlrCreditoDisponivelTemp = VlrCreditoOri - parseFloat(CreditosUtilizados);

           
            //alert("Utilizado=> " + xxx + "\nDisponivel => " + VlrCreditoDisponivelTemp);
            if(VlrCreditoDisponivelTemp <= 0 ) {
                alertify.error("Limite total de Desconto ja utilizado!");
                return false;
            }
            vlrTemp2 = VlrCreditoDisponivelTemp.toFixed(2);
            VlrCreditoDisponivelTemp = vlrTemp2;
            $("#modalLancaCredito").modal('show');
            $("#vlrCredito").val(vlrTemp2).keyup();
            setTimeout(function () {
                $("#vlrCredito").focus();
            }, 500);

        }

        function ConfirmaLancaCredito() {

            $("#modalLancaCredito").modal('hide');
            vlrCredito = $("#vlrCredito").val();
            vlrCredito = vlrCredito.replace(".", "");
            vlrCredito = vlrCredito.replace(",", ".");
            //console.log(vlrCredito);
            


            CreditosUtilizados   = parseFloat(SomaDescontoDeCreditos());
            //console.log("Já Utilizado => " + CreditosUtilizados);
            //console.log("Original  => " + VlrCreditoOri);
            
            VlrCreditoDisponivel = VlrCreditoOri - CreditosUtilizados;
            //VlrCreditoDisponivel = VlrCreditoDisponivel.toFixed(2);


            //Valor a ser utilizado
            //console.log("A Utilizar => " + vlrCredito + " disponível => " + VlrCreditoDisponivel);

            //console.log("Resultado => " + vlrCredito +" > "+ VlrCreditoDisponivel);
            if(vlrCredito > VlrCreditoDisponivel ){
                alertify.error("Valor do desconto maior que o valor em créditos disponível!");
                return false;
            }            
            //LancaParcelaAdicional(vlrCredito *-1 );
            valor = vlrCredito.replace(".", ",");

            var i = 0;
            element = "";
            element = element + "<tr id='Parc_" + i + "' class='trParcelas ParcelasAdic TrAddC ' valing='middle'>";
            element = element + "<td class='ParcN' >" + i + "</td>";
            element = element + "<td class='ParcDescri' ><input type='text' readonly='readonly' class='ParcDescriTxt' value='Desconto Crédito' class='form-control' style='border: 0px;  box-shadow: none;' /></td>";
            element = element + "<td class='ParcTpDoc'><select class='form-control' readonly='readonly' style='border: none;'><option value='DESC'>Dinheiro</option></select></td>";
            element = element + "<td class='ParcVenc'><input type='text' class='form-control  cursor' readonly='readonly' title='Clique para alterar o vencimento' data-toggle='tooltip' value='<?php echo date('d/m/Y') ?>' style='border: 0; box-shadow: none;' ></td>";
            element = element + "<td class='ParcValor'> R$ <input type='text' readonly='readonly' value='-" + valor + "' onkeyup='CalculaValor();' class='cursor valorparcelasx valorparcelasc' value='0' style='border: 0; box-shadow: none; width: 100px;'/></td>"
            element = element + "<td><i class='fa fa-remove cursor' onclick='removerParcela(this)'></i></td>";
            element = element + "</tr>";
            
            
            //VlrCreditoDisponivel = VlrCreditoDisponivel - xxx;
            /* */

                    /**/
            $("#tableParcelasAd").prepend(element);
            $(".valorparcelas").keyup();

            CreditosUtilizados   = SomaDescontoDeCreditos();
            VlrCreditoDisponivel = VlrCreditoOri - CreditosUtilizados;
            $(".saldoCredito").html(VlrCreditoDisponivel.toFixed(2));

            ContaParcelas();

            e = $(".ParcelasOrigi").length;
            
            //se existir parcelas lancadas
            if(e > 0) {
                id =  $(".ParcelasOrigi").first().attr("id");
                element = $("#"+id);
                RecalculaParcelas(element);    
            }
            

        }
<?php } ?>

    function RecalculaParcelas(e) {
        /*
        //Creditos dos Aluno
        ValorCredito = SomaDescontoDeCreditos(); 
        VlrCreditoDisponivel = VlrCreditoOri - ValorCredito;
        if(ValorCredito > VlrCreditoDisponivel) {
            alertify.error("OPS, valor do credito maior que disponivel");
            return false;
        }
        if(ValorCredito > 0) {
            ValorCredito = ValorCredito.parseFloat();    
        } else {
            ValorCredito = 0.00 ;
        }
        
            //ValorCredito = ValorCredito.replace(".", "");
            //ValorCredito = ValorCredito.replace(",", ".");
            //ValorCredito = parseFloat(ValorCredito);
            //ValorCredito = ValorCredito.toFixed(2);

        //Desconto aplicado pelo usuário
        ValorDesconto = $("#desconto").val();
            ValorDesconto = ValorDesconto.replace(".", "");
            ValorDesconto = ValorDesconto.replace(",", ".");        
            ValorDesconto = parseFloat(ValorDesconto);
            ValorDesconto = ValorDesconto.toFixed(2);

        //Valor total do Lancamento
        ValorMatricula = <?php echo $Matricula['amat_vlr'] ?>;
            
            ValorMatricula = ValorMatricula.toFixed(2);

        //Valor do Processo menos descontos e creditos
        SubValorMatricula = ValorMatricula - ValorDesconto - ValorCredito;
        
        console.log(SubValorMatricula.toFixed(2));

        //Numero de Parcelas
        NumParcelas = 0;
        $(".valorparcelas").each(function(){
            NumParcelas = NumParcelas + 1;
        });

        if(e == '') {
            id =  $(".ParcelasOrigi").first().attr("id");
            element = $("#"+id);            
        } else {
            element = e;
        }

        //Indice para a proxima parcela
        ParcelaAtual      = $(element).attr("id");
        ParcelaAtual      = ParcelaAtual.replace("txtParcVlr_", "");
        PrxParcela        = NumParcelas - ParcelaAtual;
        PrxParcela = parseInt(PrxParcela);
        ValorParcelaAtual = $(element).val();
            ValorParcelaAtual = ValorParcelaAtual.replace(".", "");
            ValorParcelaAtual = ValorParcelaAtual.replace(",", ".");

        //Somando parcelas a partir da atual
        ValorParcelasLancadas = 0;
        for (i = 0; i < PrxParcela + 1; i++) {
                SomaValorPrxParcelas = $("#txtParcVlr_"+i).val();
                console.log(SomaValorPrxParcelas);
                SomaValorPrxParcelas = SomaValorPrxParcelas.replace(".","");
                SomaValorPrxParcelas = SomaValorPrxParcelas.replace(",",".");
                SomaValorPrxParcelas = SomaValorPrxParcelas = parseFloat(SomaValorPrxParcelas);            
            if(i == ParcelaAtual) {

            } else {
                ValorParcelasLancadas = ValorParcelasLancadas+SomaValorPrxParcelas;
            }

        }        


        



        ValorCadaParcela = SubValorMatricula/NumParcelas;
        console.log(ValorCadaParcela);

        //Percorrendo Parcelas e passando valor para o text
        for (i = ParcelaAtual; i < PrxParcela + 1; i++) {
            $("#txtParcVlr_" + ii).val(ValorCadaParcela);
        }
        $(".txtParcVlr").keyup();        





        return;*/
        //Somando Valor das Parcelas
        ValorTotal = 0;        
        $(".valorparcelas").each(function() {
            
            valorParcelaX = $(this).val();
            valorParcelaX = valorParcelaX.replace(".", "");
            valorParcelaX = valorParcelaX.replace(",", ".");
            valorParcelaX = parseFloat(valorParcelaX);
            
            ValorTotal = ValorTotal+valorParcelaX;
            
        });

        //Subtraindo Desconto Aplicado
        TotalCreditoAplicado = SomaDescontoDeCreditos();
        ValorTotal = ValorTotal-TotalCreditoAplicado*-1;

        //Numero de Parcelas
        nTotalParcelasX = 0;
        $(".valorparcelas").each(function(){
            nTotalParcelasX = nTotalParcelasX + 1;
        });

       
        //Se parceça atual (ordem das parcelas no focusout, for vazio ou 1, começa a repassar
        //os valores desde a primeira parcela lançada.
        if(e === '' || e === "1") {
            atual = 1;
        } else {
            atual = $(e).attr("id");
            atual = atual.replace("txtParcVlr_", "");
        }
        //Indice da proxima parcela...
        prxParcelas = nTotalParcelasX - atual;
        
        //Percorrendo Parcelas e passando valor para o text
        for (i = atual; i < prxParcelas + 1; i++) {
            $("#txtParcVlr_" + ii).val(valorResto);
        }
        $(".txtParcVlr").keyup();

    }


    function RecalculaParcelasZZZ(e) {
        TotalCreditoAplicado = SomaDescontoDeCreditos();
        
        atual = $(e).attr("id");
        atual = atual.replace("txtParcVlr_", "");

        valorParcelaX = $(e).val();

        valorParcelaX = valorParcelaX.replace(".", "");
        valorParcelaX = valorParcelaX.replace(",", ".");
        
        valorParcelaX = valorParcelaX-TotalCreditoAplicado;
        
        $(e).attr("value", valorParcelaX);

        nTotalParcelasX = 0;
        valorDevido = 0;
        $(".txtParcVlr").each(function () {
            nTotalParcelasX = nTotalParcelasX + 1;
        });

        

        valorResto = valorJur - valorParcelaX;
        
        
        
        
        
        //proximas parcelas
        prxParcelas = nTotalParcelasX - atual;
        valorResto = valorResto / prxParcelas;
        ii = atual;
        ii = parseInt(ii);
        valorResto = valorResto.toFixed(2);
        for (i = atual; i < prxParcelas + 1; i++) {
            ii = parseInt(i) + parseInt(1);
            $("#txtParcVlr_" + ii).val(valorResto);
        }

        $(".txtParcVlr").keyup();
    }

    function LancaParcelaAdicional() {
        i = 0;
        element = "";
        element = element + "<tr id='Parc_" + i + "' class='trParcelas ParcelasAdic ' valing='middle'>";
        element = element + "<td class='ParcN' >" + i + "</td>";
        element = element + "<td class='ParcDescri' ><input type='text' class='ParcDescriTxt' value='' class='form-control' style='border: 0px;  box-shadow: none;' /></td>";
        element = element + "<td class='ParcTpDoc'><select class='form-control tpdocs'  style='border: none;'></select></td>";
        element = element + "<td class='ParcVenc'><input type='text' class='form-control datepicker cursor' title='Clique para alterar o vencimento' data-toggle='tooltip' value='<?php echo date('d/m/Y') ?>' style='border: 0; box-shadow: none;' ></td>";
        element = element + "<td class='ParcValor'> R$ <input type='text' onkeyup='formataValorNovo(this); CalculaValor();' class='cursor valorparcelas' value='0' style='border: 0; box-shadow: none; width: 100px;'/></td>"
        //element = element + "<td>";
        <?php if ($VlrCreditoDisponivel > 0) { ?>
            //UtilizaCreditos();
                    //    if(!UsaCredito) {
                    //      element = element + '<input type="checkbox" class="chkUsaCredito active" onClick="setCredito(this)" />';
                    //    }
        <?php } ?>
        //element = element + "</td>";
        element = element + "<td><i class='fa fa-remove cursor' onclick='removerParcela(this)'></i></td>";
        element = element + "</tr>";

        $("#tableParcelasAd").prepend(element);
        //$(".tpdocs").html( $("#tpdoc").html() );
        //$(".tpdocs").val( $("#tpdoc").val() );
        DatePicker('.datepicker');
        $("#Parc_" + i + " .ParcTpDoc select").html($("#tpdoc").html())
        $("#Parc_" + i + " .ParcDescri input").focus();
        $("#Parc_" + i + " .ParcTpDoc select").val('DINH')

        ContaParcelas();
    }



    function ContaParcelas() {
        i = 0;
        $(".trParcelas").each(function () {
            i++;
            //$("#Parc_"+i+" .ParcN").attr('id','Parc_'+i);
            $(this).attr('id', 'Parc_' + i);
            $("#Parc_" + i + " .ParcN").html(i);
        });
        CalculaValor();
    }

    function removerParcela(i) {
        e = $(i).parent().parent().remove();
        $(i).parent().parent().remove();
        ContaParcelas();
    }


    function SomaDescontoDeCreditos() {
        valor_cred_utili = 0;

        if(  $(".valorparcelasc").length <= 0 ) {
            return "0,00";

        }
        $(".valorparcelasc").each(function () {
            e = $(this);
            vL = $(e).val();

            vL = vL.replace(".", "");
            vL = vL.replace(",", ".");
            vL = parseFloat(vL);

            valor_cred_utili += vL;
        });
        //VlrCreditoDisponivel = VlrCreditoDisponivel - valor_cred_utili;
        xxx = valor_cred_utili*-1;
        xxx = xxx.toFixed(2);
        
        //return valor_cred_utili*-1;
        return xxx;
    }

    function round(value, ndec) {
        var n = 10;
        for (var i = 1; i < ndec; i++) {
            n *= 10;
        }

        if (!ndec || ndec <= 0)
            return Math.round(value);
        else
            return Math.round(value * n) / n;
    }

    function CalculaValor() {

        valor = 0;
        $(".valorparcelas").each(function () {
            valor = parseFloat(valor) + parseFloat($(this).val().replace(".", "").replace(",", "."));
        });

        valor = valor.toFixed(2);
        $('.valorFinal').html(valor);


    }

    function AcertaValor() {
        valor = 0;
        $(".valorparcelas").each(function () {
            valor = parseFloat(valor) + parseFloat($(this).val().replace(".", "").replace(",", "."));
        });


        dif = parseFloat(valorJur - valor);
        valor_dif = $("#tableParcelas tr:first .ParcValor input").val();
        valor_dif = valor_dif.replace(".", "").replace(",", ".");
        dif = parseFloat(valor_dif) + dif;
        dif = dif.toFixed(2)

        $("#tableParcelas tr:first .ParcValor input").val(dif);
        $(".valorparcelas").keyup();


    }

    function AcertaCentavos() {
        var n = 10;
        var Diferenca = 0;
        $(".ParcelasOrigi .ParcValor input[type=text]").each(function () {
            valor = $(this).val();
            valor = valor.replace(".", "").replace(",", ".");
            UltimaCasa = valor.slice(-1);
            if (UltimaCasa < n) {
                Diferenca = Diferenca + UltimaCasa;
                if (UltimaCasa < 5) {
                    valor = valor - parseFloat("0.0" + UltimaCasa);
                } else {
                    valor = valor - parseFloat("0." + UltimaCasa);
                }

                $(this).val(parseFloat(valor).toFixed(2))
            }


        });
        console.log(Diferenca);
        $('.valorparcelas').keyup();

    }
    var parcelas = [];
    function SalvarParcelas() {
        parcelas = {nParcelas: nParcelas, valorJur: valorJur};
        i = 1;
        erro = false;
        valorAdicional = 0;
        valorParcelas = 0;
        $(".trParcelas").each(function () {

            descricao = $("#Parc_" + i + " .ParcDescri input").val();
            documento = $("#Parc_" + i + " .ParcTpDoc select").val();
            valor = $("#Parc_" + i + " .ParcValor input").val();
            vencimento = $("#Parc_" + i + " .ParcVenc input").val();
            parcela = $("#Parc_" + i + " .ParcN").html();

            if (descricao == '') {
                erro = true;
                alertify.error("<b>Lançamento #" + i + "</b><br/> Informe uma descrição!");
                return false;
            }

            if (documento == '') {
                erro = true;
                alertify.error("<b>Lançamento #" + i + "</b><br/> Informe o tipo de documento");
                return false;
            }

            if (valor <= 0 || valor == "00,00") {
                erro = true;
                alertify.error("<b>Lançamento #" + i + "</b><br/> Valor informado não é válido!");
                return false;
            }

            if (vencimento == '') {
                erro = true;
                alertify.error("<b>Lançamento #" + i + "</b><br/> Informe a data de vencimento");
                return false;
            }

            valor = valor.replace(".", "").replace(",", ".")
            valor = parseFloat(valor);
            if ($(this).hasClass('ParcelasAdic')) {
                tipo = "ADICIONAL";
                valorAdicional = parseFloat(valorAdicional);
                valorAdicional = valorAdicional + valor
            } else {
                valorParcelas = parseFloat(valorParcelas);
                valorParcelas = valorParcelas + valor;
                tipo = "PARCELA";
            }

            parcelas[i] = {parcela: parcela, valor: valor, valorOriginal: valorOrig, vencimento: vencimento, descricao: descricao, documento: documento, tipo: tipo}
            i++;
        });


        valorAdicional = valorAdicional.toFixed(2);
        valorParcelas = valorParcelas.toFixed(2);

        if (nParcelas == 0 || valorJur == 0) {
            return;
        }

        msg = '';
        if (valorParcelas < valorJur) {
            msg = '';
            //alertify.error("A Soma das parcelas são menor que o valor devido! <br/> Por favor, verifique o valor das parcelas ou continue");
            //erro = true;
            //return false;
        }
        if (erro) {
            return false;
        }

        alertify.confirm('Atenção', 'Confirmar lançamento de parcelas',
                function () {
                    confirmaLancamento();
                },
                function () {

                })





    }

    function confirmaLancamento() {
        $("#lancamentos").fadeOut();
        $.ajax({
            type: 'post',
            data: {parcelas: parcelas, nEntrada : nEntrada },
            url: "/index.php?route=AlunosFinanceiro/SalvaParcelas/&codigo=<?php echo $_GET['codigo'] ?>&matricula=<?php echo $_GET['matricula'] ?>",
            success: function (e) {
                if (e == '') {
                    alertify.success("Lancamento realizado!");
                    setTimeout(function () {
                        window.location = 'index.php?route=AlunosFinanceiro/index/&codigo=<?php echo $_GET["codigo"] ?>';
                    }, 150)
                } else {
                    alertify.error(e);
                    $("#lancamentos").fadeIn();
                }
            },
            error: function(e) {
                alertify.error("Falha ao recuperar informações, tente novamente!");
                $("#lancamentos").fadeIn();
            }
        })
    }


</script>