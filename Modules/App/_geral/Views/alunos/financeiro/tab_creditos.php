<div class="row">
    <div class="col-md-12">
        <h3 class="page-title text-orange">
            <center> Creditos </center>
        </h3>
    </div>
</div>
<br/>    
<div class="form" style="font-size: 18px;">
    <span class="text-blue">
        Total de Creditos disponivel: R$ <span class="totCredDisponivel"></span>
    </span>

    <span class="text-red pull-right">

    </span>
</div>
<br/><br/>	
<?php if ($Creditos) { ?>
    <div class="form">
        <table class="table table-condensed table-bordered table-hover table-stripedx table-gridx">
            <thead>
                <tr>
                    <th width="70">&nbsp;</th>
                    <th width="25">#</th>
                    <th width="85">Valor</th>
                    <th>Descrição / Motivo</th>
                    <th width="85">Data</th>
                    <th width="85">Utilizado</th>
                    <th width="85">Vlr. Utilizado</th>
                </tr>
            </thead>
            <tbody >
                <?php
                $totalDiponivel = 0;
                foreach ($Creditos as $item) {
                    $style = "";


                    if (!empty($item['alucre_utiliza']) || !empty($item['can_data'])) {
                        $style = "color: #9e8f00;";                      
                    } else {
                        $totalDiponivel = $totalDiponivel + $item['alucre_vlr'];
                    }
                    
                            if ( !empty($item['can_data']) ) {
                                $style ="color: #c31231";
                            }                    
                    ?>
                    <tr class=" <?php echo ""; ?>" style="<?php echo $style?>" id="cred-<?php echo $item['alucre_id']; ?>">
                        <td>
                            <?php if (is_null($item['alucre_utiliza']) ) { ?>
                                <?php if(is_null($item['can_data']) ) { ?>
                                    <a href="javascript: void(0)" onclick="Utilizar(this)" data-action="x" data-id="<?php echo $item['alucre_id']; ?>" data-valor="<?php echo number_format($item['alucre_vlr'], 2); ?>" class=" btn btn-danger  btn-xs" title="Remover Registro"><i class="fa fa-trash"></i></a>
                                    <a href="javascript: void(0)" onclick="Utilizar(this)" data-action="u" data-id="<?php echo $item['alucre_id']; ?>" data-valor="<?php echo number_format($item['alucre_vlr'], 2); ?>" class=" btn btn-primary btn-xs" title="Utilizar Crédito"><i class="fa fa-check-circle "></i></a>
                                   <?php } else { ?>
                                    <i class="fa fa-asterisk text-red" title="Cancelado" data-toggle="tooltip"></i>
                                   <?php } ?>
                            <?php } ?>
                        </td>
                        <td><?php echo $item['alucre_id'] ?></td>
                        <td>R$ <?php echo number_format($item['alucre_vlr'], 2, ",", ".") ?></td>
                        <td>
                            <small>
                                <b>Motivo Crédito: </b> <?php echo $item['alucre_descri'] . " / " . $item['alucre_motivo'] ?>
                                <?php if (!empty($item['alucre_utiliza'])) { ?>
                                    <br />
                                    <b>Motivo Utilização:</b> <?php echo $item['alucre_motivoutiliza']; ?>
                                <?php } ?>
                            </small>
                        </td>
                        <td><?php echo DataBR($item['alucre_data']) ?></td>
                        <td><?php echo DataBR($item['alucre_utiliza']) ?></td>
                        <td>R$ <?php echo number_format($item['alucre_vlrutilizado'], 2, ",", ".") ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <script>
            $(".totCredDisponivel").html("<?php echo number_format($totalDiponivel, "2", ",", ".") ?>");
            //            $(".totDevido").html("<?php echo number_format($totValorDevido, "2", ",", ".") ?>");
        </script> 
    </div>

    <!-- Modal Utilizar Créditos -->
    <div class="modal" id="modalUtilizaCreditos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="frmUtilizaCreditos">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Utilizar Créditos</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="idcredito" id="idcredito" class="form-control"/>
                        <label>Valor</label>
                        <input type="text" name="valorcredito" id="valorcredito" onkeyup="formataValor(this)" class="form-control"/>

                        <label>Motivo</label>
                        <input type="text" name="motivo" id="motivo" onkeyup="" class="form-control"/>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript: void(); " type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <a href="javascript: UtilizaCreditos(); "  class="btn btn-primary">Confirmar</a>
                    </div>
                    <div class="modal-footer modal-footer-2" style="display: none;">
                        ...Aguarde...
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    
<!-- Modal Utilizar Créditos -->
    <div class="modal" id="modalCancelarCreditos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="frmCancelarCreditos">
                    <input type="hidden" name="valorcredito" id="valorcreditox" onkeyup="formataValor(this)" class="form-control"/>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabelx">Cancelar Créditos</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="idcredito" id="idcreditox" class="form-control"/>
                        <label>Motivo</label>
                        <input type="text" name="motivo" id="motivox" onkeyup="" class="form-control"/>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript: void(); " type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <a href="javascript: UtilizaCreditos(); "  class="btn btn-primary">Confirmar</a>
                    </div>
                    <div class="modal-footer modal-footer-2" style="display: none;">
                        ...Aguarde...
                    </div>
                </form>
            </div>
        </div>
    </div>    


    <script>


        var acao;
        function Utilizar(obj) {
            
            var idx = $(obj).attr("data-id");
            var valorx = $(obj).attr("data-valor");

            if ($(obj).attr("data-action") == 'u') {
                acao = "u";
                $("#valorcredito").val(valorx).keyup();
                $("#idcredito").val(idx);                
                $("#modalUtilizaCreditos").modal('show');
                setTimeout(function () {
                    $("#motivo").focus();
                }, 500)                
            }
            
            if ($(obj).attr("data-action") == 'x') {
                acao = "x";
                $("#valorcreditox").val(valorx).keyup();
                $("#idcreditox").val(idx);                
                $("#modalCancelarCreditos").modal('show');
                setTimeout(function () {
                    $("#motivox").focus();
                }, 500)                
            }            

        }

        function UtilizaCreditos() {

            if(acao == "u" ) {
                $("#modalUtilizaCreditos .modal-footer").hide();
                $("#modalUtilizaCreditos .modal-footer-2").show();                
                var datas = $("#frmUtilizaCreditos").serializeArray(); } 

            if(acao == "x" ) {
                $("#modalCancelarCreditos .modal-footer").hide();
                $("#modalCancelarCreditos .modal-footer-2").show();                
                var datas = $("#frmCancelarCreditos").serializeArray(); }

            $.ajax({
                url: "<?php echo Permalink("Alunos", "UtilizaCreditos", "codigo=" . $_GET['codigo']); ?>&t="+acao,
                data: datas,
                type: 'POST',
                dataType: 'json',
                success: function (e) {
                    console.log(e.erro);
                    if (typeof e.erro !== 'undefined') {
                        alertify.error(e.erro);
                        $(".modal-footer-2").hide();
                        $(".modal-footer").show();
                        return false;
                    } else {
                        alertify.success(e.menssagem);
                        location.reload();
                    }
                },

                error: function () {
                    alertify.error("Houve um erro ao utilizar Créditos")
                    $(".modal-footer-2").hide();
                    $(".modal-footer").show();
                }
                
            });

        }
    </script>

<?php } else { ?>
    <div class="form">
        <div class="text-danger"> Aluno não possui créditos! </div>
    </div>
    <?php
}?>