<?php
$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
#$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->CondicaoExtra = " AND amf_dataprocesso = '$data'";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<style>
    @media print{
        table{ page-break-inside:avoid; }
    }
</style>
<section class="content" >
    <?php if ($dados->num_rows >= 1) {?>
        <?php $i=0; while($report = $dados->fetch_assoc()) { ?>
        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -10px;  ">
            <tr class="text-center">
                <td height="80" width="*">                                         <img src="/Public/img/brasaosp.jpg" width="60" style="margin-left: 30px;" class="pull-left"> <img src="/Public/img/logo_detransp3.jpg" width="250"> <br/></td>
                <td rowspan="2" width="320" style="font-size: 30px"> <b> NOTIFICAÇÃO INICIAL </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="20">
                <td> <b> &nbsp; NOME DA ENTIDADE: Maria Ceclia de Paula Pimentel MC - Botutrânsito </b> </td>
            </tr>
            <tr height="20">
                <td> <b> &nbsp; Portaria de credenciamento/renovação: GETPT Nº. 014/015 de 13/03//2017 </b> </td>
            </tr>
            <tr height="10">
                <td> </td>
            </tr>
            <tr height="20">
                <td height="20"> <b> &nbsp; ENDEREÇO: Rua Curuzu, nº. 945, Centro, Botucatu/SP </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="20">
                <td rowspan="2" width="*"> <b> &nbsp; TELEFONE: (14) 3881-1672 </b> </td>
                <td rowspan="2" width="200" style="font-size: 12px"> <b> &nbsp; E-MAIL: <br> &nbsp; botutransito@uol.com.br </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="10">
                <td> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <b> &nbsp; NOME DO CURSO: <?php echo $report['cur_descricao']; ?>  </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="10">
                <td> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="350"> <b> &nbsp; TURMA: &nbsp; <?php echo $report['cur_turma']; ?> </b> </td>
                <td width="350"> <b> &nbsp; SALA: &nbsp; <?php echo $report['cur_sala']; ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="10">
                <td> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td align="center"> <b> PERÍODO DO CURSO </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <h5> <b> &nbsp; DIAS: &nbsp; <?php echo $report['cur_dias']; ?> </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <h5> <b> &nbsp; HORÁRIO DE INÍCIO: &nbsp; <?php echo $report['cur_hora']; ?> </b> </h5> </td>
                <td> <h5> <b> &nbsp; HORA DO TÉRMINO: &nbsp; <?php echo $report['cur_hrtermino']; ?> </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="352"> <h5> <b> &nbsp; HORÁRIO DO ALMOÇO: &nbsp; <?php echo $report['cur_hralmoco']; ?> </b> </h5> </td>
                <td width="350"> <h5> <b> &nbsp; </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="10">
                <td> </td>
            </tr>
        </table>


        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="30">
                <td align="center"> <b> RELAÇÃO DE ALUNOS MATRICULADOS  </b> </td>
            </tr>
        </table>


        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <thead>
                <tr>
                    <th with="50"> &nbsp; Nº.</th>                        
                    <th with="*"> &nbsp; NOME</th>
                    <th width="110"> &nbsp; C P F</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($dados->num_rows >= 1) {
                    $i = 1;
                    ?>
                    <?php // while ($report = $dados->fetch_assoc()) { ?>

                    <?php foreach ($dados as $xdados) { ?>
                        <tr>
                            <td> &nbsp; <?php echo $i; ?> <br/></td>
                            <td> &nbsp; <?php echo $xdados['usu_nomecompleto'] ?> <br/></td>
                            <td> &nbsp; <?php echo $xdados['usu_cpf'] ?> <br/></td>
                        </tr>
                    <?php $i++;}//for ?>
                <?php } else {//if ?>
                    <tr>
                        <td  colspan="7" style="font-size: 28px">
                <center>
                    Sem resultados
                </center>
                </td>
                </tr>

            <?php } ?>
            </tbody>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="10">
                <td> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*"> <b> &nbsp; TIPO DE CURSO </b> </td>
                <td width="20"> <b> &nbsp; <?php echo $report['cur_formacao']; ?> </b> </td>
                <td width="*"> <b> &nbsp; FORMAÇÃO </b> </td>
                <td width="20"> <b> &nbsp; <?php echo $report['cur_atualizacao']; ?> </b> </td>
                <td width="*"> <b> &nbsp; ATUALIZAÇÃO </b> </td>
            </tr>
        </table>

        <br/>        

       <span style="font-size: 15px">
           <center> 
              <b> BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
              <br><br><br>
              &nbsp; <b>__________________________________________</b> <br>
              &nbsp; <b> BRUNA BERALDO PEREZ - CRED. Nº 90382 </b> <br>
              &nbsp; <b> COORDENADORA GERAL </b> <br>
           </center>
       </span>

        <?php } //while ?>
    <?php } //if ?>
</section>
<!--
<tr class="hide">
            <td>
                <table style="width: 100%">
                    <td><img src="/Public/img/RodapeBO.jpg" width="1200"></td>
                </table>
            </td>
        </tr>-->
