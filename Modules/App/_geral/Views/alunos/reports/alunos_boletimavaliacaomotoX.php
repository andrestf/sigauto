<?php
$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<style>
    @media print{
        table{ page-break-inside:avoid; }
    }
</style>
<section class="content" >
    <?php if ($dados->num_rows >= 1) {?>
        <?php while ($report = $dados->fetch_assoc()) { ?>
        <table class="" style="page-break-before: always; width: 100%; border: none; margin-top: 5px; ">
            <tr class="text-center">
                <td><img src="/Public/img/logo_detransp.jpg" width="100"></td>
                <td width="" class="text-bold"><h3>&nbsp;&nbsp;&nbsp;BOLETO DE AVALIAÇÃO DE EXAME PRÁTICO DE <br /> &nbsp;&nbsp;&nbsp;DIREÇÃO VEICULAR Categoria “A” &nbsp;&nbsp;&nbsp;</h3></td>
                <td><img src="/Public/img/NumeroBO.jpg" width="100"></td>
            </tr>
        </table>
        
    <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: 0px; font-size: 9px;  ">
        <tr>
            <td colspan="3" class="text-center bg-gray">DADOS DO CANDIDATO</td>
            <td colspan="3" class="text-center bg-gray">DADOS DO EXAME</td>
        </tr>
        
        <tr>
            <td width="200" class="text-center bg-gray">CPF DO CANDIDATO</td>
            <td width="400" class="text-center bg-gray">NOME DO CANDIDATO</td>
            <td width="200" class="text-center bg-gray">RENACH</td>
            <td colspan="3" class="text-center bg-gray">Nº PROTOCOLO DO EXAME</td>
        </tr>
        
        <tr>
            <td style="font-size: 11px"> &nbsp;&nbsp;&nbsp; <b> <?php echo $report['usu_cpf']; ?> </b> </td>
            <td style="font-size: 11px"> &nbsp;&nbsp;&nbsp; <b> <?php echo strtoupper($report['usu_nomecompleto']); ?> </b> </td>
            <td style="font-size: 11px"> &nbsp;&nbsp;&nbsp; <b> <?php echo $report['usu_renach']; ?> </b> </td>
            <td>DATA</td>
            <td>HORA</td>
            <td width="120">CATEGORIA</td>
        </tr>
        
        <tr>
            <td></td><td></td><td></td>
            <td> <b> <?php echo $_POST['data_fase']; ?> </b> </td>
            <td></td>
            <td> &nbsp;&nbsp;&nbsp; <b> “A” </b> </td>
        </tr>
        
    </table>
    
    <table class="tablex table-condensed" style="margin-top: 5px; width: 100%; font-size: 9px;" border="1">
        <tr>
            <td width="200" class="text-center bg-gray">CÓDIGO CFC</td>
            <td width="500" class="text-center bg-gray">NOME DO CFC</td>
            <td width="150" class="text-center bg-gray">EDITAL</td>
            <td width="500" class="text-center bg-gray">MUNICÍPIO DO EDITAL</td>
        </tr>
        
        <tr>
            <td> <b> <?php echo $_SESSION['CLI_CODCFC']; ?> </b> </td>
            <td> <b> <?php echo $_SESSION['APP_LOCALFANTASIA']; ?> </b> </td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <p style="font-size: 9px; margin-top: 0px; margin-bottom: 0px;  ">
        &nbsp;Para a avaliação, assinalar de acordo com as faltas cometidas pelo candidato durante o exame prático de direção veicular. &nbsp; Conforme a Resolução nº 168/04 do Contran, o candidato que cometer falta eliminatória ou cuja soma dos pontos negativos ultrapassar a 3 (três) será considerado reprovado.        
    </p>
    
    <table style="margin-top: 5px; width: 100%;  font-size: 9px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> I - Faltas Eliminatórias: </b> </td>
        </tr>
        <tr>
            <td width="30"></td>
            <td>Iniciar a prova sem estar com o capacete devidamente ajustado à cabeça ou sem viseira ou óculos de proteção </td>
        </tr>
        <tr>
            <td></td>
            <td>Descumprir o percurso pre-estabelecido</td>
        </tr>
        <tr>
            <td></td>
            <td>Abalroar um ou mais cones de balizamento</td>
        </tr>
        <tr>
            <td></td>
            <td>Cair do veículo, durante a prova</td>
        </tr>
        <tr>
            <td></td>
            <td>Não manter equilíbrio na prancha, saindo lateralmente dela</td>
        </tr>
        <tr>
            <td></td>
            <td>Avançar sobre o meio fio ou parada obrigatória</td>
        </tr>
        <tr>
            <td></td>
            <td>Colocar o(s) pé(s) no chão, com o veículo em movimento</td>
        </tr>
        <tr>
            <td></td>
            <td>Provocar acidente durante a realização do exame</td>
        </tr>
        <tr>
            <td></td>
            <td>Cometer qualquer outra infração de trânsito de natureza gravíssima</td>
        </tr>
    </table>

    <table style="margin-top: 5px; width: 100%;  font-size: 9px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> II - Faltas Graves (3 pontos): </b> </td>
            <td colspan="4">&nbsp;&nbsp; <b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>Deixar de colocar um pé no chão e o outro no freio ao parar o veículo</td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>Invadir qualquer faixa durante o percurso</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Fazer incorretamente a sinalização ou deixar de fazê-la</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Fazer o percurso com o farol apagado</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Cometer qualquer outra infração de trânsito de natureza grave</td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="margin-top: 5px; width: 100%;  font-size: 9px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> III - Faltas Médias (2 pontos): </b> </td>
            <td colspan="4">&nbsp;&nbsp; <b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>Utilizar incorretamente os equipamentos</td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>Engrenar ou utilizar marchas inadequadas durante o percurso</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Não recolher o pedal de partida ou o suporte do veículo, antes de iniciar o percurso</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Interromper o funcionamento do motor sem justa razão, após o início da prova</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Conduzir o veículo durante o exame sem segurar o guidom com ambas as mãos, salvo
eventualmente para indicação de manobras</td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="margin-top: 5px; width: 100%;  font-size: 9px;" border="1">
        <tr class="bg-gray">
            <td width="30"></td>
            <td width="*">&nbsp;&nbsp; <b> IV - Faltas Leves (1 ponto): </b> </td>
            <td colspan="4">&nbsp;&nbsp; <b> Quantidade de Faltas </b> </td>
        </tr>
        <tr>
            <td></td>
            <td>Colocar o motor em funcionamento, quando já engrenado</td>
            <td width="30"></td><td width="30"></td><td width="30"></td><td width="30"></td>
        </tr>
        <tr>
            <td></td>
            <td>Conduzir o veículo provocando movimento irregular no mesmo sem motivo justificado</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Regular os espelhos retrovisores durante o percurso do exame</td>
            <td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td></td>
            <td>Cometer qualquer outra infração de trânsito de natureza leve</td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>

    <table style="width: 100%; margin-top: 5px;">
        <tr>
            <td width="48%">

                <table style="width: 100%; font-size: 9px;     margin-top: -13px;" class="text-center" border="1">
                    <tr> <td colspan="4" class="text-center text-bold">&nbsp</td></tr>
                    <tr> <td colspan="4" class="text-center text-bold">Não cometeu nenhuma falta</td></tr>
                    <tr  style="font-size: 9px; " class="bg-gray"> <td colspan="4" class="text-center text-bold">
                        Dados do Examinador (carimbar e assinar)
                    </td>
                    </tr>
                    <tr>    
                        <td>CPF</td>
                        <td>Nome</td>
                        <td>Data</td>
                        <td>Assinatura</td>
                    </tr>
                    <tr>    
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>             


            </td>
            <td width="48%">
                <table style="width: 100%; font-size: 9px;" class="text-center" border="1">
                    <tr> <td colspan="4" class="text-center text-bold">RESULTADO</td></tr>
                    <tr class="bg-gray">
                        <td>Aprovado</td>
                        <td>Reprovado</td>
                        <td colspan="2">Ausente</td>
                    </tr>
                    <tr class="">
                        <td>&nbsp;</td>
                        <td></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr  style="font-size: 9px; " class="bg-gray"> <td colspan="4" class="text-center text-bold">Dados do Presidente da Banca (carimbar e assinar)</td>
                    </tr>
                    <tr>    
                        <td>CPF</td>
                        <td>Nome</td>
                        <td>Data</td>
                        <td>Assinatura</td>
                    </tr>
                    <tr>    
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
    <div style="margin-top: 60px; border-top: solid 2px; width: 100%; padding-top: 2px;" class="text-center">
        Departamento Estadual de Trânsito de São Paulo - Diretoria de Habilitação
    </div>

        <?php }//while ?>
    <?php } //if ?>
</section>
<!--
<tr class="hide">
            <td>
                <table style="width: 100%">
                    <td><img src="/Public/img/RodapeBO.jpg" width="1200"></td>
                </table>
            </td>
        </tr>-->
