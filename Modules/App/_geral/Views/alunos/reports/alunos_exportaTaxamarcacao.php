<?php

$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
//$report->CondicaoExtra = "AND (amf_examepratico ='E' OR amf_examepratico = 'M') AND (amf_faltoso is NULL OR amf_faltoso='') ";
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='M') AND (amf_faltoso is NULL OR amf_faltoso='') ";
$report->OrderBy = "usu_nomecompleto";// LIMIT 8";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();

### aqui deixa listado a select em tela
//echo "<pre>" . $report->last_query();

$config = new ConfigHelper();
$ValorEmissao  = $config->Get('nValorTaxaEmissao');
$ValorMarcacao = $config->Get('nValorTaxaMarcacao');

if ($dados->num_rows >= 1) {
    $nomeArquivo = "exporta/01-".date("dmY").".txt";
    #echo str_pad(substr("123",0,5),5,"0",STR_PAD_LEFT);
    #echo str_pad(substr($_SESSION['APP_LOCALNOME'],0,35),35,".");
    #echo substr($nome.fReplicate(".",40),0,35); Direita
    #echo substr(fReplicate("0",5).$conta,-5); Esquerda

    $tpServ  = "0";
    $agencia = "0001";
    $conta   = "44069000";
    $nomeemp = substr($_SESSION['APP_LOCALNOME'].fReplicate(" ",35),0,35);
    $teleemp = $_SESSION['APP_LOCALFONE'].fReplicate(" ",1);
    $filler  = fReplicate(" ",318);
    $versao  = "000003";
    $datahj  = date("dmY");
    $nSeq1   = "00001";


    $header = $tpServ.$agencia.$conta.$nomeemp.$teleemp.$filler.$versao.$datahj.$nSeq1.chr(13).chr(10);

    $body = "";

    $nAlunos = 0;
    $nVlrTx  = 0;
    $nValorTotTaxas = 0;
    $nQtdRegs = 0;
    $nvlrnovo = 0;
    while ($report = $dados->fetch_assoc()) {
        $nAlunos++;

        // valor total das taxas
        $nValorTotTaxas = $nValorTotTaxas;

        // quantidade de registros
        $nQtdRegs++;

        $geraMarcacaoEmisao = false;

        $tpReg   = "1";
        $tpIde   = "SERVICO12".fReplicate(" ",1);
        $cCpf    = "".substr(fLimpaNumero($report['usu_cpf']).fReplicate(" ",14),0,14)."";
        $Servic  = "12";
        if($report['amf_examepratico'] == "E") {
            $xx = "e-";
            $tpSub   = "003";
            $SerAut  = "020";
            $subSer  = "01";
            $placa   = fReplicate(" ",7);
            $filler1 = fReplicate(" ",264);
            $nVlrTx  = substr(fReplicate("0",15).fLimpaNumero($ValorEmissao),-15);
            $geraMarcacaoEmisao = true;
            $nvlrnovo = $nvlrnovo+$ValorEmissao;
            if($geraMarcacaoEmisao) {

            }
        } else {
            $xx = "m-";
            $tpSub   = "006";
            $SerAut  = "017";
            $subSer  = "02";
            $placa   = fReplicate(" ",7);
            $filler1 = fReplicate(" ",264);
            $nVlrTx  = substr(fReplicate("0",15).fLimpaNumero($ValorMarcacao),-15);
            $nvlrnovo = $nvlrnovo+$ValorMarcacao;
        }

        //$nVlrTx  = substr(fReplicate("0",15).fLimpaNumero($ValorMarcacao),-15);
        $filler2 = fReplicate(" ",74);
        $nSeq2   = substr(fReplicate(0,5).$nAlunos,-5);

        $body .= $tpReg.$tpIde.$cCpf.$Servic.$subSer.$tpSub.$SerAut.$placa.$filler1.$nVlrTx.$filler2.$nSeq2.chr(13).chr(10);

        //se duplica linha para M+E
        if($geraMarcacaoEmisao) {
            $nAlunos++;
            $geraMarcacaoEmisao = false;
            $xx = "m2-";
            $tpSub   = "006";
            $SerAut  = "017";
            $subSer  = "02";
            $placa   = fReplicate(" ",7);
            $filler1 = fReplicate(" ",264);
            $nVlrTx  = substr(fReplicate("0",15).fLimpaNumero($ValorMarcacao),-15);
            $nSeq2   = substr(fReplicate(0,5).$nAlunos,-5);

            $nvlrnovo = $nvlrnovo+$ValorMarcacao;

            $body .= $tpReg.$tpIde.$cCpf.$Servic.$subSer.$tpSub.$SerAut.$placa.$filler1.$nVlrTx.$filler2.$nSeq2.chr(13).chr(10);
        }

        // valor total das taxas
        $nValorTotTaxas = $nVlrTx+$nVlrTx;

    } //while
    //echo $nvlrnovo;
    //exit();
    $footer = "";
    $nSeq9   = substr(fReplicate(0,6).$nSeq2,-6);
    $nTlSeq  = ($nSeq2);
    //$nTlSeq  = ($nSeq1.+$nSeq2.+$nSeq9.+1);

    //tirar ponto valor novo
    $nvlrnovo = str_replace(".",'',$nvlrnovo);
    $nvlrnovo = str_replace(",",'',$nvlrnovo);
    $nAlunos = $nAlunos+1;

    $tpTra   = /* "<br /> tpd regi " .*/  "9";                                     // Gravar sempre "9"
    $nqGui   = /* "<br /> qtd guia " .*/  substr(fReplicate("0",6).$nSeq9,-6);     // Quantidade total de guias sendo pagas (deve ser equivalente à quantidade de Registros Detalhe 1 sendo enviados no arquivo)
    $nqReg   = /* "<br /> qtd regs " .*/  substr(fReplicate("0",6).$nTlSeq,-6);    // Quantidade total de Registros Detalhe (1 e 2) deste arquivo (EXCLUINDO Header e Trailler)
    $nvtlPg  = /* "<br /> tot paga " .*/  substr(fReplicate("0",15).$nvlrnovo,-15);  // Somatória dos campos “Valor Total” dos Registros Detalhe 1, quando campo “Tipo Serviço=P"
    $nvtlImp = /* "<br /> tot impr " .*/  substr(fReplicate("0",15).$nvlrnovo,-15);  // Somatória dos campos “Valor Total” dos Registros Detalhe 1, quando campo “Tipo Serviço=I"
    $nvlrTl  = /* "<br /> vlr tota " .*/  substr(fReplicate("0",15).$nvlrnovo,-15);  // Somatória de todos os campos "Valor Total" dos Registros Detalhe 1 (Serviços P + I)
    $nqGpgr  = /* "<br /> qtd gpag " .*/  substr(fReplicate("0",6).$nTlSeq,-6);    // Total de Guias “Tipo Serviço = P”
    $nqGimp  = /* "<br /> qtd gimp " .*/  substr(fReplicate("0",6).$nTlSeq,-6);    // Total de Guias “Tipo Serviço = I”
    $filler3 = /* "<br /> 000 fill " .*/  fReplicate(" ",325);                     // (uso futuro)
    $nSeq9   = /* "<br /> seq sequ " .*/  substr(fReplicate(0,5).$nAlunos,-5);     // Número sequencial do registro (Vide definição do Registro Header).

    $footer = $tpTra.$nqGui.$nqReg.$nvtlPg.$nvtlImp.$nvlrTl.$nqGpgr.$nqGimp.$filler3.$nSeq9;

    $content = $header.$body.$footer;

###########################################################################################
    ### aqui deixa encript
    $content = encExporta($content);

    ###  Aqui escreve na tela as duas linhas
    #echo $content;
    #exit();
###########################################################################################

    $fp = fopen($nomeArquivo,"wb");
    fwrite($fp,$content);
    fclose($fp);

    echo "<br /> <br /> 
    <center>Arquivo Gerado com sucesso... clique <a href='$nomeArquivo' />aqui</a> para fazer o download.</center>
    <br /> <br /> 
    ";

    @header('Content-disposition: attachment; filename='.$nomeArquivo);
    @header('Content-type: text/plain');


    #readfile($nomeArquivo);
    exit();

} //if