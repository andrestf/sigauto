<?php
$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$data = DataDB($_POST['data_fase']);
#$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->CondicaoExtra = " AND amf_dataprocesso = '$data'";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<style>
    @media print{
        table{ page-break-inside:avoid; }
    }
</style>
<section class="content" >
    <?php if ($dados->num_rows >= 1) {?>
        <?php $i=0; while($report = $dados->fetch_assoc()) { ?>
        <?php $i++ ?>
        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -10px;  ">
            <tr class="text-center">
                <td height="80" width="*">                                         <img src="/Public/img/brasaosp.jpg" width="60" style="margin-left: 30px;" class="pull-left"> <img src="/Public/img/logo_detransp3.jpg" width="250"> <br/></td>
                <td rowspan="2" width="120" style="font-size: 18px"> <b> ALUNO Nº <br> <?php echo $i ?> </b> </td>
            </tr>
            <tr>
                <td class="text-center"> <h4> <b> FICHA DE MATRÍCULA - Cursos Especializados </b> </h4> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="20">
                <td> <b> &nbsp; NOME DA ENTIDADE: Maria Ceclia de Paula Pimentel ME - Botutrânsito </b> </td>
            </tr>
            <tr height="20">
                <td> <b> &nbsp; PORTARIA DE CREDENCIAMENTO/RENOVAÇÃO: GEPT Nº 014/015 de 13/03/2017 </b> </td>
            </tr>
            <tr height="20">
                <td height="20"> <b> &nbsp; ENDEREÇO: Rua Curuzu, nº. 945, Centro, Botucatu/SP </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">            
            <tr height="20">
                <td rowspan="2" width="*"> <b> &nbsp; TELEFONE: (14) 3881-1672 </b> </td>
                <td rowspan="2" width="200" style="font-size: 12px"> <b> &nbsp; E-MAIL: <br> &nbsp; botutransito@uol.com.br </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <b> &nbsp; TIPO DE CURSO: <?php echo $report['cur_descricao']; ?>  </b> </td>
                <td width="200"> <b> 
                    <center>FOTO DO ALUNO ( 2X2 )</center>
                    <div style="border: solid 1px; width: 60px; height: 40px; margin: 0 auto ">                    
                    </div> </b>
                </td>
            </tr>
        </table>

        <table cellpadding="2" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; RG </b> </td>
                <td width="200" style="font-size: 12px"> <b> &nbsp; CPF </b> </td>
                <td width="200" style="font-size: 12px"> <b> &nbsp; Nº REGISTRO CNH </b> </td>
                <td width="120" style="font-size: 12px"> <b> &nbsp; CATEGORIA </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"> <b> &nbsp; <?php echo $report['usu_rg']; ?> </b> </td>
                <td width="200"> <b> &nbsp; <?php echo $report['usu_cpf']; ?> </b> </td>
                <td width="200"> <b> &nbsp; <?php echo $report['usu_numdoc']; ?> </b> </td>
                <td width="120"> <b> &nbsp; <?php echo $report['usu_catatual']; ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; NOME </b> </td>
                <td width="100" style="font-size: 12px"> <b> &nbsp; SEXO </b> </td>
                <td width="100" style="font-size: 12px"> <b> &nbsp; DATA NASC </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"> <b> &nbsp; <?php echo $report['usu_nomecompleto']; ?> </b> </td>
                <td width="100"> <b> &nbsp; <?php if($report['usu_sexo'] == 'M') {
                                                          echo 'MASCULINO';
                                                        } else {
                                                          echo 'FEMININO';
                                                        }?> </b> </td>
                <td width="100"> <b> &nbsp; <?php echo databr($report['usu_nascimento']); ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; NACIONALIDADE </b> </h5> </td>
                <td width="200" style="font-size: 12px"> <b> &nbsp; NATURALIDADE </b> </h5> </td>
                <td width="50" style="font-size: 12px"> <b> &nbsp; UF </b> </h5> </td>
                <td width="150" style="font-size: 12px"> <b> &nbsp; ESTADO CIVIL </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"> <b> &nbsp; <?php if($report['usu_nacionalidade'] == '1') {
                                                          echo 'BRASILEIRO';
                                                        } elseif ($report['usu_nacionalidade'] == '2') {
                                                          echo 'BRASILEIRO NATURALIZADO';
                                                        } elseif ($report['usu_nacionalidade'] == '3') {
                                                          echo 'ESTRANGERIO';
                                                        } else {
                                                          echo 'BRASILEIRO NASCIDO NO EXTERIOR';
                                                        };?> </b> </td>
                <td width="200"> <b> &nbsp; <?php echo $report['usu_naturalidade']; ?> </b> </td>
                <td width="50"> <b> &nbsp; <?php echo $report['usu_uf']; ?> </b> </td>
                <td width="150"> <b> &nbsp; <?php echo $report['usu_estadocivil']; ?> </b> </td>
            </tr>    
        </table>
        
        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; PAI </b> </td>
                <td width="350" style="font-size: 12px"> <b> &nbsp; MÃE </b> </td>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"> <b> &nbsp; <?php echo $report['usu_nomepai']; ?> </b> </td>
                <td width="350"> <b> &nbsp; <?php echo $report['usu_nomemae']; ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; ENDEREÇO: (RUA, AV. PÇA, ETC) </b> </td>
                <td width="100" style="font-size: 12px"> <b> &nbsp; Nº </b> </td>
                <td width="150" style="font-size: 12px"> <b> &nbsp; COMPLEMENTO </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"> <b> &nbsp; <?php echo strtoupper($report['usu_logradouro']); ?> </b> </td>
                <td width="100"> <b> &nbsp; <?php echo strtoupper($report['usu_logranumero']); ?> </b> </td>
                <td width="150"> <b> &nbsp; <?php echo strtoupper($report['usu_logracomplemento']); ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="10">
                <td width="*" style="font-size: 12px"> <b> &nbsp; BAIRRO </b> </td>
                <td width="*" style="font-size: 12px"> <b> &nbsp; CIDADE </b> </td>
                <td width="50" style="font-size: 12px"> <b> &nbsp; UF </b> </td>
                <td width="100" style="font-size: 12px"> <b> &nbsp; CEP </b> </td>
                <td width="150" style="font-size: 12px"> <b> &nbsp; TELEFONE </b> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td width="*"><b> &nbsp; <?php echo strtoupper($report['usu_lograbairro']); ?> </b> </td>
                <td width="*"> <b> &nbsp; <?php echo strtoupper($report['usu_logramunicipio']); ?> </b> </td>
                <td width="50"> <b> &nbsp; <?php echo strtoupper($report['usu_lograuf']); ?> </b> </td>
                <td width="100"> <b> &nbsp; <?php echo $report['usu_cep']; ?> </b> </td>
                <td width="150"> <b> &nbsp; <?php echo $report['usu_telcelular']; ?> </b> </td>
            </tr>    
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td align="center"> <h5> <b> PERÍODO DO CURSO </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <h5> <b> &nbsp; DIAS: &nbsp; <?php echo $report['cur_dias']; ?> </b> </h5> </td>
            </tr>
        </table>

        <table class="tablex table-condensedx" border="1" width="100%" style="margin-top: -1px;  ">
            <tr height="30">
                <td> <h5> <b> &nbsp; HORÁRIO DE INÍCIO: &nbsp; <?php echo $report['cur_hora']; ?> </b> </h5> </td>
                <td> <h5> <b> &nbsp; HORA DO TÉRMINO: &nbsp; <?php echo $report['cur_hrtermino']; ?> </b> </h5> </td>
            </tr>
        </table>

       <br>
       <div style="text-align: justify font-size: 13px" class="bg-gray">
             <b>
             Estou ciente de que o não atendimento aos requisitos de matrícula, ao artigo 329 do CTB, bem como a 
             ausência da documentação exígida para registro do curso e/ou seu preenchimento incorreto, resultará na
             denegação do registro, devendo neste caso, frequentar novamenteas aulas quando atendido todos os
             requisitos e apresentada a documentação pertinente. </b> <br>
        </div>
       <br>

       <span style="font-size: 20px text-left"> 
          <b> LOCAL E DATA DA INSCRIÇÃO: BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
       </span>
       <br><br>

       <table class="tablex table-condensedx" border="0" width="100%" style="margin-top: -1px;  ">
           <tr>
               <td width="350"> &nbsp; <b>__________________________________________</b>
               <td width="350"> &nbsp; <b>__________________________________________</b> <br>
           </tr>

           <tr>
               <td width="350"> &nbsp; <b> <?php echo $report['usu_nomecompleto']; ?> </b> 
               <td width="350"> 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <b> <?php echo $report['cur_contato1']; ?> </b> <br>
           </tr>
       </table>

       <table>           
           <tr>
               <td> 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <b> CRED. Nº 90382 </b> <br>
           </tr>
       </table>
       <br>

       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
       &nbsp; &nbsp;  
       <b>Observaçoes (para uso da Escola Pública de Trânsito)</b>
       <center>
           <table class="tablex table-condensedx" border="1" width="80%" style="margin-top: -1px; ">
                   
                   <tr>
                        <td>&nbsp;</td> 
                   </tr>
                   <tr>
                        <td>&nbsp;</td> 
                   </tr>
                   <tr>
                        <td>&nbsp;</td> 
                   </tr>
                   <tr>
                        <td>&nbsp;</td> 
                   </tr>
           </table>
       </center>

        <?php } //while ?>
    <?php } //if ?> 
</section>
<!--
<tr class="hide">
            <td>
                <table style="width: 100%">
                    <td><img src="/Public/img/RodapeBO.jpg" width="1200"></td>
                </table>
            </td>
        </tr>-->
