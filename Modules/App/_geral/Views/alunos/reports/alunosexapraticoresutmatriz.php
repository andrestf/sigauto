<?php
$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE, serv_descapelido AS PROD_LETRA";
$data = DataDB($_POST['data_fase']);
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND amf_resultado = 'AGENDADO' ";
$report->OrderBy = "replace(serviten_descricao,'Exame Prático - ','') ASC, usu_nomecompleto ASC ";
$report->GroupBy = "usu_id";
//Aqui
$dados   = $report->Gerar();
##if ($dados->num_rows >= 1
$registros = array();
while($report = $dados->fetch_assoc() ) {
    $registros[] = $report;
}
if(!isset($_POST['fase'])) {
    exit("Selecione a Fase");
}
$fases = $_POST['fase'];

if($fases == "" || !is_array($fases)) {
    exit("Selecione a Fase");
}

?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-titlez" style="width: 100%">
                <center>

                    DEPARTAMENTO ESTADUAL DE TRÂNSITO<br>
                    UNIDADE DE TRÂNSITO DE LENÇÓIS PAULISTA
                    <br><br>

                    <u><b> CANDIDATOS A EXAME PRÁTICO </b></u>

                </center>
            </h3><br><b>
                <?php echo strtoupper($_SESSION['APP_LOCALNOME']);?><br>
                DATA: <?php echo $_POST['data_fase']; ?>
            </b>
        </div>
        <div class="box-body">
            <table class='table table-bordered'>
                <thead>
                <tr>
                    <th width="50">Nº.</th>
                    <th width="50">CAT.</th>
                    <th width="*">NOME DO EXAMINADO</th>
                    <th width="120">C P F</th>
                    <th width="80">RESULTADO</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach($fases as $fase) { ?>
                <?php foreach ($registros as $report) {
                    if($report['amf_servitenid'] == $fase) { ?>
                        <tr>
                            <td><?php echo $i; ?> <br/></td>
                            <td><?php echo  /*$report['serv_descapelido'];//*/str_replace("Exame Prático - ","",$report['serviten_descricao'])?> <br/></td>
                            <td><?php echo $report['usu_nomecompleto'] ?> <br/></td>
                            <td><?php echo $report['usu_cpf'] ?> <br/></td>
                            <td><br/></td>
                        </tr>
                        <?php $i++;}//if ?>

                <?php }//foreach $reports ?>
                <tr><td  colspan="5">&nbsp;</td><tr>
                    <?php }//foreach fases ?>

                </tbody>
            </table>
            <br/>
            <b>CARIMBO : </b>
        </div>
    </div>
</section>

