
<?php

$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE, usu_cpf, usu_renach";
$data = DataDB($_POST['data_fase']);
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' ";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-titlez" style="width: 100%">
                <center>
                    <u><b><?php echo strtoupper($_SESSION['APP_LOCALNOME']);?> <br/></u></b>
                    
                    <br/>
                    <small>
                        Relação de alunos que prestarão exame <b> TEÓRICO </b> para motoristas e motociclistas dia <b> <?php echo $_POST['data_fase']; ?> </b>
                    </small>
                </center>
            </h3>
        </div>
        <div class="box-body">
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th with="50">Nº.</th>                        
                        <th width="50">CAT.</th>
                        <th with="*">NOME DO EXAMINADO</th>
                        <th width="70">INST.</th>
                        <th width="110">CPF</th>
                        <th width="90">RENACH</th>
                        <th width="80">RESULTADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($dados->num_rows >= 1) {
                        $i = 1;
                        ?>
                        <?php while ($report = $dados->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $i; ?> <br/></td>
                                <td><?php echo str_replace("Primeira Hab.","",$report['serv_descapelido'])?> <br/></td>
                                <td><?php echo $report['usu_nomecompleto']; ?> <br/></td>
                                <td></td>
                                <td><?php echo $report['usu_cpf']; ?> <br/></td>
                                <td><?php echo $report['usu_renach']; ?> <br></td>
                                <td></td>
                            </tr>
                        <?php $i++;}//while ?>
                    <?php } else {//if ?>
                        <tr>
                            <td  colspan="7" style="font-size: 28px">
                    <center>
                        Sem resultados
                    </center>
                    </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
        <br/>
            <small>
                <b>BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
            </small>
        <div class="row">
            <div class="col-xs-3">Aprovados</div>
            <div class="col-xs-3">Reprovados</div>
            <div class="col-xs-3">Faltoso</div>
            <div class="col-xs-3">Cancelado</div>
        </div>
        
        </div>
    </div>
</section>