<div class="content">
    <h3 class="page-title">Colaboradores - <?php echo $TipoFuncionario?></h3>
    <br/>
    
    <div class="form">
        <form id="frm_alunos_busca">
            <div class="row">
                <div class="col-sm-4">
                    <label>Nome</label>
                    <input type="text" name="nome" class="form-control" />
                </div>

                <div class="col-sm-4">
                    <label>CPF</label>
                    <input type="text" name="cpf" class="form-control"/>
                </div>

                <div class="col-sm-2">
                    <label>Situação</label>
                    <select class="form-control" name="situacao" >
                        <option value="">Todos</option>
                        <option value="1">Ativos</option>
                        <option value="2">Inativos</option>
                    </select>
                </div>
                
                <div class="col-sm-2">
                    <label>LImite</label>
                    <select class="form-control" name="limite" >
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                </div>                
                
                <div class="col-sm-12">
                    <span class="pull-right">
                        <label>&nbsp;</label><br/>

                        <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                        <a href="<?php echo $this->Link('colaboradores',"cadastrar","tipo=".$TipoFuncionario);?>" ><span class="btn btn-default btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
                    </span>
                </div>                
            </div>
        </form>
        
        <hr/>
        <div id="lista_colaboradores">
        </div>
    </div>
    
</div>
<script>
<?php if($TipoFuncionario != "") { ?>
    function Lista() {
        $("#lista_colaboradores").html( "<center><img src='/Public/img/ld01.gif' /></center>" );
        $.ajax({
            url : "/index.php?route=colaboradores/listagem/&tipo=<?php echo $TipoFuncionario?>",
            success : function(e) {
                $("#lista_colaboradores").html(e);
            }
        });
    };
    Lista();
    
    
    
    function EditarColaborador(id) {
        window.location = "<?php echo Permalink('colaboradores/cadastrar/&tipo='.$_GET['tipo'].'&codigo=')?>"+id;
    }
<?php } ?>
</script>