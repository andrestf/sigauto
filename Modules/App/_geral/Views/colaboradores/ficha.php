<form class="" method="post" name="frm_colaborador" id="frm_colaborador" action="" onsubmit="salvar(); return false;">
    <?php if(isset($colaborador->usu_id) && $colaborador->usu_id != '' && $colaborador->usu_id != "0") { ?>
        <input type="hidden" name="id" id="id" value="<?php echo $colaborador->usu_id; ?>" />
    <?php } else { ?>
    <input type="hidden" name="id" id="id" value="0" />
    <?php } ?>
    <div class="content asec-box" id="ficha_novo_colaborador">
        <h3 class="page-title">Cadastrar Novo Colaborador <small> - <?php echo $TipoFuncionario; ?></small> </h3>
        <section id="dados_pessoais">
            <h4 class="ficha_title">Dados Pessoais</h4>
            <div class="form">
                <div class="form-group">
                   <div class="col-md-2">
                        <label>Foto</label>
                        <div class="div-foto">
                            <img src="/alunos/GetFoto/?codigo=<?php echo $colaborador->usu_id?>" class="foto-aluno"  />
                            <div class="foto-edit">
                                <i class="fa fa-pencil cursor" onclick="FotoUpload();"></i>
                                &nbsp;&nbsp;&nbsp;
                                <i class="fa fa-trash cursor" onclick="FotoRemove();"></i>
                            </div>
                        </div>
                    </div>                
                    <div class="col-sm-7 bottom10">
                        <label>Nome Completo <span class="obg">*</span> </label>
                        <input type="text" name="nomecompleto" id="nomecompleto" class="form-control" value="<?php echo @$colaborador->usu_nomecompleto; ?>" />
                    </div>
                    <div class="col-sm-3 bottom10">
                        <label>Apelido <span class="obg">*</span> </label>
                        <input type="text" name="apelido" id="apelido" class="form-control" maxlength="23" value="<?php echo @$colaborador->usu_apelido; ?>" />
                    </div>

                    <div class="col-sm-5 bottom10">
                        <label>Nome da Mãe <span class="obg">*</span> </label>
                        <input type="text" name="nomemae" id="nomemae" class="form-control"  value="<?php echo @$colaborador->usu_nomemae; ?>" />
                    </div>

                    <div class="col-sm-5 bottom10">
                        <label>Nome do Pai <span class="obg">*</span> </label>
                        <input type="text" name="nomepai" id="nomepai" class="form-control" value="<?php echo @$colaborador->usu_nomepai; ?>"  />
                    </div>

                    <div class="form-group">
                        <div class="col-sm-5 bottom10">							
                            <label for="nascimento">Data nascimento <span class="obg">*</span></label>

                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar"></span></span>
                                <input id="nascimento" name="nascimento" maxlength="10" placeholder="dd/mm/aaaa" type="text" class="form-control datepicker" value="<?php echo DataBR(@$colaborador->usu_nascimento); ?>" requiredx="">
                            </div>
                        </div>

                        <div class="col-sm-5 bottom10">
                            <input type="hidden"  name="sexo"/>
                            <label for="sexo">Sexo <span class="obg">*</span></label><br>
                            <input id="sf" type="radio"  name="sexo" value="F" <?php echo (@$colaborador->usu_sexo == "F") ? "checked='checked'" : '';  ?> > Feminino &nbsp;&nbsp;
                            <input id="sm" type="radio" name="sexo" value="M"  <?php echo (@$colaborador->usu_sexo == "M") ? "checked='checked'" : '';  ?>> Masculino
                        </div>
                    </div>                
                </div>
            </div>
        </section>
        
        <section id="documentos">
            <h4 class="ficha_title">Documentos</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="cpf">CPF <span class="obg">*</span></label>
                        <input id="cpf" name="cpf" placeholder="000.000.000-00" onkeyup="formataCPF(this)" maxlength="14" class="form-control" type="text" value="<?php echo @$colaborador->usu_cpf; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rg">RG <span class="obg">*</span></label>
                        <input id="rg" name="rg" placeholder="00.000.000-0" maxlength="20" class="form-control" type="text" value="<?php echo @$colaborador->usu_rg; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rgemissor">Orgão Emissor <span class="obg">*</span></label>
                        <input id="rgemissor" name="rgemissor" placeholder="Ex.: SSP" maxlength="25" class="form-control" type="text" value="<?php echo @$colaborador->usu_rgemissor; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rguf">UF <span class="obg">*</span></label>
                        <select id="rguf" name="rguf" class="form-control select2 " requiredx="">
                            <option value=""> Selecione </option>
                                <?php while($uf2 = $ListaUF2->fetch_object()) { ?>
                                    <option value="<?php echo $uf2->uf; ?>"  <?php if($uf2->uf == @$colaborador->usu_rguf) { echo 'selected="selected"'; }?> > <?php echo $uf2->uf; ?> </option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
        
        <section id="endereco">
            <h4 class="ficha_title">Endereço</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="cep">CEP <span class="obg">*</span></label>
                        <input id="cep" name="cep" placeholder="CEP" maxlength="9" class="form-control cep" type="text" value="<?php echo @$colaborador->usu_cep; ?>" requiredx="" onblur="buscacep();">
                    </div>
                    <div class="col-sm-6 bottom10">
                        <label for="logradouro">Logradouro <span class="obg">*</span></label>
                        <input id="logradouro" name="logradouro" onkeyup="filtraCampo(this)" placeholder="Logradouro" maxlength="40" class="form-control" type="text" value="<?php echo @$colaborador->usu_logradouro; ?>" requiredx="">
                    </div>                
                    <div class="col-sm-3 bottom10">
                        <label for="logranumero">Numero <span class="obg">*</span></label>
                        <input id="logranumero" name="logranumero" onkeyup="formataNumerico(this)" placeholder="Número" maxlength="9" class="form-control" type="text" value="<?php echo @$colaborador->usu_logranumero; ?>" requiredx="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="lograbairro">Bairro <span class="obg">*</span></label>
                        <input id="lograbairro" name="lograbairro" onkeyup="filtraCampo(this)" placeholder="Bairro" maxlength="40" class="form-control" type="text" value="<?php echo @$colaborador->usu_lograbairro; ?>" requiredx="">
                    </div>
                    <div class="col-sm-6 bottom10">
                        <label for="logramunicipio">Município <span class="obg">*</span></label>
                        <input id="logramunicipio" name="logramunicipio" onkeyup="filtraCampo(this)" placeholder="Município" maxlength="40" class="form-control" type="text" value="<?php echo @$colaborador->usu_logramunicipio; ?>" requiredx="">
                    </div>                
                    <div class="col-sm-3 bottom10">
                        <label for="lograuf">UF <span class="obg">*</span></label>
                        <input id="lograuf" name="lograuf" placeholder="UF" maxlength="2" class="form-control" type="text" value="<?php echo @$colaborador->usu_lograuf; ?>" requiredx="">
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </section>
        
        
        <section id="contatos" style="display: block;">
            <h4><b>Telefones</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label>Celular</label>
                        <input type="text" class="form-control telefone" name="telcelular" value="<?php echo @$colaborador->usu_telcelular?>" />
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Residencial</label>
                        <input type="text" class="form-control telefone" name="telresid" value="<?php echo @$colaborador->usu_telresid?>" />
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Comercial</label>
                        <input type="text" class="form-control telefone" name="telcomercial" value="<?php echo @$colaborador->usu_telcomercial?>" />
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Contato</label>
                        <input type="text" class="form-control telefone" name="telrecado" value="<?php echo @$colaborador->usu_telrecado?>" />
                    </div>
                </div>
                
                <div class="clearfix"></div>
             </div>
            
        </section>
        
        <section id="acesso">
            <h4><b>Acesso</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>E-mail <span class="obg">*</span></label>
                        <input type="email" class="form-control" name="login" id="login" value="<?php echo @$colaborador->usu_email?>" />
                    </div>

                    <div class="col-sm-4">
                        <label>Senha <span class="obg">*</span></label>
                        <input type="password" class="form-control" name="senha" />
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Situação <span class="obg">*</span></label>
                        <select class="form-control" name="sitacesso">
                            <option value="A" <?php echo (@$colaborador->usu_sitacesso == "A") ? "selected='selected'" : '';  ?> >Ativo</option>
                            <option value="I" <?php echo (@$colaborador->usu_sitacesso == "I") ? "selected='selected'" : '';  ?> >Inativo</option>
                        </select>
                    </div>
                    
                    <div class="col-sm-4">
                        
                    </div>

                </div>
                
                <div class="clearfix"></div>
             </div>            
            
        </section>
        <hr/>
        <div class="pull-right grupo-btn">
            <button type="reset" onclick="cancelarC();" class="btn btn-default"> <i class="fa fa-arrow-left"></i>  Cancelar </button> &nbsp;
            
            
            <span class="grupo-btn-01">
                <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Salvar</button>
            </span>
        
            <span class="grupo-btn-02" style="display: none">
                <span class="btn btn-primary" id="btnEditar"> <i class="fa fa-save"></i> Salvar</span>
            </span>
        </div>        
    </div><!--content -->
</form>

<script>
    var codigo = "";
    
    <?php if(isset($colaborador->usu_id) && $colaborador->usu_id != '' && $colaborador->usu_id != "0") { ?>
        $(".grupo-btn-01").hide();
        $(".grupo-btn-02").show();
    <?php } ?>
 
    
    $(function(){
        alertify.set('notifier','position', 'top-right');
    });
    
   function cancelarC() {
       window.location = "<?php echo Permalink('colaboradores/lista/&tipo='.strtolower($_GET['tipo']));?>";
   }
    
    
   $("#btnEditar").click(function(){
       Editar();
   })
   function salvar() {
       datas = $("#frm_colaborador").serializeArray();
       $.ajax({
           type: 'post',
           dataType : 'json',
           data : datas,
           url : '/index.php?route=colaboradores/inserir/&tipo=<?php echo strtolower($_GET['tipo']);?>',
           success: function(e) {
               if(e.erro != "") {
                   alertify.error(e.mensagem);
                   return false;
               }
               
               alertify.success("Colaborador cadastrado!");
               $("#id").val(e.id);
               $(".grupo-btn-01").hide();
               $(".grupo-btn-02").show();
               return false;
               
           },
           error: function(e,x,s){
               console.log(e+"\n"+x+"\n"+s)
           }
       });
   }
   
   function Editar() {
       datas = $("#frm_colaborador").serializeArray();
       $.ajax({
           type: 'post',
           dataType : 'json',
           data : datas,
           url : '/index.php?route=colaboradores/editar/&tipo=<?php echo strtolower($_GET['tipo']);?>',
           success: function(e) {
               if(e.erro != "") {
                   alertify.error(e.mensagem);
                   return false;
               }
               
               alertify.success("Colaborador editado com sucesso!");
               return false;
               
           },
           error: function(e,x,s){
               console.log(e+"\n"+x+"\n"+s)
           }
       });    
   }
 
    
</script>


