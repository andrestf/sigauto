<section class="content asec-box" id="aluno">
    <h3 class="page-title">
        Análise de Débitos > Filtro
    </h3>
    <br>

    <script type="text/javascript">
    	


    </script>

	<div class="box" style="width: 500px; margin: 0 auto;">
		<div class="box-header">
		    <form id="formDeb" class="formx" method="post" action="<?php echo permalink('Debitos/Analise/')?>">
		    	<div class="row">
		    		<div class="col-sm-6">
			    		<label>Matricula </label>
			    		<input type="number" name="matricula" onkeyup="MatKeyPress(this)" class="form-control formInput formInput0 mat frm01" />
		    		</div>
		    		<div class="col-sm-6">
			    		<label>CPF</label>
			    		<input type="number" name="cpf" class="form-control formInput formInput1 cpf frm02" onkeyup="CpfKeyPress(this)" />
		    		</div>
		    	</div>
		    	
		    	<center>
		    	<label class="">OU</label>
		    	</center>
		    	<hr>

		    	<div class="row">
		    		<div class="col-sm-6">
			    		<label>Matricula Inicial</label>
			    		<input type="number" name="matIni" class="form-control formInput formInput2" />
		    		</div>
		    		<div class="col-sm-6">
			    		<label>Matricula Final</label>
			    		<input type="number" name="matFim" class="form-control formInput formInput2" />
		    		</div>
		    	</div>

		    	<div class="row">
		    		<div class="col-sm-6">
			    		<label>Vencimento Inicial</label>
			    		<input type="text" name="matIni" class="form-control formInput formInput2 datepicker " autocomplete="off" />
		    		</div>
		    		<div class="col-sm-6">
			    		<label>Vencimento Final</label>
			    		<input type="text" name="matFim" class="form-control formInput formInput2 datepicker" autocomplete="off" />
		    		</div>
		    	</div>


		    	<div class="row">
		    		<div class="col-sm-12">
			    		<label>Fase</label>
			    		<select class="form-control formInput formInput2 select2" name="fase[]" multiple="">
				    		<?php foreach ($fases as $fase) { ?>
				    			<option value="<?php echo $fase['serviten_id']; ?>"> <?php echo $fase['serviten_descricao']; ?> </option>
				    		<?php } ?>
			    		</select>		    		
		    		</div>
		    	</div>

		    	<div class="row">
		    		<div class="col-sm-6">
			    		<label>Situação</label>
			    		<input type="text" name="situacao" class="form-control formInput formInput2">
		    		</div>
		    		<div class="col-sm-6">
			    		<label>é</label>
			    		<select class="form-control formInput formInput2 select2" name="situacaocond">
				    		<option value="igual"> Igual </option>
				    		<option value="diferente"> Diferente </option>
				    		<option value="contem"> Contém </option>
			    		</select>		    		
		    		</div>
		    	</div>


		    	<div class="row">
		    		<div class="col-sm-6">
			    		<label>Núm. Débitos</label>
			    		<input type="number" name="numdebitos" class="form-control formInput formInput2">
		    		</div>
		    		<div class="col-sm-6">
			    		<br/>
			    		<select class="form-control formInput formInput2 select2" name="numdebitoscond">
				    		<option value="mais"> + </option>
				    		<option value="menos"> - </option>
				    		<option value="igual"> = </option>
			    		</select>		    		
		    		</div>
		    	</div>

		    	<div class="row">
		    		<div class="col-sm-12">
                        <label>Documento</label>
						<select class="form-control select2 formInput formInput2" name="tpDocs[]" multiple="">
                            <option value=""></option>
                            <?php foreach ($TpDocs as $doc) { ?>
                            	<option value="<?php echo $doc['tpd_cd']?>"> <?php echo $doc['tpd_descricao']?> </option>
                            <?php } ?>
                        </select>		    			
		    		</div>
		    	</div>

				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-primary pull-right">Analisar</button>
					</div>
				</div>


		    </form>
		</div>
	</div>

</section>

<script type="text/javascript">

    $(function() {
    	$(".hasDatepicker").datepicker();
    })


    	function MatKeyPress(e) {   		
    		val = $(".mat").val();
    		if( val != '') {
    			$(".formInput2").attr("disabled","disabled");
    			$(".formInput2").attr("readonly","readonly");
    			$(".cpf").attr("disabled","disabled");
    			$(".cpf").attr("readonly","readonly");

    		} else {
    			$(".formInput").removeAttr("disabled");
    			$(".formInput").removeAttr("readonly");
    			$(".cpf").removeAttr("disabled");
    			$(".cpf").removeAttr("readonly");
    		}
    	}

    	function CpfKeyPress(e) {   		
    		val = $(".cpf").val();
    		if( val != '') {
    			$(".formInput2").attr("disabled","disabled");
    			$(".formInput2").attr("readonly","readonly");
    			$(".mat").attr("disabled","disabled");
    			$(".mat").attr("readonly","readonly");    			


    		} else {
    			$(".formInput").removeAttr("disabled");
    			$(".formInput").removeAttr("readonly");
				$(".mat").removeAttr("disabled");
				$(".mat").removeAttr("readonly");
    		}
    	}  

</script>

<style type="text/css">
	#formDeb .row {
		margin-bottom: 15px;
	}
</style>