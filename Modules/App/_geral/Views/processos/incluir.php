<form id="form_processo_ini" onsubmit="SalvaMatricula(); return false; ">
    <input type="hidden" name="aluno" value="<?php echo $view_Aluno->usu_id;?>" />
    <input type="hidden" name="key" value="<?php echo $_SESSION['APP_KEYTEMP'];?>" />
<div class="content">
    <h3 class="page-title">Iniciar Processo - <?php echo $view_Aluno->usu_nomecompleto?></h3>


    <section id="section_processos">
        <h4 class="ficha_title">Tipo de Processo</h4>
        <div class="form">
            <div class="col-md-4">
                <label>Processo<span class="obg">*</span></label>
                <select name="processo" class="form-control" id="processos" tabindex="10">
                    <option value="">Selecione</option>
                    <?php while($mat = $matriculas->fetch_object()) {?>
                    <option data-itens="<?php echo $mat->tpmat_categoriascnh?>" value="<?php echo $mat->tpmat_id?>"> <?php echo $mat->tpmat_descricao; ?> </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-4">
                <label>Categorias <span class="obg">*</span></label>
                <select name="categorias" id="categorias" class="form-control select2" multiple="multiple" tabindex="20" >
                </select>
            </div>
        </div>
    </section>
    
    <section id="section_servicos" style="display: none;">
        <h4 class="ficha_title">Serviço a Contratar</h4>
        <div class="form">
            <div class="col-md-4">
                <label>Serviço <span class="obg">*</span></label>
                <select name="servicos" id="servicos" class="form-control select2" tabindex="30" >
                    <option value="0">Selecione</option>
                    <?php while($serv = $servicos->fetch_object() ) { ?>
                        <option data-itens="<?php echo $serv->serv_itens; ?>" value="<?php echo $serv->serv_id?>"> <?php echo $serv->serv_descricao; ?> </option>
                    <?php }?>
                </select>
            </div>

            <div class="col-md-4">
                <label>Taxa de Registro <span class="obg">*</span></label>
                <select name="taxa" id="taxa" class="form-control select2" tabindex="30" >                 
                    <option value="1">Incluir</option>
                    <option value="0" selected="selected">Não incluir</option>
                </select>
            </div>            
        </div>
    </section>    

    <section id="section_itens" style="display: none;">
        <h4 class="ficha_title">Serviços</h4>
        <div class="form">
            <div class="col-md-12">
                <input type="hidden" id="servicoitens" name="servicoitens" value="">
                <div id="servicos_lista"></div>
            </div>
            
            <div class="col-md-12">
            <span class="btn btn-success pull-right" onClick="AbreFinanceiro()">Continuar</span>
            </div>
        </div>
    </section>    
    
    
    <section id="section_financeiro" style="display: none;">
        <h4 class="ficha_title">Financeiro</h4>

        <div class="form">
            <div class="col-md-12">
                <span class="pull-right">
                    <span class="btn btn-warning" onclick="CancelaFinanceiro()"><i class="fa fa-arrow-left"></i> Voltar</span>&nbsp;
                    <span class="btn btn-success pull-right" onclick="SalvaMatricula()"><i class="fa fa-floppy-o"></i> Concluir</span>
                </span>
            </div>
            <div class="clearfix"></div>
            <hr/>
            
            <div class="row" style="padding: 10px 15px;">
                <div class="col-md-4">
                    <label>Valor</label>
                    <input type="text" name="valor_servicos" id="valor_servicos" class="form-control" onkeyup="formataValorNovo(this)" value="1.300,00"/>
                </div>
                
                <div class="col-md-4">
                    <label>Desconto em R$</label>
                    <input type="text" name="desconto_servicos" id="desconto_servicos" class="form-control" onkeyup="formataValorNovo(this)" value="0"/>
                </div>

                <div class="col-md-4">
                    <label>Forma de Pagamento<span class="obg">*</span></label>
                    <select name="forma_paga" id="forma_paga" class="form-control">
                        <option value=""></option>
                        <option value="DINHE">Dinheiro</option>
                        <option value="CHEQU">Cheque</option>
                        <option value="CHEPR">Cheque Pré</option>
                        <option value="CDEBI">Cartão Débito</option>
                        <option value="CCRED">Cartão Crédito</option>
                        <option value="BOLET">Boleto</option>
                        <option value="DUPLI">Nota Promissória</option>
                    </select>
                </div>
            </div>
            <div class="row finan finan01" style="padding: 10px 15px; display: none;">
                <div class="col-md-4 parcelas_select">
                    <label>Parcelas</label>
                    
                    <select name="parcelas" id="parcelas" class="form-control">
                        <option value="0"> Selecione </option>
 
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Vencimento</label>
                    <input type="text" name="primeiro_vencimento" id="primeiro_vencimento" class="form-control datepicker" value="<?php echo date("d/m/Y"); ?>"/>
                </div>
            </div>
        </div>
    </section>
    <br/>
    <section id="section_aguarde" style="display: none;">
        <div class="form" style="padding: 10px 15px;">
            <center> 
            
                Aguarde enquanto o processo é salvo!!! <br/>
                <br />
                <b class="text-red">Não atualize ou feche a página.</b>
                <br/>
            
            
                <img src="/Public/img/ld01.gif" />
            </center>
            
        </div>
    </section>
    
</div>

</form>
<script>
    
    var forma_paga;
    var valor_servico;
    var valor_desconto;
    var valor_final;
    var valor_parcela_i;
    var proc_id;
    var proc_itens;    
    var serv_id;
    var serv_itens;
    var n_parcelas;

    function CancelaFinanceiro() {
        $("#section_processos").fadeIn();
        $("#section_servicos").fadeIn();
        $("#section_itens").fadeIn();        
        $("#section_financeiro").fadeOut();
        $("#section_financeiro .finan").hide();
        
        $(".finan01").fadeOut();
        
    }
    
    function AbreFinanceiro() {

        

        


        if($("#categorias").val() == null)  {
            alertify.error("Informe a categoria do processo");
            return false;
        }
        $("#section_processos").fadeOut();
        $("#section_servicos").fadeOut();
        $("#section_itens").fadeOut();
        
        $("#section_financeiro").fadeIn();        
        valor_servico = $("#valor_servicos").val(  $("#x_valor_servicos").html() );
        $("#valor_servicos").keyup();

        SalvaMatricula();
    }
    
    $("#valor_servicos").keyup(function() {
        $("#forma_paga").val("0");
        $("#parcelas").val("0");
    });
    
    
    $("#desconto_servicos").keyup(function() {
        $("#forma_paga").val("0");
        $("#parcelas").val("0");
    });

    $("#forma_paga").change(function() {
        $(".finan01").fadeIn();
        
        forma_paga = $("#forma_paga").val();
        
            valor_servico = $("#valor_servicos").val();
            valor_servico = valor_servico.replace(".","");
            valor_servico = valor_servico.replace(",",".");
            valor_servico = parseFloat(valor_servico);
            
            
            
            valor_desconto= $("#desconto_servicos").val();
            if(valor_desconto == ''){
                valor_desconto = "0,00";
            }
                
             
            
            valor_desconto = valor_desconto.replace(".","");
            valor_desconto = valor_desconto.replace(",",".");
            valor_desconto = parseFloat(valor_desconto);
            
            
            
            
            
            
            valor_final = valor_servico - valor_desconto;        
            

        
        
        if( forma_paga == "DINHE" ||
            forma_paga == "CDEBI" ||
            forma_paga == "CHEQU"  ) {
            
            valor_parcela_i = valor_final.toFixed(2);
            $("#parcelas").html('<option value="0">Selecione</option><option value="1">1 x R$ '+valor_final.toFixed(2)+' </option>');
            
        } else {
            
            options = "<option value='0'>Selecione</option>";
            for(i=1;i<=12;i++) {
                valor_parcela_i = valor_final/i;
                jur = 0.00;
                
                if(i >= 4) {
                    jur = 0.00
                }
                if(i >= 10) {
                    jur = 0.00
                }

                valor_parcela_i = (valor_final/i)*jur+(valor_final/i);
                
                valor_parcela_i = valor_parcela_i.toFixed(2);
                options = options + "<option value='"+i+"'> "+ i +" x R$ " + valor_parcela_i +" </option>";
            }
            
            $("#parcelas").html(options);
        }
            
        
    });
    

    
    $("#processos").change(function(){
        $("#categorias").html('<option>CARREGANDO VALORES</option>');
        proc_itens = $("#processos option:selected" ).attr("data-itens");
        proc_id = $("#processos option:selected").val();
        $("#servicos").select2('val', 1);
        $("#section_servicos").fadeOut();
        $.ajax({
            url : "/index.php?route=processos/CategoriasMatricula/&ids="+proc_itens+"&procid="+proc_id,
            success: function(e) {
                $("#categorias").html('');
                $("#categorias").html(e);
                
                if(proc_id > 0) {
                    $("#section_servicos").fadeIn();
                } else {
                    $("#section_servicos").fadeOut();
                }
                
            },
            error : function(){
                alertify.error("Erro ao recuperar itens do processo!");
            }
        });
        
    });
    
    
    
    function SalvaMatricula() {
//        console.log("forma_paga " + forma_paga);
//        console.log("valor_servico " + valor_servico);
//        console.log("valor_desconto " + valor_desconto);
//        console.log("valor_final " + valor_final);
//        console.log("valor_parcela_i " + valor_parcela_i);
//        console.log("proc_id " + proc_id);
//        console.log("proc_itens " + proc_itens);
//        console.log("serv_id " + serv_id);
//        console.log("serv_itens " + serv_itens);
/*
        n_pacelas = $("#parcelas").val();
        vencimento = $("#primeiro_vencimento").val();
        
        
        if(forma_paga === "0" || forma_paga === "" || forma_paga === undefined) {
            alertify.error("Informe a forma de pagamento!");
            return false;
        }

        if(n_pacelas === "0" || n_parcelas === "" || n_pacelas === undefined) {
            alertify.error("Informe a quantidade de parcelas!");
            return false;
        }
        
        
        if(vencimento === "0" || vencimento === "" || vencimento === undefined || vencimento == '__/__/____') {
            alertify.error("Informe a data de primeiro vencimento");
            return false;
        }
                

        
        
        if(valor_final === "0" || valor_final === "" || valor_final === undefined) {
            alertify.error("Valor não é válido!");
            return false;
        }
        
        if(proc_id === "0" || proc_id === "" || proc_id === undefined) {
            alertify.error("Tipo de Processo inválido!");
            return false;
        }
        
        
        if( $("#servicoitens").val() === "")  {
            alertify.error("Tipo de Serviço inválido!");
            return false;
        }
        
        //$("#section_financeiro").fadeOut();
        var datas = $("#form_processo_ini").serializeArray();
*/        
        var categorias = $("#categorias").val();
        var taxa       = $("#taxa option:selected").val();
        var key =  "<?php echo $_SESSION['APP_KEYTEMP'];?>";
        $("#section_aguarde").fadeIn();
        $.ajax({
            type: 'POST',
            data: {servicos : serv_id , servicoitens: serv_itens, categorias : categorias,taxa:taxa, processo : proc_id, aluno : "<?php echo $_GET['aluno']?>", key : key},
            dataType : 'json',
            url : "/index.php?route=processos/IniciarProcesso/&codigo_a=<?php echo $_GET['aluno'];?>",
            success: function(e) {
                if(e.erro != '') {
                    //CancelaFinanceiro();
                    //$("#section_aguarde").fadeOut();
                    alertify.error(e.mensagem);
                    return false;
                }
                $("#section_aguarde").html(e.mensagem);
                alertify.success("Matricula registrada com sucesso!!");
                window.location = "<?php echo $this->Link("alunos","ListaMatriculas","aluno=") ?><?php echo $_GET['aluno'];?>&processo="+e.id;
            } 
            
        });
        
        
        
    }
    
    

    $("#servicos").change( function() {
        serv_id = $("#servicos option:selected").val();
        serv_itens   = $("#servicos option:selected" ).attr("data-itens");
        
        if(serv_id > 0) {
        
            $("#servicos_lista").html('');
            $("#servicoitens").val('');
            $.ajax({
                url : "/index.php?route=processos/SelecionaServicos/&servid="+serv_id+"&ids="+serv_itens,
                success : function(e) {
                    $("#servicos_lista").html(e);
                    $("#servicoitens").val(serv_itens);
                    $("#section_itens").fadeIn();
                }
            });
        } else {
            $("#section_itens").fadeOut();
            $("#servicoitens").val('');
            return false;
        }
        
    });
</script>