  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" id="asec-menu-primary">
        <li class="header"></li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="<?php echo $this->Link("dashboard")?>" ><i class="fa fa-home"></i> <span>Gráficos</span></a></li>
        

        <li class='treeview hide'><a href="/principal/" ><i class="fa fa-calendar"></i> <span>Agenda</span></a></li>

        <?php if ($this->ValidaNivel2(40)) { ?>
        <li class='treeview'>
          <a href="#"><i class="fa fa-money"></i> <span>Financeiro</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>          
          <ul class='treeview-menu'>
            <li class='hide'><a href="<?php echo $this->Link("financeiro/caixa")?>"><i class="fa fa-arrow-right"></i> <span>Caixa</span></a></li>
            <li><a href="<?php echo $this->Link("financeiro")?>"><i class="fa fa-arrow-right"></i> <span>Financeiro</span></a></li>
            <?php if($this->ValidaNivel2(40)) { ?>
            <li class='menu-sep'></li>
            <li class=''><a href="<?php echo $this->Link("financeiro/TipoDocumentos")?>"><i class="fa fa-arrow-right"></i> <span>Tipos de Documentos</span></a></li>
            <li class='hide'><a href="<?php echo $this->Link("financeiro/FomasPagamentos")?>"><i class="fa fa-arrow-right"></i> <span>Formas de Pagamento</span></a></li>
            <li class='menu-sep'></li>
            <li class=''><a href="<?php echo $this->Link("financeiro/Reports")?>"><i class="fa fa-arrow-right"></i> <span>Relatórios</span></a></li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <li class='hide'><a href="<?php echo $this->Link("tipoDocumentos")?>"><i class="fa fa-car"></i> <span>Tipo de Documentos</span></a></li>

        <?php if($_SESSION['APP_LOCALID'] != "13") { ?>
        <li class=''><a href="<?php echo $this->Link("veiculos")?>"><i class="fa fa-car"></i> <span>Veículos</span></a></li>
        <?php } ?>

        <li class=''><a href="<?php echo $this->Link("agenda/agenda/?cod=")?>"><i class="fa fa-calendar"></i> <span>Agenda</span></a></li>

        <li class='hide'><a href="<?php echo $this->Link("produtos")?>"><i class="fa fa-shopping-cart"></i> <span>Produtos / Serviços</span></a></li>
        <li class='hide'><a href="<?php echo $this->Link("exames")?>"><i class="fa fa-medkit"></i> <span>Exames</span></a></li>
        
        <li><a href="<?php echo $this->Link("alunos")?>" ><i class="fa fa-graduation-cap"></i> <span>Alunos</span></a></li>


        <?php if($_SESSION['APP_LOCALID'] == "1") { ?>
            <?php ## REPORTS AUTO ESCOLA MATRIZ LENCOIS PAULISTA?>
            <li class=''><a href="<?php echo $this->Link("financeiro/Cursos")?>"><i class="fa fa-institution"></i> <span>Cursos</span></a></li>
        <?php } ?>

        
        <li class="treeview hide">
          <a href="#"><i class="fa fa-user"></i> <span>Alunos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $this->Link("alunos")?>" ><i class="fa fa-list"></i> <span>Alunos</span></a></li>
            <li><a href="<?php echo $this->Link("alunos","listar")?>" ><i class="fa fa-list"></i> <span>Listar</span></a></li>

          </ul>
        </li>

        

        <li class="treeview">
          <a href="#"><i class="fa fa-group"></i> <span>Colaboradores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=atendentes")?>" ><i class="fa fa-group"></i> <span>Atendentes</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=instrutores")?>"><i class="fa fa-graduation-cap"></i> <span>Instrutores</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=gerentes")?>"><i class="fa fa-user-secret"></i> <span>Gerentes</span></a></li>
          </ul>
        </li>


        <li class="treeview hide">
          <a href="#"><i class="fa fa-group"></i> <span>Sistema</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=atendentes")?>" ><i class="fa fa-group"></i> <span>Configurações</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=instrutores")?>"><i class="fa fa-graduation-cap"></i> <span>Contratos</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=instrutores")?>"><i class="fa fa-graduation-cap"></i> <span>Financeiro</span></a></li>

          </ul>
        </li>        

        
        <li><a href="<?php echo $this->Link("login","end")?>" data-async="false"><i class="fa fa-sign-out"></i> <span>Sair</span></a></li>
      </ul>
      
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="clearfix"></div>
  <div class="content-wrapper" id="asec-content" ><!-- Content Wrapper. Contains page content -->