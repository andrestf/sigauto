    <div class="clearfix"></div> </br>
</div><!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      &nbsp;
    </div>
    <!-- Default to the left -->
    <strong><?php echo @$_SESSION['APP_LOCALNOME']; ?>.
  </footer>

</div><!-- ./wrapper -->


<script src="/Public/plugins/slimScroll/jquery.slimscroll.js"></script>
<script src="/Public/plugins/select2/select2.min.js"></script>
<script src="/Public/plugins/select2/select2-tab-fix.min.js"></script>
<script src="/Public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/Public/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/Public/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js"></script>
<script src="/Public/plugins/input-mask/jquery.inputmask.js"></script>

<script src="/Public/js/maskedinput.js"></script>
<script src="/Public/plugins/alertify/alertify.min.js"></script>

<!-- AQUI 1 -->
<script src="/Public/plugins/jQueryUI/jquery-ui.min.js"></script>

<script src="/Public/js/print.js"></script>
<script src="/Public/js/asec.js"></script>
<script src="/Public/js/asec-2.js"></script>
<script src="/Public/js/app.min.js"></script>
<script src="/Public/js/moment-with-locales.js"></script>
<div style="display: none;" id="ld">
	<center>
		<br/>
                <img src="/Public/img/ld03.gif" />
	</center>
</div>


<div class="modal fadex" tabindex="-1" role="dialog" id="EmptyModal" data-backdrop="static" data-keyboard="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body"></div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fadex" tabindex="-1" role="dialog" id="ModalAlert" data-backdrop="static" data-keyboard="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title titleText"></h4>
			</div>
			<div class="modal-body bodyText"></div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary btnText"  data-dismiss="modal"></button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    function FotoUpload() {
        var codigo = "<?php echo (isset($_GET['codigo'])) ? $_GET['codigo'] : ""; ?>";
        if(codigo == "") {
            alertify.error("Salve o cadastro antes de adicionar foto!");
            return false;
        }

        window.location = "<?php echo $this->Link("alunos","FotoUpload")?>/&codigo="+codigo
    }

    $(function(){
        alertify.set('notifier','position', 'top-right');
        
        $(".btn").addClass("btn-flat");
    });

    var ld = $("ld").html();
    var request;
	$("#asec-menu-primaryx a").click( function(e) {



		if( $(this).data('async') == false ) {
			window.location = $(this).attr("href")
			return false;
		}

		e.preventDefault();



		if(typeof request !== "undefined") {
			request.abort();
		}

		$("#asec-menu-primary li").removeClass("active");

		if($(this).parent().hasClass("treeview")) {
			$(this).parent().parent().parent().addClass("active");
		} else if($(this).parent().parent().parent().hasClass("treeview")) {
			$(this).parent().parent().parent().addClass("active");
			$(this).parent().addClass("active");
		} else {
			$(this).parent().addClass("active");
		}

		var url = $(this).attr("href");

		if(url != "#") {
			LoadPage(url,"#asec-content")
		}

		///

	})

	function EmptyModalReset() {
		$("#EmptyModal .modal-title").html('Aviso');
		$("#EmptyModal .modal-body").html('');
		$("#EmptyModal .modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>');
	}


	function LoadPage(url,target,ld) {

		if(typeof target == "undefined")
			target = "#asec-content";

                if(typeof ld == "undefined")
                    $(target).html( $("#ld").html());



		stopRequest();
		request = $.ajax({
            type : 'get',
			url : "" + url + "/&async=true",
            timeout : 10000,
			success: function(e) {
				$(target).html(e);
				App();
			},
            error : function(e) {
                console.log(e);
                $(target).html('Tempo de comunicação com o servidor excedido!');
            }
		})

		window.history.pushState("page2", "Title2", ""+url+"");
	}

        function stopRequest() {
            if(typeof request !== "undefined") {
                    request.abort();
            }
        }

        function DatePicker(el) {
            $(el).datepicker({
                autoclose: 'true',
                dateFormat: "dd/mm/yy",
                numberOfMonths: 1,
                dayNamesMin: [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" ],
                monthNames:  [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
            });
        }


	
    DatePicker('.datepicker');
    
	function App() {

		$(".table-grid").DataTable({
            "pageLength": 50,
            "order": [[1,"asc"]],
			"language": {
				"paginate": {
					"first": "Primeira Página",
					"next": "Próximo",
					"previous": "Anterior",
					"showing":"Mostrando"
				}
			}
		})

		$(".loadAsync").click( function(e) {
			e.preventDefault();
			var url = $(this).attr("href")
			LoadPage(url);
		})

		$('.select2').select2();
        $('.select2').prop('tabindex', 1);
		$(".select2").select2({ width: '100%' });


                $(".datepicker").mask("99/99/9999");

                $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
                function tooltip_placement(context, source) {
                        var $source = $(source);
                        var $parent = $source.closest('table')
                        var off1 = $parent.offset();
                        var w1 = $parent.width();

                        var off2 = $source.offset();
                        var w2 = $source.width();

                        if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
                        return 'left';
                }

                    var h = $(window).height();
                    $(".tab-content").css("overflow","auto");
                    $(".tab-content").css("overflow-x","hidden");
                    $(".tab-content").css("min-height",h-310);

                $(function(){
                    $(".cep").inputmask("99999-999", {"placeholder":""});

                    $(".telefone").focus(function() {
                        var len = $(this).val().length;
                        $(this).inputmask("(99) 99999-9999" , {"placeholder":""});  //static mask
                    });


                    $(".telefone").focusout(function() {
                        var len = $(this).val().length;
                        if(len == '14') {
                            $(this).inputmask("(99) 9999-9999" , {"placeholder":""});  //static mask
                        } else if(len == '15') {
                            $(this).inputmask("(99) 99999-9999" , {"placeholder":""});  //static mask
                        }
                    })
                    
                })

                $(".cep").focusout( function() {
                    buscacep();
                })



	};

                    var callbackg = null;
                    function Alerta(msg,title,btn,time,callback){
                        $("#ModalAlert").modal('hide');
                        $("#EmptyModal").modal('hide');
                        if(typeof title == "undefined" || title == '') {
                            title = "Aviso"
                        }

                        if(typeof btn == "undefined" || btn == '') {
                            btn = "Fechar"
                        }

                        if(typeof time == "undefined" || time == '') {
                            time = 5000
                        }

                        $("#ModalAlert .titleText").html( title );
                        $("#ModalAlert .bodyText").html( msg );

                        $("#ModalAlert .modal-footer").show();
                        $("#ModalAlert .btnText").html( btn )
                        if(btn == 'hide') {
                            $("#ModalAlert .modal-footer").hide();
                        }
                        $("#ModalAlert").modal('show');


//                        setTimeout(function(){
//                            $("#ModalAlert").modal('hide');
//                        },time)


                        if(typeof callback != "undefined" && callback != "") {
                            callbackg = callback
                        } else {
                            callbackg = "";
                        }

                    }

                    $('#ModalAlert').on('hidden.bs.modal', function () {

                        $("#ModalAlert .titleText").html("Aviso");
                        $("#ModalAlert .bodyText").html('');
                        $("#ModalAlert .btnText").html('fechar')
                        if(callbackg != "" && typeof callbackg != "undefined") {
                            callbackg();
                        }

                    });

                    function buscacep() {
                        var cep = $("#cep").val();

                        if(cep.length == 9) {
                            request = $.ajax({
                                dataType: 'json',
                                url : 'https://viacep.com.br/ws/'+cep+'/json/',
                                success: function(e){
                                    if(e.erro == true) {
                                        alert('CEP não encontrado...');
                                    } else {
                                        $("#lograuf").val(e.uf);
                                        $("#lograbairro").val(e.bairro);
                                        $("#logramunicipio").val(e.localidade);
                                        $("#logradouro").val(e.logradouro);

                                        $("#logranumero").focus();
                                    }
                                }
                            });
                            
                            $.ajax({
                               type: 'get',
                               dateType: 'html',
                               url : 'https://www.e-cnhsp.sp.gov.br/gefor/callback.do?method=findByCepAndIdUF&codigo='+cep+'&id=26',
                               success : function(res){
                                   eval(res);
                               }
                            })
                        }
                    }

    $(function() {
        App();
        //console.log( "ready!" );
    });
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
<?php
if(@$conexao)
    @$conexao->close(); ?>