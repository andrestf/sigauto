<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() { 

        // Create the data table.
	       var data = google.visualization.arrayToDataTable([
	         //['Serviço', 'Matriculas', { role: 'style' }, { role: 'annotation' } ],
	         ['Serviço', 'Matriculas'],
	         //['Primeira Hab. A'  , 27,'','',],
	         <?php echo $grafMatInic; ?>
	      ]);
        // Set chart options
        var options = {//'title':'How Much Pizza I Ate Last Night',
                       //'width':'auto',
                       'height':600};
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('grafMatInic'));
        chart.draw(data, options);


        // Create the data table.
	       var data = google.visualization.arrayToDataTable([
	         //['Serviço', 'Matriculas', { role: 'style' }, { role: 'annotation' } ],
	         ['Serviço', 'Matriculas'],
	         //['Primeira Hab. A'  , 27,'','',],
	         <?php echo $grafMatConc; ?>
	      ]);
        // Set chart options
        var options = {//'title':'How Much Pizza I Ate Last Night',
                       //'width':'auto',
                       'height':600};
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('grafMatConc'));
        chart.draw(data, options);

       
      }
    </script>
<div class="content">   


    <div class="box no-print">
		<div class="box-header with-border">Filtro</div>
		<div class="box-body row">
			<form action="index.php?route=dashboard" method="post">
				<div class="col-md-2 col-xs-6">
					<label>Data Inicial</label>
					<input type="text" class="datepicker form-control" value="<?php echo $dIni; ?>" name="dIni"/>
				</div>

				<div class="col-md-2 col-xs-6">
					<label>Data Final</label>
					<input type="text" class="datepicker form-control" value="<?php echo $dFim; ?>" name="dFim"/>
				</div>


				<div class="col-md-4 col-xs-6">
					<label>Tipo Serviço</label>
					<select class="form-control select2" name="tpservicos[]" multiple="multiple" >
						<?php foreach($oSERVICOS as $servicos => $servico) { ?>
							<option value="<?php echo $servico['serv_id']; ?>" <?php echo (in_array($servico['serv_id'], $aSERVICOS)) ? 'selected="selected"': '';?> ><?php echo $servico['serv_descricao'] ?></option>
						<?php } ?>

					</select>
				</div>


				<div class="col-md-4 col-xs-6">
					<label><?php echo (count($oLOCAIS) > 1 ) ? 'Local' : ''; ?></label>
					<select class="form-control <?php echo (count($oLOCAIS) > 1 ) ? 'select2' : ' hidden'; ?>" name="local[]"  <?php echo (count($oLOCAIS) > 1 ) ? 'multiple="multiple"' : ''; ?> >
						<?php foreach($oLOCAIS as $local => $ldescri) { ?>
							<option value="<?php echo $local; ?>" <?php echo (in_array($local, $aLocais) || count($oLOCAIS) == 1) ? 'selected="selected"': '';?> ><?php echo $ldescri ?></option>
						<?php } ?>

					</select>
				</div>

				<div class="col-md-1 col-xs-1">
					<label>&nbsp;</label>
					<input type="submit" class="form-control btn btn-primary" value="OK">
				</div>
			</form>
		</div>
    </div>

	<div class="row">
		<div class=" col-md-4 col-xs-4">
			<div class="box no-border">
				<div class="box-header bg-orange ">Fichas Cadastradas</div>
				<div class="box-body text-center" id="">
					<?php echo $alunosTotal; ?>
				</div>
		    </div>
		</div>

		<div class=" col-md-4 col-xs-4">
			<div class="box no-border">
				<div class="box-header bg-blue">Matriculas Iniciadas</div>
				<div class="box-body text-center" id="">
					<?php echo $alunosTotalMatriculaI; ?>
				</div>
		    </div>
		</div>

		<div class=" col-md-4 col-xs-4">
			<div class="box no-border">
				<div class="box-header bg-aqua">Matriculas Concluídas</div>
				<div class="box-body text-center" id="">
					<?php echo $alunosTotalMatriculaC; ?>
				</div>
		    </div>
		</div>

		
   </div>

	<div class="row">
		<div class=" col-md-12">
			<div class="box">
				<div class="box-header with-border">Alunos x Serviços - INICIADAS</div>
				<div class="box-body">
					<div id="grafMatInic"></div>
						<table class="table table-bordered table-hover dataTable table-gridx">
							<thead>
							<tr>
								<th>Serviço</th>
								<th>Matriculas</th>
							</tr>
							</thead>

							<tbody>
							<?php foreach ($CategoriasInic as $key => $value) { ?>
								<tr>
									<td><?php echo $value; ?></td>
									<td>
										<?php 
										$total = 0;
	 										foreach ($DadosInic as $mats => $mat) {
	                							if($mat['serv_id'] == $key)  {
	                    							$total = $total+1;
	                							}
					            			} 
					            		echo $total; ?>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>				
				</div>
		    </div>
		</div>

		<div class=" col-md-12">
			<div class="box">
				<div class="box-header with-border">Alunos x Serviços - CONCLUÍDAS</div>
				<div class="box-body">
					<div id="grafMatConc"></div>
						<table class="table table-bordered table-hover dataTable  table-gridx">
							<thead>
							<tr>
								<th>Serviço</th>
								<th>Matriculas</th>
							</tr>
							</thead>
							

							<tbody>
							<?php foreach ($CategoriasConc as $key => $value) { ?>
								<tr>
									<td><?php echo $value; ?></td>
									<td>
										<?php 
										$total = 0;
	 										foreach ($DadosConc as $mats => $mat) {
	                							if($mat['serv_id'] == $key)  {
	                    							$total = $total+1;
	                							}
					            			}
					            		echo $total; ?>
									</td> 
								</tr>
							<?php } ?>
							</tbody>
						</table>					
				</div>
		    </div>
		</div>		
   </div>  

</div>

<script>
    $(function(){
		$(".table-gridx").DataTable({
            "pageLength": 20,
            "order": [[1,"desc"]],
			"language": {
				"paginate": {
					"first": "Primeira Página",
					"next": "Próximo",
					"previous": "Anterior",
					"showing":"Mostrando"
				}
			}
		})
    });
</script>