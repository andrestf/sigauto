<?php
/*
foreach ($lista_dos_produtos as $produto) {
    echo $produto['serv_descricao'] . "</br>";
}
 * 
 * 
 */
?>

<div class="content">
    <h3 class="page-title">Produtos</h3>
    
    <br/>
    
    
    
    <div class="form">
        
<form id="frm_alunos_busca">
            <div class="row">
                <div class="col-sm-4">
                    <label>Nome</label>
                    <input type="text" name="nome" class="form-control" />
                </div>

                <div class="col-sm-4">
                    <label>CPF</label>
                    <input type="text" name="cpf" class="form-control"/>
                </div>

                <div class="col-sm-2">
                    <label>Situação</label>
                    <select class="form-control" name="situacao" >
                        <option value="">Todos</option>
                        <option value="1">Ativos</option>
                        <option value="2">Inativos</option>
                    </select>
                </div>
                
                <div class="col-sm-2">
                    <label>Limite</label>
                    <select class="form-control" name="limite" >
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                </div>                
                
                <div class="col-sm-12">
                    <span class="pull-right">
                        <label>&nbsp;</label><br/>

                        <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                        <a href="<?php echo $this->Link('colaboradores',"cadastrar","tipo=".$TipoFuncionario);?>" ><span class="btn btn-default btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
                    </span>
                </div>                
            </div>
        </form>        
        
        
        <hr/>
        <table class="table table-borderedx table-condensed table-gridx table-hover table-striped table-responsive">
            <thead> 
                <tr>
                    <th width="80">Código</th>
                    <th>Descrição</th>
                    <th width="120">Valor</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lista_dos_produtos as $produto) { ?>
                    <tr>
                        <td><?php echo $produto['serv_id']?></td>
                        <td><?php echo $produto['serv_descricao']?></td>
                        <td>R$ <?php echo number_format($produto['serv_valor'],2,",",".");?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    
</div>


<script> 
    
    
    
</script>