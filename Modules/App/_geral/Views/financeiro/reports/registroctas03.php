
<?php


$report = new FinanceiroRelatoriosModel();
$report->Campos        = " a.usu_observafinan, finmov_descricao, finmov_tipo, sum(finmov_valor) as TOTAL, sum(1) as ctas ";
$report->CondicaoExtra = " and finmov_databaixareal is null";
$report->OrderBy = " order by nome_aluno, finmov_dtvenc desc ";
$report->GroupBy = " group by finmov_alunoid";

$report->Debug = false;

$dados  = $report->Gerar();
$processos  = new FasesModel();
        //$matriculas = $processos->GetMatriculas($codigo);
        

$Funcoes = new UsuariosHelper();

if($dados->num_rows < 1) {
	echo "sem resultado";
	return;
} ?>

<div class='content'>
	<div class='formx'>
        <h3>
            <center>
                <u><?php echo strtoupper($_SESSION['APP_LOCALNOME']);?></u><br/>
                Contas a Receber [Por Nome do Aluno - Sintético ]
            </center>
	    </h3>
	<div class="row">
		<div class="col-sm-12">
			<b>Lçto Inicial: </b><?php echo $_POST['datamov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Lçto Final: </b><?php echo $_POST['datamov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<b>Baixa Inicial: </b><?php echo $_POST['databxmov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Baixa Final: </b><?php echo $_POST['databxmov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Caixa Bx.Inicial: </b><?php echo $_POST['databxreal_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Caixa Bx.Final: </b><?php echo $_POST['databxreal_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
			<b>Vcto Inicial: </b><?php echo $_POST['venc_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Vcto Final: </b><?php echo $_POST['venc_fim']?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Usuário:</b> <?php
            if (isset($_POST['usubaixa'])) {
                foreach ($_POST['usubaixa'] as $usuXX) {
                    echo $Funcoes->fRetCampo('sis_usuarios', 'usu_apelido', "usu_id = '$usuXX'") . ", ";
                };
            } ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Documento:</b>
            <?php
            if (isset($_POST['tipodocumento'])) {
                foreach ($_POST['tipodocumento'] as $tdXX) {
                    echo $Funcoes->fRetCampo('sis_tpdocum', 'tpd_descricao', "tpd_cd = '$tdXX'") . ", ";
                };
            } ?>
        </div>
    </div>
        <hr/>

	<table class="table table-condensed">
		<thead>
			<tr>
                <th width="70">Código</th>
				<th width="250">Aluno</th>
				<th width="50">Parc</th>
				<th width="150">Tl a Receber</th>
                <th width='100'>Vencimento</th>
                <th width="150">Celular</th>
                <th width="70">Situação</th>
                <th width="200">Fase</th>
                
			</tr>
		</thead>

		<tbody>
			<?php $valor = 0;?>
		 	<?php while ($report = $dados->fetch_assoc()) { ?>

		 		<?php if(strtoupper($report['usu_situacao']) != "SPC" AND strtoupper($report['usu_situacao']) != "SERASA") {
                           $valor = $valor+$report['TOTAL'];
                            $fases      = $processos->GetFaseAtual($report['finmov_alunoid']); ?>


		 			<tr>
	                    <td><?php echo $report['finmov_alunoid'];?> <br/></td>
	                    <td><?php echo $report['nome_aluno'];?></td>
		 				<td><?php echo $report['ctas'];?></td>
		 				<td>R$ <?php echo number_format($report['TOTAL'],2,",",".");?></td>
	                    <td>
	                    	<?php echo  DataBR($Funcoes->fRetCampo('fin_financeiro', 'finmov_dtvenc', "finmov_alunoid = ".$report['finmov_alunoid']." AND (finmov_databaixa = '' or finmov_databaixa IS NULL)",1," finmov_dtvenc DESC ")); ?>
	                    	<?php //echo DataBR($report['finmov_dtvenc']);?>
	                    	
	                    	</td>
	                    <td><?php echo $report['usu_telcelular'];?></td>
	                    <td><?php echo $report['usu_situacao'];?></td>
	                    <td>
							<?php if($fases) { ?>
		                        <?php foreach ($fases as $fasex) { ?>
		                            <?php echo $fasex['serviten_descricao']; ?> <br/>
		                        <?php } ?>
		                    <?php } else {  ?>

		                    <?php } ?>                    	
	                    </td>
                        
		 			</tr>
		 			<?php if( $report['usu_observafinan'] != '') {?>
		 				<tr style="background: #E6E6E6;">
		 					<td></td>
		 					<td colspan="7">
		 						<b>Observações Financeiro:</b> 
		 						<?php echo $report['usu_observafinan'];?>
							</td>
		 				</tr>

		 			<?php }?>

		 			<tr style="background: #555; height: 3px; padding: 0;"><td colspan="8" style="padding: 0px;"></td></tr>
	 			<?php } //if SPC?>
	 		<?php } ?>
	 	</tbody>

	 	<tfoot>
	 		<tr>
	 			<td colspan="6">
	 				<hr/>
	 				<b>
	 				VALOR TOTAL MOVIMENTADO: R$ <?php echo number_format($valor,2,",",".");?>
					</b>
	 			</td>
	 		</tr>
	 	</tfoot>
	</table>
	</div>
</div>








