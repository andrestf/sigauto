
<?php


$report = new FinanceiroRelatoriosModel();
$report->Campos        = " finmov_descricao, finmov_tipo, sum(finmov_valorbaixa) as TOTAL  ";
$report->CondicaoExtra = " and finmov_tprecpag='CAIXA' ";
$report->OrderBy = " order by f.finmov_usubaixa ";
$report->GroupBy = " group by f.finmov_usubaixa ";

$report->Debug = false;

$dados  = $report->Gerar();

$Funcoes = new UsuariosHelper();

if($dados->num_rows < 1) {
	echo "sem resultado";
	return;
} ?>

<div class='content'>
	<div class='formx'>
	<h3>
		Extrato de Sintético - Por usuário
	</h3>
	<div class="row">
		<div class="col-sm-12">
			<b>Lçto Inicial: </b><?php echo $_POST['datamov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Lçto Final: </b><?php echo $_POST['datamov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<b>Baixa Inicial: </b><?php echo $_POST['databxmov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Baixa Final: </b><?php echo $_POST['databxmov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Caixa Bx.Inicial: </b><?php echo $_POST['databxreal_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Caixa Bx.Final: </b><?php echo $_POST['databxreal_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
			<b>Vcto Inicial: </b><?php echo $_POST['venc_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Vcto Final: </b><?php echo $_POST['venc_fim']?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                <b>Usuário:</b> <?php
                if (isset($_POST['usubaixa'])) {
                    foreach ($_POST['usubaixa'] as $usuXX) {
                        echo $Funcoes->fRetCampo('sis_usuarios', 'usu_apelido', "usu_id = '$usuXX'") . ", ";
                    };
                } ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Documento:</b>
                <?php
                if (isset($_POST['tipodocumento'])) {
                    foreach ($_POST['tipodocumento'] as $tdXX) {
                        echo $Funcoes->fRetCampo('sis_tpdocum', 'tpd_descricao', "tpd_cd = '$tdXX'") . ", ";
                    };
                } ?>
            </div>
        </div>
	<hr/>

	<table class="table table-condensed">
		<thead>
			<tr>
				<th width="130">Usuário</th>
				<th width="1"></th>
				<th width='770'>Valor Recebido</th>
			</tr>
		</thead>

		<tbody>
			<?php $valor = 0; ?>
		 	<?php while ($report = $dados->fetch_assoc()) {
                            $valor = $valor+$report['TOTAL']?>
	 			<tr>
	 				<td><?php echo ($report['usu_apelido'] != "") ? $report['usu_apelido'] : 'SEM APELIDO';?></td>
	 				<td><?php echo '                       ';?></td>
	 				<td>R$ <?php echo number_format($report['TOTAL'],2,",",".");?></td>
	 			</tr>
	 		<?php } ?>
	 	</tbody>

	 	<tfoot>
	 		<tr>
	 			<td colspan="6">
	 				<hr/>
	 				<b>
	 				VALOR TOTAL MOVIMENTADO: R$ <?php echo number_format($valor,2,",","."); ?>
					</b>
	 			</td>
	 		</tr>
	 	</tfoot>
	</table>
	</div>
</div>	