<form method="post" action="<?php echo Permalink('financeiro/ReportsGerar/')?>" id="frmFiltro">
<div class="content">
    <div class='box box-primary' style="width: 550px; margin:0 auto; ">
        <div class='box-header with-border'>
            <h3 class='box-title'>Filtro de Movimentação</h3>
        </div>
        
        <div class='box-body'>
            <div class='row'>
                <div class='col-sm-6'>
                    <label>Lançamento Inicial</label>
                    <input type="text" name="datamov_ini" id="datamov_ini" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Lançamento Final</label>
                    <input type="text" name="datamov_fim" id="datamov_fim" class="form-control datepicker" />
                </div>

                <div class='col-sm-6'>
                    <label>Financeiro Baixa Inicial</label>
                    <input type="text" name="databxmov_ini" id="databxmov_ini" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Financeiro Baixa Final</label>
                    <input type="text" name="databxmov_fim" id="databxmov_fim" class="form-control datepicker" />
                </div>

                <div class='col-sm-6'>
                    <label>Caixa Baixa Inicial</label>
                    <input type="text" name="databxreal_ini" id="databxreal_ini" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Caixa Baixa Final</label>
                    <input type="text" name="databxreal_fim" id="databxreal_fim" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Vencimento Inicial</label>
                    <input type="text" name="venc_ini" id="venc_ini" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Vencimento Final</label>
                    <input type="text" name="venc_fim" id="venc_fim" class="form-control datepicker" />
                </div>
                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Tipo Documento</label>
                    <select name="tpdoc" id="tpdoc" class="select2 form-control">
                        <option value="">&nbsp;</option>
                        <?php foreach ($TipoDocumentos as $tpdoc) {?>
                        <option value="<?php echo $tpdoc['tpd_cd']?>" > <?php echo $tpdoc['tpd_descricao']?> </option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Usuário</label>
                    <select name="usubaixa[]" id="usubaixa" class="select2 form-control" multiple="multiple" >
                        <option value="">&nbsp;</option>
                        <?php foreach ($Usuarios as $usuario) {?>
                        <option value="<?php echo $usuario['usu_id']?>" ><?php echo $usuario['usu_apelido']?> </option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Relatório</label>
                    <select name="report" class="select2">
                        <option></option>
                        <option value="extrato1" selected="selected">Extrato Analítico de Caixa</option>
                        <option value="extrato2" selected="selected">Extrato Sintético de Caixa (Por Usuário)</option>
                        <option value="extrato3" selected="selected">Extrato Sintético de Caixa (Por Tipo Documento)</option>
                        <option value="registroctas01" selected="selected">Registro de Contas</option>
                        <option value="registroctas02" selected="selected">Contas a Receber [Por Aluno]</option>
                    </select>
                </div>
            </div>
            
            <div class='box-footer'>
                <br/>
                <span class='pull-right'>
                    <input type="submit" value="Gerar" class='btn btn-primary'/>
                </span>
            </div>
        </div>
    </div>
    
</div>
</form>

<script>
    
</script>