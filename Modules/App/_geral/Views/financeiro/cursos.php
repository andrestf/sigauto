<section class="content asec-box" id="aluno">
    <h3 class="page-title">
        Financeiro > Cursos
    </h3>

    <br/>
    <div class="box box-body asex-box">
        <div class="pull-right grupo-btn">        
            <span class="grupo-btn-01">
                <a href="/index.php?route=Financeiro/CursosAdicionar" class="btn btn-success"> <i class="fa fa-plus-square"></i> Adicionar </a>
            </span>
        </div>
    </div>
    
    

    <div class="box asex-box">
        <div class="box-body">
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>   
                        <th width="20"></th>
                        <th>Cód.</th>
                        <th>Descrição</th>
                        <th>Dias</th>
                        <th>Hora</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listaCursos as $cur) { ?>
                        <tr>
                            <td><i class="fa fa-edit cursor" onclick="edit('<?php echo $cur['cur_id']; ?>')"></i></td>
                            <td><?php echo $cur['cur_id']; ?> </td>
                            <td><?php echo $cur['cur_descricao']; ?> </td>
                            <td><?php echo $cur['cur_dias']; ?> </td>
                            <td><?php echo $cur['cur_hora']; ?> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

</section>

<script>
    function edit(id) {
        window.location = "/index.php?route=Financeiro/CursosEditar/&editar="+id;
    }
</script>