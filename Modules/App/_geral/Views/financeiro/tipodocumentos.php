<section class="content asec-box" id="aluno">
    <h3 class="page-title">
        Financeiro > Tipos de Documentos
    </h3>

    <br/>
    <div class="box box-body asex-box">
        <div class="pull-right grupo-btn">        
            <span class="grupo-btn-01">
                <a href="/index.php?route=Financeiro/TipoDocumentosAdicionar" class="btn btn-success"> <i class="fa fa-plus-square"></i> Adicionar </a>
            </span>
        </div>
    </div>
    
    

    <div class="box asex-box">
        <div class="box-body">
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>   
                        <th width="20"></th>
                        <th>Cód.</th>
                        <th>Descrição</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listaDocumentos as $doc) { ?>
                        <tr>
                            <td><i class="fa fa-edit cursor" onclick="edit('<?php echo $doc['tpd_id']; ?>')"></i></td>
                            <td><?php echo $doc['tpd_cd']; ?> </td>
                            <td><?php echo $doc['tpd_descricao']; ?> </td>
                            <td><?php echo $doc['tpd_destino']; ?> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

</section>

<script>
    function edit(id) {
        window.location = "/index.php?route=Financeiro/TipoDocumentosEditar/&editar="+id;
    }
</script>