<section class="content asec-box" id="aluno">
    <h3 class="page-title">
        <?php echo $view_PageTitle ?>
    </h3>

    <br/>

    <form action="/index.php?route=Financeiro/Cursos<?php echo $acao; ?>/&editar=<?php echo $cur['cur_id']; ?>" method="post">
    <div class="box asex-box">
        <input type='hidden' class='form-control' name="idEdit"  value="<?php echo $cur['cur_id']; ?>" />
        
        <div class="box-body">
            <div class="row">    
                <div class="col-md-12 col-sm-12">
                    <label>Descrição: </label>  <input type='text' name="descricao" class='form-control'  value="<?php echo $cur['cur_descricao']; ?>" />
                </div>

                <div class="col-md-3 col-sm-3">
                    <label>Dias: </label>  <input type='text' name="dias" class='form-control'  value="<?php echo $cur['cur_dias']; ?>" />
                </div>

                <div class="col-md-2 col-sm-2">
                    <label>Hora: </label>  <input type='text' name="hora" class='form-control'  value="<?php echo $cur['cur_hora']; ?>" />
                </div>

                <div class="col-md-3 col-sm-3">
                    <label>Hora Almoço: </label>  <input type='text' name="hralmoco" class='form-control'  value="<?php echo $cur['cur_hralmoco']; ?>" />
                </div>

                <div class="col-md-2 col-sm-2">
                    <label>Termino: </label>  <input type='text' name="hrtermino" class='form-control'  value="<?php echo $cur['cur_hrtermino']; ?>" />
                </div>


                <div class="col-md-1 col-sm-3">
                    <label>Turma: </label>  <input type='text' name="turma" class='form-control'  value="<?php echo $cur['cur_turma']; ?>" />
                </div>

                <div class="col-md-1 col-sm-3">
                    <label>Sala: </label>  <input type='text' name="sala" class='form-control'  value="<?php echo $cur['cur_sala']; ?>" />
                </div>

                <div class="col-md-2 col-sm-2">
                    <label>Formação: </label>  <input type='text' name="formacao" class='form-control'  value="<?php echo $cur['cur_formacao']; ?>" />
                </div>

                <div class="col-md-2 col-sm-2">
                    <label>Atualização: </label>  <input type='text' name="atualizacao" class='form-control'  value="<?php echo $cur['cur_atualizacao']; ?>" />
                </div>
                
                <div class="col-md-2 col-sm-2">
                    <label>Tipo : </label>     
                    <select name="ativo" class="form-control">
                        <option value="A" <?php echo ($cur['cur_ativo'] == 'A') ? 'selected="selected"' : ''?> >Ativo</option>
                        <option value="I" <?php echo ($cur['cur_ativo'] == 'I') ? 'selected="selected"' : ''?> >Inativo</option>
                    </select>
                </div>

                <div class="col-md-6 col-sm-10">
                    <label>Contato: </label>  <input type='text' name="contato1" class='form-control'  value="<?php echo $cur['cur_contato1']; ?>" />
                </div>

                <div class="col-md-6 col-sm-10">
                    <label>Contato 2: </label>  <input type='text' name="contato2" class='form-control'  value="<?php echo $cur['cur_contato2']; ?>" />
                </div>

                <div class="col-md-6 col-sm-10">
                    <label>Contato 3: </label>  <input type='text' name="contato3" class='form-control'  value="<?php echo $cur['cur_contato3']; ?>" />
                </div>
            </div>
        </div>
            

        <div class="box-footer">
            <input type="submit" value="Confimar" class="btn btn-primary" />
        </div>


    </div>
    </form>

</section>
