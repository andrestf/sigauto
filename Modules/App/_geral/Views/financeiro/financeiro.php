<?php $Funcoes = new FuncoesHelper(); ?>

<section class="content asec-box" id="aluno">
    <h3 class="page-title">Financeiro</h3>
    <br/>

    <div class="box no-print">
        <div class="box-header">
            <a href="<?php echo $this->Link('financeiro',"Reports");?>" ><span class="btn btn-info btnRel"> <i class="fa fa-list-alt"></i> Relatórios</span> </a>
            &nbsp;
    <?php if($this->ValidaNivel2(50)) { ?>
            <a href="#NovoMovimento" data-toggle="modal"><span class="btn btn-warning btnAdd"> <i class="fa fa-plus"></i> Novo Lançamento</span> </a>
    <?php } ?>
        </div>
    </div>

    <div class="box box-primary no-print" id="filtro">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-filter"></i>
                Filtro</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool hide" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body" id="">
            <form id="frmListaFinan" method="post" action="<?php echo $this->Link("financeiro","index");?>">
            <input type="hidden" name="lista" class="form-control" value="listagem"/>
            <input type="hidden" name="!deonde" class="form-control" value="CREDITO"/>
            	<div class="row">
                    <div class="col-sm-2">
                        <label>Parcela</label>
                        <input type="number" name="parcela" class="form-control" maxlength="14" value="<?php echo @$_POST['parcela']?>" />
                    </div>
                    <div class="col-sm-2">
                        <label>Cod.Aluno</label>
                        <input type="number" name="codaluno" class="form-control" maxlength="14" value="<?php echo @$_POST['codaluno']?>" />
                    </div>                         		
                    <div class="col-sm-5">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control" value="<?php echo @$_POST['nome']?>" />
                    </div>         
                       			
                    <div class="col-sm-3">
                        <label>CPF</label>
                        <input type="text" name="cpf" class="form-control" onkeyup="formataCPF(this)" maxlength="14" value="<?php echo @$_POST['cpf']?>" />
                    </div>

            	</div>
            	<br/>

            	<div class="row">
                    
                    <div class='col-sm-3'>
                        <label>Lançamento Inicial</label>
                        <input type="text" name="datamov_ini" class="form-control datepicker" value="<?php echo @$_POST['datamov_ini']?>"/>
                    </div>

                    <div class='col-sm-3'>
                        <label>Lançamento Final</label>
                        <input type="text" name="datamov_fim" class="form-control datepicker" value="<?php echo @$_POST['datamov_fim']?>"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Vencimento Inicial</label>
                        <input type="text" name="vencimentoini" class="form-control datepicker" value="<?php echo @$_POST['vencimentoini']?>"/>
                    </div>

                    <div class="col-sm-3">
                        <label>Vencimento Final</label>
                        <input type="text" name="vencimentofin" class="form-control datepicker" value="<?php echo @$_POST['vencimentofin']?>" />
                    </div>                
                  
                    <div class="col-sm-3" id="DtPaga" style="display:none;">
                        <label>Data Pagto.</label>
                        <input type="text" name="dtpaga" class="form-control datepicker dtpaga"  value="<?php echo @$_POST['dtpaga']?>" />
                    </div>                    
            	</div>
            	<br/>

                <div class="row">

                    <div class="col-sm-3">
                        <label>Situação</label>
                        <select class="form-control" name="situacao" id="situacao" >
                            <option value=""  <?php echo (@$_POST['situacao'] == "") ? "selected" : ""; ?> ></option>
                            <option value="1" <?php echo (@$_POST['situacao'] == "1") ? "selected" : ""; ?> >Pago</option>
                            <option value="0" <?php echo (@$_POST['situacao'] == "0") ? "selected" : ""; ?> >Não Pago</option>
                        </select>
                    </div>    
                    
                    <div class="col-sm-3">
                        <label>Documento</label>
						<select class="form-control" name="documento" >
                            <option value=""></option>
                            <?php foreach ($TpDocs as $doc) { ?>
                            	<option value="<?php echo $doc['tpd_cd']?>"> <?php echo $doc['tpd_descricao']?> </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 hide">
                        <label>Plano de Contas</label>
                        <input type="text" name="pcontas" class="form-control" />
                    </div>
					
					<div class="col-sm-3 hide">
                        <label>Centro de Custo</label>
                        <input type="text" name="ccusto" class="form-control" />
                    </div>
					
                    <div class="col-sm-3">
                        <label>Tipo</label>
                        <select class="form-control" name="tipo" >
                            <option value=""> Pagar + Receber</option>
                            <option value="S" <?php echo (@$_POST['tipo'] == "S") ? "selected" : ""; ?> >A Pagar</option>
                            <option value="E" <?php echo (@$_POST['tipo'] == "E") ? "selected" : ""; ?> >A Receber</option>
                        </select>
                    </div> 

                    <div class="col-sm-3">
                        <label>Local</label>
                        <select class="form-control" name="deondetprecpag" >
                            <option value=""> Ambos</option>
                            <option value="CAIXA" <?php echo (@$_POST['deondetprecpag'] == "CAIXA") ? "selected" : ""; ?> >Caixa</option>
                            <option value="FINAN" <?php echo (@$_POST['deondetprecpag'] == "FINAN") ? "selected" : ""; ?> >Financeiro</option>
                        </select>
                    </div>  
                </div>
                <br>
                <div class="row"> 
                    <div class="col-sm-3">
                        <label>Limite</label>
                        <select class="form-control" name="limit" >
                            <option value="10" selected="selected">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                            <option value="*">Sem Limite</option>
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                            
                        </span>
                    </div>
                    
                </div>                

                <div class="row">
                

                </div>
                    
                
            </form>
            <hr/>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php if($parcelas != '') { ?>
    <div class="box box-primary" id="parcelas">
    	<div class="box-header with-border">

    		<?php
    		$nTipo = 0;
            $tipo = "Movimentos ";
    		if(isset($_POST['tipo'])) {
    			$nTipo = $_POST['tipo'];
    			switch ($_POST['tipo']) {
	    			case 'S':
	    				$nTipo = 0;
	    				$tipo = "Contas a Pagar";
	    				break;
					case 'E':
						$nTipo = 1;
	    				$tipo = "Contas a Receber";
	    				break;
	    			default:
	    				$nTipo = 2;
	    				$tipo = "Contas [ Pagar / Receber 2 ]";
	    				break;
    			}
    		}?>
            <h3 class="box-title">
                <i class="fa fa-list"></i>
                <?php echo $tipo?></h3>    		


    	</div>
    	<div class="box-body">
            <?php if($_POST) { ?>
    		<b>Nome:</b> <?php echo $_POST['nome'];?> <br/>
    		<b>Vencimento Inicial: </b> <?php echo $_POST['vencimentoini'];?> &nbsp;&nbsp;&nbsp; <b>Vencimento Final:</b> <?php echo $_POST['vencimentofin'];?>&nbsp;&nbsp;&nbsp;
    		<b>Documento:</b> <?php echo ($_POST['documento'] != "") ? $Funcoes->fRetCampo("sis_tpdocum","tpd_descricao","tpd_cd = '".$_POST['documento']."'") : "" ;?>
            <?php } ?>
    	</div>
    	<div class="box-body table-responsive">
            <table class='table table-bordered table-condensedx  table-hover table-striped'>
            	<thead>
            		<tr>
            			<th width="10">#</th>
            			<th width="10"></th>
            			<?php if($nTipo !=0) { ?>
            				<th>Aluno</th>
            			<?php } ?>
            			<th>Descrição</th>
            			<th>Tp. Documento</th>
            			<th width="90">Vencimento</th>
            			<th width="90">Dt. Paga</th>
            			<th width="90">Valor</th>
            			<th width="90">Valor Pago</th>
            			<th>Usu. Baixa</th>
            			<th width="100" class='no-print'></th>
            		</tr>
            	</thead>
            	<?php foreach ($parcelas as $parcela) {
                    $bgColor = "";
                    if($parcela['parcela']['mov_candata'] != '') {
                        $bgColor = "#ffcccc";
                    }
                 ?>
            		<tr style="background: <?php echo $bgColor; ?>">
            			<td><?php echo $parcela['parcela']['finmov_id'] .  $parcela['parcela']['can_data']; ?></td>
        				<td><?php echo ($parcela['parcela']['finmov_tpmov'] == "S") ? "<span class='label label-danger' title='A Pagar'>S</span>" : "<span class='label label-success' title='A Receber'>E</span>" ?></td>
        				<?php if($nTipo != 0) { ?>
            			<td>
            				<?php if($parcela['parcela']['finmov_alunoid'] != "") {  echo $parcela['parcela']['finmov_alunoid']; } ?>
            			</td>
            			<?php } ?>
            			<td><?php echo $parcela['parcela']['finmov_descricao']; ?></td>
            			<td><?php echo $parcela['parcela']['tpd_descricao']; ?></td>
            			<td><?php echo DataBR($parcela['parcela']['finmov_dtvenc']); ?></td>
            			<td><?php echo DataBR($parcela['parcela']['finmov_databaixa']); ?></td>
            			<td>R$ <?php echo $parcela['parcela']['finmov_valor']; ?></td>
            			<td>R$ <?php echo $parcela['parcela']['finmov_valorbaixa']; ?></td>
            			<td><?php echo $Funcoes->fRetCampo('sis_usuarios','usu_apelido',"usu_id = '".$parcela['parcela']['finmov_usubaixa']."'"); ?></td>
            			<td class="no-print"> 
                            <?php if(isset($parcela['log'])) { ?>
                                <span class="pull-right">
                                    &nbsp;

                                    <?php 
                                    $log = "";
                                    foreach ($parcela['log'] as $key => $value) {
                                        $log .= DataBRHora($value['finlog_caddata']) . " => " .$value['finlog_alteracao'] . "<br/>";
                                    } ?>

                                    <div id="modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog">  
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <p><?php echo $log;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <i class="fa fa-bars" title="Histórico do Movimento" data-target="#modal-log-<?php echo $parcela['parcela']['finmov_id']; ?>" data-toggle="modal"></i>
                                </span>
                            <?php } ?>
                            <?php if($parcela['parcela']['mov_candata'] == '') { ?>

                                <span class="pull-right">
                				<?php if($parcela['parcela']['finmov_databaixa'] == '') {?>

                					<i class='fa fa-download cursor' title="Baixar Movimento" onclick="AbreParcela(<?php echo $parcela['parcela']['finmov_id']; ?>,'<?php echo $parcela['parcela']['finmov_tpdocum']; ?>','<?php echo $parcela['parcela']['finmov_valor']; ?>',1)" data-toggle="tooltip"></i>
                				<?php } else { ?>
                                    <?php if($parcela['parcela']['finmov_dataestorno'] == '') {?>
                					   <i class="fa fa-refresh cursor text-red" title="Estonar Movimento" onclick="AbreParcela(<?php echo $parcela['parcela']['finmov_id']; ?>,'<?php echo $parcela['parcela']['finmov_tpdocumbx']; ?>','<?php echo $parcela['parcela']['finmov_valor']; ?>',0)" data-toggle="tooltip"></i>
                                    <?php } ?>
                				<?php } ?>

                                <?php if($this->ValidaNivel2(40) && $parcela['parcela']['finmov_databaixareal'] == "") {?>
                                    &nbsp;
                                    <i class="fa fa-edit cursor text-black" data-venc="<?php echo $parcela['parcela']['finmov_dtvenc'];?>" title="Editar Movimento" onclick="EditarParcela(<?php echo $parcela['parcela']['finmov_id']; ?>,'<?php echo $parcela['parcela']['finmov_tpdocumbx']; ?>','<?php echo $parcela['parcela']['finmov_valor']; ?>','e')" data-toggle="tooltip"></i>
                                <?php } ?>
                                </span>
                            <?php } else { ?>

                                <span class="">
                                    <center>
                                    <i class="fa fa-history cursor text-black" data-venc="<?php echo $parcela['parcela']['finmov_dtvenc'];?>" title="Recuperar Movimento" onclick="cancelaParcela(<?php echo $parcela['parcela']['finmov_id']; ?>,'<?php echo $parcela['parcela']['finmov_tpdocumbx']; ?>','<?php echo $parcela['parcela']['finmov_valor']; ?>','e')" data-toggle="tooltip"></i>
                                    </center>
                                </span>
                            <?php }?>



            			</td>
            		</tr>
            	<?php }?>
            </table>

            
    	</div>
    </div>
    <?php } else { ?>
    <div class="box box-primary" id="parcelas">
    	<div class="box-body">
    		<h3>Nenhuma parcela para mostrar</h3>
    	</div>
    </div>
    <?php } ?>

</section><!-- /.content -->






<!-- Modal -->
<div class="modal fade" id="NovoMovimento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Novo Movimento</h4>
      </div>

      <div class="modal-body">
          <form id="frmNovoMov">
            <div class="row">
                <div class="col-md-4">
                    <label>Aluno</label>
                    <input type="text" class="form-control" onkeyup="formataNumerico(this)" name="aluno" id="idaluno"/>
                </div>
            </div>
            <br/>
            
            
            <div class="row">
                <div class="col-md-4">
                    <label>Valor</label>
                    <input type="text" class="form-control" onkeyup="formataValorNovo(this)" name="valor" id="novoValor"/>
                </div>

                <div class="col-md-4">
                    <label>Vencimento</label>
                    <input type="text" class="form-control datepicker"  name="vencimento" id="novoVencimento"/>
                </div>

                <div class="col-md-4">
                    <label>Tipo</label>
                    <select id="novoTipo" name="tipo" class="form-control">
                    	<option value=""></option>
                    	<option value="S">Saída</option>
                    	<option value="E">Entrada</option>
                    </select>
                </div>

            </div>
            <br/>
            
            
            <div class="row">
                <!--   Emerson - 13/06/2017 - Foi substituida pela select direto do banco de dados
                <div class="col-sm-4">
                    <label>Centro de Custo</label>
                    <select id="ccusto" name="ccusto[]" class="form-control select2" multiple="multiple">
                        <option value=""></option>
                        <option value="1">Alvorada</option>
                        <option value="2">Alternativa</option>
                        <option value="3">Central</option>
                        <option value="4">Major Matheus</option>
                        <option value="5">Futura</option>
                        <option value="6">Formula1</option>
                        <option value="7">Matriz</option>     
                    </select>
                </div>
                
                <div class="col-sm-4 hide">
                    <label>Plano de Contas</label>
                    <select id="pcontas" name="pcontas" class="form-control">
                        <option value="1"></option>
                    </select>
                </div>
                Emerson - 13/06/2017 - Foi substituida pela select direto do banco de dados -->
                
                <!--   Emerson - 13/06/2017 - Adicionando select direto do banco de dados -->                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Centro de Custo</label>
                    <select name="centrocusto[]" id="centrocusto" class="select2 form-control" multiple="multiple" >
                         <option value="">&nbsp;</option>
                         <?php foreach ($CentroDeCustos as $centrocusto) {?>
                         <option value="<?php echo $centrocusto['cec_codigo']?>" ><?php echo $centrocusto['cec_descri']?> </option>
                         <?php } ?>
                     </select>
                </div>
                
                <div class='col-sm-6' style="margin-top: 5px;">
                    <label>Plano de Contas 2</label>
                    <select name="planoconta" id="planoconta" class="select2x form-control" >
                        <option value="">&nbsp;</option>
                        <?php foreach ($PlanoDeContas as $planocontas) {?>
                        <option value="<?php echo $planocontas['plc_cdPlano']?>" ><?php echo $planocontas['plc_descri']?> </option>
                        <?php } ?>
                    </select>
                </div>                
                <!--   Emerson - 13/06/2017 -->
                
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Local</label>
                    <select id="deOnde" name="tprecpaga" class="form-control">
                        <option value=""></option>
                        <option value="CAIXA">Caixa</option>
                        <option value="FINAN">Financeiro</option>
                    </select>
                </div>              
                
                <div class="col-sm-6">
                    <label>Repetir por</label>
                    <select id="repetir" name="repetir" class="form-control">
                        <option value="1">01 Mês </option>
                        <option value="2">02 Meses </option>
                        <option value="3">03 Meses</option>
                        <option value="4">04 Meses</option>
                        <option value="5">05 Meses</option>
                        <option value="6">06 Meses</option>
                        <option value="7">07 Meses</option>
                        <option value="8">08 Meses</option>
                        <option value="8">09 Meses</option>
                        <option value="10">10 Meses</option>
                        <option value="11">11 Meses</option>
                        <option value="12">12 Meses</option>
                    </select>
                </div>              
            </div>
            <br/>
            
            <div class="row">
            	<div class="col-sm-12">
                    <label>Descrição</label>
                    <input type="text" name="descri" id="novoDescri" class="form-control"/>
                </div>
           </div>
          </form>
          
      </div>
        
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary btnNovoMovimento btnContinuar">Salvar</button>
      </div>
    </div>
  </div>
</div>




<!-- Modal EdParc-->
<div class="modal fade" id="form_edtparcela" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="">Editar parcela</h4>
      </div>

      <div class="modal-body">
          <form id="edtparc_frm" method="">
            <div id="edtparc_dados">
            </div>
          </form>
          
      </div>
        
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-danger pull-left text-white " onclick="cancelarParcela();" data-dismiss="modal"><i class="fa fa-trash"></i> Cancelar Parcela</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar Edição</button>
        <button type="button" class="btn btn-primary" onclick='confirmaEditParcela();' >Salvar Edição </button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="form_bxparcela" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Baixa da parcela</h4>
      </div>

      <div class="modal-body">
          <form id="baixa-parcela">
            <input type="hidden" name="parc-id" value="" id="parc-id"/>
            <input type="hidden" name="mat-id"  value="" id="mat-id"/>
            <input type="hidden" name="alu-id"  value="" id="alu-id"/>
            
            
            <div class="row">
                <div class="col-md-4">
                  <label>Data Baixa</label>
                  <input type="text" class="form-control datepicker" name="baixa-data" id="fase-data" value="<?php echo date('d/m/Y')?>"/>
                </div>                

                <div class="col-md-4">
                    <label>Valor Baixa</label>
                    <input type="text" class="form-control" onkeyup="formataValorNovo(this)" name="baixa-valor" id="baixa-valor"/>
                </div>

                <div class="col-md-4">
                    <label>Tipo Documento</label>
                    
                    <?php #var_dump($TpDocumentos) ?>
                    <select name="tipo-documento" class="form-control" id='tipo-documento'>
                    <?php foreach($TpDocs as $TipoDoc) { ?>
                        <option value="<?php echo $TipoDoc['tpd_cd'] ?>"> <?php echo $TipoDoc['tpd_descricao'] ?>  </option>
                    <?php } ?>
                    </select>
                </div>
              </div>

            
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <label>Observações</label> 
                    <input type="text" name="observacoes" id="" class="form-control" />

                </div>
            </div>
          </form>
          
      </div>
        
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary btnBxParc btnContinuar" >Baixar Parcela</button>
      </div>
    </div>
  </div>
</div>



<script>

var t, nxtUrl, msgRet, tmp_IDAtualParc;
    tmp_IDAtualParc = "0";

<?php if($this->ValidaNivel2(40)) {?>
    function EditarParcela(id_parc) {
        tmp_IDAtualParc = id_parc;
        $.ajax({
            url : "<?php echo permalink('Financeiro/CarregaDadosParcela/')?>&from=TelaFinan&n="+id_parc,
            success:  function(res) {
                $("#edtparc_dados").html(res);
                $("#form_edtparcela").modal('show');
                $(".vlr").keyup();
                $(".hasDatepicker").datepicker();
            },
            error: function() {
                tmp_IDAtualParc = "0";
                return false;
            }
        })

    }

    function confirmaEditParcela() {
        alertify.confirm("Atenção","Confirmar edição de parcela?",
            function() {
                EditParcela(tmp_IDAtualParc);
            },
            function() {}
        );
    }

    function EditParcela(tmp_IDAtualParc) {
        var datas = "";
        datas = $("#edtparc_frm").serializeArray();
        $.ajax({
            dataType: 'json',
            type : 'post',
            data : datas,
            url : "<?php echo permalink('Financeiro/EditarParcela/')?>&from=TelaFinan&n="+tmp_IDAtualParc,
                    success: function(res) {
                        tmp_IDAtualParc = "0";
                        if(typeof res.erro !== "undefined") {
                            alertify.error(res.mensagem);
                            return;
                        }
                        else {
                            alertify.success(res.mensagem);
                            location.reload();
                        }
                    },
                    error: function(e,x,s) {
                        tmp_IDAtualParc = "0";
                        alertify.error("ERRO DE SERVIDOR! <br/> Falhar na edição de parcela! <br/>" + s)
                    }
        });
    }

    function cancelaParcela(parcx) {
        tmp_IDAtualParc = parcx;
        cancelarParcela();
    }
    function cancelarParcela() {
        if(tmp_IDAtualParc == "" || tmp_IDAtualParc == 0) {
            alertify.error("Parcela inválida para cancelamento");
            return false;
        }

        alertify.confirm("Atenção!","Confirmar Cancelamento de parcela?",
            function(){

                $.ajax({
                    dataType: 'json',
                    url : "<?php echo permalink('Financeiro/CancelarParcela/')?>&n="+tmp_IDAtualParc,
                    success: function(res) {
                        tmp_IDAtualParc = "0";
                        if(typeof res.erro !== "undefined") {
                            alertify.error(res.mensagem);
                            return;
                        }
                        else {
                            alertify.success(res.mensagem);
                            location.reload();
                        }
                    },
                    error: function(e,x,s) {
                        tmp_IDAtualParc = "0";
                        alertify.error("ERRO! <br/> Falhar no cancelamento de parcela! <br/>" + s)
                    }
                });

            },
            function() {
                $("#form_edtparcela").modal('show');
            }
        )
    }



    function CarregaDadosParcela(id_parc) {

    }
<?php } else { ?>
    function cancelarParcela() {}
    function EditarParcela() {}
    function confirmaEditParcela() {}
    function EditParcela(){};
<?php }?>

function AbreParcela(id_parc,tp_docum,valor,t){

    $("#baixa-parcela")[0].reset();
    $("#parc-id").val(id_parc);
    $("#baixa-valor").val(valor)
    $("#baixa-valor").keyup();
    $("#tipo-documento").val(tp_docum);



    $('#form_bxparcela').modal('show');

    t = t
    if(t == 0) {
    	 $("#baixa-parcela input").attr("readonly","readonly")
    	 $("#baixa-parcela select").attr("readonly","readonly")
    	 $(".btnBxParc").html('Estornar Movimento');
    	 nxtUrl = "index.php?route=financeiro/EstornaParcela/";
    	 msgRet = "ESTORNO </br> Movimento estonado com sucesso!";
    } else if(t == 'e') {
         $(".btnBxParc").html('Editar Movimento');      
         nxtUrl = "index.php?route=financeiro/EditarParcela/";
         msgRet = "BAIXA </br> Movimento baixado com sucesso!";
   
    } else {
    	 $("#baixa-parcela input").removeAttr("readonly");
    	 $("#baixa-parcela select").removeAttr("readonly");
    	 $(".btnBxParc").html('Baixar Movimento');    	
    	 nxtUrl = "index.php?route=financeiro/BaixaParcela/";
    	 msgRet = "BAIXA </br> Movimento baixado com sucesso!";
    }
}

$(".btnNovoMovimento").click( function() {
    var dados = $("#frmNovoMov").serializeArray();

    if( $("#deOnde").val() == "") {
        alertify.error("Informe o local da movimentação");
        return false;
    }

    $(".btnContinuar").attr('disabled',"disabled");
    $.ajax({
        type : 'post',
        data : dados,
        url  : "index.php?route=financeiro/CriaMovimento/",
        success : function(e) {
            if(e == "") {
            	$('#NovoMovimento').modal('hide');
                alertify.success("Sucesso!")
                setTimeout(function(){ location.reload(); },600);
            } else {
                alertify.error(e)
            }
        },
        complete : function() {
            $(".btnContinuar").removeAttr('disabled');
        }
    })
})

$(".btnBxParc").click( function() {
    var dados = $("#baixa-parcela").serializeArray();
    $(".btnContinuar").attr('disabled',"disabled");
    $.ajax({
        type : 'post',
        //dataType : 'json',
        data : dados,
        url  : nxtUrl,
        success : function(e) {
            if(e == "") {
            	$('#form_bxparcela').modal('hide');
                alertify.success(msgRet)
                setTimeout(function(){ location.reload(); },600);
            } else {
                alertify.error(e)
            }
        },
        complete : function() {
            $(".btnContinuar").removeAttr('disabled');
        }       
    })
})

$("#situacao").change( function() {
	val = $("#situacao option:selected").val();
	if(val == 1) {
		$(".dtpaga").val('');
		$("#DtPaga").show();
		$(".dtpaga").focus();
	} else {
		$("#DtPaga").hide();
		$(".dtpaga").val('');

	}

});

   
    $(function() {



		<?php
		########################################################################
		if($parcelas != '') {
			//echo " $('#filtro').addClass('collapsed-box'); ";
		}

		if(@$_POST['situacao'] == "1") {
			echo "$('#DtPaga').show();"	;
		}
		########################################################################
		?>        
    })
</script>