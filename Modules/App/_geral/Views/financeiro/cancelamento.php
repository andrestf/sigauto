    <div class="content asec-box" id="ficha_novo_aluno">
        <h3 class="page-title">
            Cancelamento de Processo
        </h3>    	

		<br/>
        <div class="form">
			<div class="text-red">O processo foi cancelado com sucesso!</div>
			<br/>

			<div class="text-blue text-bold">
			<?php 

				if($valor < "0") {
					echo "Nenhuma parcela foi criada para esse Processo! <br/> Aluno não tem direito a créditos referente a esse processo.";
				}

				if($valor == "0") {
					echo "As parcelas lançadas para esse processo NÃO FORAM CANCELADAS<br/> 
					    Caro usuário, sugiro que verifique se há necessidade de cancelamento da parcela<br> Nenhuma parcela foi paga, aluno sem direito a créditos!";
				}

				if($valor > "0") {
					echo "O Valor total pago pelo aluno foi de R$ ".number_format($valor,2,",",".")." <br/><br/> <div class='alert alert-success'>Aluno com direito a créditos no valor de R$ ".number_format($valor,2,",",".")." !!!</div>";
				}

			?>
			</div>


			<a class="btn btn-default" href="<?php echo permalink('alunos/editar',"&codigo=".$_GET['aluno'])?>"><i class="fa fa-user"></i> Ficha do Aluno</a>  &nbsp;
			<a class="btn btn-default" href="<?php echo permalink('AlunosFinanceiro/',"&codigo=".$_GET['aluno'])?>"><i class="fa fa-money"></i> Financeiro do Aluno</a>
			
        </div>


    </div>