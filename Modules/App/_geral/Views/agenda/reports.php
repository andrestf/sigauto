<form method="post" action="<?php echo Permalink('Agenda/RelatorioGerar/')?>" onsubmit="return true;"  id="newReport">
<div class="content">
    <div class='box box-primary' style="width: 550px; margin:0 auto; ">
        <div class='box-header with-border'>
            <h3 class='box-title'>Filtro</h3>
        </div>
        
        <div class='box-body'>
            <div class='row'>

                <div class="col-sm-6" >
                  <label>Cód Aluno</label>
                  <input type="number" name="idAluno" id="idAluno" class="form-control" />
                </div>

            </div>
            <div class='row'>

                <div class='col-sm-6'>
                    <label>Data Inicial</label>
                    <input type="text" name="data_ini" id="data_ini" class="form-control datepicker" autocomplete="off" value="<?php echo date("01/m/Y")?>" />
                </div>
                
                <div class='col-sm-6'>
                    <label>Data de Final</label>
                    <input type="text" name="data_fim" id="data_fim" class="form-control datepicker" autocomplete="off" value="<?php echo date("d/m/Y")?>" />
                </div>
                


                <div class="col-sm-6" >
                  <label>Instrutor</label>
                  <select name="cboInstrutorPrin" id="cboInstrutorPrin" class="form-control select2">
                    <option value=""></option>
                    <?php foreach ($instrutores as $instrutor) { ?>
                        <option value="<?php echo $instrutor['usu_id']?>"> <?php echo $instrutor['usu_nomecompleto']?> </option>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-sm-6" >
                  <label>Veículo</label>
                  <select name="cboVeicPrin" id="cboVeicPrin" class="form-control select2">
                    <option value=""></option>
                    <?php foreach ($veiculos as $veic) { ?>
                        <option value="<?php echo $veic['veic_id']?>"> <?php echo $veic['veic_descricao']?> </option>
                    <?php } ?>
                  </select>
                </div>                
                
               
                
                <div class='col-sm-12' style="margin-top: 5px;">
                    <label>Relatório</label>
                    <select name="report" class="select2">
                        <option></option>
                        <option value="aulas01">Aulas 01</option>
                        <option value="marcacaoaulas">Marcação de Aulas</option>
                    </select>
                </div>
            </div>
            
            <div class='box-footer'>
                <br/>
                <span class='pull-right'>
                    <input type="submit" value="Gerar" class='btn btn-primary'/>
                </span>
            </div>
        </div>
    </div>
    
</div>
</form>


<script>
    $("#newReport").on("submit",function(e){
        e.preventDefault();
        var datas = $("#newReport").serialize();
        window.open("<?php echo Permalink('Agenda/RelatorioGerar/')?>&"+datas,"_blank","width=400, height: 550");
    })
</script>