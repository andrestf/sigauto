<form id="frmEdtLanc">
<div class="modal fade" id="modal-edtlanc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Lançamento</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div style="font-size: 18px;
                        text-align: center;
                        font-weight: bold;
                        color: chocolate;
                        border-bottom: solid 1px;
                        padding-bottom: 5px;">
              <?php echo $aluno->usu_nomecompleto;?>
              <input type="hidden" name="eventoid" id="eventoidEdt" class="form-control" value="<?php echo $evento['au_id'];?>" readonly>
              <input type="hidden" name="alunoid" id="alunoidEdt" class="form-control" value="<?php echo $aluno->usu_id;?>" readonly>
              <input type="hidden" name="aluno" id="alunoEdt" class="form-control" value="<?php echo $aluno->usu_nomecompleto;?>" readonly>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="col-sm-6" >
            Instrutor
            <select name="cboInstrutorPrin" id="cboInstrutorPrinEdt" class="form-control select2">
              <option value=""></option>
              <?php foreach ($instrutores as $instrutor) { ?>
                  <option <?php echo ($instrutor['usu_id'] == $evento['au_idinstrut']) ? 'selected="selected"' : ''; ?> value="<?php echo $instrutor['usu_id']?>"> <?php echo $instrutor['usu_nomecompleto']?> </option>
              <?php } ?>
            </select>
          </div>

          <div class="col-sm-6" >
            Veículo
            <select name="cboVeicPrin" id="cboVeicPrinEdt" class="form-control select2">
              <option value=""></option>
              <?php foreach ($veiculos as $veic) { ?>
                  <option <?php echo ($veic['veic_id'] == $evento['au_idcarro']) ? 'selected="selected"' : ''; ?> value="<?php echo $veic['veic_id']?>"> <?php echo $veic['veic_descricao']?> </option>
              <?php } ?>
            </select>
          </div>     
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="col-sm-6">
            Início
            <input type="text" name="dini" class="dataini form-control datetimepicker datetimepickerini" value="<?php echo $evento['au_ini']?>" autocomplete="off">
          </div>

          <div class="col-sm-6">
            Fim
            <input type="text" name="dfim" class="datafim form-control datetimepicker datetimepickerend" value="<?php echo $evento['au_fim']?>" autocomplete="off">
          </div>
        </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left btnRemAula" > <i class="fa fa-trash"></i> Remover Aula</button>
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="fa fa-close"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary"> <i class="fa fa-check"></i> Confirmar</button>
      </div>
    </div>
  </div>
</div>
</form>

<script>
  $("#modal-edtlanc").modal('show');

    $('#frmEdtLanc .datetimepicker').datetimepicker({
      language: 'pt-BR',
      format: "dd/mm/yyyy hh:ii",
          autoclose: true,
          todayBtn: true,
          minuteStep: 5,
          startDate: "<?php echo date("d/m/Y h:i")?>",
    });

    $("#frmEdtLanc .datetimepickerini").datetimepicker()
    .on('changeDate', function(ev){
      $("#frmEdtLanc .datetimepickerend").val('');
      newDate = $("#frmEdtLanc .datetimepickerini").val();
      setTimeout( function(){
        $("#frmEdtLanc .datetimepickerend").datetimepicker('setStartDate', newDate);
      },100)
      
    });

  $(".btnRemAula").on('click',function() {
        alertify.confirm("Atenção","Remover aula?",
            function() {
                RemAula();
            },
            function() {}
        );

  });
  function RemAula() {
    //alertify.error("OPS!<br/>Falha ao remover aula!!");
    //return;
    idAluno = $("#alunoidEdt").val();
    idAula  = $("#eventoidEdt").val();
    

    $(".modal").modal('hide');

    $.ajax({
      url: "<?php echo $this->link('agenda/RemoverAula/')?>",
      data : { ial : idAluno, iau : idAula },
      type : 'POST',
      dataType: 'JSON',
        success: function(res) {
          if(res.erro != 0) {
            Alerta(res.mensagem);
            return 0;
          }
          Alerta(res.mensagem);
          $('#calendar').fullCalendar('removeEvents', idAula);
          return 1;
        },
        error: function(e,x,s) {
          Alerta(s);
          return 0;
        }      

    })


  }
  $("#frmEdtLanc").on("submit", function(e) {
    e.preventDefault();
      var datas   = $("#frmEdtLanc").serializeArray();
          instrut = $("#frmEdtLanc #cboInstrutorPrinEdt option:selected").val();
          veic    = $("#frmEdtLanc #cboVeicPrinEdt option:selected").val();
          dIni    = $("#frmEdtLanc .datetimepickerini").val();
          dFim    = $("#frmEdtLanc .datetimepickerend").val();
      /**/
      if(instrut == '') {
        Alerta("Informe um instrutor!");
        return; }

      if(veic == '') {
        Alerta("Informe um Veículo!");
        return; }

      if(dIni == '') {
        Alerta("Informe uma data de inicio!!");
        return; }

      if(dFim == '') {
        Alerta("Informe uma data de fim!");
        return; }
      /**/
      dIni = moment(dIni, 'DD/MM/YYYY HH:mm')
      dFim = moment(dFim, 'DD/MM/YYYY HH:mm')    

      $.ajax({
        url: "<?php echo $this->link('agenda/NovoEvento/')?>",
        data: datas,
        type: 'POST',
        dataType: 'JSON',
        success: function(res) {
          if(res.erro != 0) {
            Alerta(res.mensagem);
            return;
          }

          if(res.erro == 0) {
            eventData = {
                id    : res.id,
                title : res.title,
                start : res.start,
                end   : res.end
            };
            
            //var item = $("#calendar").fullCalendar( 'clientEvents', res.id );
            $('#calendar').fullCalendar('removeEvents', res.id);
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

            $("#modal-edtlanc").modal('hide');
          }


        },
        error: function(e,x,s) {
          Alerta(x);
        }
      });      

  });
</script>