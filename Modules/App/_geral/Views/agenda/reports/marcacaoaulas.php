<?php
 
$report = new AgendaRelatoriosModel();
//$report->CondicaoExtra = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL  ";
//$report->OrderBy = "serv_descricao";
$report->Campos = "inst.usu_nomecompleto as instrutor, TIMEDIFF(au_fim,au_ini) AS TEMPO ";
$dados  = $report->Gerar();

?>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>Aluno</th>
			<th>Instrutor</th>
            <th>Veículo</th>
			<th>Inicio</th>
            <th>Fim</th>
            <th>Tempo</th>
		</tr>
	</thead>
	<tbody>
		<?php if($dados->num_rows >= 1) { ?>
                    <?php while($report = $dados->fetch_assoc() ) {?>
                    <tr>
                        <td><?php echo $report['usu_nomecompleto']?> <br/></td>
                        <td><?php echo $report['instrutor']?> <br/></td>
                        <td><?php echo $report['veic_descricao']?> <br/></td>
                        <td><?php echo DataHoraBr($report['au_ini']); ?> <br/></td>
                        <td><?php echo DataHoraBr($report['au_fim']); ?> <br/></td>
                        <td><?php echo substr($report['TEMPO'],0,5); ?> <br/></td>
                    </tr>
                    <?php }//while ?>
                <?php } else {//if ?>
                    <tr>
                        <td  colspan="5" style="font-size: 28px">
                            <center>
                            Sem resultados
                            </center>
                        </td>
                    </tr>
                    
                <?php } ?>
	</tbody>
</table>

