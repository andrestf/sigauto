<?php
$fileName = "Aulas1";
$report = new AgendaRelatoriosModel();
//$report->CondicaoExtra = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL  ";
//$report->OrderBy = "serv_descricao";
$report->Campos = "inst.usu_nomecompleto as instrutor, TIMEDIFF(au_fim,au_ini) AS TEMPO ";
$report->GroupBy = "au_idaluno";
$dados  = $report->Gerar();


if($_POST['data_ini'] == "") {
    exit("ERRO: data inicial nao informada!");
}

if($_POST['data_fim'] == "") {
    exit("ERRO: data final nao informada!");
}

$DB = new DB();
$DB->CnCliente();

$wDataIni = '';
if(isset($_POST['data_ini']) AND $_POST['data_ini'] != '') {
    $data_ini = DataDB($_POST['data_ini']);
    $data_ini = $DB->Prepare($data_ini);
    $wDataIni = " AND date(au_ini) >= '$data_ini' ";
    $DataPadrao = "";
}

$wDataFim = '';
if(isset($_POST['data_fim']) AND $_POST['data_fim'] != '') {
    $data_fim = DataDB($_POST['data_fim']);
    $data_fim = $DB->Prepare($data_fim);
    $wDataFim = " AND date(au_fim) <= '$data_fim' ";
    $DataPadrao = "";
}

$wInstrutor = '';
if($_POST['cboInstrutorPrin'] != '') {
    $Instrutor = $_POST['cboInstrutorPrin'];
    $wInstrutor = " and au_idinstrut = '".$Instrutor."' ";
}


$wVeic = '';
if($_POST['cboVeicPrin'] != '') {
    $Veic = $_POST['cboVeicPrin'];
    $wVeic = " and au_idcarro = '".$Veic."' ";
}

$numLinhas = 13;
$numCols = 2;
?>


        <style type="text/css">
            .tb { font-size: 12px; }
            .auContent td { height: 35px; }
            .auContent tr { }   
        </style>
		<?php if($dados->num_rows >= 1) { ?>
            <div class="rowx">
            <?php 
            $col = 0;
            while($report = $dados->fetch_assoc() ) { $col++;?>
                    <div style="float: left; width: 375px;" >
                        <div style="border: solid 2px; margin-left: 10px; margin-right: 13px; padding: 0px;">
                            <table class="tb" width="100%" border="1">
                                <thead>
                                <tr border="0">
                                    <th colspan="6" class="text-center">
                                        <b>
                                            <h3>
                                                DIÁRIO DE AULAS
                                                <BR/>
                                                50 MINUTOS
                                                <BR/>
                                            </h3>
                                            Instrutor: <?php echo $report['instrutor']; ?>
                                            <br/>
                                            Aluno: <?php echo $report['usu_nomecompleto']; ?>
                                        </b>
                                    </th>
                                </tr>
                                <tr>
                                    <th class="text-center">Dia<br/> Semana</th>
                                    <th class="text-center">Dia</th>
                                    <th class="text-center">Mes</th>
                                    <th class="text-center">Inicio</th>
                                    <th class="text-center">Final</th>
                                    <th class="text-center">Nº <br/> Aulas</th>
                                </tr>
                                </thead>
                                <tbody style="font-size: 12px;" class="auContent">
                                    <?php 
                                        $IDALUNO = $report['au_idaluno'];
                                        $query = "
                                                    SELECT * FROM au_aulas
                                                    WHERE au_idlocal = '".$_SESSION['APP_LOCALID']."'
                                                    AND au_idaluno = '".$IDALUNO."'

                                                    $wDataIni $wDataFim $wInstrutor $wVeic";
                                        $aulas = $DB->ExecQuery($query);?>

                                    <?php
                                    $linhas = 0;
                                    foreach ($aulas as $aula) { ?>
                                        <tr height="25" class="text-center">
                                            <td><?php echo fDiaSemana(substr($aula['au_ini'],8,2),false,true); ?></td>
                                            <td><?php echo substr($aula['au_ini'],8,2); ?></td>
                                            <td><?php echo substr($aula['au_ini'],5,2); ?></td>
                                                
                                            <td><?php echo substr(DataHoraBr($aula['au_ini']),11,5);?></td>
                                            <td><?php echo substr(DataHoraBr($aula['au_fim']),11,5);?></td>
                                            <td>
                                                <?php 
                                                    $n = HoraDif(substr(DataHoraBr($aula['au_ini']),11,5).":00",substr(DataHoraBr($aula['au_fim']),11,5).":00");
                                                    echo CalcNumAulas($n);
                                                ?>
                                            </td>
                                        </tr>
                                    <?php $linhas++; } ?>

                                    <?php do { ?>
                                        <tr height="25" class="text-center">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php $linhas++; } while ($linhas < $numLinhas);?>
                                </tbody>
                            </table>
                            <div style="font-size: 10px; padding: 5px;">
                                <b>OBS1:</b> Aluno que não comparecer a aula marcada pagará a mesma, salvo se cancelar com 72h de antecedência.
                                <br/><br/>
                                <b>OBS2:</b> O sistema do Detran não tolera atraso nas aulas (portaria Detran 606), sendo que se o aluno atrasar o sistema não valida a aula, assim o aluno terá que remarcar a mesma tendo um custo adicional de R$ 60,00 cada aula de carro e R$ 60,00 para cada aula de moto.
                                <br/><br/>
                                <b>OBS3:</b> Para o aluno não ter o custo extra o correto é chegar sempre 5 minutos antes do início da aula.
                            </div>
                        </div>
                    </div>
            <?php 
                if($col > $numCols-1) {
                   echo  "<pagebreak>";
                } 
            }//while ?>
            </div>
        <?php } else {//if ?>
            <div  style="font-size: 28px">
                <center>
                Sem resultados
                </center>
            </div>
        <?php } ?>