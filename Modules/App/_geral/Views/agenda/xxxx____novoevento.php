




<link rel="stylesheet" href="/Public/plugins/bootstrapdatepicker/css/bootstrap-datetimepicker.min.css">
<script src='/Public/plugins/bootstrapdatepicker/js/bootstrap-datetimepicker.min.js'></script>
<script src='/Public/plugins/bootstrapdatepicker/js/locales/bootstrap-datetimepicker.pt-BR.js'></script>


<section class="content asec-box" id="agenda">
    <h3 class="page-title">Agenda  <small> - Nova Agenda</small> </h3>
    <br/>

	<div class="box no-print">
		<div class="box-header">
			<a href="<?php echo $this->Link('Agenda',"");?>" ><span class="btn btn-default "> <i class="fa fa-arrow-left"></i> Cancelar</span> </a>
			<a href="<?php echo $this->Link('Agenda',"");?>" ><span class="btn btn-success "> <i class="fa fa-check"></i> Confirmar</span> </a>
		</div>
	</div>

	<div class="box" style="background-color: #fff;">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-12"><b>Informações do Aluno</b></div>

			</div>
			<div class="row">
				<div class="col-sm-6">
					<small>Nome</small>
					<input type="text" name="alunonome" class="form-control" onClick="ProcuraAluno()">
				</div>

				<div class="col-sm-3">
					<small>CPF</small>
					<input type="text" name="alunocpf" class="form-control">
				</div>

				<div class="col-sm-3">
					<small>Matrícula</small>
					<input type="text" name="alunomatricula" class="form-control">
				</div>				
			</div>

			<div class="row">
				<br/>
				<div class="col-sm-6"><b>Instrutor</b></div>
				<div class="col-sm-6"><b>Veículo</b></div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<select name="cboInstrutorPrin" id="cboInstrutorPrin" class="form-control select2">
						<option value=""></option>
						<?php foreach ($instrutores as $instrutor) { ?>
								<option value="<?php echo $instrutor['usu_id']?>"> <?php echo $instrutor['usu_nomecompleto']?> </option>
						<?php } ?>
					</select>
				</div>

				<div class="col-sm-6">
					<select name="cboVeicPrin" id="cboVeicPrin" class="form-control select2">
						<option value=""></option>
						<?php foreach ($veiculos as $veic) { ?>
								<option value="<?php echo $veic['veic_id']?>"> <?php echo $veic['veic_descricao']?> </option>
						<?php } ?>
					</select>
				</div>
			</div>



			<div class="row">
				<br/>
				<div class="col-sm-12"><b>Aulas</b></div>
				<hr/>
				
			</div>

			<div class="box" style="background-color: #fff;">
				<div id='calendar'></div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<button onClick="AddLine()" class=" btn btn-info btn-xs"> <i class="fa fa-plus"></i> Adicionar</button>
				</div>
				<hr/>
			</div>			

			<div class="aulas-h" >
				<div class="row" id="">
					<div class="col-sm-2">
						Inicio</div>
					<div class="col-sm-2">
						Fim</div>
					<div class="col-sm-4">
						Instrutor</div>

					<div class="col-sm-3">
						Veículo</div>	

					<div class="col-sm-1 text-right" style="padding-top: 22px;">

					</div>
				</div>			
			</div>


			<div class="aulas-lines" >

				<div class="row hide " id="aula-0" data-id="0">
					<div class="aula-linha" style="margin-top: 8px;">
						<div class="col-sm-2">
							<input type="text" class="dataini form-control datetimepicker datetimepickerini"></div>

						<div class="col-sm-2">

							<input type="text" class="datafim form-control datetimepicker datetimepickerend"></div>

						<div class="col-sm-4">
							
							<select name="cboInstrutor" class="form-control select cboInstrutor">
								<option value=""></option>
								<?php foreach ($instrutores as $instrutor) { ?>
										<option value="<?php echo $instrutor['usu_id']?>"> <?php echo $instrutor['usu_nomecompleto']?> </option>
								<?php } ?>
							</select>
						</div>

						<div class="col-sm-3">
							
							<select name="cboVeic" class="form-control select cboVeic">
								<option value=""></option>
								<?php foreach ($veiculos as $veic) { ?>
										<option value="<?php echo $veic['veic_id']?>"> <?php echo $veic['veic_descricao']?> </option>
								<?php } ?>
							</select>
						</div>	

						<div class="col-sm-1 text-right" style="padding-top: 2px;">
							<button onClick="RemLine(this)" class=" btnRem btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </button>
							<button onClick="AddLine()" class=" btn btn-info btn-xs hide"> <i class="fa fa-plus"></i> </button>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</section>

<script>



	function datep(id) {
		$('.datetimepicker').datetimepicker({
			language: 'pt-BR',
			format: "dd/mm/yyyy hh:ii",
	        autoclose: true,
	        todayBtn: true,
	        minuteStep: 5,
	        startDate: "<?php echo date("d/m/Y h:i")?>",
		});

		$("#aula-"+id+" .datetimepickerini").datetimepicker()
		.on('changeDate', function(ev){
			newDate = $("#aula-"+id+" .datetimepickerini").val();
			setTimeout( function(){
				$("#aula-"+id+" .datetimepickerend").datetimepicker('setStartDate', newDate);
			},100)
			
		});
	}
	//datep(0);

	
	var content;
	var nAulas;
	nAulas = 1;
	function AddLine() {

		idAtual = 0;
		nTotAulas = 0;

		$(".aula-linha").each(function() {
			idAtual = $(this).parent().attr("data-id");
			//$(this).attr("id","aula-"+idAtual);
			idAtual++;
			nTotAulas++;
			
		});

		content = $("#aula-0").html();
		content = "<div class='row' id='aula-"+idAtual+"' data-id='"+idAtual+"' >"+content+"</div>";
		$(".aulas-lines").append(content);

		$("#aula-"+idAtual+ " .cboInstrutor").val( $("#cboInstrutorPrin").val() );
		$("#aula-"+idAtual+ " .cboVeic").val( $("#cboVeicPrin").val() );
		datep(idAtual);
	}

	function RemLine(e) {

		id = $(e).parent().parent().parent().attr("data-id");

		if(id >= 1 ) {
			$("#aula-"+id).remove();
		}
	}


	var aulas;
	aulas = [];
	function PrepAulas() {
		aulas = [];

		i = 0;
		$(".aula-linha").each(function() {
			id = $(this).parent().attr("data-id");
			if(id > 0) {
				aulas.push({
					'dataini' : $("#aula-"+id+" .dataini").val(),
					'datafim' : $("#aula-"+id+" .datafim").val(),
					'instrutor' : $("#aula-"+id+" .cboInstrutor").val(),
					'veiculo' : $("#aula-"+id+" .cboVeic").val(),
				});
			}
			i++;
		});

	}


	

</script>