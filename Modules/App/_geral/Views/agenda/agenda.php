<link rel="stylesheet" href="/Public/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="/Public/plugins/fullcalendar/fullcalendar.print.css" media="print">


<script src="/Public/plugins/fullcalendar/lib/moment.min.js"></script>
<script src="/Public/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/Public/plugins/fullcalendar/gcal.min.js"></script>
<script src='/Public/plugins/fullcalendar/locale/pt-br.js'></script>

<link rel="stylesheet" href="/Public/plugins/bootstrapdatepicker/css/bootstrap-datetimepicker.min.css">
<script src='/Public/plugins/bootstrapdatepicker/js/bootstrap-datetimepicker.min.js'></script>
<script src='/Public/plugins/bootstrapdatepicker/js/locales/bootstrap-datetimepicker.pt-BR.js'></script>


<section class="content asec-box" id="agenda">
    <h3 class="page-title">Agenda</h3>
    <br/>

  <div class="box no-print">
    <div class="box-header">
      <a href="<?php echo $this->Link('Agenda',"NovoEvento");?>" ><span class="btn btn-info hide btnRel"> <i class="fa fa-plus"></i> Adicionar</span> </a>
      <a href="<?php echo $this->Link('Agenda',"Relatorios");?>" ><span class="btn btn-info btnRel"> <i class="fa fa-list"></i> Relatórios</span> </a>
    </div>
  </div>

  
  <div class="box no-print">
    <div class="box-header">
      <?php if($aluno) { ?>
        Aluno Selecionado: <br/>
        <b><?php echo $aluno->usu_nomecompleto;?></b>
      <?php } else { ?>

        <b class="text-red">Nenhum aluno selecionado!
        <?php } ?>
    </div>
  </div> 
  

	<div class="box" style="background-color: #fff;">
		<div id='calendar'></div>
	</div>

<!-- Modal -->
<?php if(isset($_GET['cod']) && $_GET['cod'] != '') { ?>
  <form id="frmNewLanc">
  <div class="modal fade" id="modal-newlanc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Novo Lançamento</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div style="font-size: 18px;
                          text-align: center;
                          font-weight: bold;
                          color: chocolate;
                          border-bottom: solid 1px;
                          padding-bottom: 5px;">
                  <?php echo $aluno->usu_nomecompleto;?>
                <input type="hidden" name="alunoid" class="form-control" value="<?php echo $aluno->usu_id;?>" readonly>
                <input type="hidden" name="aluno" class="form-control" value="<?php echo $aluno->usu_nomecompleto;?>" readonly>
              </div>
            </div>
          </div>

          <div class="row" style="margin-top: 10px">
            <div class="col-sm-6" >
              Instrutor
              <select name="cboInstrutorPrin" id="cboInstrutorPrin" class="form-control select2">
                <option value=""></option>
                <?php foreach ($instrutores as $instrutor) { ?>
                    <option value="<?php echo $instrutor['usu_id']?>"> <?php echo $instrutor['usu_nomecompleto']?> </option>
                <?php } ?>
              </select>
            </div>

            <div class="col-sm-6" >
              Veículo
              <select name="cboVeicPrin" id="cboVeicPrin" class="form-control select2">
                <option value=""></option>
                <?php foreach ($veiculos as $veic) { ?>
                    <option value="<?php echo $veic['veic_id']?>"> <?php echo $veic['veic_descricao']?> </option>
                <?php } ?>
              </select>
            </div>     
          </div>

          <div class="row" style="margin-top: 10px">
            <div class="col-sm-6">
              Início
              <input type="text" name="dini" class="dataini form-control datetimepicker datetimepickerini" autocomplete="off">
            </div>

            <div class="col-sm-6">
              Fim
              <input type="text" name="dfim" class="datafim form-control datetimepicker datetimepickerend" autocomplete="off">
            </div>
          </div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
  </form>
<?php } ?>

<div id="res"></div>
</section>

	<script>
 $(document).ready(function() {
  

	var initialLocaleCode = 'pt-BR';
  var startDate;
  var endDate;
  
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      //defaultView: 'agendaWeek',
      nowIndicator: true,  
      slotDuration: '00:05',
      locale: initialLocaleCode,
      defaultDate: '<?php echo date("Y-m-d")?>',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
      editable: false,
      select: function(start, end) {
        startDate = start;
        endDate = end;
          var dataIni = moment(start).format("DD/MM/YYYY HH:mm");
          var dataFim = moment(end).format("DD/MM/YYYY HH:mm");

          $("#modal-newlanc").modal('show');
          $(".dataini").val(dataIni);
          $(".datafim").val(dataFim);
          /*
          var title = "MARIA JOSE";
          var eventData;
          if (title) {
            eventData = {
              title: title,
              start: start,
              end: end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
          }
          */
          $('#calendar').fullCalendar('unselect');
      },
      eventDrop: function(event, delta, revertFunc) {
        alertify.confirm("Atenção", "Confirmar edição de evento ?",
          function() {
              id = event.id;
              startDate = event.start;
              

              days = delta._days;

              console.log(days);

              var dataIni = moment(startDate).format("DD/MM/YYYY HH:mm");

              if(days >= 1) {
                var dataFim = moment(dataIni,"DD/MM/YYYY HH:mm").add('subtract', days).format("DD");
              } else {
                days = days*-1;
                var dataFim = moment(dataIni,"DD/MM/YYYY HH:mm").subtract('add', days).format("DD");
              }
              r = UpdateEventStartEnd(id,dataIni,dataFim);

              if(r !== 1) {
                revertFunc();    
              }

          },

          function() {
            revertFunc();
          });
      },
      eventClick: function(calEvent, jsEvent, view) {
        var idEvt = calEvent.id;
        ModalEdt(idEvt);
        //alert('Event: ' + calEvent.id);
        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        //alert('View: ' + view.name);

        // change the border color just for fun
        //$(this).css('border-color', 'red');
      },
    


      eventLimit: false, // allow "more" link when too many events
      events : <?php echo $eventos; ?>,
      /*
      events: [
        {
          title: 'All Day Event',
          start: '2018-09-13',
        },
        {
          id: 1054,
          title: 'Maria',
          start: '2018-09-07T10:00:00',
          end: '2018-09-07T10:45:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2018-09-13T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2018-09-13T16:00:00'
        },
        {
          title: 'Conference',
          start: '2018-03-11',
          end: '2018-03-13'
        },
        {
          title: 'Meeting',
          start: '2018-03-12T10:30:00',
          end: '2018-03-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2018-03-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2018-03-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2018-03-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2018-03-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2018-03-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2018-09-14'
        }
      ]
      */


    });
  

    var instrut;
    var veic;
    $("#frmNewLanc").on('submit', function(e){
      e.preventDefault();
      
      var datas = $("#frmNewLanc").serializeArray();
          instrut = $("#cboInstrutorPrin option:selected").val();
          veic    = $("#cboVeicPrin option:selected").val();
          dIni    = $(".datetimepickerini").val();
          dFim    = $(".datetimepickerend").val();
      /**/
      if(instrut == '') {
        Alerta("Informe um instrutor!");
        return; }

      if(veic == '') {
        Alerta("Informe um Veículo!");
        return; }

      if(dIni == '') {
        Alerta("Informe uma data de inicio!");
        return; }

      if(dFim == '') {
        Alerta("Informe uma data de fim!");
        return; }
      /**/
      dIni = moment(dIni, 'DD/MM/YYYY HH:mm')
      dFim = moment(dFim, 'DD/MM/YYYY HH:mm')
      
      /*
      eventData = {
          title : 'Maria !!!',
          start :  moment(dIni).format('YYYY-MM-DD HH:mm:ss'),
          end   :  moment(dFim).format('YYYY-MM-DD HH:mm:ss'),
      };
      $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
      */

      $.ajax({
        url: "<?php echo $this->link('agenda/novoevento/')?>",
        data: datas,
        type: 'POST',
        dataType: 'JSON',
        success: function(res) {
          if(res.erro != 0) {
            Alerta(res.mensagem);
            return;
          }

          if(res.erro == 0) {
            eventData = {
                id    : res.id,
                title : res.title,
                start : res.start,
                end   : res.end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
            $(".modal").modal('hide');


          }


        },
        error: function(e,x,s) {
          Alerta(x);
        }
      });
    })

    $('.datetimepicker').datetimepicker({
      language: 'pt-BR',
      format: "dd/mm/yyyy hh:ii",
          autoclose: true,
          todayBtn: true,
          minuteStep: 5,
          startDate: "<?php echo date("d/m/Y h:i")?>",
    });

    $(".datetimepickerini").datetimepicker()
    .on('changeDate', function(ev){
      $(".datetimepickerend").val('');
      newDate = $(".datetimepickerini").val();
      setTimeout( function(){
        $(".datetimepickerend").datetimepicker('setStartDate', newDate);
      },100)
      
    });

    var datasx = [];
    function UpdateEventStartEnd(id,newini,newend) {
      

      datasx = {'dataini' : newini, 'datafim': newend, 'id': id };
      //console.log(datasx);
      $.ajax({
        url: "<?php echo $this->link('agenda/updatedates/')?>",
        data: datasx,
        type: 'POST',
        dataType: 'JSON',
        success: function(res) {
          if(res.erro != 0) {
            Alerta(res.mensagem);
            return 0;
          }

          return 1;
        },
        error: function(e,x,s) {
          Alerta(s);
          return 0;
        }
      });
    }

    function ModalEdt(id) {
      $("#res").html('');
      $.ajax({
        url : "<?php echo $this->link('agenda/edtEvt/&codee=')?>"+id,
        success: function(res) {
            $("#res").html(res);
        },
      });
    }

  });

	</script>