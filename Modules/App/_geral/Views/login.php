<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Acesso ao Sistema</title>
  <!-- Tell the browser to be responsive to screen width -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/Public/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/Public/plugins/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="/Public/css/AdminLTE.min.css">
  
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page" style="background: #2e6b92">
<div class="login-box">
  <div class="login-logo white">
    <a href="javascript: void(0)"><b>SigAuto</b><br/> Acesso ao Sistema</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body cont">
    <p class="login-box-msg">Informe seus dados para iniciar a sessão</p>


    <form action="<?php echo Permalink('login','acessar')?>" method="post" id="frmLogin">
        <?php if(isset($view_retorno)) { ?>
            <?php echo $view_retorno;?>
        <?php }?>
        <div class="form-group has-feedback">
            <label>Login</label>
            <input type="text" class="form-control" placeholder="Login" id="login" name="login" autocomplete="off" value="" />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
            
      <div class="form-group has-feedback">
      	<label>Senha</label>
        <input type="text" class="form-control" placeholder="******" id="password" name="password" autocomplete="off" value="" />
        <input type="password"  class="form-control hide" placeholder="******" id="password2" name="password2" autocomplete="off"  />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Acessar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

   

    <a href="#">Esqueci minha senha</a><br>
    
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<div style="display: none;" id="ld">
	<center>
		<img src="/assets/img/ld03.gif" />
	</center>
</div>

<script src="/Public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
<script>
	$(function() {
            $("#codigo").focus();
            $("#login").focus();

            setTimeout(function(){
                $("#password").attr("type","password");
            },10);
	});
</script>

</body>
</html>
