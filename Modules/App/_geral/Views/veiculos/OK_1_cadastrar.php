<div class="content">
    <h3 class="page-title">Cadastrar Veículos</h3>
    <br/>
    <div class="form">
        <form class="" id="frmAddVeiculo" action="<?php echo Permalink("veiculos", "cadastrar")?>" method="post" >
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Tipo</label>
                    <select name="tipo" id="tipo" class="form-control">
                        <option value="">Selecione</option>
                        <option value="1">Carro</option>
                        <option value="2">Moto</option>
                        <option value="3">Caminhão</option>
                    </select>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Marca</label>
                    <select name="marca" id="marca" class="form-control">
                        <option value=""></option>
                    </select>
                </div>
                
                <div class="form-group col-sm-4">
                    <label>Modelo</label>
                    <select name="modelo" id="modelo" class="form-control select2">
                        <option value=""></option>
                    </select>
                </div>
                
                <div class="form-group col-sm-4">
                    <label>Veículo</label>
                    <select name="veiculo" id="veiculo" class="form-control select2">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <hr/>
            
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Veículo</label>
                    <input type="text" name="txtVeiculo" class="form-control txtEdt" id="txtVeiculo" readonly=""/>
                </div>
                
                <div class="col-sm-4 form-group">
                    <label>Combustível</label>
                    <input type="text" name="txtCombustivel" class="form-control txtEdt" id="txtCombustivel" readonly=""/>
                </div>
                
                <div class="col-sm-4 form-group">
                    <label>Ano/Modelo</label>
                    <input type="text" name="txtAnoModelo" class="form-control txtEdt" id="txtAnoModelo" readonly=""/>
                </div>
                
                <div class="col-sm-4 form-group">
                    <label>Marca</label>
                    <input type="text" name="txtMarca" class="form-control txtEdt" id="txtMarca" readonly=""/>
                </div>                
                
                <div class="col-sm-4 form-group">
                    <label>Valor</label>
                    <input type="text" name="txtValor" class="form-control txtEdt" id="txtValor" readonly=""/>
                </div>                
            </div>
            <a href="javascript: void(0)" onClick="txtEditar('e')" class="btn btn-primary btnEdt btn-xs btn-flat">Corrigir Dados</a>
            <a href="javascript: void(0)" onClick="txtEditar('c')" class="btn btn-success btnEdtOK btn-xs btn-flat" style="display: none;">Finalizar</a>
            <hr/>
        </form>
    </div>
</div>

<script> 
    var MarcaID, ModeloID, TipoID, TipoName;
    $("#tipo").on("change",function() {
        TipoID = $("#tipo option:selected").val();
        if(TipoID == "1") {TipoName = "carros"; }
        if(TipoID == "2") {TipoName = "motos"; }
        if(TipoID == "3") {TipoName = "caminhoes"; }
        
        
        CarregaMarcas();
    });
    
    function txtEditar(act) {
        
        if(act == "e") {
            $(".txtEdt").removeAttr("readonly");
            $(".btnEdt").hide();
            $(".btnEdtOK").show();
        }
        
        if(act == 'c') {
            $(".txtEdt").attr("readonly","readonly");
            $(".btnEdt").show();
            $(".btnEdtOK").hide();            
        }
    }
    
    function CarregaMarcas() {
      $("#marca").html('');
      $("#modelo").html('');
      x = "<option>Selecione a marca</option>";
      $.ajax({
          url : "http://fipeapi.appspot.com/api/1/"+TipoName+"/marcas.json",
          dataType: 'json',
          success: function(res) {
            $.each(res, function(i, item) {
                x = x + '<option value="'+item["id"]+'">'+item["fipe_name"]+'</option>';
            });              
          },
          error: function(e,x,s) {
              
          },
          complete: function(e) {
              $("#marca").html(x);
          }
      });
    };    
    
    $("#marca").on("change", function() {
       MarcaID = $("#marca option:selected").val();
       CarregaModelos();
    });
    
    
    function CarregaModelos() {
        x = "<option>Selecione o modelo</option>";
        $.ajax({
          url : "http://fipeapi.appspot.com/api/1/"+TipoName+"/veiculos/"+MarcaID+".json",
          dataType: 'json',
          success: function(res) {
            $.each(res, function(i, item) {
                x = x + '<option value="'+item["id"]+'">'+item["fipe_name"]+'</option>';
            });              
          },
          error: function(e,x,s) {
              
          },
          complete: function(e) {
              $("#modelo").html(x);
          }
      });        
    }
    
    $("#modelo").on("change", function() {
        ModeloID = $("#modelo option:selected").val();
        CarregaVeiculos();
    })
    
    function CarregaVeiculos() {
      x = "<option>selecione Ano/Modelo</option>";
      $.ajax({
          url : "http://fipeapi.appspot.com/api/1/"+TipoName+"/veiculo/"+MarcaID+"/"+ModeloID+".json",
          dataType: 'json',
          success: function(res) {
            $.each(res, function(i, item) {
                x = x + '<option value="'+item["id"]+'">'+item["name"]+'</option>';
            });              
          },
          error: function(e,x,s) {
              
          },
          complete: function(e) {
              $("#veiculo").html(x);
          }
      });                
    }
    
    $("#veiculo").on("change",function(){
        VeiculoID = $("#veiculo option:selected").val();
        $.ajax({
                 
          url : "http://fipeapi.appspot.com/api/1/"+TipoName+"/veiculo/"+MarcaID+"/"+ModeloID+"/"+VeiculoID+".json",
          dataType: 'json',
          success: function(res) {
            $("#txtVeiculo").val(res.veiculo);
            $("#txtCombustivel").val(res.combustivel);
            $("#txtAnoModelo").val(res.ano_modelo);
            $("#txtMarca").val(res.marca);
            $("#txtValor").val(res.preco);
            
          },
          error: function(e,x,s) { },
          complete: function(e) { }
      });
    });
    
    $(function(){
        //CarregaMarcas();
    });
</script>
