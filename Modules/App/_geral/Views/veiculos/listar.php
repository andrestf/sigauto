
<div class="content">
    <h3 class="page-title">Veículos</h3> <br/>

    <div class="box">
        <div class="box-header">
            <a href="<?php echo $this->Link('veiculos',"relatorio");?>" ><span class="btn btn-info btnRel"> <i class="fa fa-list-alt"></i> Relatórios</span> </a>
            <a href="<?php echo $this->Link('veiculos',"cadastrar");?>" ><span class="btn btn-primary btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
        </div>
    </div>


    <div class="box box-primary ">
        <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="fa fa-car"></i>
                    Cadastrados</h3>
        </div>    


        <div class="box-body">
            <form id="frmBuscaVeic" onsubmit="return;">
                <input type="hidden" name="filtro" value="filtro" class="form-control"/>

                <div class="row form-group">
                    <div class="col-sm-2 col-xs-6">
                        <label>Placa</label>
                        <input type="text" name="placa" class="form-control"/>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <label>Descrição</label>
                        <input type="text" name="descricao" class="form-control"/>
                    </div>

                    <div class="col-sm-3">
                        <label>Local</label>
                        <select name="local" id="local" class="form-control select2">
                            <option value="">Selecione</option>
                            <?php foreach ($locais as $local) { ?>
                                <option value="<?php echo $local['lcl_id']; ?>" ><?php echo $local['lcl_descri']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>Marca</label>
                        <select name="marca" id="marca" class="form-control select2">
                            <option value="">Selecione</option>
                            <?php foreach ($marcas as $marca) { ?>
                                <option value="<?php echo $marca['veicmarc_cod']; ?>" ><?php echo $marca['veicmarc_descri']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <label>Tipo</label>
                            <select name="tipo" id="tipo" class="form-control select2">
                                <option value="">Selecione</option>
                                <?php foreach ($tipos as $tipo) { ?>
                                    <option value="<?php echo $tipo['veictipo_cod']; ?>"><?php echo $tipo['veictipo_descri']; ?></option>
                                <?php } ?>
                            </select>
                    </div>

                    <div class="col-sm-3">
                        <label>Situação</label>
                        <select name="situacao" id="situacao" class="form-control select2">

                            <option value=""> Selecione </option>
                            <option value="ATIVO"> Ativo </option>
                            <option value="INATIVO"> Inativo </option>
                            <option value="VENDIDO"> Vendido </option>

                        </select>
                    </div> 

                    <div class="col-sm-2">
                        <label>Limite</label>
                        <select class="form-control" name="limite" >
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                            <option value="100" selected="selected">100</option>
                        </select>
                    </div>

                    <div class="col-sm-2">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                        </span>
                    </div>
                </div>
            </form>

            <hr/>

            <table class="table table-borderedx table-condensed table-gridx table-hover table-striped table-responsive">
                <thead>
                    <tr>
                        <th width="80">Cód</th>
                        <th width="200">Tipo</th>
                        <th width="*">Descrição</th>                
                        <th width="80">Placa</th>
                        <th width="80">Ano</th>
                        <th width="20">&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="ListaVeiculos">
                    <tr><td colspan='6'><center> <img src="/Public/img/ld03.gif" /> </center></td></tr>
                </tbody>
            </table>
        </div>
    </div>

</div>



<script>


var xLD = "<tr><td colspan='6'><center> <img src=\"/Public/img/ld03.gif\" /> </center></td></tr>";
$("#frmBuscaVeic").on("submit", function(e) {
    e.preventDefault();

    Lista();
})

function Lista() {
    $("#ListaVeiculos").html(xLD);
    datas = $("#frmBuscaVeic").serializeArray();
    $.ajax({
        data: datas,
        type: 'post',
        url : "<?php echo Permalink("veiculos","listar","tipo=table&filtro=1")?>",
        //url : "/index.php?route=alunos/listagem/?&"+$datas,
        success: function(res) {
            $("#ListaVeiculos").html(res);
        },
        error: function(e,x,s) {
          $("#ListaVeiculos").html("<tr><td colspan='6'>"+s+"</td></tr>");  
        }
    })
}

Lista();



</script>
