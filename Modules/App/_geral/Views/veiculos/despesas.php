<?php
//echo $this->CCUSTO_DESPVEIC; ?>
<div class="content">
    <h3 class="page-title">Despesas Veículos</h3> <br/> 

    <div class="box">
        <div class="box-header">
            <span class="pull-right">
                <a href="#" ><span class="btn btn-default btn-sm btnAddDesp"> <i class="fa fa-plus"></i> Nova despesa</span></a>
            </span>
        </div>
    </div>


    <div class="box box-primary ">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-car"></i>
                Despesas - <?php echo $veiculo['veicmarc_descri'] . " " . $veiculo['veic_modelo']  ?></h3>
        </div>


        <div class="box-body">
            <form id="frmBuscaDespVeic" onsubmit="return;">
                <input type="hidden" name="filtro" value="filtro" class="form-control"/>
                <input type="hidden" name="veiculo" value="<?php echo $_GET['cod'];?>" class="form-control"/>

                <div class="row form-group">
                    <div class="col-sm-2">
                        <label>Data Ini</label>
                        <input type="text" name="dataIni" class="form-control datepicker"/>
                    </div>

                    <div class="col-sm-2">
                        <label>Data Fim</label>
                        <input type="text" name="dataFim" class="form-control datepicker"/>
                    </div>

                    <div class="col-sm-4">
                        <label>Tipo</label>
                            <select name="tipo" id="tipo" class="form-control select2">
                                <option value="">Selecione</option>
                                <?php foreach ($tiposDesp as $tipo) { ?>
                                    <option value="<?php echo $tipo['veicdesp_cod']; ?>"><?php echo $tipo['veicdesp_descri']; ?></option>
                                <?php } ?>
                            </select>
                    </div>

                    <div class="col-sm-2">
                        <label>Limite</label>
                        <select class="form-control" name="limite" >
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                        </select>
                    </div>

                    <div class="col-sm-2">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                        </span>
                    </div>
                </div>
            </form>

            <hr />


            <table class="table table-borderedx table-condensed table-gridx table-hover table-striped table-responsive">
                <thead>
                    <tr>
                        <th width="200">Tipo</th>
                        <th width="*">Descrição</th>                
                        <th width="120">Valor</th>
                        <th width="120">Data</th>
                        <th width="50">&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="ListaDespesas">
                    <tr><td colspan='6'><center> <img src="/Public/img/ld03.gif" /> </center></td></tr>
                </tbody>
            </table>            



        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ModalAddDesp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Adicionar Despesa</h4>
      </div>
      <div class="modal-body">
        <form id="frmAddDesp" class="">
        <input type="hidden" name="cod" value="<?php echo $veiculo['veic_id'];?>" class="form-control" />
            <div class="row form-group">
                <div class="col-sm-4">
                    <label>Tipo</label>
                    <select name="tipo" id="AddDespTipo" class="form-control select2">
                        <option value="">Selecione</option>
                        <?php foreach ($tiposDesp as $tipo) { ?>
                            <option value="<?php echo $tipo['veicdesp_cod']; ?>"><?php echo $tipo['veicdesp_descri']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4">
                    <label>Valor</label>
                    <input type="text" name="valor" value="" class="form-control" onkeyup="formataValorNovo(this)" />
                </div>

                <div class="col-sm-4">
                    <label>Data</label>
                    <input type="text" name="data" value="<?php echo date('d/m/Y'); ?>" class="form-control datepicker" />
                </div>

                <div class="col-sm-4">
                    <label>Km</label>
                    <input type="number" name="kmatual" value="" class="form-control" />
                </div>


                <div class="col-sm-4">
                    <label for="respon">Responsável </label>
                    <select name="respon" id="respon" class="form-control select2">
                        <option value="">Selecione</option>
                        <?php foreach ($respons as $respon) { ?>
                            <option  <?php echo ($veiculo['veic_respon'] == $respon['usu_id']) ? "selected='selected'" : "" ?>  value="<?php echo $respon['usu_id']; ?>"><?php echo $respon['usu_apelido']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4">
                    <label>Nº Recibo</label>
                    <input type="text" name="recibo" value="" class="form-control" />
                </div>                

            
                
                <div class='col-sm-4' style="margin-top: 5px;">
                    <label>Centro de Custo</label>
                    <select name="ccusto" id="centrocusto" class="select2 form-control" >
                         <option value="">&nbsp;</option>
                         <?php foreach ($CentroDeCustos as $centrocusto) {?>
                         <option value="<?php echo $centrocusto['cec_codigo']?>" ><?php echo $centrocusto['cec_descri']?> </option>
                         <?php } ?>
                     </select>
                </div>
                
                <div class='col-sm-4' style="margin-top: 5px;">
                    <label>Plano de Contas 2</label>
                    <select name="pconta" id="planoconta" class="select2x form-control" >
                        <option value="">&nbsp;</option>
                        <?php foreach ($PlanoDeContas as $planocontas) {?>
                        <option value="<?php echo $planocontas['plc_cdPlano']?>" ><?php echo $planocontas['plc_descri']?> </option>
                        <?php } ?>
                    </select>
                </div> 
                
                <?php if($this->DESPVEIC_LANCAFINAN) { ?> 
                <div class='col-sm-4' style="margin-top: 5px;">
                    <label>Lança Financeiro</label>
                    <select name="lancafinan" id="lancafinan" class="select2x form-control" >
                        <option value="">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div> 
                <?php } ?>
        
            </div>
        
            <div class="row form-group">
                <div class="col-sm-12">
                    <label>Descrição</label>
                        <input type="text" name="descri" value="" class="form-control" />
                </div>
            </div>



        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary btnCconfirmaAddDesp">Confirmar</button>
      </div>
    </div>
  </div>
</div>





<script>

$(".btnAddDesp").on("click", function() {
    $("#ModalAddDesp").modal('show');
})

$(".btnCconfirmaAddDesp").on("click", function() {
    datas = $("#frmAddDesp").serializeArray();

    $.ajax({
        type: 'post',
        dataType: 'json',
        data: datas,
        url : "<?php echo Permalink("veiculos","DespesasAdicionar","cod=".$_GET['cod'])?>",

        success: function(e) {

            if(e.erro != "") {
                alertify.error(e.mensagem);
                return;
            }

            alertify.success(e.mensagem);

            setTimeout(function() {
                window.location = "<?php echo Permalink("veiculos","despesas","cod=".$_GET['cod'])?>";    
            },300);

        }
    })
})

var xLD = "<tr><td colspan='6'><center> <img src=\"/Public/img/ld03.gif\" /> </center></td></tr>";
$("#frmBuscaDespVeic").on("submit", function(e) {
    e.preventDefault();

    Lista();
})

function Lista() {
    $("#ListaDespesas").html(xLD);
    datas = $("#frmBuscaDespVeic").serializeArray();
    $.ajax({
        data: datas,
        type: 'post',
        url : "<?php echo Permalink("veiculos","DespesasLista","tipo=table&filtro=1")?>",
        success: function(res) {
            $("#ListaDespesas").html(res);
        },
        error: function(e,x,s) {
          $("#ListaDespesas").html("<tr><td colspan='6'>"+s+"</td></tr>");  
        }
    })
}

Lista();

</script>