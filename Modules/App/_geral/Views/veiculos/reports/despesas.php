<?php
	#variaveis gerais
	$valorTotal = 0;


$xdb = new DB();
$xdb->CnCliente();
$report = new VeiculosRelatoriosModel();
$report->Campos = " * ";
#$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->CondicaoExtra = " AND veic_situacao != 'VENDIDO' ";
$report->OrderBy = " ORDER BY lcl_descri ";
$report->GroupBy = " ";
$report->Debug  = true;

$dados  = $report->Gerar();
	
/* */
	$dataIni = isset( $_POST['dataini'] ) ? $_POST['dataini'] : "";
	$dataFim = isset( $_POST['datafim'] ) ? $_POST['datafim'] : "";
	######################################################################################
	$planoconta      = isset( $_POST['planoconta'] ) ? $_POST['planoconta'] : "";
	$centrocusto     = isset( $_POST['centrocusto'] ) ? $_POST['centrocusto'] : "";


        $wPlanoConta = "";
        if($planoconta != '') {
            $_pc = "";
            foreach($planoconta as $pc ){
                $_pc .= "'$pc',";
            }

            $_pc = substr($_pc,0,-1);
            $wPlanoConta = " and veicxdesp_pconta in ($_pc)";
        }        


        $wCentroCusto = "";
        if($centrocusto != '') {
            $_cc = "";
            foreach($centrocusto as $cc ){
                $_cc .= "'$cc',";
            }

            $_cc = substr($_cc,0,-1);
            $wCentroCusto = " and veicxdesp_ccusto in ($_cc)";
        }

	######################################################################################

	$wDataIni = "";
	if($dataIni != '') {
		$wDataIni = " AND veicxdesp_data >= '" . DataDB($dataIni). "'";
	}

	$wDataFim = "";
	if($dataFim != '') {
		$wDataFim = " AND veicxdesp_data <= '" . DataDB($dataFim). "'";
	}
/**/
?>

<?php foreach ($dados as $veic) {
	#variaveis por veiculo
	$valorTotalViculo = 0; 

	
	?>



	<?php $query = "SELECT * FROM veic_veicxdespesas LEFT OUTER JOIN veic_despesas ON veicdesp_cod = veicxdesp_despcod 
					WHERE veicxdesp_veicid = '".$veic['veic_id']."' 
				
					"; ?>
	

	<?php $xdb->ExecQuery($query); $desps = $xdb->result_array(); ?>
	<?php if($desps) { ?>
			<hr/>
			<span style="font-weight: bold; ">Veiculo: </span><?php echo $veic['veicmarc_descri']; ?> - <?php echo $veic['veic_modelo']; ?>
			<br/>
			<?php foreach ($desps as $desp) { ?>
				<?php echo $desp['veicdesp_descri']; ?> Valor: R$ <?php echo $desp['veicxdesp_valor']; ?> <br/>

			<?php 
				$valorTotalViculo += $desp['veicxdesp_valor'];
				$valorTotal += $desp['veicxdesp_valor'];
			?>

			<?php }//foreach despesas ?>
			
			<br/>
			<span class="text-red">Valor Total do Veiculo: R$ <?php echo number_format($valorTotalViculo,2,",","."); ?></span>
	<?php }// if se tem despesas ?>
<?php }//foreach veiculos ?>

<hr />
<div class="text-center text-red">
Valor Total Despesas: <?php echo number_format($valorTotal,2,",","."); ?>
</div>