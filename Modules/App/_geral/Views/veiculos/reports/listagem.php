<?php 

$report = new VeiculosRelatoriosModel();
$report->Campos = " * ";
#$report->CondicaoExtra = " AND amf_dataprocesso = '$data' AND (amf_examepratico='E') ";
$report->CondicaoExtra = " AND veic_situacao != 'VENDIDO' ";
$report->OrderBy = " ORDER BY lcl_descri ";
$report->GroupBy = " GROUP BY veic_id ";

$dados  = $report->Gerar();
?>

<?php if(!$dados) { ?>
	nenhum resultado
<?php } ?>

<section class="content" >
    <?php if ($dados->num_rows >= 1) {?>
        <?php $i=0; while($report = $dados->fetch_assoc()) { ?>

			<center>
			 <h1> Listagem de veículos </h1>
			</center>
			
			<hr/>

			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Local</th>
						<th>Cód</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Placa</th>
						<th>Ano</th>
						<th>Renavam</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($dados as $veic) { ?>
					<?php $i++; ?>

					<tr>
						<td><?php echo $veic['lcl_descri']; ?></td>
						<td><?php echo $veic['veic_id']; ?></td>
						<td><?php echo $veic['veicmarc_descri']; ?></td>
						<td><?php echo $veic['veic_modelo']; ?></td>
						<td><?php echo $veic['veic_placa']; ?></td>
						<td><?php echo $veic['veic_anofab']; ?></td>
						<td><?php echo $veic['veic_renavam']; ?></td>
					</tr>

					<?php } ?>
				</tbody>
			</table>
        <?php } //while ?>
    <?php } //if ?>

    <span style="font-size: 15px">
    	<b> Total de Veiculos Listado: &nbsp; <?php echo $i; ?> </b>
	</span>

</section>
