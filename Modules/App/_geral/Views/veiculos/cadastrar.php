
<?php if(!$veiculo) { ?>
	<div class="content">
		<div class="alert alert-info">Veiculo nao encontrado!</div>
	</div>
<?php } else { ?>
	<div class="content">
		<?php if($ficha) { ?>
			<h3 class="page-title">Ficha do Veiculo</h3>
			<br/>
			<div class="box box-body ">
				<a href="javascript: void(0);" onClick="updKm()"    class="btn btn-default" ><i class="fa fa-info"></i> Km Atual </a>
				<a href="javascript: void(0);" onClick="Cancelar()" class="btn btn-default" ><i class="fa fa-arrow-left"></i> Cancelar </a>
				<span class="pull-right btn-group">
                	<span class="btn btn-linkedin btnDespesas" > <i class="fa fa-cogs"></i> Despesas</span>
                </span>
            </div>
		<?php } else { ?>
	    	<h3 class="page-title">Cadastrar Novo Veículos</h3>
	    <?php } ?>

	    <div class="box">


			<div class="box-body">
		    	<form onsubmit="return false;" class="form-horizontal" id="frm-veiculos">
					<input type="hidden" name="id" value="<?php echo $veiculo['veic_id']; ?>" />
		    		<div class="form-group row divBottom10">
						<div class="col-md-3 col-sm-6">
		                	<label for="nomecompleto">Marca <span class="obg">*</span></label>
		                    <select name="marca" id="marca" class="form-control select2">
		                    	<option value="">Selecione</option>
		                    	<?php foreach ($marcas as $marca) { ?>
		                    		<option value="<?php echo $marca['veicmarc_cod']; ?>" <?php echo ($veiculo['veic_marca'] == $marca['veicmarc_cod']) ? "selected='selected'" : "" ?> ><?php echo $marca['veicmarc_descri']; ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

		                <div class="col-md-9 col-sm-6">
		                	<label for="modelo">Modelo <span class="obg">*</span></label>
		                    <input id="modelo" name="modelo" placeholder="Descrição" maxlength="100" class="form-control" type="text"  value="<?php echo $veiculo['veic_modelo']; ?>" >
		                </div>

						<div class="col-md-3 col-sm-3">
		                	<label for="comb">Combustível <span class="obg">*</span></label>
		                    <select name="comb" id="comb" class="form-control select2">
		                    	<option value="">Selecione</option>
		                    	<?php foreach ($combs as $comb) { ?>
		                    		<option  <?php echo ($veiculo['veic_comb'] == $comb['veiccomb_cod']) ? "selected='selected'" : "" ?> value="<?php echo $comb['veiccomb_cod']; ?>"><?php echo $comb['veiccomb_descri']; ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>



		                <div class="col-md-3 col-sm-3">
		                	<label for="anofab">Ano Fáb. <span class="obg">*</span></label>
		                	<?php $ano = (date("Y")-20);?>
		                	<?php $anoatual = date("Y")+1;?>
		                    <select name="anofab" id="anofab" class="form-control">
		                    	<option value="">Selecione</option>
		                    	<?php for($x = $ano ; $x <= $anoatual; $x++) {?>
			                    	<option  <?php echo ($veiculo['veic_anofab'] == $x) ? "selected='selected'" : "" ?>  value="<?php echo $x; ?>"><?php echo $x;?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

		                <div class="col-md-3 col-sm-3">
		                	<label for="anomod">Ano Mod. <span class="obg">*</span></label>
		                	<?php $ano = (date("Y")-20);?>
		                	<?php $anoatual = date("Y")+1;?>
		                    <select name="anomod" id="anomod" class="form-control">
		                    	<option value="">Selecione</option>
		                    	<?php for($x = $ano ; $x <= $anoatual; $x++) {?>
			                    	<option  <?php echo ($veiculo['veic_anomod'] == $x) ? "selected='selected'" : "" ?>  value="<?php echo $x; ?>"><?php echo $x;?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

		                <div class="col-md-3 col-sm-3">
			                <label>Local <span class="obg">*</span></label>
	                        <select name="local" id="local" class="form-control select2">
	                            <option value="">Selecione</option>
	                            <?php foreach ($locais as $local) { ?>
	                                <option value="<?php echo $local['lcl_id']; ?>" <?php echo ($veiculo['veic_local'] == $local['lcl_id']) ? "selected='selected'" : "" ?> ><?php echo $local['lcl_descri']; ?></option>
	                            <?php } ?>
	                        </select>
			            </div>

						<div class="col-md-12 col-sm-12">
		                	<label for="descricao">Descrição <span class="obg">*</span></label>
		                    <input id="descricao" name="descricao" placeholder="Descrição" maxlength="100" class="form-control" type="text" value="<?php echo $veiculo['veic_descricao']; ?>" />
		                </div>

		                <div class="col-md-3 col-sm-6">
		                	<label for="cor">Cor Predominante <span class="obg">*</span></label>
		                    <input id="cor" name="cor" placeholder="Cor predominante" maxlength="100" class="form-control" type="text" value="<?php echo $veiculo['veic_cor']; ?>" />
		                </div>

						<div class="col-md-3 col-sm-3">
		                	<label for="tipo">Tipo <span class="obg">*</span></label>
		                    <select name="tipo" id="tipo" class="form-control select2">
		                    	<option value="">Selecione</option>
		                    	<?php foreach ($tipos as $tipo) { ?>
		                    		<option  <?php echo ($veiculo['veic_tipo'] == $tipo['veictipo_cod']) ? "selected='selected'" : "" ?>  value="<?php echo $tipo['veictipo_cod']; ?>"><?php echo $tipo['veictipo_descri']; ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>                
					
		                <div class="col-md-3 col-sm-6">
		                	<label for="placa">Placa <span class="obg">*</span></label>
		                    <input id="placa" name="placa" placeholder="" maxlength="100" class="form-control" type="text" value="<?php echo $veiculo['veic_placa']; ?>">
		                </div>

		                <div class="col-sm-3">
		                	<label for="municipio">Municipio </label>
		                    <input id="municipio" name="municipio" placeholder="" maxlength="100" class="form-control" type="text"  value="<?php echo $veiculo['veic_municipio']; ?>"  >
		                </div>

		                <div class="col-sm-3">
		                	<label for="uf">UF </label>
		                    <input id="uf" name="uf" placeholder="" maxlength="100" class="form-control" type="text"  value="<?php echo $veiculo['veic_uf']; ?>" >
		                </div>

		                <div class="col-sm-4">
		                	<label for="chassi">Chassi </label>
		                    <input id="chassi" name="chassi" placeholder="" maxlength="100" class="form-control" type="text" value="<?php echo $veiculo['veic_chassi']; ?>" >
		                </div>

		                <div class="col-sm-4">
		                	<label for="renavam">Renavam </label>
		                    <input id="renavam" name="renavam" placeholder="" maxlength="100" class="form-control" type="text"  value="<?php echo $veiculo['veic_renavam']; ?>" >
		                </div>

		                <div class="col-sm-3">
		                	<label for="respon">Responsável </label>
		                    <select name="respon" id="respon" class="form-control select2">
		                    	<option value="">Selecione</option>
		                    	<?php foreach ($respons as $respon) { ?>
		                    		<option  <?php echo ($veiculo['veic_respon'] == $respon['usu_id']) ? "selected='selected'" : "" ?>  value="<?php echo $respon['usu_id']; ?>"><?php echo $respon['usu_apelido']; ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

		                <div class="col-sm-3">
		                	<label for="mesipva">Mês IPVA </label>
		                    <select id="mesipva" name="mesipva" class="form-control">
		                    	<option value="">Selecione</option>
		                    	<?php for($m = 1; $m <= 12; $m++) { ?>
		                    		<option  <?php echo ($veiculo['veic_mesipva'] == $m) ? "selected='selected'" : "" ?>  value="<?php echo $m; ?>"><?php echo $m?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

		                <div class="col-sm-3">
		                	<label for="meslicenc">Mês Licenciamento </label>
		                    <select id="meslicenc" name="meslicenc" class="form-control">
		                    	<option value="">Selecione</option>
		                    	<?php for($m = 1; $m <= 12; $m++) { ?>
		                    		<option  <?php echo ($veiculo['veic_meslicenc'] == $m) ? "selected='selected'" : "" ?>  value="<?php echo $m; ?>"><?php echo $m?></option>
		                    	<?php } ?>
		                    </select>
		                </div>

	                    <div class="col-sm-3">
	                        <label>Situação</label>
	                        <select class="form-control" name="situacao">
	                            <option value="ATIVO" <?php echo ($veiculo['veic_situacao'] == "ATIVO") ? "selected='selected'" : '';  ?> >Ativo</option>
	                            <option value="INATIVO" <?php echo ($veiculo['veic_situacao'] == "INATIVO") ? "selected='selected'" : '';  ?> >Inativo</option>
	                            <option value="VENDIDO" <?php echo ($veiculo['veic_situacao'] == "VENDIDO") ? "selected='selected'" : '';  ?> >Vendido</option>
	                        </select>
	                    </div> 
                    
		                <div class="col-sm-12">
		                	<label for="obs">Observações </label>
		                    <textarea name="obs" id="obs" class="form-control" style="height: 100px;"><?php echo $veiculo['veic_obs']; ?></textarea>
		                </div>

		            </div>
		            <div class="form-group row divBottom10">
		            	<div class="col-sm-12">
		            		<a href="javascript: void(); " onClick="Cancelar()" class="btn btn-default" ><i class="fa fa-arrow-left"></i> Cancelar </a>
		            		<span class="pull-right">
		            			<button type="submit" class="btn btn-success btnSubmit" ><i class="fa fa-save"></i> Salvar </button>
		            		</span>
		            	</div>

		            </div>

		    	</form>
		    </div>
	    </div>
	</div>


	<style>
		.divBottom10 div {
			margin-bottom: 10px;
		}

		.prepAsecSelect {
		    position: absolute;
		    margin-top: 25px;
		    border: 0px;
		    height: 34px;
		    background: #e1e1e1;
		    width: 90px;
		}
	</style>

	<script>

		//$("#frm-veiculos .form-control").prop("disabled", true);

		var canSave = true;

		function Cancelar() {
			alertify.confirm("Atenção","Deseja realmente cancelar?",
				function() {
					window.location = "<?php echo Permalink("veiculos/")?>";	
				},
				function() { return true;}
				)
		}

		<?php if(isset($_GET['cod']) && $_GET['cod'] != '') { ?>
		function  updKm() {
			alertify.prompt('Veículo','Kilometragem atual','<?php echo $KmAtual?>'
			   , function(evt, value) { 
			   		$.ajax({
			   			data : {valor : value, cod : '<?php echo $_GET['cod']?>' },
			   			type: 'post',
			   			url : "<?php echo Permalink("veiculos/updKm/")?>",  
			   			success: function(e) {
			   				if(e == '') {
			   					alertify.success('KM alterada')
			   					return;
			   				}

			   				alertify.error("ERRO ao atualizar");
			   				return;

			   			},

			   			error: function() {
			   				alertify.error("Falha no servidor...!");
			   			}
			   		})

			   }
               , function() { 
               		alertify.error("Cancelado");
               });
		}
		<?php } ?>

		$("#frm-veiculos").on("submit", function() {
				$(".btnSubmit").attr("disabled","disabled");
				var datas = $("#frm-veiculos").serializeArray();

				if(!canSave) {
					$(".btnSubmit").removeAttr("disabled");
					return false;
				}


				canSave = false;
				$.ajax({
					data: datas,
					dataType: 'json',
					type: 'post',
					url : "<?php echo Permalink("veiculos/cadastrar/")?>",
					success: function(e) {

						if(e.erro != "") {
							alertify.error(e.mensagem);
							$(".btnSubmit").removeAttr("disabled");
							return;
						}

						alertify.success(e.mensagem);

						
						if(typeof e.id !== "undefined") {
							setTimeout(function() {
								window.location = "<?php echo Permalink("veiculos/ficha/")?>&cod="+e.id;	
							},300)

							return;
						}

						$(".btnSubmit").removeAttr("disabled");
					},
					error: function(e,x,s) {
						alertify.error(x + "<br/>" + s);	
						$(".btnSubmit").removeAttr("disabled");
					},

					complete: function() {
						
						setTimeout(function(){
							canSave = true;
						},2000)
					}
				})

		})


		<?php if($ficha) { ?>
		$(".btnDespesas").on("click", function() {
			window.location = "<?php echo Permalink("veiculos","despesas","cod=".$_GET['cod'])?>";
		})
		<?php } ?>
	</script>

<?php }//if veiculo ?>