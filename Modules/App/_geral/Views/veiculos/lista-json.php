<?php if($lista_dos_veiculos) { ?>
    <?php foreach ($lista_dos_veiculos as $veiculo) { ?>
        <tr>
            <td><?php echo $veiculo['veic_id']?></td>
            <td><?php echo $veiculo['veictipo_descri']?></td>
            <td><?php echo $veiculo['veic_descricao'] . "<small> - " . $veiculo['veiccomb_descri'] . "</small>"; ?></td>
            <td><?php echo $veiculo['veic_placa']?></td>
            <td><?php echo $veiculo['veic_anofab']?></td>
            <td><a href="<?php echo Permalink("veiculos","ficha","cod=".$veiculo['veic_id'])?>"><i class="fa fa-edit"></i></a></td>
        </tr>
    <?php } ?>
<?php } else { ?>
		<tr>
			<td colspan="6">nemhum resultado</td>
		</tr>
<?php } ?>