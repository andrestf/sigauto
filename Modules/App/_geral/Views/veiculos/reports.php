<form method="post" action="<?php echo Permalink('veiculos/RelatorioGerar/')?>" onsubmit="return true;"  id="newReportZ">
<div class="content">
 	<div class='box box-primary' style="width: 550px; margin:0 auto; ">
        <div class='box-header with-border'>
            <h3 class='box-title'>Filtro</h3>
        </div>

		<div class='box-body'>
            <div class='row form-group'>
                <div class='col-sm-8'>
                    <label>Veículo</label>
                    
                    <select name="veiculos[]" id="veiculos" class="form-control select2"  multiple="multiple">
                    	<option value=""></option>
                    	<?php foreach ($veiculos as $veiculo) { ?>
	                    	<option  value="<?php echo $veiculo['veic_id']; ?>"><?php echo $veiculo['veicmarc_descri'] . " - " . $veiculo['veic_modelo'];?></option>

                    	<?php } ?>
                    </select>

                </div>

                <div class="col-md-4">
                    <label for="local">Local</label>
                    <select name="local[]" id="local" class="form-control select2" multiple="multiple">
                        <option value=""></option>
                        <?php foreach ($locais as $local) { ?>
                            <option value="<?php echo $local['lcl_id']; ?>" ><?php echo $local['lcl_descri']; ?></option>
                        <?php } ?>
                    </select>
                </div>
				
			</div>

			<div class='row form-group'>
				<div class="col-md-6">
                	<label for="nomecompleto">Marca</label>
                    <select name="marca[]" id="marca" class="form-control select2" multiple="multiple">
                    	<option value=""></option>
                    	<?php foreach ($marcas as $marca) { ?>
                    		<option value="<?php echo $marca['veicmarc_cod']; ?>" ><?php echo $marca['veicmarc_descri']; ?></option>
                    	<?php } ?>
                    </select>
                </div>

				<div class="col-md-6">
                	<label for="tipo">Tipo <span class="obg">*</span></label>
                    <select name="tipo[]" id="tipo" class="form-control select2"  multiple="multiple">
                    	<option value=""></option>
                    	<?php foreach ($tipos as $tipo) { ?>
                    		<option value="<?php echo $tipo['veictipo_cod']; ?>"><?php echo $tipo['veictipo_descri']; ?></option>
                    	<?php } ?>
                    </select>
                </div>                  
			</div>


			<div class='row form-group'>
                <div class='col-sm-6'>
                    <label>Placa</label>
                    <input type="text" name="placa" id="placa" class="form-control" />
                </div>

                <div class='col-sm-3'>
                    <label>Ano Fáb</label>
                    
                	<?php $ano = (date("Y")-10);?>
                    <select name="anofab[]" id="anofab" class="form-control select2"  multiple="multiple">
                    	<option value="">Selecione</option>
                    	<?php for($x = $ano ; $x <= 2018; $x++) {?>
	                    	<option value="<?php echo $x; ?>"><?php echo $x;?></option>
                    	<?php } ?>
                    </select>                    
                </div>

                <div class='col-sm-3'>
                    <label>Ano Mod.</label>
                    <?php $ano = (date("Y")-10);?>
                    <select name="anomod[]" id="anomod" class="form-control select2"  multiple="multiple">
                        <option value="">Selecione</option>
                        <?php for($x = $ano ; $x <= 2018; $x++) {?>
                            <option  value="<?php echo $x; ?>"><?php echo $x;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class='row form-group'>
                <div class='col-sm-6'>
                    <label>Mês IPVA</label>
                    <select id="mesipva" name="mesipva" class="form-control">
                        <option value="">Selecione</option>
                        <?php for($m = 1; $m <= 12; $m++) { ?>
                            <option  <?php echo ($veiculo['veic_mesipva'] == $m) ? "" : "" ?>  value="<?php echo $m; ?>"><?php echo $m?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class='col-sm-6'>
                    <label>Mês Licenciamento</label>
                    <select id="meslicenc" name="meslicenc" class="form-control">
                        <option value="">Selecione</option>
                        <?php for($m = 1; $m <= 12; $m++) { ?>
                            <option  <?php echo ($veiculo['veic_meslicenc'] == $m) ? "" : "" ?>  value="<?php echo $m; ?>"><?php echo $m?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
 

            <div class='row form-group'>
                <div class='col-sm-6'>
                    <label>Plano de Contas</label>
                    <select name="planoconta[]" id="planoconta" class="select2 form-control" multiple="multiple" >
                        <option value="">&nbsp;</option>
                        <?php foreach ($PlanoDeContas as $planocontas) {?>
                        <option value="<?php echo $planocontas['plc_cdPlano']?>" ><?php echo $planocontas['plc_descri']?> </option>
                        <?php } ?>
                    </select>
                </div>

                <div class='col-sm-6'>
                    <label>Centro de Custo</label>
                    <select name="centrocusto[]" id="centrocusto" class="select2 form-control" multiple="multiple" >
                         <option value="">&nbsp;</option>
                         <?php foreach ($CentroDeCustos as $centrocusto) {?>
                         <option value="<?php echo $centrocusto['cec_codigo']?>" ><?php echo $centrocusto['cec_descri']?> </option>
                         <?php } ?>
                     </select>
                </div>
            </div>

            <div class='row form-group'>
                <div class='col-sm-12'>
                    <label>Relatório</label>
                    <select name="report" id="report" class="form-control">
                    	<option value="">Selecione</option>
                    	<?php foreach ($reports as $report) { ?>
	                    	<option  value="<?php echo $report['rep_arquivo']; ?>"><?php echo $report['rep_nome'];?></option>
                    	<?php } ?>
                    </select>
                </div> 

                <div class='col-sm-12'>
                	<br/>
                    <input type="submit" value="Gerar" class="btn btn-primary pull-right">
                    </select>
                </div>

            </div>

        </div>

    </div>
</div>
</form>

<style>
.form-group div {
	margin-bottom: 10px;
}
</style>