<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessosModel
 *
 * @author andre
 */
class ServicosModel extends DB {
    
    public function __construct() {
        $this->cn = $this->CnCliente();
        
        $LOCAL = $_SESSION['APP_LOCALID'];
        $this->CONDICAO_PADRAO = " (serv_localid = '$LOCAL' or serv_localid = '0') ";
        $this->AND_CONDICAO_PADRAO = ' AND '.$this->CONDICAO_PADRAO . '';
        
    }
    

    public function Seleciona($campos = "", $condicao = "") {
        if($campos != "") {
            $campos = ", ".$campos;
        }
        
        if($condicao != '') {
            $condicao = $condicao;
        }        
         
         $selec = "SELECT * $campos FROM sis_servicos WHERE $this->CONDICAO_PADRAO $condicao ORDER by serv_descricao ASC";
         $selec = $this->ExecQuery($selec);
         
         return $selec;
    }
    
    
    /**
     * 
     * Seleção de fase do aluno*/
    public function SelecionaItens($campos = "",$condicao ='') {
        if($campos != "") {
            $campos = "* , ".$campos;
        } else {
            $campos = "*";
        }
        
        if($condicao != '') {
            $condicao = $condicao;
        }
         
         $LOCAL = $_SESSION['APP_LOCALID'];
         $selec = "SELECT $campos FROM sis_servicositens WHERE (serviten_localid = '$LOCAL' or serviten_localid = '0') ";
         $selec = $this->ExecQuery($selec);
         
         return $selec;
    }    
}
