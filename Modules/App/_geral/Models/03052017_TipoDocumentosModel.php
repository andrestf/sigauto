<?php

class TipoDocumentosModel extends DB {

    private $table, $campolocal, $campoid, $campoorder;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->table      = "sis_tpdocum";
        $this->campoid    = "tpd_id";
        $this->campolocal = "tpd_localid";
        $this->campoorder = "tpd_ordem";
    }
    
    public function Listar() {
        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) ORDER BY  $this->campoorder ASC";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }


}
