<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FasesModel
 *
 * @author andre maninho
 */
class FasesModel extends DB {
    
    private $CONDICAO_PADRAO;
    public function __construct() {
        $this->cn = $this->CnCliente();
        
    }

    /**
     * 
     * Seleção de fase do aluno*/
    public function Seleciona($campos = "",$condicao ='', $join="") {
        if($campos != "") {
            $campos = "* , ".$campos;
        } else {
            $campos = "*";
        }
        
        if($condicao != '') {
            $condicao = $condicao;
        }
         
         $selec = "SELECT $campos FROM sis_alunosmatfases
                   $join  
                   WHERE TRUE $condicao";
         $selec = $this->ExecQuery($selec);
         
         return $selec;
    }

    public function ValidaScript($ID_SERV) {
        //SELECIONA SERVICO ITEN E VERIFICA SCRIPTS
        $ID_SERVICO = $ID_SERV;
        $query = "SELECT * FROM sis_servicositens WHERE serviten_id = '$ID_SERVICO' ";
        $this->ExecQuery($query);
        $x = $this->result_array();

        PRINT_R($x);
        exit("A");
        exit();
    }

    /**
     * Atualizar fases do processo do aluno
     * */
    public function AtualizaFaseAluno($ID_ALUNO,$ID_FASE,$ID_MATRICULA,$FASE_DATA,$FASE_RESULTADO,$FASE_CONCLUIDO,$MARCACAO_EXAME = NULL) {
     $FASE_DATA = DataDB($FASE_DATA);
     


     if($FASE_CONCLUIDO != '') {
        $FASE_CONCLUIDO = "'".DataDB($FASE_CONCLUIDO)."'";
        $conclui = true;
     } else {
        $conclui = false;
        $FASE_CONCLUIDO = "null";
     }


     if($FASE_RESULTADO != 'APROVADO') {
        $FASE_CONCLUIDO = "null";
     }

     $faltoso = null;
     if($FASE_RESULTADO == "FALTOSO") {
        $faltoso = ",amf_faltoso = '$FASE_DATA', amf_faltosoreal = CURRENT_TIMESTAMP()";
     }

     $reprovado = null;
     if($FASE_RESULTADO == "REPROVADO") {
        $reprovado  = ", amf_reprovado = '$FASE_DATA', amf_reprovadoreal = CURRENT_TIMESTAMP()";
        $reprovado .= ", amf_faltoso = NULL ";
     }     
     $updMarcacaoExame  = '';



     $MARCACAO_EXAME_DESCRI = "";
     if($MARCACAO_EXAME != '' AND $MARCACAO_EXAME != NULL) {
        $updMarcacaoExame = ", amf_examepratico = '$MARCACAO_EXAME' ";

        if($MARCACAO_EXAME == "M") {
            $MARCACAO_EXAME_DESCRI = " | MARCAÇÃO";

        } elseif ($MARCACAO_EXAME == "E") {
            $MARCACAO_EXAME_DESCRI = " | MARCAÇÃO+EMISSÃO";
        }
        elseif($MARCACAO_EXAME == "T") {
            $MARCACAO_EXAME_DESCRI = " | TAXA EMISSÃO CNH";   
        } else  {
            $MARCACAO_EXAME_DESCRI = "| ";   
            $updMarcacaoExame = ", amf_examepratico = NULL ";
        }
     }


    if($MARCACAO_EXAME == "XXX") {
        //echo $FASE_DATA;
        $FASE_CONCLUIDO =  "'".$FASE_DATA."'";
        $conclui = true;
    }


     
    $update = "UPDATE sis_alunosmatfases SET amf_data = current_date(), amf_dataprocesso = '$FASE_DATA',
                amf_resultado = '$FASE_RESULTADO',
                amf_concluido = $FASE_CONCLUIDO
                $updMarcacaoExame
                $faltoso $reprovado
                WHERE amf_matid = '$ID_MATRICULA' and amf_id = '$ID_FASE' and amf_alunoid = '$ID_ALUNO'; "; 
    
        $x = $this->ExecNonQuery($update);
        $ret['erro'] = '';
        $ret['conclui'] = $conclui;


                $campos = array(
                            "amfl_idaluno" => "$ID_ALUNO",
                            "amfl_idmat" => "$ID_MATRICULA",
                            "amfl_idfase" => "$ID_FASE",
                            "amfl_data" => "$FASE_DATA",
                            "amfl_resultado" => "$FASE_RESULTADO " . $MARCACAO_EXAME_DESCRI);

                $this->SalvaLogFase($campos);

        return $ret;
    }
    
    public function GetMatriculas($ID_ALUNO) {
            $query = "SELECT * FROM sis_alunosmat LEFT OUTER JOIN sis_servicos ON serv_id = amat_servico where amat_alunoid = '$ID_ALUNO' ";
            $this->ExecQuery($query);
            $x = $this->result_array();
            if($x != '') {
                return $x;
            } else {
                return false;
            }
            
    }
    
    public function GetFaseAtual($ID_ALUNO) {
        $query = "SELECT * FROM sis_alunosmatfases LEFT OUTER JOIN sis_servicositens ON serviten_id = amf_servitenid WHERE amf_alunoid = '$ID_ALUNO' and NOT amf_iniciado IS NULL and amf_concluido IS NULL ";
        $this->ExecQuery($query);
            $x = $this->result_array();
            if($x != '') {
                return $x;
            } else {
                return false;
            }        
    }

    public function GetFaseLogs($idfase) {
        $query = "SELECT * FROM sis_alunosmatfaseslog WHERE amfl_idfase = '$idfase' order by 1 asc";
        $this->ExecQuery($query);
            $x = $this->result_array();
            if($x != '') {
                return $x;
            } else {
                return false;
            }                
    }

    public function SalvaLogFase($campos = array()) {
        $campos['amfl_cadusua'] = $_SESSION['APP_USUID'];
        $campos['amfl_caddata'] = date("Y-m-d G:i:s");
        $this->insert('sis_alunosmatfaseslog',$campos);
    }
}