<?php

class CentrodecustoModel extends DB {

    private $table, $campolocal, $campoid, $campoorder;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->table      = "sis_centrocusto";
        $this->campoid    = "cec_id";
        $this->campolocal = "cec_localid";
        $this->campoorder = "cec_id";
    }
    
    public function Listar() {
        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) ORDER BY  $this->campoorder ASC";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }
    
    
    public function seleciona($id = "") {
        
        if($id == "") {
            exit();
        }

        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) AND tpd_id = '$id'";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno[0];

    }
    
    public function Update($id,$campos = array()) {
        if(!is_array($campos) || $campos == "" || count($campos) == 0) {
            return false;
        }
        
        $upd = "";
        foreach ($campos as $campo => $valor) {
            $upd .= "`$campo` = '$valor',";
        }
        
        $upd = substr($upd,0,-1);
        
        $update = "UPDATE $this->table SET $upd WHERE $this->campoid = '$id' ";
        if($this->ExecQuery($update)) {
            return true;
        }
        return false;
    }

    public function inserir($campos) {
        $this->insert($this->table,$campos);
    }     
    
    public function Insertxxxxxx($campos = array()) {
        if(!is_array($campos) || $campos == "" || count($campos) == 0) {
            return false;
        }
        
        $vals   = "";
        $fields = "";
        foreach ($campos as $campo => $valor) {
            $fields .= "$campo,";
            $vals   .= "'$valor',";
        }
        
        $vals   = substr($vals  ,0,-1);
        $fields = substr($fields,0,-1);
        
        $ins= "INSERT INTO $this->table ($fields) VALUES ($vals)";
        if($this->ExecQuery($ins)) {
            return true;
        }
        return false;
    }    


}
