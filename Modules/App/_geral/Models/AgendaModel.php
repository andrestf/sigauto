<?php

class AgendaModel extends DB {

	public function __construct()
	{
		parent::__construct();
        $this->cn = $this->CnCliente();
        $this->APP_LOCALID = $_SESSION['APP_LOCALID'];
	}

	public function NovaAula($evento) {

		$campos = '';
		$valores = '';
		foreach ($evento as $key => $value) {
			$campos  .= $key.",";
			$valores .= "'".$this->Prepare($value)."',";
		}

		$campos = substr($campos, 0,-1);
		$valores = substr($valores, 0,-1);

		$query = "INSERT into au_aulas ($campos) VALUES ($valores)";

        $c = $this->ExecNonQuery($query);
        


        //print_r($c);
        if($this->GetErro("",false)) {
            $x = $this->GetErro();
            $r['erro'] = 1;
            $r['erromes'] = $x['mensagem'];

            return $r;
        }
        $r['erro'] 	= 0;
        $r['id'] 	= $c->insert_id;
        $r['title']	= $evento['au_descricao'];
        $r['start']	= str_replace(" ", "T",$evento['au_ini']);
        $r['end']	= str_replace(" ", "T",$evento['au_fim']);


        return $r;

	}

	public function Listar($where = '') {

		if($where != '') {
			$where = ' AND ' .$where;
		}

		$query = "SELECT * FROM au_aulas WHERE au_idlocal = '".$this->APP_LOCALID."' $where";

        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        if($retorno) {
        	return $retorno;
        }

        return false;

	}


	public function update($id,$campos) {
		$updates = "";
		foreach ($campos as $campo => $valor) {
			$updates .= " ".$campo . " = '".$this->Prepare($valor)."',";
		}

		$updates = substr($updates, 0,-1);

		$query = "UPDATE au_aulas SET $updates WHERE au_id = '$id' AND au_idlocal = '".$this->APP_LOCALID."'";
        $c = $this->ExecNonQuery($query);


        //print_r($c);
        if($this->GetErro("",false)) {
            $x = $this->GetErro();
            $r['erro'] = 1;
            $r['erromes'] = $x['mensagem'];

            return $r;
        }
        $r['erro'] 	= 0;
        $r['id'] 	= $id;
        $r['title']	= $campos['au_descricao'];
        $r['start']	= str_replace(" ", "T",$campos['au_ini']);
        $r['end']	= str_replace(" ", "T",$campos['au_fim']);


        return $r;		
	}

    public function Remover($idAula,$idAluno) {
        $query = "DELETE FROM au_aulas WHERE au_id = '$idAula' AND au_idaluno = '$idAluno' AND au_idlocal = '".$this->APP_LOCALID."'";
        $c = $this->ExecNonQuery($query);
        //print_r($c);
        if($this->GetErro("",false)) {
            $x = $this->GetErro();
            $r['erro'] = 1;
            $r['erromes'] = $x['mensagem'];

            return $r;
        } 

        $r['erro']    = 0;
        $r['mensagem'] = "Evento Removido com sucesso!";

        return $r;
    }
}