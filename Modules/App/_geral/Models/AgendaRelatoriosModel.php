<?php

class AgendaRelatoriosModel extends ReportsModel {

	public function Gerar() {
		echo $this->GetReport();
        if($this->GetReport() == '') {
            //exit('script nao informado');
        }


        $DataPadrao = "  "; 
        if(!$_POST){
            $_POST = $_GET;
        }


 		$wDataIni = '';
        if(isset($_POST['data_ini']) AND $_POST['data_ini'] != '') {
            $data_ini = DataDB($_POST['data_ini']);
            $data_ini = $this->Prepare($data_ini);
            $wDataIni = " AND date(au_ini) >= '$data_ini' ";
            $DataPadrao = "";
        }
        
        $wDataFim = '';
        if(isset($_POST['data_fim']) AND $_POST['data_fim'] != '') {
            $data_fim = DataDB($_POST['data_fim']);
            $data_fim = $this->Prepare($data_fim);
            $wDataFim = " AND date(au_fim) <= '$data_fim' ";
            $DataPadrao = "";
        }


		$wIDAluno = '';
        if(isset($_POST['idAluno']) AND $_POST['idAluno'] != '') {
            $wIDAluno = "AND au_idaluno = ".$_POST['idAluno'];
        }

		$wIDInstrut = '';
        if(isset($_POST['cboInstrutorPrin']) AND $_POST['cboInstrutorPrin'] != '') {
            $wIDInstrut = "AND au_idinstrut = ".$_POST['cboInstrutorPrin'];
        }

		$wIDVeic = '';
        if(isset($_POST['cboVeicPrin']) AND $_POST['cboVeicPrin'] != '') {
            $wIDVeic = "AND au_idcarro = ".$_POST['cboVeicPrin'];
        }

        if($this->GroupBy != '') {
            $this->GroupBy = 'GROUP BY '.$this->GroupBy;
        }

        if($this->OrderBy != '') {
            $this->OrderBy = 'ORDER BY '.$this->OrderBy;
        }        

        if($this->Campos != '') {
            $this->Campos = ",".$this->Campos;
        }

        $report  = "
        		SELECT 
					au_id, au_seqlanc, au_idaluno, au_idinstrut, au_idlocal, au_idcarro, au_descricao, au_ini, au_fim,
					usua.usu_nomecompleto, usua.usu_rg, usua.usu_cpf,
					veic_descricao

        		$this->Campos

        		FROM au_aulas
        		
        		LEFT OUTER JOIN sis_usuarios as usua	 ON usua.usu_id  = au_idaluno
        		LEFT OUTER JOIN veic_veiculos    		 ON veic_id = au_idcarro
        		LEFT OUTER JOIN sis_usuarios as inst     ON inst.usu_id  = au_idinstrut
        		
        		WHERE au_idlocal = '".$_SESSION['APP_LOCALID']."'
		
				$wDataIni
				$wDataFim
        		$wIDAluno
        		$wIDInstrut
        		$wIDVeic
        		$this->CondicaoExtra
        		$this->GroupBy

        ";
        $reportx = $this->ExecQuery($report);

        return $reportx;        

	}

}