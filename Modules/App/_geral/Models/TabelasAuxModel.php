<?php

class TabelasAuxModel extends DB {

    public $tabela, $tabela_prefixo, $cn, $MostraCancelados, $joins, $orderby;
    
    public function __construct($tabela) {
        $this->cn = $this->CnCliente();

        $this->MostraCancelados = true;

        $this->tabela = $tabela;
        $query = $this->ExecQuery("SHOW KEYS FROM ".$this->tabela." WHERE Key_name = 'PRIMARY'");
        $query = $this->result_array();
        $this->PrimaryKey = $query[0]['Column_name'];
    }
    
    
    public function join($tabela,$on,$tipo = "LEFT ") {
        $this->joins[] = " $tipo OUTER JOIN $tabela ON $on ";
    }


    public function GetJoins() {
        $ret = "";

        if(is_array($this->joins)) {
            foreach ($this->joins as $j) {
                $ret .= " $j ";
            }
        } else {
            $ret = $this->joins;
        }


        return $ret;
    }

    public function Listar($condicao = '',$limite = '' , $order = ' ORDER BY 1 DESC ') {
        
        if($this->orderby != '') {
            $order = " ORDER BY " . $this->orderby;
        }

        $query = "  SELECT * FROM ".$this->tabela." 
                    ".$this->GetJoins()."
                    WHERE TRUE
                    $condicao 
                    AND (".$this->tabela_prefixo."_localid = 0 OR ".$this->tabela_prefixo."_localid = ".$_SESSION['APP_LOCALID']."  )
                    $order 
                    $limite
                    ";

        $query = $this->ExecQuery($query);
        

        if($query->num_rows >=1 ) {
            $retorno = $this->result_array();
            return $retorno;
        } 

        return false;
    }
    

}