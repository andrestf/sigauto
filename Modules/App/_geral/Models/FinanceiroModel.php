<?php
 
class FinanceiroModel extends DB {

    private $table, $campoid, $campolocal, $alunoid;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->table      = "fin_financeiro";
        $this->campoid    = "finmov_id";
        $this->campolocal = "finmov_localid";
    }

    public function SetAluno($id) {
        $this->alunoid = $id;
    }

    public function Listar() {
        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  )";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }

    public function SelecionaParcela($id, $where = '') {
        $query = "SELECT * FROM $this->table WHERE finmov_id = $id $where";
        $this->ExecQuery($query);
        $parcela =  $this->result_array();
        return $parcela[0];
    }

    public function BaixaParcela($id_parcela, $valorbx, $data_baixa, $usuariobx, $tprecpag, $tpdocbx,$observacoes, $vlrcredito = 0) {

        $id_parcela     = $this->Prepare($id_parcela);
        $valorbx        = $this->Prepare($valorbx);
        $data_baixa     = $this->Prepare($data_baixa);

        $dia = substr($data_baixa, 8,2);
        $mes = substr($data_baixa, 5,2);
        $ano = substr($data_baixa, 0,4);

        $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
        if($dia >= $ultimo_dia) {
            $data_baixa = $ano . "-" . $mes . "-" . $ultimo_dia . " ". date("G:i:s");
        }

        $usuariobx      = $this->Prepare($usuariobx);
        $tprecpag       = $this->Prepare($tprecpag);
        $tpdocbx        = $this->Prepare($tpdocbx);
        $observacoes    = $this->Prepare($observacoes);

        $parcelaA = $this->SelecionaParcela($id_parcela);
        //print_r($parcela); exit();
            if( $parcelaA['finmov_tpmov'] == "S" && $valorbx > 0 ) {
                $valorbx = $valorbx*-1;

            }

        $parcela_atual = $this->SelecionaParcela($id_parcela);

        $update = "UPDATE fin_financeiro SET finmov_valorbaixa = '$valorbx', finmov_databaixareal = current_timestamp() ".
                  ", finmov_dataestorno = NULL, finmov_usuestorno = NULL" .
                  ", finmov_databaixa = '$data_baixa', finmov_usubaixa = '$usuariobx', finmov_obs = '$observacoes' ".
                  ", finmov_tprecpag = '$tprecpag', finmov_tpdocumbx = '$tpdocbx' WHERE finmov_id = '$id_parcela' "; 
        $this->ExecNonQuery($update) ? $lOK = true : $lOK = false;
        #$lOK = true;
                  
        if(!$lOK) {
            echo "err";
            exit();
        }

        //se tem utiliza creditos
        $lancaDif = true;
        if($vlrcredito > 0) {
        #if(false) {
            $CreditosModel = new CreditosModel();
            $CreditosModel->DescontaValor($this->alunoid,$vlrcredito,"BAIXA PARCELA $id_parcela");
            $lancaDif = FALSE;
        }

        //forçanca o lancamento da diferença do valor
        $lancaDif = true;

        //lançando diferença de parcelas
        
            if($valorbx < $parcela_atual['finmov_valor'] && $lancaDif) {
                $resto = $parcela_atual['finmov_valor'] - $valorbx;
                $resto = number_format($resto,2,".",",");
                /*
                echo "resto = " . $resto;
                echo "valor = " . $valorbx;
                */

                $x = $this->CriaMovimento($parcela_atual['finmov_alunoid'], $resto, DataBR($parcela_atual['finmov_dtvenc']), $parcela_atual['finmov_tpmov'], $parcela_atual['finmov_descricao'], $parcela_atual['finmov_tipo'], $parcela_atual['finmov_deonde'], $parcela_atual['finmov_deondeid'],false);
                
                if(is_object($x)) {
                    $LASTID = $x->insert_id;
                } else {
                    $LASTID = $x;
                }
                $this->SalvaLog($LASTID,"INS","Criação de Movimento, motivo: Diferença de movimento id = $id_parcela");
            } else {
                $this->SalvaLog($id_parcela,"BX","BAIXA [ VALOR: $valorbx DATA: $data_baixa ]");
            }

  
    }



    public function EstornaParcela($id_parcela,$usuariobx) {

        $parcela = $this->SelecionaParcela($id_parcela);
        
        
        
        if($parcela['finmov_deonde'] != "MATRIC") {

            $valor      = $parcela['finmov_valor'] *-1;
            $vencimento = date("d-m-Y");

            if($valor < 0) {
                $tipo       = "S";
            } else {
                $tipo       = "E";
            }

            $descricao  = "Estorno Mov. $id_parcela";
            $movtipo    = $parcela['finmov_tipo'];
            $deonde     = $parcela['finmov_deonde'];
            $deondeid   = $parcela['finmov_deondeid'];
            $alunoid    = $parcela['finmov_alunoid'];
            $tprecpag   = $parcela['finmov_tprecpag'];
            $ccusto     = $parcela['finmov_ccusto'];
            

            $x = $this->CriaMovimento($alunoid, $valor,$vencimento,$tipo,$descricao,$movtipo,$deonde,$deondeid,'',$tprecpag,$ccusto);
            $LASTID = $x;
            $this->SalvaLog($LASTID,"INS","Criação de Movimento, motivo: Estorno do de movimento id = $id_parcela");

            $update = "UPDATE fin_financeiro SET finmov_usuestorno = '$usuariobx', finmov_dataestorno = CURRENT_DATE WHERE finmov_id = '$id_parcela' "; 
            $this->ExecNonQuery($update);
            $this->SalvaLog($id_parcela,"EST","ESTORNO");
            return;
        }


        $update = "UPDATE fin_financeiro SET finmov_valorbaixa = NULL, finmov_databaixareal = NULL ".
                  ", finmov_databaixa = NULL, finmov_usuestorno = '$usuariobx', finmov_dataestorno = CURRENT_DATE ".
                  " WHERE finmov_id = '$id_parcela' ";
        $this->ExecNonQuery($update);

        $this->SalvaLog($id_parcela,"EST","ESTORNO= [ MATRICULA ]");

    }


    public function CriaMovimento($alunoid = "0",$valor,$vencimento,$tipo,$descricao,$movtipo,$deonde,$deondeid= '',$autobaixa = true,$tprecpag="",$ccusto="",$pconta="",$nMesesRepetir = 1) {

 

        #$valor = str_replace(".","", $valor);
        $valor = str_replace(",","", $valor);

        if($ccusto == "") {
            $ccusto = "NULL";
        }
        
        if($pconta == "") {
            $pconta = "NULL"; } else {
            $pconta = "'$pconta'";
         }
        
        $ID_LOCAL = $_SESSION['APP_LOCALID'];
        $CAD_USUA = $_SESSION['APP_USUID'];
        $CAD_DATA = "current_timestamp()";
        $DATA_ATUAL = "current_date()";
        
        $vencimento = DataDB($vencimento);

        $dia = substr($vencimento, 8,2);
        $mes = substr($vencimento, 5,2);
        $ano = substr($vencimento, 0,4);

        $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
        if($dia >= $ultimo_dia) {
            $vencimento = $ano . "-" . $mes . "-" . $ultimo_dia;
        }

        if($tipo == "S" AND $valor > 0) {
            $valor  = $valor*-1;
        }
        $tpdocum = "DINH";





        ## repetir lancamentos
        ########################################################################################
        $lote  = "";
        $lotex = sha1(time());
        for($m=0;$m <= $nMesesRepetir-1;$m++) {
            $data = $vencimento;
            if($m > 0) {          
                $lote = $lotex;
                $vencimento = date('Y-m-d', strtotime("+1 month",strtotime($data)));
            }
            #echo $vencimento;
            #echo "<br/>";
            //lancamento para cada centro de custo
            if(is_array($ccusto)) {
                    $ins = "INSERT INTO fin_financeiro
                    (finmov_alunoid,finmov_valor, finmov_valorori, finmov_dtvenc, finmov_tpmov, finmov_descricao, finmov_tpdocum, finmov_tpdocumbx, finmov_localid, finmov_tipo, finmov_deonde, finmov_deondeid, finmov_data, finmov_status, cad_data , cad_usua   ,finmov_pconta,finmov_ccusto, finmov_tprecpag) VALUES ";
                    foreach ($ccusto as $key => $valorCCusto) {
                        $ID_LOCAL = $valorCCusto;
                        $ins .= "('$alunoid'    ,'$valor'    , '$valor'       ,'$vencimento' , '$tipo'     , '$descricao'    , '$tpdocum'    , '$tpdocum'      , '$ID_LOCAL'   , '$movtipo' , '$deonde'    , '$deondeid'    , $DATA_ATUAL  , 'ATIVO'      , $CAD_DATA, '$CAD_USUA',$pconta,$valorCCusto, '$tprecpag'),";
                    }

                $ins = substr($ins,0,-1).";";
                
            //lancamento para apenas um centro de custo
            } else {

              $ins = "INSERT INTO fin_financeiro
                (finmov_alunoid,finmov_valor, finmov_valorori, finmov_dtvenc, finmov_tpmov, finmov_descricao, finmov_tpdocum, finmov_tpdocumbx, finmov_localid, finmov_tipo, finmov_deonde, finmov_deondeid, finmov_data, finmov_status, cad_data , cad_usua   ,finmov_pconta,finmov_ccusto, finmov_tprecpag) VALUES 
                ('$alunoid'    ,'$valor'    , '$valor'       ,'$vencimento' , '$tipo'     , '$descricao'    , '$tpdocum'    , '$tpdocum'      , '$ID_LOCAL'   , '$movtipo' , '$deonde'    , '$deondeid'    , $DATA_ATUAL  , 'ATIVO'      , $CAD_DATA, '$CAD_USUA',$pconta,$ccusto,'$tprecpag')";
            }            
            
            $ret = $this->ExecNonQuery($ins);
        }
        
        //exit();

        
        $LASTID = $ret->insert_id;

        if($autobaixa) {
            if($vencimento <= date("Y-m-d")) {
                if($tprecpag == "") {
                    $tprecpag = $deonde;
                }
                                        //$id_parcela, $valorbx, $data_baixa  , $usuariobx, $tprecpag, $tpdocbx,$observacoes
                $this->BaixaParcela($ret->insert_id  , $valor  , date("Y-m-d"), $CAD_USUA , $tprecpag  , "DINH", "");
            }
        }

        return $LASTID;
    }


    public function ListaParcelas( $dados, $limit = "" ) {

        if($limit != "") {
            $limit = " LIMIT $limit ";
        }
        $wALUNOID = ''; $wTipo = ''; $wDtPagamento = ''; $wPaga = ''; $wLancamento = ''; $wVencimento = ''; $wDocumento = ''; $wCPF = ''; $wNome = ''; $wParcela = ''; $jUsuario = ''; $Campos = ''; $wTpRecpag = ''; 

            if(is_array($dados)) {
            ## Parcela numero do registro finmov_id
            $wParcela = '';
            if(isset($dados['parcela']) && $dados['parcela'] != '') {
                $wParcela = " AND finmov_id = '".$this->Prepare($dados['parcela'])."' ";
            }

            ## NOME
            $jUsuario = '';
            $Campos = '';
            $wNome = ''; 
            if(isset($dados['nome']) && $dados['nome'] != '') {
                $wNome    = " AND usu_nomecompleto like '%".$this->Prepare($dados['nome'])."%' ";
                $jUsuario = " LEFT OUTER JOIN sis_usuarios ON usu_id = finmov_alunoid ";
            }

            ## CPF
            $wCPF = '';
            if(isset($dados['cpf']) && $dados['cpf'] != '') {
                #$dados['cpf'] = str_replace(".", "", $dados['cpf']);
                #$dados['cpf'] = str_replace("-", "", $dados['cpf']);
                $wCPF    = " AND usu_cpf = '".$this->Prepare($dados['cpf'])."' ";
                $jUsuario = " LEFT OUTER JOIN sis_usuarios ON usu_id = finmov_alunoid ";
            }

            ## TIpo de documento
            $wDocumento = ''; 
            if(isset($dados['documento']) && $dados['documento'] != '') {
                $wDocumento = " AND finmov_tpdocum = '".$this->Prepare($dados['documento'])."' ";
            }

            ## ID ALUNO
            $wALUNOID = ''; 
            if(isset($dados['alunoid']) && $dados['alunoid'] != '') {
                $wALUNOID = " AND finmov_alunoid = '".$this->Prepare($dados['alunoid'])."' ";
            }

            ## LANCAMENTO INICIAL
            $wLancamento = ''; 
            if(isset($dados['datamov_ini']) && $dados['datamov_ini'] != '') {
                $dados['datamov_ini'] = DataDB($dados['datamov_ini']);
                $wLancamento = " AND finmov_data >= '".$this->Prepare($dados['datamov_ini'])."' ";
            }

            ## LANCAMENTO FINAL
            if(isset($dados['datamov_fim']) && $dados['datamov_fim'] != '') {
                $dados['datamov_fim'] = DataDB($dados['datamov_fim']);
                $wLancamento = " AND finmov_data >= '".$this->Prepare($dados['datamov_fim'])."' ";
            }
            
            ## VENCIMENTO
            $wVencimento = ''; 
            if(isset($dados['vencimentoini']) && $dados['vencimentoini'] != '') {
                $dados['vencimentoini'] = DataDB($dados['vencimentoini']);
                $wVencimento = " AND finmov_dtvenc >= '".$this->Prepare($dados['vencimentoini'])."' ";
            }

            if(isset($dados['vencimentofin']) && $dados['vencimentofin'] != '') {
                $dados['vencimentofin'] = DataDB($dados['vencimentofin']);
                $wVencimento .= " AND finmov_dtvenc <= '".$this->Prepare($dados['vencimentofin'])."' ";
            }

            ## se parcela paga ou nao!
            $wPaga = ''; 
            if(isset($dados['situacao']) && $dados['situacao'] != '') {

                if($dados['situacao'] == '1') {
                    $wPaga = " AND NOT finmov_databaixa IS NULL"; }

                if($dados['situacao'] == '0') {
                    $wPaga .= " AND finmov_databaixa IS NULL"; }
            }


            ## DATA DE PAGAMENTO
            $wDtPagamento = ''; 
            if(isset($dados['dtpaga']) && $dados['dtpaga'] != '') {
                if($dados['dtpaga'] == 'IS NULL') {
                    $wDtPagamento = " AND finmov_databaixa IS NULL ";                    
                } else {
                    $dados['dtpaga'] = DataDB($dados['dtpaga']);
                    $wDtPagamento = " AND finmov_databaixa = '".$this->Prepare($dados['dtpaga'])."' ";                    
                }

            }    

            ## DATA DE PAGAMENTO
            $wTipo = ''; 
            if(isset($dados['tipo']) && $dados['tipo'] != '') {
              $wTipo = " AND finmov_tpmov = '".$this->Prepare($dados['tipo'])."' ";
            }    

            ## EMERSON - 22/08/2017 - AJUSTADO IMPRESSÃO NOTA PROMISSORIA EM LOTE SEM CANCELADOS
            ## CANCELADO
            $wCancelado = '';
            if(isset($dados['candata'])) {
                if($dados['candata'] == 'IS NULL') {
                   $wCancelado = " and fin_financeiro.can_data IS NULL ";                    
                }
            }
            ## DE ONDE
            /** ATÉ 12/07/2017 nao esta sendo utilizado,nao sei porque 
            $wDeOndeNot = ''; 
            if(isset($dados['!deonde']) && $dados['!deonde'] != '') {
              $wDeOndeNot = " AND finmov_deonde != '".$this->Prepare($dados['!deonde'])."' ";
            }   
            */

            ## TPRECPAQG LOCAL (CAIXA OU FINANCEIRO)
            $wTpRecpag = ''; 
            if(isset($dados['deondetprecpag']) && $dados['deondetprecpag'] != '') {
              $wTpRecpag = " AND finmov_tprecpag = '".$this->Prepare($dados['deondetprecpag'])."' ";
            }    
            
            ##pcontas:PCONTA
            ##ccusto:CCUSTO
        }//dados != ""

        $ID_LOCAL = $_SESSION['APP_LOCALID'];
        $query = "SELECT *, fin_financeiro.can_data AS mov_candata $Campos 

            FROM fin_financeiro

            $jUsuario 

            LEFT OUTER JOIN sis_tpdocum ON tpd_cd = finmov_tpdocumbx
            
            WHERE finmov_localid = '$ID_LOCAL'  

            $wParcela
            $wALUNOID
            $wNome $wCPF
            $wDocumento
            $wLancamento
            $wVencimento
            $wPaga
            $wDtPagamento
            $wTipo
            $wTpRecpag
            $wCancelado
                

            order by finmov_dtvenc ASC, finmov_id ASC

            $limit
        ";

        $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;        
    }



    public function MarcaCanceladoLote($parcelas) {

        if( substr($parcelas, -1) == ",") {
            $parcelas = substr($parcelas, 0,-1);
        }

        #$parcelas = explode(",", $parcelas);

        #foreach ($parcelas as $key => $value) {
        #    $this->MarcaCancelado($value);
        #}

        $this->SalvaLog("000","EXC","Cancelamento em LOTE parcelas: [$parcelas] ");

        $update = " UPDATE fin_financeiro SET can_data = current_timestamp(), can_usua = '".$_SESSION['APP_USUID']."' WHERE finmov_id in ($parcelas)  ";
        $lOK = $this->ExecNonQuery($update);

        if($lOK->affected_rows >= 1){
            echo 1;
        } else {
            echo 0;    
        }
        
    }
    public function MarcaCancelado($PARCELA) {
        $parcela = $this->SelecionaParcela($PARCELA);
        $parcelaID = $parcela['finmov_id'];

        

        if($parcela['can_data'] != '') {
            //recupenrando parcela
            $CAN_DATA = "NULL";
            $CAN_USUA = "NULL";

            $LOG = "Recuperação de Parcela";
            $LOG_ACAO = "REC";


        } else {
            $CAN_DATA = 'current_timestamp()';
            $CAN_USUA = $_SESSION['APP_USUID'];

            $LOG = "Cancelamento de Parcela";
            $LOG_ACAO = "EXC";
        }

        $this->SalvaLog($parcelaID,$LOG_ACAO,$LOG);

        $update = " UPDATE fin_financeiro SET can_data = $CAN_DATA, can_usua = '".$_SESSION['APP_USUID']."' WHERE finmov_id = '$parcelaID' ";
        $lOK = $this->ExecNonQuery($update);

        return $lOK;

    }


    public function CancelaParcelas($IDALUNO = '') {
        $lOK = true;


        if($IDALUNO == '') {
            echo "INFORME ID ALUNO";
            return false;
        }

        $this->SalvaLog($IDALUNO,"CAN01","CANCELAMENTO FINANCEIRO - CONTRATO CANCELADO");

        $CAN_DATA = 'current_timestamp()';

        $update = " UPDATE fin_financeiro SET finmov_status = 'CAN01', can_data = $CAN_DATA, can_usua = '".$_SESSION['APP_USUID']."' WHERE finmov_alunoid = '$IDALUNO' AND (finmov_databaixareal IS NULL OR finmov_databaixareal = '')   ";
        $lOK = $this->ExecNonQuery($update);

        return $lOK;        


    }


    public function EditarMovimento($IDPARC,$DTVENC,$VALOR,$TPDOC,$DESCR,$OBSCOBS) {
        $PARCELA = $this->SelecionaParcela($IDPARC);
        $LOG_ACAO = "EDT";
        $LOG_TXT  = "EDIÇÃO: \n\n";


        $DTVENC_ORI = $PARCELA['finmov_dtvenc'];
        $VALOR_ORI  = $PARCELA['finmov_valor'];
        $TPDOC_ORI  = $PARCELA['finmov_tpdocumbx'];
        $DESCR_ORI  = $PARCELA['finmov_descricao'];
        $OBSER_ORI  = $PARCELA['finmov_obscobranca'];


        $VALOR = str_replace(".","",$VALOR);
        $VALOR = str_replace(",",".",$VALOR);

        if($DTVENC_ORI != $DTVENC) 
            $LOG_TXT .= "VENCIMENTO DE $DTVENC_ORI PARA $DTVENC \n ";

        if($VALOR_ORI != $VALOR) 
            $LOG_TXT .= "VALOR DE $VALOR_ORI PARA $VALOR \n ";

        if($TPDOC_ORI != $TPDOC) 
            $LOG_TXT .= "TIPODOC DE $TPDOC_ORI PARA $TPDOC \n ";

        if($DESCR_ORI != $DESCR) 
            $LOG_TXT .= "DESCRICAO DE $DESCR_ORI PARA $DESCR \n ";

        if($OBSER_ORI != $OBSCOBS) 
            $LOG_TXT .= "$OBSCOBS \n ";


        $this->SalvaLog($IDPARC,$LOG_ACAO,$LOG_TXT);

        ## $update = " UPDATE fin_financeiro SET finmov_dtvenc = '$DTVENC', finmov_valor='$VALOR', finmov_valorbaixa='$VALOR', finmov_tpdocumbx='$TPDOC',  finmov_descricao='$DESCR' WHERE finmov_id = '$IDPARC' ";
        ## Removido valor paga da edição em 10/05/2017
        ## Alterado edição, so e permitido editar se parcela estiver em aberto... 

        $update = " UPDATE fin_financeiro SET finmov_dtvenc = '$DTVENC', finmov_valor='$VALOR', finmov_tpdocumbx='$TPDOC', finmov_descricao='$DESCR', finmov_obscobranca='$OBSCOBS' WHERE finmov_id = '$IDPARC' ";
        $lOK = $this->ExecNonQuery($update);

        return $lOK;

    }

    public function SalvaLog($id_mov,$acao,$altera) {
        $usua = $_SESSION['APP_USUID'];
        $idlocal = $_SESSION['APP_LOCALID'];
        $altera = "<div>".$altera . " <b>USUÁRIO:</b> " . $_SESSION['APP_USUNOME']."</div>";
        $sql = "INSERT INTO fin_financeirolog (finlog_localid,finlog_acao,finlog_movid,finlog_alteracao,finlog_usua, finlog_caddata) VALUES ('$idlocal','$acao','$id_mov','$altera','$usua',CURRENT_TIMESTAMP())";
        $lOK = $this->ExecNonQuery($sql);
        return $lOK;
    }

    public function ListaLogs($id_mov) {
        $query = "SELECT * FROM fin_financeirolog WHERE finlog_movid = '$id_mov' order by 1 desc";
        $this->ExecQuery($query);
        $retorno = $this->result_array();
        return $retorno; 
    }

}