<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AlunosMatriculasModel extends DB {

    private $ALUNO_ID;
    public function SetAlunoID($valor) {
        $this->ALUNO_ID = $valor;
    }

    public
            //ID DO TIPO DE MATRICULA
            $TPMAT,

            //ID CAT CNH
            $CATCNH,

            //ID DOS SERVICOS
            $SERVICO, $SERVICOITENS,

            //VLR DO SERVIÇO
            $VLRSERVICO,

            //VALOR DESCONTO
            $VLRDESCONTO,

            //FORMA DE PAGAMENTO
            $FORMAPAGA,

            //QTD DE PARCELAS
            $NPARCELAS,

            //DT DE VENCIMENTO DA PRIMEIRA PARCELA
            $VENCIMENTO,

            //VALOR MENOS DESCONTO
            $VALORFINAL,

            //TAXA PAGA OU NAO (1 = )
            $TAXA;


    /**
     * Incluir matricula(inicar um processo para o aluno)
     * @return type
     */
    public function IncluirMatricula() {
        $this->CnCliente();

        if($this->ALUNO_ID == "" || $this->ALUNO_ID <= "0" ) {
            exit("ID ALUNO INVALIDO");
            return false;
        }

       /*
        if($this->VENCIMENTO == "" || $this->VENCIMENTO == "__/__/____" ) {
            exit("VENCIMENTO não é válido!");
            return false;
        }

        $this->VENCIMENTO = DataDB($this->VENCIMENTO);
        */
        $this->VENCIMENTO = "NULL";
        $this->VLRDESCONTO = "0.00";//str_replace(".", "",$this->VLRDESCONTO);
        $this->VLRDESCONTO = "0.00";//str_replace(",", ".",$this->VLRDESCONTO);

        $this->VLRSERVICO = str_replace(".", "",$this->VLRSERVICO);
        $this->VLRSERVICO = str_replace(",", ".",$this->VLRSERVICO);

        $this->VALORFINAL = "0.00";//$this->VLRSERVICO-$this->VLRDESCONTO;

        $this->CATCNH = implode(",",$this->CATCNH);

        unset($_SESSION['SERV_VALORTOTAL']);

        $q = "INSERT INTO `sis_alunosmat` (`amat_localid`                ,`amat_alunoid`   ,`amat_tpmat`  ,`amat_catcnh`  ,`amat_servico`  ,`amat_servitens`     ,`amat_incluitaxa`,`amat_vlr`         ,`amat_desconto`     ,`amat_vlrfinal`    ,`amat_venc`        ,`amat_formapaga`  ,`amat_nparcelas`  ,`amat_situacao`,`amat_situacaodata`,`amat_exporta`,`cad_data`,`cad_usua`) VALUES (
                                           '".$_SESSION['APP_LOCALID']."','$this->ALUNO_ID','$this->TPMAT','$this->CATCNH','$this->SERVICO','$this->SERVICOITENS','$this->TAXA'    ,'$this->VLRSERVICO','$this->VLRDESCONTO','$this->VALORFINAL',current_date(),'$this->FORMAPAGA','$this->NPARCELAS'     ,'INICIADA'     ,current_timestamp(), null,current_timestamp(), '".$_SESSION['APP_USUID']."')";

        $this->autocommit(FALSE);
        $Exec = $this->ExecNonQuery($q);

        if($Exec->connect_error) {
            $ret['erro']     = '1';
            $ret['mensagem'] = 'Erro ao salvar matricula <br />' . $Exec->error;

            echo json_encode($ret);
            return false;
        } else {
            $ID_MATRICULA = $Exec->insert_id;
            $itens = explode(",", $this->SERVICOITENS);
            $insq = "";

            $xx = 0;
            foreach ($itens as $iten) {
                $xx++;
                $iniciado = "null";
                $concluido = "null";
                if($iten == "1") {
                    $iniciado  = "'".date("Y-m-d")."'";
                    $concluido = "'".date("Y-m-d")."'";

                }

                if($xx == "1" || $iten == '2') {
                    $iniciado  = "'".date("Y-m-d")."'";
                }

                $insq = "
                        INSERT INTO sis_alunosmatfases (amf_alunoid     ,amf_localid                   ,amf_matid      ,amf_servitenid,amf_datalista,amf_dataprocesso,amf_iniciado, amf_concluido,cad_data,cad_usua) VALUES
                                                      ('$this->ALUNO_ID','".$_SESSION['APP_LOCALID']."','$ID_MATRICULA','$iten'       ,$iniciado    ,$iniciado       ,$iniciado   , $concluido   ,current_timestamp(),'".$_SESSION['APP_USUID']."') ";
                $INS = $this->ExecNonQuery($insq);


                //$insq = substr($insq, 0,-1);

                if($INS->connect_error) {
                    $this->roolback();
                    $ret['erro']     = '1';
                    $ret['mensagem'] = 'Erro ao salvar matricula <br />' . $Exec->error;
                    echo json_encode($ret);
                    return false;
                }
            }
            $this->commit();
            $this->autocommit();
            $ret['erro']     = "";
            $ret['mensagem'] = "Aluno Matriculado com sucesso!";
            $ret['id']       = $ID_MATRICULA;
            echo json_encode($ret);
            return true;
        }

        //$this->LancarParcelasFinanceiro();
    }

    /**
     * Selcionar matricular do aluno
     * */
    public function SelecionaMatriculas($aluno,$condicao = '') {
        $this->CnCliente();

        $query = "SELECT * FROM sis_alunosmat LEFT OUTER JOIN sis_servicos ON serv_id = amat_servico WHERE amat_alunoid = '$aluno ' $condicao";
        $query = $this->ExecQuery($query);
        if($query->num_rows > 0) {
            return $this->result_array($query);
        } else {

            return false;
        }

    }
    private function LancarParcelasFinanceiro() {
        $DEONDE  = "MATRI";
        $IDALUNO = $this->ALUNO_ID;
        $IDPRODCESSO = "45"; // alunomat

    }

}