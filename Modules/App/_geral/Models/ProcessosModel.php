<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessosModel
 *
 * @author andre
 */
class ProcessosModel extends DB {
    
    public function __construct() {
        $this->cn = $this->CnCliente();
      
        $LOCAL = $_SESSION['APP_LOCALID'];
        $this->CONDICAO_PADRAO = " (pct_localid = '$LOCAL' or pct_localid = '0') ";
        $this->AND_CONDICAO_PADRAO = ' AND '.$this->CONDICAO_PADRAO . '';
        
    }
    
    public function Seleciona($campos = "", $condicao = "") {
        if($campos != "") {
            $campos = ", ".$campos;
        }
         
         $selec = $this->ExecQuery($selec);
         
         return $selec;
    }
    
    public function SelecionaServicos($campos = "", $condicao = "") {
        if($campos != "") {
            $campos = ", ".$campos;
        }
         
         $selec = "SELECT * $campos FROM sis_servicositens WHERE true AND can_data IS NULL $condicao";
         $selec = $this->ExecQuery($selec);
         
         return $selec;
    }    
    
    public function SelecionaCancelado($ID_PROCESSO,$ID_ALUNO) {
        if($ID_PROCESSO == '') {
            exit("ERRO, ID PROCESSO nao definido");
        }
        
        if($ID_ALUNO == '') {
            exit("ERRO, ID ALUNO nao definido");
        }
        
        $cExec = "SELECT * FROM sis_alunosmat WHERE amat_id = '$ID_PROCESSO' AND amat_alunoid = '$ID_ALUNO' ";
        $this->ExecQuery($cExec);
        $result = $this->result_array();
        $result = $result[0];



        
        if(count($result) == 0 ){
            exit("PROCESSO CANCELADO");
        }
        


        
        ##verifica se processo esta cancelado para lancamento de CRÉDITOS
        if($result['can_data'] != '') {
            
            ## se processo tem financeiro lancado
            #####################################################################
            if($result['amat_exporta'] != '') {
                $ID_MAT = $result['amat_id'];
                
                ##soma somente parcelas pagas
                $parcelasPagas = "SELECT * FROM fin_financeiro WHERE finmov_alunoid = '$ID_ALUNO' and finmov_deonde = 'MATRIC' AND finmov_deondeid = '$ID_MAT' AND NOT finmov_databaixa IS NULL";
                $this->ExecQuery($parcelasPagas);
                $parcelasPagas = $this->result_array();
                
                
                $valorTotalCredito = 0;

                if($parcelasPagas) {
                    foreach ($parcelasPagas as $item) {
                        $valorTotalCredito = $valorTotalCredito + $item['finmov_valorbaixa'];
                    }
                }
                

                return $valorTotalCredito;
                if($valorTotalCredito > 0) {
                    return $valorTotalCredito;
                    exit("ATEBÇÃO!!!! <BR/>O VALOR PAGO PELO ALUNO É DE" . $valorTotalCredito ." ");
                } else {
                    return 0;
                    exit("Valor pago pelo aluno é zero");
                }
                

            ## PROCESSO nao tem financeiro lancado... apenas cancela o mesmo!
            #####################################################################
            } else {
                return "-1";
            }
            
        }

        
        
        
    }
    
    public function Cancelamento($ID_PROCESSO,$ID_ALUNO, $MOTIVO = '', $CANFINAN = false) {
        
        
        if($MOTIVO == '') {
            exit("ERRO, Motivo de cancelamento nao informado!!!");
        }
        
        if($ID_PROCESSO == '') {
            exit("ERRO, ID PROCESSO nao definido");
        }
        
        if($ID_ALUNO == '') {
            exit("ERRO, ID ALUNO nao definido");
        }
        
        ## CANCELANDO MATRICULA
        $can_usua = $_SESSION['APP_USUID'];
        $cExec = "UPDATE sis_alunosmat SET can_data = current_timestamp(), can_usua = '$can_usua', can_motivo = '$MOTIVO', amat_situacao='CANCELADO', amat_situacaodata = current_timestamp() WHERE amat_id = '$ID_PROCESSO' AND amat_alunoid = '$ID_ALUNO' ";
        $this->ExecNonQuery($cExec);
        
        ## REMOVENDO REGISTROS DAS FASES
        $cExec = "DELETE FROM sis_alunosmatfases WHERE amf_alunoid = '$ID_ALUNO' and amf_matid = '$ID_PROCESSO'";
        $this->ExecNonQuery($cExec);

        if($CANFINAN) {
            ## CANCELANDO FINANCEIRO EM ABERTO
            $xExec = "UPDATE fin_financeiro SET can_data = current_timestamp(), can_usua = '".$_SESSION['APP_USUID']."', finmov_obs = 'PROC. CANCELADO'
                    WHERE 
                        can_data IS NULL and 
                        finmov_databaixa IS NULL and 
                        finmov_alunoid = '$ID_ALUNO' and
                        finmov_deonde = 'MATRIC' and finmov_deondeid = '$ID_PROCESSO'
                        
                    ";
            $this->ExecNonQuery($xExec);        
        }
        
        return $this->SelecionaCancelado($ID_PROCESSO,$ID_ALUNO);

        
        
        
        
    }
    
}
