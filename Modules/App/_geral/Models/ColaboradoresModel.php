<?php

class ColaboradoresModel extends DB {
    
    public function Seleciona($tipo, $condicao = "") {
        $this->CnCliente();
        $tipo = $this->Prepare($tipo);

        if($condicao != '') {
            $condicao = $condicao;
        }

        //TODO ACERTAR CONDIÇÃO
        // condição vem do controller
        // parece estar certo em 16/02/2017
        if($condicao != '') {
            $condicao = $condicao;
        }
        
        $QUERY = "SELECT * FROM sis_usuarios WHERE usu_tipo = '$tipo' $condicao ";
        $cExec = $this->ExecQuery($QUERY);
        return $cExec;
    }
    
    public function Listar($condicao = "") {
        $this->CnCliente();
        
        if($condicao != '') {
           $condicao = $condicao;
        }

        $query = "SELECT * FROM sis_usuarios WHERE (usu_localid = 0 OR usu_localid = ".$_SESSION['APP_LOCALID']."  ) AND usu_tipo != 'ALUNO' $condicao ORDER BY usu_apelido ASC";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }
    
    public function InserirNovo($campos = array()) {
        $this->CnCliente();

        $campos_q = "";
        $valores_q = "";
        $ignorarCampos = array("acao","chave","acesso","id");
        $sep = ",";
        
        $obrigatorios = array(
            "nomecompleto" => "Nome Completo",
            "nomemae" => "Nome da Mãe",
            "nomepai" => "Nome do Pai",
            "nascimento" => "Data de Nascimento",
            "sexo" =>"Sexo",
            "nacionalidade" => "Nacionalidade",
            "uf" => "UF Nacionalidade",
            "naturalidade" => "Naturalidade",
            "cpf" =>"CPF",
            "rg" => "RG",
            "rgemissor" => "orgão emissor RG",
            "rguf" => "UF emissão RG",
            "cep" => "CEP",
            "logradouro" => "Logradouro",
            "logranumero" => "Número do logradouro",
            "lograbairro" => "Bairro do logradouro",
            "logramunicipio" => "Município do logradouro",
            "lograuf" => "UF do logradouro"
        );

                
        
        foreach ($campos as $campo => $valor) {
            if(!in_array($campo, $ignorarCampos)) {
            
                $campos_q .= "`usu_$campo`".$sep;

                if($campo == "nascimento") {
                    if($valor != '') {
                        $valor = DataDB($valor);
                    }
                }

                //validando campos
                foreach ($obrigatorios as $obg => $descri) {
                    if($campo == $obg) {
                        if($valor == "") {
                            $this->SetErro('Informe ' . $descri);
                            $this->GetErro();                            
                        }
                    }
                }               

                if($valor == "") {
                    $valores_q .= "null".$sep;
                } else {
                    $valor = $this->Prepare($valor);
                    $valores_q .= "'$valor'".$sep;
                } 
            }//if
        }//foreach
        
        $campos_q = substr($campos_q,0, -1);
        $valores_q = substr($valores_q,0, -1);        
        
        $QUERY = "INSERT INTO sis_usuarios ($campos_q, `cad_data`,`cad_usua`) VALUES ($valores_q, current_timestamp(), ".$_SESSION['APP_USUID'].") ";
        $c = $this->ExecNonQuery($QUERY);
        if($this->GetErro("",false)) {
            return $this->GetErro();
        } else {
            $ret['erro'] = "";
            $ret['id'] = $c->insert_id;
            echo json_encode($ret);
        }
        
    }////////////
    
    
    public function editar($campos) {
        $this->CnCliente();
        
        $ID = $this->Prepare($campos['id']);
        
        if(!$campos) {
            exit('ERRO ' . __FILE__ . " Linha=> ". __LINE__);
        }

        $ignorarCampos = array("acao","chave","acesso","id");
        
        if($campos['senha'] == '') {
            array_push($ignorarCampos, 'senha');
        }
        $obrigatorios = array(
            "nomecompleto" => "Nome Completo",
            "nomemae" => "Nome da Mãe",
            "nomepai" => "Nome do Pai",
            "nascimento" => "Data de Nascimento",
            "sexo" =>"Sexo",
            "nacionalidade" => "Nacionalidade",
            "uf" => "UF Nacionalidade",
            "naturalidade" => "Naturalidade",
            "cpf" =>"CPF",
            "rg" => "RG",
            "rgemissor" => "orgão emissor RG",
            "rguf" => "UF emissão RG",
            "cep" => "CEP",
            "logradouro" => "Logradouro",
            "logranumero" => "Número do logradouro",
            "lograbairro" => "Bairro do logradouro",
            "logramunicipio" => "Município do logradouro",
            "lograuf" => "UF do logradouro"
        );        

        $upCxV  = "UPDATE sis_usuarios SET ";
        
        foreach ($campos as $campo => $valor) {
            //$campo = "usu_".$campo;

            foreach ($obrigatorios as $obg => $descri) {
                if($campo == $obg) {
                    if($valor == "") {
                        $this->SetErro('Informe ' . $descri);
                        $this->GetErro();                            
                    }
                }
            }            

            if($campo == 'nascimento') {
                $valor = DataDB($valor);
            }
            
            if($valor == '') {
                $valor = "null";
            } else {

                
                $valor = "'$valor'";
            }

            if(!in_array($campo, $ignorarCampos)) {
                $upCxV .= " `usu_$campo` = $valor,";
            }
            
            
        }
        $upCxV = substr($upCxV,0,-1);
        $upCxV .= " WHERE usu_id = '$ID' ";
        #$ret['erro'] = "1";
        #$ret['mensagem']= "Update de COLOBARADORES desativado para testes externos";
        #echo json_encode($ret);
        #return;
        $b = $this->ExecNonQuery($upCxV);
        if($this->GetErro("",false)) {
            return $this->GetErro();
        } else {
            $ret['erro'] = "";
            $ret['id'] = $b->affected_rows;
            echo json_encode($ret);
        }
        
        
    }
}