<?php

class VeiculosCtrModel extends DB {
    
    public $APP_LOCALID, $cn, $CAD_USUA;
    
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->APP_LOCALID = $_SESSION['APP_LOCALID'];
        $this->CAD_USUA    = $_SESSION['APP_USUID'];
    }
    
	

	public function Criar($idVeiculo, $object = array()) {

		$campos  = "";
		$valores = "";
		
		foreach ($object as $c => $v) {
			$campos  .= "`veicctr_$c`, ";

			if($v == "" || $v == NULL) {
				$valores .= "NULL";
			} else {
				$valores .= "'".$this->Prepare($v)."', ";
			}

		}
			$campos  .= "veicctr_veicid,cad_usua, cad_data, veicctr_localid";
			$valores .= "'$idVeiculo', '$this->CAD_USUA', CURRENT_TIMESTAMP(), '$this->APP_LOCALID'";


		$query = "INSERT INTO veic_controle ($campos) values ($valores)";

        $c = $this->ExecNonQuery($query);
        return $c;
/*
        if($this->GetErro("",false)) {
            return $this->GetErro("",false);
        } else {
            return $c;
        }  
-*/
	}	


	public function AlteraKM($veic,$km) {
		// SELECIONANDO MAIOR KM
		$SELECT = "SELECT * FROM veic_controle WHERE veicctr_veicid = '$veic' AND veicctr_localid = '$this->APP_LOCALID' ORDER BY 1 DESC";
		$x = $this->ExecQuery($SELECT);
		if($x->num_rows >= 1) {
        	$retorno = $this->result_array();
        	
            $ID = $retorno[0]["veicctr_id"];
       	}

		//REALIZANDO UPDATE
		$update = " UPDATE veic_controle set veicctr_kmfim = '$km' WHERE veicctr_id = '$ID' AND veicctr_localid = '$this->APP_LOCALID' ";
        $c = $this->ExecNonQuery($update);
        return $c;
	}


	public function KmAtual($idVeiculo) {
		//SELECIONA KM MAXIMA
		//$query = " SELECT max(veicctr_kmfim) as KM FROM veic_controle WHERE veicctr_veicid = '$idVeiculo' and (CAN_DATA IS NULL OR CAN_DATA = '')  ";

		//SELCIONA ULTIMO LANCAMENTO
		$query = " SELECT veicctr_kmfim as KM FROM veic_controle WHERE veicctr_veicid = '$idVeiculo' and (CAN_DATA IS NULL OR CAN_DATA = '') ORDER BY 1 ASC ";
		$x = $this->ExecQuery($query);

		if($x->num_rows >= 1) {
        	$retorno = $this->result_array();
            //echo $retorno[0]["KM"];
            return $retorno[0]["KM"];
            //exit();
       	} else {
       		return "0";
       	}
	}

}

/* End of file VeiculosCtrModel.php */
/* Location: .//C/Users/andre/Dropbox/Desenvolvimento/Asec/sigauto/Modules/App/_xdev/Models/VeiculosCtrModel.php */