<?php


class FinanceiroRelatoriosModel extends DB {
    
    private $report;
    public $OrderBy, $GroupBy, $CondicaoExtra, $Campos, $Debug;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->report = '';
        
        $this->OrderBy = '';
        $this->GroupBy = '';
        $this->CondicaoExtra = '';
        $this->Campos = '';
    }
    
    
    /**
     * define report
     * @param type $valor
     */
    public function SetReport($valor) {
        $this->report = $valor;
    }
    
    /**
     * Retornar valor report
     * @return type
     */
    public function GetReport() {
        return $this->report;
    }
    
    /**
     * Gera o relatório
     */
    public function Gerar() {
        
        if($this->report == '') {
        //    exit('report nao informado');
        }
        
        //$tprecpag           = "CAIXA";
        $datamovimento_ini  = $_POST['datamov_ini'];#data Cadastro no Sistema
        $datamovimento_fim  = $_POST['datamov_fim'];#data Cadastro no Sistema
        
        $databaixa_ini      = $_POST['databxmov_ini'];#databaixa Data Aluno Pagou
        $databaixa_fim      = $_POST['databxmov_fim'];#databaixa Data Aluno Pagou

        $databaixareal_ini  = $_POST['databxreal_ini'];#databaixareal Data Lcto Sistema
        $databaixareal_fim  = $_POST['databxreal_fim'];#databaixareal Data Lcto Sistema

        $datavenci_ini      = $_POST['venc_ini'];#dtvenc Vencimento Parcela
        $datavenci_fim      = $_POST['venc_fim'];#dtvenc Vencimento Parcela
        
        $usuariobaixa       = @$_POST['usubaixa'];
       // $tipoDocumento      = $_POST['tpdoc'];

        $tipodocumento = "";
        if(isset($_POST['tipodocumento'])) 
            $tipodocumento      = $_POST['tipodocumento'];
        
        $planoconta = "";
        if(isset($_POST['planoconta'])) 
            $planoconta      = $_POST['planoconta'];

        $centrocusto = "";
        if(isset($_POST['centrocusto']) )
            $centrocusto      = $_POST['centrocusto'];

        $report             = $this->GetReport();



        $wDataIni = "";
        if($datamovimento_ini != ""){
            $wDataIni = "and finmov_data >= '".DataDB($datamovimento_ini)."' ";
        }

        $wDataFim = "";
        if($datamovimento_fim != ""){
            $wDataFim = "and finmov_data <= '".DataDB($datamovimento_fim)."' ";
        }

        $wBaixaIni = "";
        if($databaixa_ini != ""){
            $wBaixaIni = "and date(finmov_databaixa) >= '".DataDB($databaixa_ini)."' ";
        }

        $wBaixaFim = "";
        if($databaixa_fim != ""){
            $wBaixaFim = "and date(finmov_databaixa) <= '".DataDB($databaixa_fim)."' ";
        }

        $wBaixaRealIni = "";
        if($databaixareal_ini != ""){
            $wBaixaRealIni = "and date(finmov_databaixareal) >= '".DataDB($databaixareal_ini)."' ";
        }

        $wBaixaRealFim = "";
        if($databaixareal_fim != ""){
            $wBaixaRealFim = "and date(finmov_databaixareal) <= '".DataDB($databaixareal_fim)."' ";
        }
        
        $wVencIni = '';
        if($datavenci_ini != '') {
            $wVencIni = " and finmov_dtvenc >= '" . DataDB($datavenci_ini)."'";
        }

        ## data vencfim
        $wVencFim = '';
        if($datavenci_fim != '') {
            $wVencFim = " and finmov_dtvenc <= '" . DataDB($datavenci_fim)."'";
        }
        
        ## USuário de baixa 
        $wUsuBaixa = "";
        if($usuariobaixa != '') {
            $usuarios = "";
            foreach($usuariobaixa as $usuario ){
                $usuarios .= "$usuario,";
            }

            $usuarios = substr($usuarios,0,-1);
            $wUsuBaixa = " and finmov_usubaixa in ($usuarios)";
        }


        $wPlanoConta = "";
        if($planoconta != '') {
            $_pc = "";
            foreach($planoconta as $pc ){
                $_pc .= "$pc,";
            }

            $_pc = substr($_pc,0,-1);
            $wPlanoConta = " and finmov_pconta in ($_pc)";
        }        


        $wCentroCusto = "";
        if($centrocusto != '') {
            $_cc = "";
            foreach($centrocusto as $cc ){
                $_cc .= "$cc,";
            }

            $_cc = substr($_cc,0,-1);
            $wCentroCusto = " and finmov_ccusto in ($_cc)";
        }

        $wTipoDocumento = "";
        if($tipodocumento != '') {
            $_td = "";
            foreach($tipodocumento as $td ){
                $_td .= "'$td',";
            }

            $_td = substr($_td,0,-1);
            $wTipoDocumento = " and finmov_tpdocumbx in ($_td)";
        }
        
        ##Tipo de doc
     //   $wTpDoc = "";
     //   if($tipoDocumento != '') {
     //       $wTpDoc = " and finmov_tpdocumbx = '$tipoDocumento'";
     //   }        

        
        if($this->Campos != '') {
            $this->Campos = ", " . $this->Campos;
        }

        $query = "SELECT f.finmov_id, f.finmov_alunoid, f.finmov_databaixareal, f.finmov_valorbaixa
                    , f.finmov_tpdocumbx, f.finmov_usubaixa, finmov_valor, f.finmov_dtvenc
                    , f.finmov_data, f.can_data
                    , s.usu_nomecompleto, s.usu_apelido, s.usu_cpf, c.usu_apelido as cadapelido
                    , s.usu_telresid, s.usu_telcelular, s.usu_telcomercial, s.usu_telrecado 
                    , d.tpd_descricao
                    , a.usu_nomecompleto AS nome_aluno
                    , a.usu_id, a.usu_telresid 
                    , a.usu_telcelular, a.usu_procvenc ,a.usu_renach, a.usu_observa 

                    $this->Campos

                    FROM fin_financeiro f
                    LEFT OUTER JOIN sis_usuarios as c on f.cad_usua = c.usu_id
                    LEFT OUTER JOIN sis_usuarios as s on f.finmov_usubaixa = s.usu_id
                    LEFT OUTER JOIN sis_usuarios AS a ON f.finmov_alunoid = a.usu_id
                    LEFT OUTER JOIN sis_tpdocum as d on f.finmov_tpdocumbx = d.tpd_cd
                    LEFT OUTER JOIN sis_centrocusto as cc on f.finmov_ccusto = cc.cec_id
                    LEFT OUTER JOIN sis_planoconta as pl on f.finmov_pconta = pl.plc_id
                    where f.can_data is null
                    
                    $wDataIni  $wDataFim $wBaixaIni $wBaixaFim $wBaixaRealIni $wBaixaRealFim $wVencIni $wVencFim
                    $wUsuBaixa $wTipoDocumento
                    $wPlanoConta $wCentroCusto

                    $this->CondicaoExtra
                        
                    AND f.finmov_localid = ".$_SESSION['APP_LOCALID']."

                    $this->GroupBy
                    $this->OrderBy
                ";

        if($this->Debug == true) {
            echo "<pre>".$query."</pre>";
        }

        //echo "<pre>".$report."</pre>";
        $reportx = $this->ExecQuery($query);

        return $reportx;

    }
}
