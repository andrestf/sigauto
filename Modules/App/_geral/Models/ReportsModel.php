<?php
 
class ReportsModel extends DB {
    
    private $report;
    public $OrderBy, $GroupBy, $CondicaoExtra, $Campos;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->report = '';
        
        $this->OrderBy = '';
        $this->GroupBy = '';
        $this->CondicaoExtra = '';
        $this->Campos = '';
    }
    
    
    /**
     * define report
     * @param type $valor
     */
    public function SetReport($valor) {
        $this->report = $valor;
    }
    
    /**
     * Retornar valor report
     * @return type
     */
    public function GetReport() {
        return $this->report;
    }


    
    /**
     * Gera o relatório
     */

}
