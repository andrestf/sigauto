<?php


class VeiculosRelatoriosModel extends DB {
    
    private $report;
    public $OrderBy, $GroupBy, $CondicaoExtra, $Campos;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->report = '';
        
        $this->OrderBy = '';
        $this->GroupBy = '';
        $this->CondicaoExtra = '';
        $this->Campos = '';
    }


    public function GetReport() {
        return $this->report;
    }

    public function SetReport($valor) {
        $this->report = $valor;
    }

    public function Gerar() {
        if($this->report == '') {
         #   exit('report nao informado');
        }


        
    	########################################################################
    	$wVeiculos = "";
    	$veiculos = isset($_POST['veiculos']) ? $_POST['veiculos'] : "";
    	if($veiculos != "") {
    		$valores = implode($veiculos,",");
    		$wVeiculos .= " AND veic_id IN ($valores)";
    	}   

        ########################################################################
        $wLocal = "";
        $local = isset($_POST['local']) ? $_POST['local'] : "";
        if($local != "") {
            $valores = "";
            foreach ($local as $value) {
                $valores .= "'".trim($value)."',";
            }
            
            if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
            $wLocal .= " AND veic_local IN ($valores)";
        }  

    	########################################################################
    	$wMarca = "";
    	$marca = isset($_POST['marca']) ? $_POST['marca'] : "";
    	if($marca != "") {
    		$valores = "";
    		foreach ($marca as $value) {
    			$valores .= "'".trim($value)."',";
    		}
    		
    		if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
    		$wMarca .= " AND veic_marca IN ($valores)";
    	}

    	########################################################################
    	$wTipo = "";
    	$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : "";
    	if($tipo != "") {
    		$valores = "";
    		foreach ($tipo as $value) {
    			$valores .= "'".trim($value)."',";
    		}
    		
    		if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
    		$wTipo .= " AND veic_tipo IN ($valores)";
    	} 

    	########################################################################
    	$wPlaca = "";
    	$placa = isset($_POST['placa']) ? $_POST['placa'] : "";
    	if($placa != "") {
    		$wPlaca .= " AND veic_placa = '$placa'";
    	}


    	########################################################################
    	$wAnoFab = "";
    	$AnoFab = isset($_POST['anofab']) ? $_POST['anofab'] : "";
    	if($AnoFab != "") {
    		$valores = "";
    		foreach ($AnoFab as $value) {
    			$valores .= "'".trim($value)."',";
    		}
    		
    		if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
    		$wAnoFab .= " AND veic_anofab IN ($valores)";
    	}  

    	########################################################################
    	$wAnoMod = "";
    	$AnoMod = isset($_POST['anomod']) ? $_POST['anomod'] : "";
    	if($AnoMod != "") {
    		$valores = "";
    		foreach ($AnoMod as $value) {
    			$valores .= "'".trim($value)."',";
    		}
    		
    		if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
    		$wAnoMod .= " AND veic_anomod IN ($valores)";
    	}  

        ########################################################################
        $wIpva = "";
        $Ipva = isset($_POST['mesipva']) ? $_POST['mesipva'] : "";
        if($Ipva != "") {
            $wIpva .= " AND veic_mesipva = '$Ipva'";
        }

        ########################################################################
        $wLicen = "";
        $Licen = isset($_POST['meslicenc']) ? $_POST['meslicenc'] : "";
        if($Licen != "") {
            $wLicen .= " AND veic_meslicenc = '$Licen'";
        } 

        ########################################################################
        $wcentroCusto = "";
        $centroCusto = isset($_POST['centrocusto']) ? $_POST['centrocusto'] : "";
        if($centroCusto != "") {
            $valores = "";
            foreach ($centroCusto as $value) {
                $valores .= "'".trim($value)."',";
            }
            
            if(substr($valores,-1) == ",") { $valores = substr($valores,0,-1);}
            $wcentroCusto .= " AND veicctr_ccusto IN ($valores)";
        }


    	$query = "SELECT $this->Campos FROM veic_veiculos
				  LEFT OUTER JOIN veic_tipos    ON veictipo_cod   = veic_tipo
                  LEFT OUTER JOIN veic_comb     ON veiccomb_cod   = veic_comb
                  LEFT OUTER JOIN veic_marcas   ON veicmarc_cod   = veic_marca
                  LEFT OUTER JOIN veic_local    ON lcl_id         = veic_local 
                  LEFT OUTER JOIN veic_controle ON veicctr_veicid = veic_id 


    			  WHERE TRUE $wVeiculos $wLocal $wMarca $wTipo $wPlaca $wAnoFab $wAnoMod $wIpva $wLicen $wcentroCusto
                    
                    
                    $this->CondicaoExtra
                    $this->GroupBy
                    $this->OrderBy
                  
                  ";
        $reportx = $this->ExecQuery($query);

        return $reportx;

    }

}

/* End of file VeiculosRelatoriosModel.php */
/* Location: .//C/Users/andre/Dropbox/Desenvolvimento/Asec/sigauto/Modules/App/_xdev/Models/VeiculosRelatoriosModel.php */