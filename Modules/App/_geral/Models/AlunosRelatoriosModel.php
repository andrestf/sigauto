<?php

class AlunosRelatoriosModel extends DB {
    
    private $report;
    public $OrderBy, $GroupBy, $CondicaoExtra, $Campos;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->report = '';
        
        $this->OrderBy = '';
        $this->GroupBy = '';
        $this->CondicaoExtra = '';
        $this->Campos = '';
    }
    
    
    /**
     * define report
     * @param type $valor
     */
    public function SetReport($valor) {
        $this->report = $valor;
    }
    
    /**
     * Retornar valor report
     * @return type
     */
    public function GetReport() {
        return $this->report;
    }
    
    /**
     * Gera o relatórios
     */
    public function Gerar() {
        
        if($this->report == '') {
        //    exit('report nao informado');
        }
        
       #$DataPadrao = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL "; 
       $DataPadrao = "  "; 
        
        $wDataFase = '';
        
        if(!$_POST){
            $_POST = $_GET;
        }


        if(isset($_POST['data_fase']) AND $_POST['data_fase'] != '') {
            $data_fase = DataDB($_POST['data_fase']);
            $data_fase = $this->Prepare($data_fase);
            $wDataFase = " AND date(amf_dataprocesso) >= '$data_fase' ";
            $DataPadrao = "";
        }
        
        
        if(isset($_POST['data_fasefin']) AND $_POST['data_fasefin'] != '') {
            $data_fasefin = DataDB($_POST['data_fasefin']);
            $data_fasefin = $this->Prepare($data_fasefin);
            $wDataFase .= " AND date(amf_dataprocesso) <= '$data_fasefin' ";
            $DataPadrao = "";
        }

        $wDataIni = '';
        if(isset($_POST['data_ini']) AND $_POST['data_ini'] != '') {
            $data_ini = DataDB($_POST['data_ini']);
            $data_ini = $this->Prepare($data_ini);
            $wDataIni = " AND date(amf_iniciado) >= '$data_ini' ";
            $DataPadrao = "";
        }
        
        $wDataFim = '';
        if(isset($_POST['data_fim']) AND $_POST['data_fim'] != '') {
            $data_fim = DataDB($_POST['data_fim']);
            $data_fim = $this->Prepare($data_fim);
            $wDataFim = " AND date(amf_concluido) <= '$data_fim' ";
            $DataPadrao = "";
        }

        $wMatricula = "";
        $wProcesso  = "";
        $wFase      = "";
        $wResultado = "";

        if(isset($_POST['matricula']) AND $_POST['matricula'] != '') {
            $wMatricula = "AND tpmat_id = ".$_POST['matricula'];
        }

        if(isset($_POST['processo']) && $_POST['processo'] != '') {
            $processo = implode(",",$_POST['processo']);
            $wProcesso = "AND amat_servico in ($processo)";
        }
        
        if(isset($_POST['fase']) && $_POST['fase'] != '') {
            $fase = implode(",",$_POST['fase']);
            $wFase = "AND amf_servitenid in ($fase)";
        }
        
        if(isset($_POST['resultado']) && $_POST['resultado'] != '') {
            //$resultado = implode(",",$_POST['resultado']);
            $wResultado = "AND amf_resultado = '".$_POST['resultado']."' ";
        }        

        if($this->GroupBy != '') {
            $this->GroupBy = 'GROUP BY '.$this->GroupBy;
        }
        
        if($this->OrderBy != '') {
            $this->OrderBy = 'ORDER BY '.$this->OrderBy;
        }
        
        if($this->Campos != '') {
            $this->Campos = ",".$this->Campos;
        }
        $report = "
                    SELECT
                    usu_id, usu_nomecompleto, usu_rg, usu_cpf, usu_telresid, 
                    usu_telcelular, usu_procvenc, usu_renach, usu_observa, usu_observafinan, 
                    usu_numdoc, usu_catatual, usu_sexo, usu_nascimento,
                    usu_uf, usu_nacionalidade, usu_naturalidade, usu_estadocivil,
                    usu_nomemae, usu_nomepai,
                    usu_logradouro, usu_logranumero, usu_lograbairro, usu_lograuf,
                    usu_logramunicipio, usu_logracomplemento, usu_cep,
                    tpmat_descricao, 
                    amat_incluitaxa, amat_servico, amat_vlrfinal, amat_venc, 
                    amat_formapaga, amat_nparcelas, amat_vlrentrada, 
                    serv_descricao, serv_marcacao, 
                    serviten_descricao, serviten_apelido, serv_descapelido,
                    amf_servitenid, amf_dataprocesso, amf_iniciado, amf_concluido, 
                    amf_resultado, amf_alunoid, amf_examepratico,
                    tpd_descricao, cur_descricao, cur_dias, cur_hora, cur_hralmoco,
                    cur_hrtermino, cur_turma, cur_sala, cur_formacao, cur_atualizacao,
                    cur_contato1, cur_contato2, cur_contato3, cur_contato4, cur_ativo,
                    amat_situacaodata
                    
                    $this->Campos

                    FROM sis_alunosmatfases

                    LEFT OUTER JOIN sis_usuarios      ON amf_alunoid = usu_id
                    LEFT OUTER JOIN sis_alunosmat     ON amat_id     = amf_matid
                    LEFT OUTER JOIN sis_servicos      ON serv_id     = amat_servico
                    LEFT OUTER JOIN sis_tpmatriculas  ON tpmat_id    = amat_tpmat
                    LEFT OUTER JOIN sis_servicositens ON serviten_id = amf_servitenid
                    LEFT OUTER JOIN sis_tpdocum       ON tpd_cd      = amat_formapaga
                    LEFT OUTER JOIN sis_cursos        ON cur_id      = usu_cursos
                    

                    where TRUE
 
                    $wDataIni
                    $wDataFim
                    $wDataFase
                        
                    $wMatricula
                    $wProcesso
                    $wFase
                    $wResultado
                    $DataPadrao
                    
                    $this->CondicaoExtra
                    

                    AND usu_localid = ".$_SESSION['APP_LOCALID']."

                    
                    $this->GroupBy
                    
                    $this->OrderBy

                  ";
        #echo "<pre>".$report."</pre>";
        $reportx = $this->ExecQuery($report);

        return $reportx;

    }
}
