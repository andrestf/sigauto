<?php

class VeiculosModel extends DB {


    public $APP_LOCALID, $cn;
    
    public function __construct() {
        //parent::__construct();
        $this->cn = $this->CnCliente();
        $this->APP_LOCALID = $_SESSION['APP_LOCALID'];

        $cfg = new ConfigHelper();
        $x = $cfg->getVars();
        eval($x);
        
    }
    

    public function Seleciona($id) {

        $query = "  SELECT * FROM veic_veiculos 
                    LEFT OUTER JOIN veic_tipos   ON veictipo_cod = veic_tipo
                    LEFT OUTER JOIN veic_comb    ON veiccomb_cod = veic_comb
                    LEFT OUTER JOIN veic_marcas  ON veicmarc_cod = veic_marca
                    WHERE (veic_localid = 0 OR veic_localid = ".$_SESSION['APP_LOCALID']." AND veic_id = '$id' )";
        $query = $this->ExecQuery($query);
        

        if($query->num_rows >= 1) {
            $retorno = $this->result_array();    
            return $retorno[0];
        }
        

        return false;
    }
    
    public function Listar($condicao = '', $limite = "", $order = ' ORDER BY 1 DESC ') {
        $query = "  SELECT * FROM veic_veiculos 
                    LEFT OUTER JOIN veic_tipos ON veictipo_cod = veic_tipo
                    LEFT OUTER JOIN veic_comb  ON veiccomb_cod = veic_comb
                    LEFT OUTER JOIN veic_marcas  ON veicmarc_cod = veic_marca
                    WHERE 
                    $condicao 
                    AND (veic_localid = 0 OR veic_localid = ".$_SESSION['APP_LOCALID']."  )
                    $order 
                    $limite
                    ";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        if($retorno) {
        	return $retorno;
        }

        return false;
    }


    public function Update($id,$campos = array()) {
        
        $ups = "";

        foreach ($campos as $campo => $valor) {
            $ups .= " `veic_$campo` = '$valor',";
        }

        $ups = substr($ups,0,-1);

        $up = "UPDATE veic_veiculos SET $ups WHERE veic_id = '$id' AND veic_localid = '$this->APP_LOCALID' ";

        //echo "<br/>";
        $c = $this->ExecNonQuery($up);

        if($this->GetErro("",false)) {
            return $this->GetErro();
        } else {
            return $c;
        }                
    }

    public function Cadastrar($object = array()) {

        $object['localid'] = $this->APP_LOCALID;
        #$object['cad_data'] = "CURRENT_TIMESTAMP()";
        #$object['cad_usua'] = $_SESSION['APP_USUID'];

        $nCampos = count($object);
        $n = 1;

        $campos  = "";
        $valores = "";
        foreach ($object as $key => $value) {
            $sep = ",";
            if($n >= $nCampos) {
                $sep = "";
            }
            $campos  .= "`veic_".$key."`".$sep." ";

            if($value == "" || $value == NULL) {
                $valores .= "NULL".$sep;
            } else {
                $valores .= "'".$this->Prepare($value)."'".$sep;    
            }
            

            $n++;
        }

        $campos  .= ",cad_usua                     ,cad_data";
        $valores .= ",'".$_SESSION['APP_USUID']."' ,CURRENT_TIMESTAMP";



        $query = "INSERT into veic_veiculos ($campos) VALUES ($valores)";
        
        $c = $this->ExecNonQuery($query);

        if($this->GetErro("",false)) {
            return $this->GetErro();
        } else {
            return $c;
        }        
    }

    public function ListaDespesas($condicao, $limite, $order = " ORDER BY 1 DESC ") {
    }

    public function InsertDespesas($object = array()) {

        $Controle = new VeiculosCtrModel();

        $this->autocommit(false);

        $lOK = true;

        if($object['lancafinan']) {
            $this->DESPVEIC_LANCAFINAN = true;
        } else {
            $this->DESPVEIC_LANCAFINAN = false;
        }


        $object['veicxdesp_localid'] = $this->APP_LOCALID;
        $idVeiculo = $object['veicxdesp_veicid'];


        unset($object['lancafinan']);

        $campos = "";
        $valores = "";
        foreach ($object as $campo => $valor) {
            $campos  .= "$campo,";
            
            if($valor == "NULL" || $valor == "") {
                $valores .= "NULL,";
            } else {
                $valores .= "'".$this->Prepare($valor)."', ";
            }
            
        }

        $campos  .= "cad_usua                     ,cad_data";
        $valores .= "'".$_SESSION['APP_USUID']."' ,CURRENT_TIMESTAMP";

        
        # VEFIRICANDO kM ATUAL DO VEICULO
        ####################################################################################
        $KmAtual = $Controle->KmAtual($idVeiculo);
        
        if($object['veicxdesp_km'] < $KmAtual) {
            $this->SetErro("KM Informada é menor que atual");
            return $this->GetErro();
        }


        # Inserindo despesa do veiculo
        ####################################################################################
        $query = "INSERT into veic_veicxdespesas ($campos) VALUES ($valores)";
        $d = $this->ExecNonQuery($query);


        if($this->GetErro("",false)) {
            $lOK = false;
        } else {

        $idDespesa = $d->insert_id;

        if($this->DESPVEIC_LANCAFINAN) {
            #echo "LANCA FINAN";
        } else {
            #echo "NAO VAI LANCA FINAN";
        }

        ## SE LANCAR DESPESAS NO FINANCEIRO
        if( $this->DESPVEIC_LANCAFINAN ) {

            # Criando Movimento novo no Financeiro
            ####################################################################################
            $finan = new FinanceiroModel();
            $idaluno = 0;
            $valor =$object['veicxdesp_valor'];

            $vencimento = DataBR($object['veicxdesp_data']);
            $tipo = "S";
            $descricao = $object['veicxdesp_descri'];
            $movtipo = $object['veicxdesp_despcod'];
            $deonde = "VEICDXESPESAS";
            $deondeid = $idDespesa;
            $autobaixa = true;
            $tprecpag = "FINAN";
            
            
            $ccusto = "$this->CCUSTO_DESPVEIC";
            $pconta = "$this->PCONTAS_DESPVEIC";

             $pconta = $object['veicxdesp_pconta'];
             $ccusto = '"'.$object['veicxdesp_ccusto'].'"';


            $nMesesRepetir = 1;

            ## LANCANDO FINANEIRO
            $f = $finan->CriaMovimento($idaluno,$valor,$vencimento,$tipo,$descricao,$movtipo,$deonde,$deondeid,$autobaixa,$tprecpag,$ccusto,$pconta,$nMesesRepetir);
            if(!$f) {
                $lOK = False;
            }

            # CRIANDO LOG FINANCEIRO
            ####################################################################################     
            $id_mov   = $f;
            $idFinan = $f;
            $acao   = "INS";
            $altera = "Criação Movimento Despesa Veiculo #$idVeiculo Depesa: #$d->insert_id ";
            $l = $finan->SalvaLog($id_mov,$acao,$altera);            
        }




        # Criando Controle Novo
        ####################################################################################
        $camposx = array(
            "descri"   => "LANCAMENTO DESPESAS",
            "data"   => $object['veicxdesp_data'],
            "respon" => $object['veicxdesp_respon'],
            "tipo" => "SIS_AUT",
            "deonde" => "VEICDXESPESAS",
            "deondeid" => $idDespesa,
            "respon"   => $object['veicxdesp_respon'],
            "kmfim"    => $object['veicxdesp_km'],
            "pconta" => $object['veicxdesp_pconta'],
            "ccusto" => $object['veicxdesp_ccusto']

        );
        
        
        if(!$Controle->Criar($idVeiculo,$camposx)) {
            $lOK = false;
        }
        
        ## SE LANCAR FINANCEIRO, ATUALIZA A DESPESA COM ID DO FINANCEIRO CRIADO
        if( $this->DESPVEIC_LANCAFINAN ) {
            $up = "UPDATE veic_veicxdespesas SET veicxdesp_finanid = '$idFinan' WHERE veicxdesp_id = '$idDespesa' ";
            $up = $this->ExecNonQuery($up);
            if($up->affected_rows == "1") {
                
            } else {
                $lOK = false;
                $this->SetErro("FALHA AO SINCRONIZAR DESPESAS COM FINANCEIRO");
            }
        }


        # Se tudo der certo da um commit()
        ####################################################################################
        if($lOK) {
            $this->commit();
            $this->autocommit(true);
            return $d;
        } else {
            $this->rollback();
            #remover para trazer erro  do banco de dados

            if($this->GetErroMsg() == null || $this->GetErroMsg() == "") {
                $this->SetErro("FALHA AO TENTAR SALVAR RESGISTRO");                
            }

            
            return $this->GetErro();
        }

            

        }  

    }

    public function EditarDespesas($id,$campos = array()) {

    }

    public function Exporta($condicao = "",$campos = array(),) {

        foreach ($campos as $campo => $valor) {
            //
        }

        //Data de Cadastro
        $wDataIni = "";
        $wDataFim = "";

        //Data de vencimento do Licenciamento
        $wDataVencLicIni = ""; 
        $wDataVencLicFim = "";

        // vencimento do licenciamento
        $wVencLic = "";

        // marca
        $wMarca = "";

        $query = "
                    SELECT * FROM veic_veiculos 
                    LEFT OUTER JOIN veic_tipos ON veictipo_cod = veic_tipo
                    LEFT OUTER JOIN veic_comb  ON veiccomb_cod = veic_comb
                    LEFT OUTER JOIN veic_marcas  ON veicmarc_cod = veic_marca
                    WHERE 
                    $condicao 

                    $wDataIni $wDataFim

        ";

    }

}
