<?php 

//lanca no financeiro, mas nao desconta credito do aluno

class AlunosFinanceiroModel extends DB {
    
    /** 
     *  
     * Verifica a se existem parcelas lançadas para o aluno!
     * */
    public function VerificaLancamento($id) {
            $this->cn = $this->CnCliente();
            $query = "SELECT count(finmov_id) as TOT FROM fin_financeiro WHERE finmov_alunoid = '$id' AND finmov_databaixa IS NULL AND can_data IS NULL  ";
            $query = $this->ExecQuery($query);
            $query = $query->fetch_object();
            return $query->TOT;
    }

    /**
     * Verifica a se existem parcelas lançadas para o aluno referente a matricula
     * */
    public function VerificaLancamentoMatricula($aluno,$matricula) {
            $this->cn = $this->CnCliente();
            $query = "SELECT count(finmov_id) as TOT FROM fin_financeiro WHERE finmov_alunoid = '$id'
                              AND finmov_deonde = 'MATRIC' AND finmov_deondeid = '$matricula' AND can_data IS NULL  ";
            $query = $this->ExecQuery($query);
            $query = $query->fetch_object();
            return $query->TOT;
            return 0;
    }

    public function ListaParcelas($aluno) {
            $this->cn = $this->CnCliente();
            $query = "SELECT * FROM fin_financeiro WHERE finmov_alunoid = '$aluno' ORDER BY finmov_dtvenc ASC";
            $this->ExecQuery($query);
            $retorno = $this->result_array();

            if(!$retorno)
                    return false;

            return $retorno;
    }

    /**
     * Lanca parcelas no financeiro do aluno
     * @param type|array $parcelas 
     * @return type
     */
    public function LancaParcelas($parcelas = array(),$tp = '') {
        
        $nEntrada = 0;
        $vlrEntrada = 0;
        if(isset($_POST['nEntrada'])) {
            $nEntrada = $_POST['nEntrada'];
            $vlrEntrada = $parcelas[0]['valor'];
        }
        #echo $vlrEntrada;
       # exit("<br> aa");
        
        if(isset($_POST['parcelas'])) {
            $parcelaInfos = $_POST['parcelas'];
        }
        else {
            $parcelaInfos['nParcelas'] = count($parcelas);
            $parcelaInfos['valorJur'] = 0;
        }
            /** formato das parcelas 
            [0] => Array
                    (
                        [parcela] => N
                        [valor] => nn.nn
                        [vencimento] => dd/mm/yyyy
                        [descricao] => nonon
                        [documento] => DINH*
                        [tipo] => ADICIONAL
                    );
            */

            $this->cn = $this->CnCliente();
            $this->autocommit(false);

            $LOCALID = $_SESSION['APP_LOCALID'];
            $deonde   = 'MATRIC';

            $deondeid =  "";
            if(isset($_GET['matricula'])) {
                $deondeid =  $_GET['matricula'];
            }
            $alunoid  = $_GET['codigo'];
            $i = 0;
            $CAD_USUA = $_SESSION['APP_USUID'];
            
            $NumTotParcelas = count($parcelas) - $nEntrada;
            
            
            $valorFinal = $parcelaInfos['valorJur'];

            ## se nao tiver definido isset($parcelas[1])
            $ValorDesconto = 0;
                    
            if(isset($parcelas[1])) {
                $ValorDesconto = $parcelas[1]['valorOriginal'] - $parcelaInfos['valorJur'];
                $formaPaga = $parcelas['1']['documento'];              
            }
            
            foreach ($parcelas as $xparcela) {



                    $formaPaga = $xparcela['documento'];     

                    $i++;

                    $valor0 		= $xparcela['valor'];
                    $valor 		= $xparcela['valor'];
                    $vencimento = DataDB($xparcela['vencimento']);
                    
                    $dia = substr($vencimento, 8,2);
                    $mes = substr($vencimento, 5,2);
                    $ano = substr($vencimento, 0,4);

                    $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
                    if($dia >= $ultimo_dia) {
                        $vencimento = $ano . "-" . $mes . "-" . $ultimo_dia;
                    }                    
                                      
                    
                    $descricao  = $xparcela['descricao'];
                    $documento  = $xparcela['documento'];
                    $tipo		= $xparcela['tipo'];
                    $data 		= date('Y-m-d');
                    $valorOriginal = $xparcela['valorOriginal'];

                    $valor = number_format($valor,2,".",",");
                    $valor = str_replace(",","",$valor);

                    //$valorOriginal = number_format($valorOriginal, 2, ".", ",");
                    $valorOriginal = str_replace(".", "", $valorOriginal);
                    $valorOriginal = str_replace(",", ".", $valorOriginal);      
                    
                    

                    //TIPOS DE DOCUMENTOS;            
                    ########################################################################
                    ## SE FOR UM DESCONTO DE CRÉDITOS
                    if($documento == 'DESC' && $valor < 0)  {
                        $valorOriginal = $valor;
                        $Creditos = new CreditosModel();
                        
                        $Creditos->DescontaValor($alunoid,$valor*-1);
                    }
                    $lOK = true;    
                    
                    ########################################################################
                    ##SE FOR UM CREDITO
                    if($tp == 'CREDITO') {
                        $descri = $_POST['descricao'];
                        $query = "INSERT INTO sis_alunoscreditos (alucre_localid, alucre_alunoid, alucre_vlr, alucre_descri, alucre_data, alucre_motivo, cad_data, cad_usua, alucre_tpdocum )  VALUES ('$LOCALID', '$alunoid', '$valor', '$descri', current_timestamp(), 'MANUAL', current_timestamp(), '$CAD_USUA','$documento')";
                        

                        if (!$this->ExecNonQuery($query)) {
                            $this->rollback();
                            $lOK = false;
                            return false;
                        }

                        //Alterar os valore do lancamento no financeiro
                        $descricao = $descri;
                        $tipo = "CREDITO";
                        $deonde = "CREDITO";
                        $deondeid = $this->cn->insert_id;

                        //se for crédito o finmov_tpdocum  sera igual a finmov_tpdocumbx
                    }
                    
                    ########################################################################
                    ## SE LANCAMENTO DA PARCELA FOR DIFERENTE DE CREDITO, SEMPRE VAI CAIR AQUI!HAUEAHUEHAUHAE                    
                    //if($descricao != "Desconto Crédito")  {
                    
                        $query = "INSERT INTO fin_financeiro
                                (finmov_alunoid, finmov_tpmov, finmov_status, finmov_localid, finmov_tpdocum, finmov_tpdocumbx ,finmov_valor,finmov_valorori,finmov_data, finmov_dtvenc,finmov_deonde, finmov_deondeid, finmov_tipo, finmov_descricao, cad_usua   , cad_data) VALUES (
                                '$alunoid'     , 'E'         , 'ATIVO'      , '$LOCALID'    , '$documento'  , '$documento'     ,'$valor'    ,'$valorOriginal','$data'    , '$vencimento', '$deonde'   , '$deondeid'    , '$tipo'    , '$descricao'    , '$CAD_USUA', CURRENT_TIMESTAMP())
                        ";
                    
                        
                        $i++;
                        $lOK = true;
                        if(!$this->ExecNonQuery($query)) {
                                $this->rollback();
                                $lOK = false;
                                return false;                                
                        } 
                        /*    */
                    //}
                    
                    $last_id = $this->cn->insert_id;

                    if($lOK) {
                        if($documento != 'NOTP') {
                            
                            if($vencimento != '')
                                $vencimento = $vencimento ." ".date("G:i:s");
                            

                            $update = "UPDATE fin_financeiro SET finmov_valorbaixa = '$valor0', finmov_databaixareal = CURRENT_TIMESTAMP() ".
                                      ", finmov_databaixa = '$vencimento', finmov_usubaixa = '$CAD_USUA', finmov_obs = 'BX_AUT_SIS' ".
                                      ", finmov_tprecpag = 'CAIXA', finmov_tpdocumbx = '$documento' WHERE finmov_id = '$last_id' ";

                            $this->ExecNonQuery($update);
                        }                    
                    }
                    /**/                       

            }
            //Atualiza mat  ricula, marcando como exportada(parcelas geradas!);
            /**/           
            
            $up = "UPDATE sis_alunosmat SET "
                    . "amat_desconto = '$ValorDesconto', amat_formapaga = '$formaPaga', amat_nparcelas = '$NumTotParcelas', amat_vlrentrada = '$vlrEntrada', "
                    . "amat_exporta = CURRENT_TIMESTAMP(), amat_vlrfinal = '$valorFinal', amat_exportausua = '$CAD_USUA' WHERE amat_id = '$deondeid' and amat_alunoid = '$alunoid' ";
             
            if(!$this->ExecNonQuery($up)) {
               $this->rollback();
               return false;
            }
            /* */


            /**/

            $this->commit();
            $this->autocommit(true);

            return true;
    }
    
    
    public function SomaValorPago($id_aluno,$campo = 'finmov_valorbaixa') {
        $this->cn = $this->CnCliente();
        
        $query = " SELECT sum($campo) as TOTAL FROM fin_financeiro WHERE finmov_alunoid = $id_aluno AND NOT finmov_databaixa IS NULL AND can_data IS NULL ";
        $this->ExecQuery($query);
            $x = $this->result_array();
            
            if($x != '') {
                return $x[0]['TOTAL'];
            } else {
                return "0";
            }        
       
    }
    
    public function SomaValorDevido($id_aluno,$campo = 'finmov_valorbaixa') {
        $this->cn = $this->CnCliente();
        
        $query = " SELECT sum($campo) as TOTAL FROM fin_financeiro WHERE finmov_alunoid = $id_aluno AND finmov_databaixa IS NULL AND can_data IS NULL";
        $this->ExecQuery($query);
            $x = $this->result_array();
            
            if($x != '') {
                return $x[0]['TOTAL'];
            } else {
                return "0";
            }                
    }
    
    public function VerificaParcelas() {
        
    }

}