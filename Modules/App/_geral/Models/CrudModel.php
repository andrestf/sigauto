<?php

class CrudModel extends DB {


    public $table, $campolocal, $campoid, $campoorder, $groupby;
    public $joins;

    public function __construct() {
        $this->cn = $this->CnCliente();
        
        $this->table      = "";
        $this->joins      = array();
        $this->campoid    = "";
        $this->campolocal = "";
        $this->campoorder = "";

        $this->groupby   = "";
        return $this;
    }
    
    public function Join($table,$condi,$type = " LEFT OUTER JOIN ") {
        $x = count($this->joins);
       $this->joins[$x] = " $type $table on $condi ";
    }


    public function Listar() {

        $_j = "";
        foreach ($this->joins as $j) {
            $_j .= $j;
        }

        

        $groupby = '';
        if($this->groupby != '') {
            $groupby = " GROUP BY $this->groupby " ;
        }

        
                    
        $query = "SELECT * 
                        FROM $this->table 
                        $_j
                        WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) 
                        ORDER BY  $this->campoorder 
                        $groupby
                ";

        //        exit();
                        
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }
    
    
    public function seleciona($id = "") {
        
        if($id == "") {
            exit();
        }

        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) AND $this->campoid = '$id'";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno[0];
    }
    
    public function Update($id,$campos = array()) {
        if(!is_array($campos) || $campos == "" || count($campos) == 0) {
            return false;
        }
        
        $upd = "";
        foreach ($campos as $campo => $valor) {
            $upd .= "`$campo` = '$valor',";
        }
        
        $upd = substr($upd,0,-1);
        
        $update = "UPDATE $this->table SET $upd WHERE $this->campoid = '$id' ";
        if($this->ExecQuery($update)) {
            return true;
        }
        return false;
    }
    

    public function inserir($campos) {
        return $this->insert($this->table,$campos);
    }   
	

}

/* End of file CrudModel.php */
/* Location: .//C/Users/andre/Dropbox/Desenvolvimento/Asec/sigauto/Modules/App/_geral/Models/CrudModel.php */