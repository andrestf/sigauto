<?php

class DashboardModel extends DB {

    public $filtro, $dIni, $dFim, $wLocal, $wServicos;


    
    public function __construct() {
        $this->cn = $this->CnCliente();

        $this->filtro = false;
    }	




    public function SetdIni($d) {
    	$d = DataDB($d);
    	$this->dIni = $d;
    	return $d;
    }

    public function SetdFim($d) {
    	$d = DataDB($d);
    	$this->dFim = $d;
    	return $d;
    }

    public function SetLocais($locais,$pref) {
    	if($locais == "") {
    		$this->wLocal = "";
    		return "";
    	}

    	$local = "";
    	foreach ($locais as $_local) {
    		$local .= $_local.",";
    	}
    	$campo = $pref."_localid";
    	$this->wLocal = " and $campo IN (" .substr($local,0,-1) . ") ";
    }

    public function SetTpServicos($servs,$campo = 'amat_servico') {

        if($servs == "") {
            $this->wServicos = "";
            return "";
        }

        $serv = "";
        foreach ($servs as $_serv) {
            $serv .= $_serv . ",";
        }
        $this->wServicos = " and $campo IN (" .substr($serv,0,-1) . ") ";
    }

    public function GetServicos() {
        $wLocais = $this->wLocal;
        

        if ($wLocais == '') {
            $wLocais = true; }


        $query = "SELECT * FROM sis_servicos WHERE $wLocais";
        $res = $this->ExecQuery($query);
        if($res->num_rows > 0) {
            return $this->result_array();
        }

        return false;

    }

    public function Cadastros($where = '') {
    	$dIni	 = $this->dIni;
    	$dFim 	 = $this->dFim;
    	$wLocais = $this->wLocal;

    	$query = "SELECT count(usu_id) as TOTAL FROM sis_usuarios WHERE usu_tipo = 'ALUNO' and date(cad_data) >= '$dIni' AND date(cad_data) <= '$dFim' $where $wLocais ";
        $query = $this->ExecQuery($query);
        $query = $query->fetch_object();
        return $query->TOTAL;
    }

    public function Matriculas($where = '') {

		$dIni	 = $this->dIni;
    	$dFim 	 = $this->dFim;
    	$wLocais = $this->wLocal;    	

        $wServicos = $this->wServicos;
        if ($wServicos == '') {
            $wServicos = " AND true "; }


        #ERA
        #$query = "SELECT count(amat_id) as TOTAL FROM sis_alunosmat WHERE date(cad_data) >= '$dIni' AND date(cad_data) <= '$dFim' $where $wLocais";
    	$query = "SELECT count(amat_id) as TOTAL FROM sis_alunosmat WHERE date(amat_situacaodata) >= '$dIni' AND date(amat_situacaodata) <= '$dFim' $where $wLocais $wServicos";
        $query = $this->ExecQuery($query);
        $query = $query->fetch_object();
        return $query->TOTAL;

    }


    public function Servicos($where = '') {

		$dIni	 = $this->dIni;
    	$dFim 	 = $this->dFim;
    	$wLocais = $this->wLocal;    	

        $wServicos = $this->wServicos;
        if ($wServicos == '') {
            $wServicos = " AND true "; }


    	$query = "SELECT *
    			  FROM sis_alunosmat 
				  LEFT OUTER JOIN sis_servicos ON serv_id = amat_servico
    			  WHERE date(amat_situacaodata) >= '$dIni' AND date(amat_situacaodata) <= '$dFim' $where $wLocais $wServicos ";

		##########################################################################################        
        $res = $this->ExecQuery($query);
        if($res->num_rows > 0) {
        	return $this->result_array();
        }

        return array();

    }    



}