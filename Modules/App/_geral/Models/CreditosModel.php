<?php

class CreditosModel extends DB {

    public function __construct() {
        $this->cn = $this->CnCliente();
    }

    public function Lista($id_aluno) {
        $query = "SELECT * FROM sis_alunoscreditos WHERE alucre_alunoid = '$id_aluno' ORDER BY alucre_data DESC";
        $select = $this->ExecQuery($query);


        if ($select->num_rows > 0) {
            return $this->result_array();
        }

        return false;
    }
    
    public function DescontaValor($id_aluno,$valor) {
        
        $valorDisponivel = $this->SomaCreditoDisponivel($id_aluno);
        $valorDisponivel = number_format($valorDisponivel,2,".","");
        $valor = number_format($valor,2,".","");
        if($valor > $valorDisponivel) {
            echo "Valor de crédito é maior que o valor disponivel!";
            return false;
        }
        
        
        /**/
        $select = "SELECT * FROM sis_alunoscreditos WHERE alucre_alunoid = '$id_aluno' AND alucre_utiliza IS NULL";
        $this->ExecQuery($select);
        $x = $this->result_array();
        $item = $x[0];
        
        if($valor <= $item['alucre_vlr']) {
            
            $ValorUsado = $valor;
            $this->MarcaUtilizado($id_aluno, $item['alucre_id'], 'UTLIZADO PARCELA', $valor);
        } else {
            
            $ValorUsado = $item['alucre_vlr'];
            $this->MarcaUtilizado($id_aluno, $item['alucre_id'], 'UTLIZADO PARCELA', $item['alucre_vlr']);
        }

        $resto = $valor - $ValorUsado;
        
        if($resto > 0) {
            $this->DescontaValor($id_aluno,$resto);
        }
        /*
        $valorDisponivel = $this->SomaCreditoDisponivel($id_aluno);
        
        $resto = $valorDisponivel - $valor; 
        
        echo "Resto => " . $resto;
        if($resto < 0 ) {
            $resto = $resto*-1;
        }
        
        if($resto > 0 ) {
            //$this->DescontaValor($id_aluno,$resto*-1);
        }
            
        /** * */
    }
    
    public function SomaCreditoDisponivel($id_aluno) {
        $query = "SELECT sum(alucre_vlr) AS TOTALDISP FROM sis_alunoscreditos WHERE alucre_alunoid = '$id_aluno' AND alucre_utiliza IS NULL AND alucre_vlrutilizado IS NULL";
        $this->ExecQuery($query);
        $re = $this->result_array();
        if($re) {
            if($re[0]['TOTALDISP'] <= 0 ) {
                return "0";
            }
            return $re[0]['TOTALDISP'];
        } else {
            return "0";
        }
    
    }


    public function Seleciona($id_credito, $id_aluno) {

        $query = "SELECT * FROM sis_alunoscreditos WHERE alucre_id = '$id_credito' AND alucre_alunoid = '$id_aluno' AND alucre_utiliza IS NULL";
        $select = $this->ExecQuery($query);

        if ($select->num_rows > 0) {
            return $this->result_array();
        }

        return false;
    }

    private function CriaCredito($valor, $id_aluno, $id_creditoAntigo, $descri) {
        $LOCALID = $_SESSION['APP_LOCALID'];
        $IDUSUA = $_SESSION['APP_USUID'];
        $ins = "INSERT INTO sis_alunoscreditos (alucre_localid,alucre_alunoid,alucre_vlr,alucre_descri,alucre_data,alucre_motivo,alucre_lancsistema,cad_data,cad_usua)"
                . " VALUE ('$LOCALID','$id_aluno','$valor','$descri',CURRENT_TIMESTAMP(),'DIFERENÇA DE CRÉDITO [#$id_creditoAntigo]','*',current_timestamp(), '$IDUSUA')";
        $x = $this->ExecNonQuery($ins);

        if ($x->affected_rows == 1) {
            return $x->insert_id;
        } else {
            return 0;
        }
    }

    public function MarcaUtilizado($id_aluno, $id_credito, $motivo, $vlr_utilizado,$cancelar = false) {
        
        
        $valor = $vlr_utilizado;
        //$valor = str_replace(".", "", $vlr_utilizado);
        //$valor = str_replace(",", ".", $valor);
        $lOK = true;
        $crAtual = $this->Seleciona($id_credito, $id_aluno);
        if (!$crAtual) {
            $ret['erro'] = "Crédito atual é invalido!";
            json_encode($ret);
        } else {
            

            $crAtual = $crAtual[0];

            $valorAtual = $crAtual['alucre_vlr'];

            if ($valor < $valorAtual) {
                $valorNovo = $valorAtual - $vlr_utilizado;
                $descri = $crAtual['alucre_descri'];
                $x = $this->CriaCredito($valorNovo, $id_aluno, $id_credito, $descri);

                if ($x == 0) {
                    $ret['erro'] = "Erro ao lançar diferença de créditos";
                    return $ret;
                }
            }
            
            
            

            if ($valor > $valorAtual) {
                $ret['erro'] = "Crédito atual é menor do que o solicitado!";
                return $ret;
            }
        
            $up00 = " alucre_vlrutilizado = '$valor', alucre_motivoutiliza = '$motivo', alucre_utiliza = CURRENT_TIMESTAMP(), alucre_usuautiliza = '" . $_SESSION['APP_USUID'] . "' ";
            if($cancelar) {
                $motivo = "[CANC] " . $motivo;
                $up00 = " alucre_motivocan = '$motivo', can_data = CURRENT_TIMESTAMP(), can_usua = '" . $_SESSION['APP_USUID'] . "' ";
            }
        
            $this->autocommit(false);
              
            $lMov = true;
#            if(true) { ##
                
                $query = "SELECT * FROM sis_alunoscreditos WHERE alucre_id = $id_credito";// and can_data IS NULL ";
                $select = $this->ExecQuery($query);

                if ($select->num_rows > 0) {
                    $update = "UPDATE sis_alunoscreditos SET $up00 WHERE alucre_id = '$id_credito' AND alucre_alunoid = '$id_aluno'";
                    $cr = $this->result_array();
                    $cr = $cr[0];
                    $cr['alucre_data'] = DataBR($cr['alucre_data']);    
                }            
            
            if($cancelar) {    
                $finan = new FinanceiroModel();
                $mov = $finan->CriaMovimento($cr['alucre_alunoid'],$cr['alucre_vlr']*-1,$cr['alucre_data'],'S',$motivo,'EST.CREDITO','CREDITO',$cr['alucre_id'],$autobaixa = false,$cr['alucre_tpdocum'],$ccusto="",$pconta="");
                if($mov > 0) {
                    $lMov = true;
                } else {
                    $lMov = false;
                }
            }
            
            if ($lMov && isset($update)) {
                $this->ExecNonQuery($update);
                $this->commit();
                $ret['menssagem'] = "Operação realizada com sucesso.";
            } else {
                $this->roolback();
                $ret['erro'] = "Erro de lançamento. ";
            }
        }
        $this->autocommit(true);
        
        
        return $ret;
    }

}
