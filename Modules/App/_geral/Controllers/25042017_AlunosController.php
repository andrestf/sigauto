<?php

class AlunosController extends Controllers {

    public function index() {
        $this->listar();
    }

    /**
     * listagem dos alunos
     * */
    public function listar() {
        $this->ValidaNivel(40, false);

        $AlunosLista = new AlunosModel();
        $AlunosLista = $AlunosLista->Listar();

        $datas = array(
            "viewDatas" => array(
                "AlunosLista" => $AlunosLista,
                "PageTitle" => "Alunos / Candidatos"
            )
        );

        $this->RenderView("alunos/lista", $datas);
    }

    public function listagem() {
        $AlunosListaModal = new AlunosModel();


        $condicao = "";
        if (isset($_GET['nome'])) {
            $nome = $AlunosListaModal->Prepare($_GET['nome']);
            if ($nome != "") {
                $condicao .= " AND usu_nomecompleto like '%$nome%' ";
            }
        }


        if (isset($_GET['cpf'])) {
            $cpf = $AlunosListaModal->Prepare($_GET['cpf']);
            if ($cpf != "") {
                $condicao .= " AND usu_cpf like '%$cpf%' ";
            }
        }


        if (isset($_GET['limite'])) {
            $limite = $AlunosListaModal->Prepare($_GET['limite']);
            if ($limite != "") {
                $limite = $limite;
            }
        }


        $AlunosLista = $AlunosListaModal->Listar("date_format(usu_nascimento, '%d/%m/%Y') nascimento", $condicao, 'usu_id desc', $limite);

        $retorno = "<table class='sortable table table-hover table-striped no-margin table-responsive'>";
        $retorno .= '<thead>
		    	<tr> 

		            <th title="Ordenar" class="cursor">Nome</th>
		            <th title="Ordenar" class="cursor">CPF</th>
		            <th title="Ordenar" class="cursor">Sexo</th>
		            <th title="Ordenar" class="cursor">Nascimento</th>
		            <th title="Ordenar" class="cursor small"></th>
		            <th title="Ordenar" class="cursor"></th>
                    <th width="10"> </th>

		        </tr>
		    </thead>';
        $retorno .= '<tbody>';

        if ($AlunosLista->num_rows == 0) {
            $retorno .= '<tr ><td colspan="7">Nenhum resultado...</td></tr>';
        } else {

            while ($lista = $AlunosLista->fetch_object()) {

                $nMatriculas = $AlunosListaModal->SelecionaMatricula($lista->usu_id);
                $nMatriculas = $nMatriculas->num_rows;

                $nLancamento = new AlunosFinanceiroModel();
                $nLancamento = $nLancamento->VerificaLancamento($lista->usu_id);

                $retorno .= '
                            <tr>
                            <td class="cursor" title="editar" onclick="EditarAluno(' . $lista->usu_id . ')">' . $lista->usu_nomecompleto . '</td>
                                <td class="cursor">' . $lista->usu_cpf . '</td>
                                <td class="cursor">' . $lista->usu_sexo . '</td>
                                <td class="cursor" title="editar">' . $lista->nascimento . '</td>
                                <td>';
                if ($nMatriculas == 0) {
                    $retorno .= '<i class="fa fa-times text-red" data-toggle="tooltip" title="O aluno não possui nenhuma matrícula ativa."></i>';
                } else {
                    $retorno .= '<i class="fa fa-check text-green text-disabled"  data-toggle="tooltip"  title="Matricula ativa"></i>';
                }

                $retorno .= '</td>';
                $retorno .= '<td>';
                if ($nLancamento == 0) {
                    $retorno .= '<i class="fa fa-money text-red" data-toggle="tooltip" title="O aluno não possui parcelas lançadas."></i>';
                } else {
                    $retorno .= '<i class="fa fa-money text-green text-disabled"  data-toggle="tooltip"  title="Parcelas OK"></i>';
                }
                $retorno .= '</td>
                            <td>
                                <div class="btn-group" title="Opções" data-toggle="tooltip" role="group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-list-ul"></span>
                                    </button>
                                    <ul class="dropdown-menu" style="left: -135px; color: #fff; border: solid 1px #b1b1b1; box-shadow: 1px 1px 1px #525252;">
                                        <li><a href="' . Permalink('alunos', 'Editar', 'codigo=' . $lista->usu_id) . '#">Ficha</a></li>
                                        <li><a href="' . Permalink('alunos', 'ListaMatriculas', 'aluno=' . $lista->usu_id) . '">Processos</a></li>
                                        <li><a href="' . Permalink('AlunosFinanceiro', 'index', 'codigo=' . $lista->usu_id) . '">Financeiro</a></li>
                                    </ul>
                                </div>
                            </td>


                            </tr>';
            }
        }
        $retorno .= "</tbody></table>";




        $retorno .= '</tbody>';

        //echo $retorno;
        exit($retorno);
    }

    /**
     * Ficha de edição
     */
    public function editar() {
        $this->ValidaNivel(40, true);
        $aluno = new AlunosModel();

        if (!isset($_GET['codigo'])) {
            $redir = new RedirectHelper();
            $redir->controller_redir_to = "alunos";
            $redir->Redirecionar();
        }
        $codigo = $_GET['codigo'];

        $aluno = $aluno->Seleciona($codigo);

        $UF = new IbgeModel();
        $UF0 = $UF->SelecionaEstados();
        $UF2 = $UF->SelecionaEstados();
        

        $processos  = new FasesModel();
        $matriculas = $processos->GetMatriculas($codigo);
        $fases      = $processos->GetFaseAtual($codigo);
        
        
        $creditos = new CreditosModel();
        $creditosDisponivel = $creditos->SomaCreditoDisponivel($codigo);
        
        $financeiro  = new AlunosFinanceiroModel();
        $valorPago   = $financeiro->SomaValorPago($codigo);
        $valorDevido = $financeiro->SomaValorDevido($codigo,"finmov_valor");
        
        


        if (!$aluno) {
            exit("Nada encontrado");
        }

        $datas = array(
            "viewDatas" => array(
                "Aluno" => $aluno,
                "PageTitle" => "Editar Cadastro"
            ),
            "ListaUF" => $UF0,
            "ListaUF2" => $UF2,
            "matriculas" => $matriculas,
            "fases" => $fases,
            "creditosDisponivel" => $creditosDisponivel,
            "valorDevido" => $valorDevido,
            "valorPago"   => $valorPago
                
        );

        $this->RenderView("alunos/cadastrar", $datas);
    }

    public function cadastrar() {
        $this->ValidaNivel(40, true);

        $UF = new IbgeModel();
        $UF0 = $UF->SelecionaEstados();
        $UF2 = $UF->SelecionaEstados();

        $datas = array(
            "viewDatas" => array(
                "Aluno" => "",
                "PageTitle" => "Adicionar Aluno"
            ),
            "ListaUF" => $UF0,
            "ListaUF2" => $UF2
        );

        $this->RenderView("alunos/cadastrar", $datas);
    }

    /**
     * Submit da ficha de edição
     */
    public function SalvaFicha() {
        $this->ValidaNivel(40, false, true);


        $ID = $_POST['id'];
        
        $aluno = new AlunosModel();
        
        ///ID_IGNORA, CAMPO_VERIFICAR, VALOR_VERIFICAR
        if( $aluno->VerificaCampoDuplicado($ID, "usu_cpf", $_POST['cpf']) ) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "CPF duplicado na base";
            echo json_encode($ret);
            exit();
        }

        ///ID_IGNORA, CAMPO_VERIFICAR, VALOR_VERIFICAR
        if( $aluno->VerificaCampoDuplicado($ID, "usu_rg", $_POST['rg']) ) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "RG duplicado na base";
            echo json_encode($ret);
            exit();
        }
        
        
        
        $aa = $aluno->UpdateFicha($ID, $_POST);
        if (!$aa->error) {
            $ret['erro'] = "";
            $ret['mensagem'] = "Aluno editado com sucesso!";
        } else {
            $ret['erro'] = "1";
            $ret['mensagem'] = "Erro ao editar ficha";
        }

        echo json_encode($ret);
    }

    /**
     * Submit do Cadastrar Aluno
     */
    public function incluir() {

        $this->ValidaNivel(40, false, true);

        $aluno = new AlunosModel();


        $campos = "";
        $valores = "";
        $sep = ",";

        $ignorarCampos = array("acao", "chave", "acesso", "id");

        $obrigatorios = array(
            "nomecompleto" => "Nome Completo",
            "nomemae" => "Nome da Mãe",
            "nomepai" => "Nome do Pai",
            "nascimento" => "Data de Nascimento",
            "sexo" => "Sexo",
            "nacionalidade" => "Nacionalidade",
            // "uf" => "UF Nacionalidade",
            // "naturalidade" => "Naturalidade",
            "cpf" => "CPF",
            "rg" => "RG",
            "rgemissor" => "orgão emissor RG",
            "rguf" => "UF emissão RG",
            "cep" => "CEP",
            "logradouro" => "Logradouro",
            "logranumero" => "Número do logradouro",
            "lograbairro" => "Bairro do logradouro",
            "logramunicipio" => "Município do logradouro",
            "lograuf" => "UF do logradouro"
        );

        if ($_POST['nacionalidade'] != 3 && $_POST['nacionalidade'] != 4) {
            $obrigatorios = array("uf" => "UF Naturalidade") + $obrigatorios;
            $obrigatorios = array("naturalidade" => "Naturalidade") + $obrigatorios;
        }

        foreach ($_POST as $campo => $valor) {

            if (!in_array($campo, $ignorarCampos)) {
                $campos .= "`usu_$campo`" . $sep;


                if ($campo == "nascimento") {
                    if ($valor != '') {
                        $valor = DataDB($valor);
                    }
                }

                //validando campos
                foreach ($obrigatorios as $obg => $descri) {
                    if ($campo == $obg) {
                        if ($valor == "") {
                            $this->SetErro('Informe ' . $descri);
                            $this->GetErro();
                        }
                    }
                }

                if ($valor == "") {
                    $valores .= "null" . $sep;
                } else {
                    $valor = $aluno->Prepare($valor);
                    $valores .= "'$valor'" . $sep;
                }
            }
        }
        $campos = substr($campos, 0, -1);
        $valores = substr($valores, 0, -1);




        $query = "INSERT INTO sis_usuarios (`usu_tipo`, `usu_localid`,`cad_data`,`cad_usua`,$campos) VALUES ('ALUNO','" . $_SESSION['APP_LOCALID'] . "',current_timestamp(),'" . $_SESSION['APP_USUID'] . "',$valores)";
        $result = $aluno->Cadastrar($query);

        if ($result) {
            $ret['erro'] = "";
            $ret['id'] = $result->insert_id;
            echo json_encode($ret);
        } else {
            $this->SetErroErro = $result->error;
            $this->GetErro();
        }
    }

    public function FotoUpload() {

        if (!isset($_GET['codigo'])) {
            $redir = new RedirectHelper();
            $redir->controller_redir_to = "alunos";
            $redir->Redirecionar();
        }
        $codigo = $_GET['codigo'];

        if (isset($_POST['action']) && $_POST['action'] == 'send') {
            $imagem = $_FILES['imagem']['tmp_name'];
            $tamanho = $_FILES['imagem']['size'];
            $tipo = $_FILES['imagem']['type'];
            $nome = $_FILES['imagem']['name'];

            //1500000 = 1,5mb
            if ($tamanho > 1500000) {
                $d = array("viewDatas" => array("codigo" => $codigo, "retorno" => "<div class='alert alert-danger'>Foto dever ser menor que 1,5Mb</div>"));
                $this->RenderView("alunos/fotoupload", $d);
            }

            if ($imagem != "") {
                $fp = fopen($imagem, "rb");
                $conteudo = fread($fp, $tamanho);
                $conteudo = addslashes($conteudo);
                fclose($fp);

                $aluno = new AlunosModel();
                $aluno->Update($codigo, array("usu_fototype" => $tipo, "usu_foto" => "$conteudo"));


                $foto = $this->GetFoto(false, $codigo);

                $d = array("viewDatas" => array("codigo" => $codigo, "foto" => "OK"));
                $this->RenderView("alunos/fotoupload", $d);
            }
        }

        $d = array("viewDatas" => array("codigo" => $codigo));
        $this->RenderView("alunos/fotoupload", $d);
    }

    function GetFoto($echo = true, $ID = "") {
        if (!isset($_GET['codigo'])) {
            exit();
        }

        $ID = $_GET['codigo'];
        $FOTO = new AlunosModel();
        $FOTOS = $FOTO->Seleciona($ID);

        if (!$FOTOS) {
            exit("404");
        }
        $tipo = $FOTOS->usu_fototype;
        $bytes = $FOTOS->usu_foto;

        //EXIBE IMAGEM


        if ($echo) {
            header("Content-type: " . $tipo . "");
            echo $bytes;
        } else {
            return $bytes;
        }
    }

    public function AtualizaTabela() {

        $Cn = new DB();
        $Cn->CnCliente();

        $cids = $Cn->ExecQuery("SELECT * FROM municipios_ibge ORDER BY codigo ASC");
        $i = 1;

        $update = "";

        while ($cid = $cids->fetch_object()) {
            echo $i++ . "<br/>";
            //echo $cid->codigo . " = " .  substr($cid->codigo, 0,-1)."<br />";
            //$codigo = substr($cid->codigo, 0,-1);
            $nome_novo = $cid->municipio;
            $nome_novo = str_replace("'", "\'", $nome_novo);
            $codigo = $cid->codigo;
            $update = "UPDATE municipios_ibge_OK SET ant = municipio, municipio = '$nome_novo', a = '*'  WHERE left(codigo,6) = $codigo AND a is null ";
            $Cn->ExecNonQuery($update);
        }
    }

    public function ListaMatriculas() {
        $this->ValidaNivel(40, true);

        if (!isset($_GET['aluno'])) {
            exit("WERRO");
        }

        $ALUNOID = (int) $_GET['aluno'];
        if ($ALUNOID <= 0) {
            exit("WERRO - 2");
        }
        $ALUNOS = new AlunosModel();
        $ALUNOS->CnCliente();
        $ALUNO = $ALUNOS->Seleciona($ALUNOID);

        if (!$ALUNO) {
            exit("ERRO - 03");
        }

        $MATRICULAS = $ALUNOS->SelecionaMatricula($ALUNOID);
        $USULOGADO = $ALUNOS->Seleciona($_SESSION['APP_USUID']);


        $datas = array("aluno_id" => $ALUNOID, "aluno" => $ALUNO, "matriculas" => $MATRICULAS, "usuario" => $USULOGADO);

        $this->RenderView("alunos/ListaMatriculas", $datas);
    }

    public function SelecionaFaseAtual($IDALUNO, $MATRICULA) {
        $FASE = new AlunosModel();
        return $FASE->SelecionaFaseAtualModel($IDALUNO, $MATRICULA);
    }

    public function NaturalidadeUF() {
        $CATEGORIAS = new CategoriasModel();

        $IDS = $_GET['ids'];


        $cats = $CATEGORIAS->Seleciona("", "AND catc_id in ($IDS )");
        $ret = "";
        while ($cat = $cats->fetch_object()) {
            $ret .= '<option value="' . $cat->catc_id . '">' . $cat->catc_categoria . '</option>';
        }

        echo $ret;
    }

    public function CarregaNaturalidade() {
        $ID = (int) $_GET['codigo'];
        $ID = (isset($_GET['codigo'])) ? (int) $_GET['codigo'] : "0";
        if ($ID == 0) {
            exit();
        }

        $NAT = new NaturalidadeModel();
        $NAT0 = $NAT->SelecionaNaturalidade($ID);
        echo "<option value='' >Selecione</option>";
        while ($NAT = $NAT0->fetch_object()) {
            echo "<option value='$NAT->municipio' >$NAT->municipio</option>";
        }
    }

    public function CarregaListaPaises() {
        /*
          $NAT  = new NaturalidadeModel();
          $NAT0 = $NAT->SelecionaPaises($ID);
          echo "<option value='' >Selecione</option>";
          while($NAT = $NAT0->fetch_object()) {
          echo "<option value='$NAT->pais_id' >$NAT->pais_paises</option>";
          } */

        echo ''
        . '<option value="">- Selecione -</option>'
        . '<option value="P0010">AFEGANISTÃO</option>'
        . '<option value="P0020">ÁFRICA DO SUL</option>'
        . '<option value="P0030">ALBÂNIA</option>'
        . '<option value="P0040">ALEMANHA</option>'
        . '<option value="P0050">ALEMANHA DEMOCRÁTICA</option>'
        . '<option value="P0060">ALEMANHA FEDERAL</option>'
        . '<option value="P0070">ALTO VOLTA</option>'
        . '<option value="P0080">ANDORRA</option><option value="P0100">ANGOLA</option>'
        . '<option value="P0090">ANGUILLA</option><option value="P0110">ANTÍGUA E BARBUDA</option>'
        . '<option value="P0120">ANTILHAS HOLANDESAS</option>'
        . '<option value="P0130">ARÁBIA SAUDITA</option>'
        . '<option value="P0140">ARGÉLIA</option><option value="P0150">ARGENTINA</option>'
        . '<option value="P0170">ARMÊNIA</option>'
        . '<option value="P0160">ARQUIPÉLAGO DE SAN ANDRES, PROVIDÊNCIA E SANTA CATALINA)</option>'
        . '<option value="P0180">ARUBA</option><option value="P0190">AUSTRÁLIA</option><option value="P0200">ÁUSTRIA</option>'
        . '<option value="P0210">AZERBAIDJÃO</option><option value="P0220">BAHAMAS</option><option value="P0230">BANGLADESH</option>'
        . '<option value="P0240">BARBADOS</option><option value="P0250">BARÉM ou BAREINE ou BAIREIN</option><option value="P0260">BÉLGICA</option>'
        . '<option value="P0270">BELIZE</option><option value="P0290">BENIN</option><option value="P0280">BERMUDAS</option>'
        . '<option value="P0300">BIELO-RÚSSIA (BELARUS)</option><option value="P0310">BIRMÂNIA</option><option value="P0320">BOLÍVIA</option>'
        . '<option value="P0330">BOSNIA-HERZEGÓVINA</option><option value="P0340">BOTSUANA (BOTSWANA)</option><option value="P0350">BRUNEI</option>'
        . '<option value="P0360">BULGÁRIA</option><option value="P0370">BURMA</option><option value="P0380">BURQUINA  FASO ou BURKINA</option>'
        . '<option value="P0390">BURUNDI</option><option value="P0400">BUTAO</option><option value="P0410">CABO VERDE</option>'
        . '<option value="P0420">CAMARÕES  (CAMEROUN)</option><option value="P0430">CAMBODJA ou CAMPUCHÉIA</option>'
        . '<option value="P0440">CANADÁ</option><option value="P0460">CASAQUISTÃO</option><option value="P0450">CATAR</option>'
        . '<option value="P0470">CAYMAN</option><option value="P0480">CEILÃO</option><option value="P0490">CEUTA E MELILLA</option>'
        . '<option value="P0500">CHADE</option><option value="P0510">CHILE</option><option value="P0520">CHINA</option><option value="P0530">CHINA NACIONALISTA</option><option value="P0540">CHIPRE</option><option value="P0550">CINGAPURA</option><option value="P0560">COLÔMBIA</option><option value="P0570">CONGO</option><option value="P0580">CORÉIA DO NORTE</option><option value="P0590">CORÉIA DO SUL</option><option value="P0600">COSTA DO MARFIM</option>'
        . '<option value="P0610">COSTA RICA</option><option value="P0620">CÔTE D\'IVOIRE</option><option value = "P0630">COVEITE</option><option value = "P0640">CROÁCIA</option><option value = "P0650">CUBA</option><option value = "P0660">DINAMARCA</option><option value = "P0670">DJIBUTI</option><option value = "P0680">DOMINICA</option><option value = "P0690">EGITO</option><option value = "P0700">EL SALVADOR</option><option value = "P0710">EMIRADOS ÁRABES UNIDOS</option><option value = "P0720">EQUADOR</option>'
        . '<option value = "P0730">ERITRÉIA</option>'
        . '<option value = "P0740">ESCÓCIA</option><option value = "P0750">ESLOVÊNIA</option><option value = "P0760">ESPANHA</option><option value = "P0770">ESTADOS UNIDOS</option><option value = "P0780">ESTÔNIA</option><option value = "P0790">ETIÓPIA</option><option value = "P0800">FIJI ou FIDJI</option>'
        . '<option value = "P0810">FILIPINAS</option><option value = "P0820">FINLÂNDIA</option><option value = "P0830">FORMOSA</option><option value = "P0840">FRANÇA</option><option value = "P0850">GABÃO</option><option value = "P0860">GÂMBIA</option><option value = "P0870">GANA</option><option value = "P0880">GEÓRGIA</option><option value = "P0890">GIBRALTAR</option><option value = "P0910">GRÃ-BRETANHA</option><option value = "P0900">GRANADA</option><option value = "P0920">GRÉCIA</option><option value = "P0930">GROENLÂNDIA</option><option value = "P0940">GUADALUPE</option><option value = "P0950">GUAM</option><option value = "P0960">GUATEMALA</option><option value = "P0970">GUIANA</option><option value = "P0980">GUIANA FRANCESA</option><option value = "P0990">GUINÉ</option><option value = "P1010">GUINÉ EQUATORIAL</option><option value = "P1000">GUINÉ-BISSAU</option><option value = "P1020">HAITI</option><option value = "P1030">HOLANDA</option><option value = "P1040">HONDURAS</option><option value = "P1050">HONG KONG</option><option value = "P1060">HUNGRIA</option><option value = "P1070">IÊMEM DO NORTE</option><option value = "P1080">IÊMEM DO SUL</option><option value = "P1090">ILHA DE MAN</option><option value = "P1100">ILHA DE PITCAIRN</option><option value = "P1110">ILHA JOHNSTON</option><option value = "P1120">ILHA NORFOLK</option><option value = "P1130">ILHA WAKE</option><option value = "P1140">ILHAS ALAND</option><option value = "P1150">ILHAS CAYMAN</option><option value = "P1160">ILHAS COCOS (KEELING)</option><option value = "P1170">ILHAS COMORES</option><option value = "P1180">ILHAS COOK</option><option value = "P1190">ILHAS DO CANAL</option><option value = "P1200">ILHAS FAROE</option><option value = "P1210">ILHAS GEÓRGIA e SANDWICH DO SUL</option><option value = "P1220">ILHAS MARIANAS DO NORTE</option><option value = "P1230">ILHAS MARSHALL</option><option value = "P1240">ILHAS MIDWAY</option><option value = "P1250">ILHAS VIRGENS (EUA)</option><option value = "P1260">ILHAS VIRGENS (GB)</option><option value = "P1270">ILHAS WALLIS E FUTUNA</option><option value = "P1280">ÍNDIA</option><option value = "P1290">INDONÉSIA</option><option value = "P1300">INGLATERRA</option><option value = "P1310">IRÃ ou IRAN</option><option value = "P1320">IRAQUE</option><option value = "P1330">IRIÃ OCIDENTAL</option><option value = "P1350">IRLANDA (SUL)</option><option value = "P1340">IRLANDA DO NORTE</option><option value = "P1360">ISLÂNDIA</option><option value = "P1370">ISRAEL</option><option value = "P1380">ITÁLIA</option><option value = "P1390">IUGOSLÁVIA</option><option value = "P1400">JAMAICA</option><option value = "P1410">JAPÃO</option><option value = "P1420">JORDÂNIA</option><option value = "P1430">KUAITE ou KUWEIT</option><option value = "P1440">LAOS</option><option value = "P1450">LESOTO</option><option value = "P1460">LETÔNIA</option><option value = "P1470">LÍBANO</option><option value = "P1480">LIBÉRIA</option><option value = "P1490">LÍBIA</option><option value = "P1500">LIECHTENSTEIN</option><option value = "P1510">LITUÂNIA</option><option value = "P1520">LUXEMBURGO</option><option value = "P1530">MACAU</option><option value = "P1540">MACEDÔNIA</option><option value = "P1550">MADAGASCAR</option><option value = "P1560">MALÁSIA ou MALAÍSIA</option><option value = "P1570">MALAVI ou MALAUI</option><option value = "P1580">MALDIVAS</option><option value = "P1590">MALI</option><option value = "P1600">MALTA</option><option value = "P1610">MALVINAS ou ILHAS FALKLAND</option><option value = "P1620">MARROCOS</option><option value = "P1630">MARTINICA</option><option value = "P1640">MAURÍCIO</option><option value = "P1650">MAURITÂNIA</option><option value = "P1660">MAYOTTE</option><option value = "P1670">MÉXICO</option><option value = "P1680">MIANMÁ</option><option value = "P1690">MICRONÉSIA</option><option value = "P1700">MOÇAMBIQUE</option><option value = "P1710">MOLDÁVIA (MOLDOVA)</option><option value = "P1720">MÔNACO</option><option value = "P1730">MONGÓLIA</option><option value = "P1740">MONTENEGRO</option><option value = "P1750">MONTSERRAT</option><option value = "P1760">MYANMAR</option><option value = "P1770">NAMÍBIA</option><option value = "P1780">NAURU</option><option value = "P1790">NEPAL</option><option value = "P1800">NICARÁGUA</option><option value = "P1810">NÍGER</option><option value = "P1820">NIGÉRIA</option><option value = "P1830">NIUE</option><option value = "P1840">NORUEGA</option><option value = "P1850">NOVA CALEDÔNIA</option><option value = "P1860">NOVA ZELÂNDIA</option><option value = "P1870">NUEVA ESPARTA</option><option value = "P1880">OMÃ</option><option value = "P1890">PAÍS DE GALES</option><option value = "P1900">PAÍSES BAIXOS</option><option value = "P1910">PALAU</option><option value = "P1920">PANAMÁ</option><option value = "P1930">PAPUA NOVA-GUINÉ</option><option value = "P1940">PAQUISTÃO</option><option value = "P1950">PARAGUAI</option><option value = "P1960">PÉRSIA</option><option value = "P1970">PERU</option><option value = "P1980">POLINÉSIA FRANCESA</option><option value = "P1990">POLÔNIA</option><option value = "P2000">PORTO RICO</option><option value = "P2010">PORTUGAL</option><option value = "P2020">QUÊNIA</option><option value = "P2040">QUIRGUÍZIA ou QUIRGUIZTÃO</option><option value = "P2030">QUIRIBATI</option><option value = "P2050">RASD</option><option value = "P2060">REINO UNIDO</option><option value = "P2070">REPÚBLICA CENTROAFRICANA</option><option value = "P2080">REPÚBLICA DO IÊMEN</option><option value = "P2090">REPÚBLICA DOMINICANA</option><option value = "P2100">REPÚBLICA ESLOVACA</option><option value = "P2110">REPÚBLICA TCHECA</option><option value = "P2120">REUNIÃO</option><option value = "P2130">RODÉSIA</option><option value = "P2140">ROMÊNIA</option><option value = "P2150">RUANDA</option><option value = "P2160">RÚSSIA</option><option value = "P2170">SAARA OCIDENTAL</option><option value = "P2180">SAINT-PIERRE E MIQUELON</option><option value = "P2190">SALOMÃO</option><option value = "P2210">SAMOA AMERICANA</option><option value = "P2200">SAMOA OCIDENTAL</option><option value = "P2220">SAN MARINO</option><option value = "P2230">SANTA HELENA</option><option value = "P2240">SANTA LÚCIA</option><option value = "P2250">SÃO CRISTÓVÃO E NEVIS (SAINT KITTS</option><option value = "P2260">SÃO TOMÉ E PRÍNCIPE</option><option value = "P2270">SÃO VICENTE E GRANADINAS</option><option value = "P2280">SEICHELLES (SEYCHELLES)</option><option value = "P2290">SENEGAL</option><option value = "P2300">SERRA LEOA</option><option value = "P2310">SÉRVIA</option><option value = "P2320">SÍRIA</option><option value = "P2330">SOMÁLIA</option><option value = "P2340">SRI LANKA</option><option value = "P2350">SUAZILÂNDIA</option><option value = "P2360">SUDÃO</option><option value = "P2370">SUÉCIA</option><option value = "P2380">SUIÇA</option><option value = "P2390">SURINAME</option><option value = "P2400">SVALBARD</option><option value = "P2410">TADJIQUISTÃO</option><option value = "P2420">TAILÂNDIA</option><option value = "P2430">TAIWAM</option><option value = "P2440">TANZÂNIA</option><option value = "P2450">TCHECO-ESLOVÁQUIA</option><option value = "P2460">TERRAS AUSTRAIS e ANTÁRTICA</option><option value = "P2470">TERRITÓRIO BRITÂNICO NA</option><option value = "P2480">TERRITÓRIO BRITÂNICO NO OCEANO</option><option value = "P2490">TIBETE</option><option value = "P2500">TIMOR</option><option value = "P2510">TOGO</option><option value = "P2520">TONGA</option><option value = "P2530">TOQUELAU</option><option value = "P2540">TRINIDAD E TOBAGO</option><option value = "P2550">TUNÍSIA</option><option value = "P2560">TURCAS E CAICOS (TURKS E CAICOS)</option><option value = "P2570">TURCOMENISTÃO (TURCOMÊNIA)</option><option value = "P2580">TURQUIA</option><option value = "P2590">TUVALU</option><option value = "P2600">UCRÂNIA</option><option value = "P2610">UGANDA</option><option value = "P2620">UNIÃO SOVIÉTICA</option><option value = "P2630">URUGUAI</option><option value = "P2640">UZBEQUISTÃO</option><option value = "P2650">VANATU</option><option value = "P2660">VATICANO</option><option value = "P2670">VENEZUELA</option><option value = "P2680">VIETNAM</option><option value = "P2690">ZAIRE</option><option value = "P2700">ZÂMBIA</option><option value = "P2710">ZIMBÁBUE</option><option value = "P2720">PALESTINA</option>';
    }

    /*
     * Atualizar fases da matricula do aluno!
     */

    public function AtualizaFase() {
        $ret['erro'] = '';

        $ID_MATRICULA = $_GET['mid'];
        $ID_FASE = $_GET['fid'];
        $ID_ALUNO = $_GET['aid'];

        $FASE_CONCLUIDO = $_POST['fase-conclui'];
        $FASE_DATA = $_POST['fase-data-exame'];
        $FASE_RESULTADO = $_POST['fase-resultado'];

        if ($FASE_RESULTADO == "APROVADO") {
            IF ($FASE_RESULTADO == "") {
                $ret['erro'] = '1';
                $ret['retorno'] = 'Informe a data de conclusão!';
            }
        }

        if ($FASE_DATA == "") {
            $ret['erro'] = '1';
            $ret['retorno'] = 'Data do exame não pode ser vazia';
        }

        if ($FASE_RESULTADO == "") {
            $ret['erro'] = '1';
            $ret['retorno'] = 'Resultado do exame não pode ser vazio';
        }


        if ($ret['erro'] == '') {
            $FASE = new FasesModel();
            $ret = $FASE->AtualizaFaseAluno($ID_ALUNO, $ID_FASE, $ID_MATRICULA, $FASE_DATA, $FASE_RESULTADO, $FASE_CONCLUIDO);
        }

        echo json_encode($ret);
        exit();
    }

    public function Relatorio() {
        $this->ValidaNivel(40);

        ###################################
        ## TIPO DE MATRICULAS
        $MatriculasExec = new MatriculasModel();
        $matriculas = array();
        $MatriculasExec = $MatriculasExec->Seleciona();
        while ($matricula = $MatriculasExec->fetch_array()) {
            $matricualas[] = $matricula;
        }

        ###################################
        ## TIPO DE PROCESSOS (SERVIÇOS)
        $ProcExec = new ServicosModel();
        $procs = array();
        $ProcExec = $ProcExec->Seleciona();
        while ($proc = $ProcExec->fetch_array()) {
            $procs[] = $proc;
        }


        ###################################
        ## TIPO DE FASES (SERVIÇOS ITENS)
        $ServitensExec = new ServicosModel();
        $itens = array();
        $ServitensExec = $ServitensExec->SelecionaItens();
        while ($iten = $ServitensExec->fetch_array()) {
            $itens[] = $iten;
        }

        $dados = array(
            'matriculas' => $matricualas,
            'procs' => $procs,
            'itenserv' => $itens
        );

        $this->RenderView('alunos/reports', $dados);
    }

    public function RelatorioGerar() {
        $report = new AlunosRelatoriosModel();
        $report->SetReport($_POST['report']);
        $r = $report->GetReport();

        //$dados_report = $report->Gerar();

        $dados = array(
            "dados" => ''
        );

        $this->RenderView('alunos/reports/' . $r, $dados, 'public/header', '', false);
    }

    public function RelatorioGerar2() {
        $report = new AlunosRelatoriosModel();
        $report->SetReport($_GET['report']);
        $r = $report->GetReport();

        //$dados_report = $report->Gerar();

        $dados = array(
            "dados" => ''
        );

        #$this->RenderView('alunos/reports/'.$r,$dados,'public/header','',false);
        $x = $this->RenderPartialView('alunos/reports/' . $r, TRUE);

        echo html_entity_decode($x);
    }

    /**
     * Contrato
     * */
    public function contrato() {
        $this->ValidaNivel(40, false);

        if (!isset($_GET['aluno'])) {
            exit("ERRO");
        }
        $ID_ALUNO = $_GET['aluno'];

        $AlunosLista = new AlunosModel();
        $AlunosLista = $AlunosLista->Seleciona($ID_ALUNO);

        $datas = array(
            "Aluno" => $AlunosLista,
            "viewDatas" => array(
                "PageTitle" => "&nbsp;"
            )
        );

        
        $txt = $this->RenderView("alunos/contrato", $datas, "public/header", "public/footer", false);
        /**
#        $mpdf = new mPDF();        
        $txt = $this->RenderPartialView("alunos/contrato", $datas, true);
        $mpdf->useSubstitutions = true;
        $mpdf->WriteHTML($txt);
        $mpdf->Output();
        **/
    }

    /**
     * Nota promissória
     * */
    public function notapromissoria() {
        $this->ValidaNivel(40, false);

        if (!isset($_GET['aluno'])) {
            exit("ERRO");
        }
        $ID_ALUNO = $_GET['aluno'];

        $AlunosLista = new AlunosModel();
        $AlunosLista = $AlunosLista->Seleciona($ID_ALUNO);

        $datas = array(
            "Aluno" => $AlunosLista,
            "viewDatas" => array(
                "PageTitle" => "&nbsp;"
            )
        );
        $nota = "alunos/notapromissoria";
        if (isset($_GET['lote'])) {
            $nota = "alunos/notapromissorialote";


            $parcelas = new FinanceiroModel();
            $parcelasx = $parcelas->ListaParcelas(
                    array(
                        "alunoid" => $ID_ALUNO,
                        "documento" => "NOTP",
                        "dtpaga" => "IS NULL"
                    )
            );

            $datas['parcelas'] = $parcelasx;
        }


        $this->RenderView($nota, $datas, "public/header", "public/footer", false);
    }

    /*
     * Recibo de pagamento de parcela
     * */

    public function GeraRecibo() {
        $this->ValidaNivel(40, false);

        if (!isset($_GET['codigo'])) {
            exit("ERRO " . __FILE__ . " LINHA " . __LINE__ . "");
        }

        if (!isset($_GET['movimento'])) {
            exit("ERRO " . __FILE__ . " LINHA " . __LINE__ . "");
        }
        $ID_ALUNO = $_GET['codigo'];
        $ID_MOVIMENTO = $_GET['movimento'];

        $AlunosLista = new AlunosModel();
        $Aluno = $AlunosLista->Seleciona($ID_ALUNO);

        $Movimento = new FinanceiroModel();
        $Movimento = $Movimento->SelecionaParcela($ID_MOVIMENTO, " AND finmov_alunoid='$ID_ALUNO' AND NOT finmov_databaixa IS NULL ");

        $datas = array(
            "aluno" => $Aluno,
            "movimento" => $Movimento,
            "viewDatas" => array(
                "PageTitle" => "&nbsp;"
            )
        );



        $this->RenderView('alunos/impressos/recibo', $datas, "public/header", "public/footer", false);
    }

    public function GeraReciboAvulso() {
        if (!isset($_GET['codigo'])) {
            exit("ERRO " . __FILE__ . " LINHA " . __LINE__ . "");
        }
        $ID_ALUNO = $_GET['codigo'];
        #$ID_MOVIMENTO = $_GET['movimento'];

        $AlunosLista = new AlunosModel();
        $Aluno = $AlunosLista->Seleciona($ID_ALUNO);

        $datas = array(
            "Aluno" => $Aluno
        );


        $this->RenderView('alunos/impressos/recibogenerico', $datas, "public/header", "public/footer", false);
    }

    public function CarregaCodMunicipioDeTranSP() {
        $Municipio = $_GET['municipio'];
        $Codigo = new AlunosModel();
        $Codigo = $Codigo->getMunicipioDetranSP($Municipio);

        return $Codigo;
    }

    public function UtilizaCreditos() {
        $ret['erro'] = "";

        if (!isset($_POST['idcredito'])) {
            $ret['erro'] = "Crédito não informado!";
        }

        if (!isset($_POST['valorcredito'])) {
            $ret['erro'] = "Valor não informado!";
        }
        $valor = $_POST['valorcredito'];
        if ($valor == '' || $valor <= 0) {
            $ret['erro'] = "O valor é inválido!";
        }

        if (!isset($_GET['codigo'])) {
            $ret['erro'] = "Aluno não informado!";
        }

        if (!isset($_POST['motivo'])) {
            $ret['erro'] = "Informe o motivo!";
        }

        $id_aluno = $_GET['codigo'];
        $id_credito = $_POST['idcredito'];
        $valorUtiliza = $_POST['valorcredito'];
        $motivo = $_POST['motivo'];
        if (str_replace(" ", "", strlen($motivo)) < "10") {
            $ret['erro'] = "Motivo deve conter 10 caracteres";
        }

        if ($ret['erro'] == '') {
            $CreditosModel = new CreditosModel();
            $ret = $CreditosModel->MarcaUtilizado($id_aluno, $id_credito, $motivo, $valorUtiliza);
        }



        echo json_encode($ret);
    }

}

//Class
