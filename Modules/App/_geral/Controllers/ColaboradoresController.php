<?php

class ColaboradoresController extends Controllers {
    
    private $TIPO_FUNCIONARIO,$NIVEL_FUNCIONARIO, $GRUPO_FUNCIONARIO;
    
    public function index() {

        if($this->TIPO_FUNCIONARIO == "") {
            exit();
        }

        $data = array("TipoFuncionario" => $this->TIPO_FUNCIONARIO);
        $this->TIPO_FUNCIONARIO = "";
        
        
    }
    
    private function SetTipo() {
        if(!isset($_GET['tipo'])) {
            exit();
        }
        $TIPO = strtolower($_GET['tipo']);
        $TIPO = ucfirst($TIPO);
        
        switch ($TIPO) {
            case "Atendentes": $this->Atendentes(); break;
            case "Gerentes": $this->Gerentes(); break;
            case "Instrutores": $this->Instrutores(); break;
            default:
                exit('ERRO'. __FILE__ ." - LINHA: ".__LINE__);
                break;
        }
        
    }
    public function Lista() {
        $this->SetTipo();
        $data = array("TipoFuncionario" => $this->TIPO_FUNCIONARIO);
        $this->RenderView("colaboradores/lista",$data);
    }
    
    public function Listagem() {
        $this->SetTipo();
        
        $COLABORADORES = new ColaboradoresModel();
        

        //
        $condicao = " and usu_localid = '".$_SESSION['APP_LOCALID']."'";

        $COLABORADORES = $COLABORADORES->Seleciona($this->GRUPO_FUNCIONARIO, $condicao);
        
        
        $retorno = "<table class='sortable table table-hover table-striped no-margin table-responsive'>";
        $retorno .= '<thead>
		    	<tr>
		            <th title="Ordenar" class="cursor">Nomes</th>
		            <th title="Ordenar" class="cursor">Email</th>
                            <th title="Ordenar" class="cursor">Nascimento</th>
		            <th title="Ordenar" class="cursor">Login</th>
		        </tr>
		    </thead>';
        $retorno .= '<tbody>';
        if($COLABORADORES->num_rows == 0) {
            $retorno .= '<tr ><td colspan="7">Nenhum resultado...</td></tr>';
        } else  {        
            while ($COLAB = $COLABORADORES->fetch_object()) {
                $retorno .= "<tr onclick='EditarColaborador($COLAB->usu_id)' class='cursor'>";
                $retorno .= "<td>$COLAB->usu_nomecompleto</td>";
                $retorno .= "<td>$COLAB->usu_email</td>";
                $retorno .= "<td>".DataBR($COLAB->usu_nascimento)."</td>";
                $retorno .= "<td>$COLAB->usu_login</td>";
                $retorno .= "</tr>";
            }
        }
        $retorno .= "</tbody></table>";
        echo $retorno;
        
        
    }
    
    public function Gerentes() {
        $this->ValidaNivel(50, FALSE,FALSE,FALSE);
        $this->NIVEL_FUNCIONARIO = "50";
        $this->GRUPO_FUNCIONARIO = "GEREN";
        $this->TIPO_FUNCIONARIO = "Gerentes";
    }
    
    public function Atendentes() {
        $this->ValidaNivel(40, FALSE,FALSE,FALSE);
        $this->NIVEL_FUNCIONARIO = "40";
        $this->GRUPO_FUNCIONARIO = "ATEND";
        $this->TIPO_FUNCIONARIO  = "Atendentes";
    }
    
    public function Instrutores() {
        $this->ValidaNivel(30, FALSE,FALSE,FALSE);
        $this->NIVEL_FUNCIONARIO = "30";
        $this->GRUPO_FUNCIONARIO = "INSTR";
        $this->TIPO_FUNCIONARIO = "Instrutores";
    }
    
    public function Administradores() {
        $this->ValidaNivel(100, FALSE,FALSE,FALSE);
        $this->NIVEL_FUNCIONARIO = "100";
        $this->GRUPO_FUNCIONARIO = "ADMIN";
        $this->TIPO_FUNCIONARIO = "Adminstrador";
    }
        
    
    
    public function Cadastrar() {
        $this->SetTipo();
        $colaborador = "";
        if(isset($_GET['codigo'])) {
            $ID = (int) $_GET['codigo'];
            $colaborador = new ColaboradoresModel();
            $ID = $colaborador->Prepare($ID);
            $colaborador = $colaborador->Seleciona($this->GRUPO_FUNCIONARIO, ' and usu_id = '.$ID.' LIMIT 1  ');
            $colaborador = $colaborador->fetch_object();
        } else {
        //TODO verificar codigo quando estiver cadastrando, 
        // permitir codigo nulo para novos cadastros, mas nao para edição
        /*
         * 
            $redir = new RedirectHelper();
            $redir->controller_redir_to = "dashboard";
            $redir->Redirecionar();*/
        }
        
        $UF  = new IbgeModel();
        $UF0 = $UF->SelecionaEstados();
        
        $data = array("TipoFuncionario" =>  $this->TIPO_FUNCIONARIO,"ListaUF2"=>$UF0,"colaborador" => $colaborador);
        $this->RenderView("colaboradores/ficha",$data);
    }
    
    public function Inserir() {
        $this->SetTipo();
        $colaborador = new ColaboradoresModel();
        $campos = $_POST;
        $campos['tipo']  = $this->GRUPO_FUNCIONARIO;
        $campos['nivel'] = $this->NIVEL_FUNCIONARIO;
        $campos['localid'] = $_SESSION['APP_LOCALID'];
        $campos['email'] = $_POST['login'];
        
        
        //array_push($campos, $add);
        
        
        $c = $colaborador->InserirNovo($campos);
    }
    
    
    public function Editar() {
        $this->SetTipo();
        $colaborador = new ColaboradoresModel();
        $campos = $_POST;
        $campos['tipo']  = $this->GRUPO_FUNCIONARIO;
        $campos['nivel'] = $this->NIVEL_FUNCIONARIO;
        $campos['localid'] = $_SESSION['APP_LOCALID'];
        $campos['email'] = $_POST['login'];
        
        $c = $colaborador->Editar($campos);

    }    
}