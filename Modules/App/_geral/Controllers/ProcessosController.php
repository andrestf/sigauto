<?php

/**
 * 
 * Permissão geral para acesso Nivel >= 50
 *
 */

class ProcessosController extends Controllers {
    

    
    public function incluir() {
        $this->ValidaNivel(40,true);
        
        if(!isset($_GET['aluno'])) {
            exit("Aluno não informado. <br/>ERRO => ProcessosController  Linha: " . __LINE__);
        }
        
        $codigo = (INT) $_GET['aluno'];
        
        $ALUNO = new AlunosModel();
        $ALUNO = $ALUNO->Seleciona($codigo);
        if(!$ALUNO) {
            exit("ERRO => ProcessosController  Linha: " . __LINE__);
        }
        $_SESSION['APP_KEYTEMP'] = md5($codigo);
        
        $MATRICULAS = new MatriculasModel();
        $MATRICULAS = $MATRICULAS->Seleciona(""," AND can_data IS NULL ");
        
        
        $SERVICOS = NEW ServicosModel();
        $SERVICOS = $SERVICOS->Seleciona(""," AND can_data IS NULL");
        
        
        $datas = array( "matriculas"=>$MATRICULAS,"servicos"=>$SERVICOS,"viewDatas"=>array("Aluno"=>$ALUNO) );
        
        $this->RenderView("processos/incluir",$datas);
    }
    
    
    public function CategoriasMatricula() {
        $CATEGORIAS = new CategoriasModel();
        
        $IDS = $_GET['ids'];
        
        
        $cats = $CATEGORIAS->Seleciona("","AND catc_id in ($IDS )");
        $ret = "";
        while($cat = $cats->fetch_object()) {
            $ret .= '<option value="'.$cat->catc_id.'">'.$cat->catc_categoria.'</option>';
        }
        
        echo $ret;
    }
    
    
    public function SelecionaServicos() {
        
        $SERVICO_ID = $_GET['servid'];
        $SERVICO_ITENS = $_GET['ids'];
        
        $SERVICOS = new ServicosModel();
        $SERVICOS = $SERVICOS->Seleciona('',' and serv_id = '.$SERVICO_ID);
        $SERVICO = $SERVICOS->fetch_object();
        
        
        $SERVICO_VALOR      = $SERVICO->serv_valor;
        if($SERVICO_VALOR == "") {
            $SERVICO_VALOR = "0.00";
        }
        $SERVICO_DESCRICAO = $SERVICO->serv_descricao;
        
        $PROCESSOS = new ProcessosModel;
        $procs = $PROCESSOS->SelecionaServicos("", "AND serviten_id IN ($SERVICO_ITENS)" );
        
        $valor = 0;
            $tr  = '<table class="table table-bordered table-condensed table-hover table-responsive table-striped">';
            $tr .= '<thead>';
            $tr .= '<tr>';
            $tr .= '<th width="80">Código</th>';
            $tr .= '<th>Descrição</th>';
            $tr .= '<th width="100">Valor</th>';
            //$tr .= '<th width="50"></th>';
            $tr .= '</tr>';
            $tr .= '</thead>';
        
        $tr .= "<tbody>";
        $tr .= "<tr id='servico_master'><td>PROC</td><td>$SERVICO_DESCRICAO</td><td>R$ $SERVICO_VALOR</td></tr>";
        while ($processo = $procs->fetch_object()) {
            $valor = $valor + $processo->serviten_valor;
            $tr .= "<tr id='servico_".$processo->serviten_id."' >";
            $tr .= "<td>".$processo->serviten_id."</td>";
            $tr .= "<td>".$processo->serviten_descricao."</td>";
            $tr .= "<td> R$ ".$processo->serviten_valor."</td>";
            //$tr .= "<td><i clas='fa fa-trash' onclick='RemoverServico()'></td>";
            $tr .= "</tr>";
        }
        $tr .= "</tbody>";
        $valor = $valor+$SERVICO_VALOR;
        $valor = number_format($valor,"2",",",".");
        $_SESSION['SERV_VALORTOTAL'] = $valor;
        $tr .= "<tfooter><tr align='center'><td colspan='3' class='text-red' style='font-weight: 700'>Valor Total R$ <span id='x_valor_servicos'>$valor</span> </td></tr></tfooter>";
        $tr .= "</table>";
        
        echo $tr;
        
        
    }
            
            
    
    public function IniciarProcesso() {
        $this->ValidaNivel(40,true);
        if(!isset($_POST['aluno']) ) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "Aluno não informado!";
            echo json_encode($ret);
            return false;
        }

        $key = $_POST['key'];
        
        if($key != $_SESSION['APP_KEYTEMP']) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "Chave de acesso inválida!";
            echo json_encode($ret);
            return false;
        }
        
        $ID_PROCESSO = $_POST['processo'];
        $CATEGORIA = $_POST['categorias'];
        $SERVICOS = $_POST['servicos'];
        $SERVICOITENS = $_POST['servicoitens'];
        $VLR_SERV = $_SESSION['SERV_VALORTOTAL'];// definido em ProcessosController => SelecionaServicos
        $VLR_DESCONTO = '';
        $FORMA_PAGA = '';
        $NPARCELAS = '';
        $VENCIMENTO = '';
        $TAXA = $_POST['taxa'];
        if($TAXA == 0) {
            $TAXA = NULL;
        } else {
            $TAXA = "*";
        }

        $matricula = new AlunosMatriculasModel();

        //ID PROCESSO TB É ID TIPO DE MATRICULA
        $matricula->SetAlunoID($_POST['aluno']);
        
        $matricula->TPMAT = $ID_PROCESSO;
        $matricula->CATCNH = $CATEGORIA;
        $matricula->SERVICO = "$SERVICOS";
        $matricula->SERVICOITENS = "$SERVICOITENS";
        $matricula->VLRSERVICO = "$VLR_SERV";
        $matricula->VLRDESCONTO = "NULL";
        $matricula->FORMAPAGA = "$FORMA_PAGA";
        $matricula->NPARCELAS = "0";
        $matricula->VENCIMENTO = "$VENCIMENTO";
        $matricula->TAXA = "$TAXA";

        $matricula->IncluirMatricula();
    }

    public function Detalhes() { 
        $this->ValidaNivel(40,true);

        $ID_MATRICULA = (isset($_GET['processo'])) ? (int) $_GET['processo'] : "0";
        $ID_ALUNO    = (isset($_GET['aluno'])) ? (int) $_GET['aluno'] : "0";

        if($ID_MATRICULA == "0" || $ID_ALUNO == "0") {
            exit("ERRO ProcessosControler Linha: ". __LINE__);
        }

        $ALUNOS = new AlunosModel();
        $ALUNO = $ALUNOS->Seleciona($ID_ALUNO);

        $datas = array("aluno"=>$ALUNO);

        $this->RenderView("alunos/ProcessosDetalhes",$datas);
    }
    
    
    public function ListaProcessosIniciados() {
        
        
        $ID_MATRICULA = (isset($_GET['processo'])) ? (int) $_GET['processo'] : "0";
        $ID_ALUNO    = (isset($_GET['aluno'])) ? (int) $_GET['aluno'] : "0";
        
        if($ID_MATRICULA == "0" || $ID_ALUNO == "0") {
            exit("ERRO ProcessosControler Linha: ". __LINE__);
        }
        
        
        $FASES = new DB();
        $FASES->CnCliente();
        $FASE_Q = "SELECT *, amf.cad_data as dtinicio
                FROM sis_alunosmatfases amf
                LEFT OUTER JOIN sis_servicositens on serviten_id = amf_servitenid 
                WHERE amf_alunoid = '$ID_ALUNO' 
                AND amf_matid = $ID_MATRICULA ";
                //AND NOT amf_iniciado IS NULL";

                ##OR if((serviten_cod = 'SIMULA' OR serviten_cod = 'EXTEOR') AND amf_matid= $ID_MATRICULA, 1, 0) = 1 ";
                
        $FASES = $FASES->ExecQuery($FASE_Q);
        $tr = "";
        $xx = 1;
                while($fase = $FASES->fetch_object()) {
                    $style = "";
                    if($fase->amf_cancelado != '') {
                        $style .= 'background: #ffd4d4; ';
                    }

                    if($fase->amf_cancelado == '') {
                        if($fase->amf_concluido == '' and $fase->amf_iniciado == '') {
                             $style .= 'display: none;';
                        }//if
                    }

                    $xx++;
                    $tr .= "<tr style='$style'>";
                    $tr .= "<td>";
                    $tr .= ($fase->amf_concluido != '' ) ? "<i class='fa fa-check-circle text-green'></i> " : "<i class='fa fa-times-circle text-red'></i> ";
                    $tr .= "$fase->serviten_descricao";
                    //Lista Log das fases

                    $logs = $this->GetFasesLogs($fase->amf_id);
                    if($logs) {
                        $tr .="<br/>";
                        foreach ($logs as $key => $value) {
                            $tr .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><b>Data:</b> ".DataBR($value['amfl_data']) . " &nbsp;<b>Resultado: </b>" .$value['amfl_resultado']."</small><br/>" ;
                        }
                    }



                    $tr .= "</td>";
                    if($fase->amf_resultado != '') {
                        $tr .= "<td align='center'>".$fase->amf_resultado." <br/><small> ".DataBR($fase->amf_dataprocesso)." </small></td>";
                    } else {
                        $tr .= "<td></td>";
                    }

                    $tr .= "<td>".DataBR($fase->amf_iniciado)."</td>";
                    $tr .= "<td>".DataBR($fase->amf_concluido)."</td>";
                    $checked = "";
                    $onClick = "onclick='ConcluirFase($fase->amf_id,$xx,$fase->serviten_id)' ";
                    $title  = "Processo Concluido ";
                    $concluir = "<i title='$title' class='btn btn-xs btn-default fa fa-check text-blue disabled $checked cursor' id='chk-$xx' value='xx' data-campos='$fase->serviten_campos' data-script='$fase->serviten_script'  ></i>";

                    $sVoltaProcesso = "";

                    if($fase->amf_concluido == '') {
                        if($this->ValidaNivel2(40)) {
                            $sVoltaProcesso  .= "<i class='btn btn-xs btn-default fa fa-history text-red' title='Voltar Processo' onClick='VoltaProc($fase->amf_id)'></i> ";
                            $sVoltaProcesso .= "<i class='btn btn-xs btn-default fa fa-trash' title='Cancelar Processo' onClick='CancelaProc($fase->amf_id)'></i>";
                        }
                        $concluir = "<i title='$title' class='btn btn-xs btn-default fa fa-check text-blue $checked cursor' id='chk-$xx' value='xx' data-campos='$fase->serviten_campos' data-script='$fase->serviten_script' $onClick ></i>";

                        if($fase->amf_pendente != '') {
                            $concluir = "";
                            $sVoltaProcesso = "";
                        }
                    }

                    if($fase->amf_cancelado != '') {
                         $concluir = "";
                         $sVoltaProcesso = "";
                    }

                    if($fase->amf_concluido == '' and $fase->amf_iniciado == '') {
                         $concluir = "";
                         $sVoltaProcesso = "";
                    }//if

                    /*
                    //* BOTOES VOLTAR PROCESSO
                    $title = "Concluir Processo";
                    if($fase->amf_iniciado != '' && $fase->amf_concluido == '') {

                        $concluir = "<i title='$title' class='btn btn-xs btn-default fa fa-check text-blue $checked cursor' id='chk-$xx' value='xx' data-campos='$fase->serviten_campos' $onClick ></i>";

                        $onClick = "";
                        $checked = " disabled ";
                        if($this->ValidaNivel2(40)) {
                            $sVoltaProcesso  .= "<i class='btn btn-xs btn-default fa fa-history text-red' title='Voltar Processo' onClick='VoltaProc($fase->amf_id)'></i> ";
                            $sVoltaProcesso .= "<i class='btn btn-xs btn-default fa fa-trash' title='Cancelar Processo' onClick='CancelaProc($fase->amf_id)'></i>";
                        } else {
                          $title = "Processo Concluido2";
                          if($fase->amf_dataprocesso != '' && $fase->amf_concluido != '') {
                        $concluido = "<i title='$title' class='btn btn-xs btn-default fa fa-check text-blue $checked cursor' id='chk-$xx' value='xx' data-campos='$fase->serviten_campos' $onClick ></i>";

                        $onClick = "";
                        $checked = " enabled ";
                          }
                        }
                    }*/

                            $tr .= ""
                            . "<td class='' align='right'>"
                                . " " . $sVoltaProcesso . " "
                                . " " . $concluir . " "
                            . "</td>";
                    $tr .= "</tr>";
                    $tr .= "";

                }//while
          echo $tr;
    }
    
    ## $CONTINUA default FALSE = usar true, quando for processo em lote!
    public function ConcluirFase($ID_MATRICULA = '',$ID_FASE = '',$ID_ALUNO = '', $CANCELADO = FALSE,$CONTINUA = FALSE) {

      
        
        $this->ValidaNivel(40);
        $IDS_TO_UPDATE = "";

        if($ID_MATRICULA == '')
            $ID_MATRICULA = isset($_GET['matricula'])  ? (int) $_GET['matricula']  : "0";

        if($ID_FASE == '')
            $ID_FASE  = isset($_GET['fase'])  ? (int) $_GET['fase']  : "0";

        if($ID_ALUNO == '')
            $ID_ALUNO = isset($_GET['aluno']) ? (int) $_GET['aluno'] : "0";

        if($ID_FASE == 0 || $ID_ALUNO == "0" || $ID_MATRICULA == "0") {
            exit("ERRO ProcessosControler Linha: ". __LINE__);
        }






        $FASE = new DB();
        $FASE->CnCliente();
        $FASE->autocommit(false);

        //SELECIONA A FASE PARA SABER O ID DO SERVITENID
        $SIID_SELECT = "SELECT amf_servitenid, amf_resultado, amf_matid, amf_dataprocesso FROM sis_alunosmatfases WHERE amf_matid = '$ID_MATRICULA' AND amf_alunoid = '$ID_ALUNO' AND amf_id = '$ID_FASE'  ";
        $SIID_SELECT = $FASE->ExecQuery($SIID_SELECT);
        $SIID_SELECT = $SIID_SELECT->fetch_array();
        $SERV_ID       = $SIID_SELECT['amf_servitenid'];
        $SERV_CONCLUI  = $SIID_SELECT['amf_dataprocesso'];
        $ID_MATRICULA  = $SIID_SELECT['amf_matid'];
    
        
        # USADO PARA TESTE
        #$SIID_SELECT['amf_resultado'] = "APROVADO";
        #$SERV_CONCLUI = "2018-08-30";

        $config = new ConfigHelper();

        ##EX MEDICO
        if($SIID_SELECT['amf_servitenid'] == 3 && $SIID_SELECT['amf_resultado'] == 'APROVADO') {
            //INICIANDO MATRICULA DO ALUNO!
            $USU = "SELECT * FROM sis_usuarios WHERE usu_id = $ID_ALUNO";
            $USU = $FASE->ExecQuery($USU);
            $USU = $USU->fetch_array();


            $num_matricula = "null";
            if($USU['usu_matricula'] == "0" OR $USU['usu_matricula'] == '' OR $USU['usu_matricula'] == null) {
                $num_matricula = "'" . ($config->Get('nMatricula')+1) . "'";
                $config->UpdateMatricula();
            }

            

            // definindo o vencimento do processo
            $UPX00 = "UPDATE sis_usuarios SET usu_procvenc = DATE_ADD('$SERV_CONCLUI',INTERVAL 1 YEAR), usu_matricula = $num_matricula WHERE usu_id = '$ID_ALUNO' ";
            /**/
            $UPX00 = $FASE->ExecNonQuery($UPX00);

            if($UPX00->error) {
                $FASE->roolback();
                $ret['erro'] = '1';
                $ret['mensagem'] = 'Erro ao atualizar registros de fases - 01 ! ProcessosController Linha => ' . __LINE__;
                
                if($RET)
                echo json_encode($ret);
                exit();
            }/**/
        }
        

        ############################################################################################################
        ############################################################################################################
        ## remover esse bloco apos os teste
        #    $XX = new DB();
        #    $XX->CnCliente();
        #    $update = "UPDATE sis_alunosmatfases SET amf_data = null, amf_dataprocesso = null, amf_iniciado = '2018-08-30',
        #                amf_resultado = null,
        #                amf_concluido = null
        #                WHERE amf_matid = '6170' and amf_id = '61698' and amf_alunoid = '$ID_ALUNO'; "; 
        #                
        #    $x = $XX->ExecNonQuery($update);  
        #    return false;
        ############################################################################################################
        ############################################################################################################        
        
        
        ## ENTREGA CNH
        if($SIID_SELECT['amf_servitenid'] == 12) {
            $UPX00 = "UPDATE sis_usuarios SET usu_procvenc = NULL WHERE usu_id = '$ID_ALUNO' ";
            /**/
            $UPX00 = $FASE->ExecNonQuery($UPX00);

            if($UPX00->error) {
                $FASE->roolback();
                $ret['erro'] = '1';
                $ret['mensagem'] = 'Erro ao atualizar registros de fases - 01 ! ProcessosController Linha => ' . __LINE__;
                echo json_encode($ret);
                exit(); 
            }/**/
        }
        
        if(!$CANCELADO) {
            
            $UP   = "UPDATE sis_alunosmatfases SET amf_concluido = current_date(), amf_usuconclui = '".$_SESSION['APP_USUID']."' WHERE amf_matid = '$ID_MATRICULA' AND amf_alunoid = '$ID_ALUNO' AND amf_id = '$ID_FASE' AND amf_cancelado IS NULL ";
        } else {
            
            $UP   = "UPDATE sis_alunosmatfases SET amf_concluido = NULL, amf_iniciado = NULL, amf_resultado = 'CANCELADO' WHERE amf_matid = '$ID_MATRICULA' AND amf_alunoid = '$ID_ALUNO' AND amf_id = '$ID_FASE' ";
        }

        $UP01 = $FASE->ExecNonQuery($UP);
        
        if($UP01->error) {
            $FASE->roolback();
            $ret['erro'] = '1';
            $ret['mensagem'] = 'Erro ao atualizar registros de fases - 01 ! ProcessosController Linha => ' . __LINE__;
            echo json_encode($ret);
            exit();
        }
        
        //Selecionando Próximo Registro
        $NEXT = "SELECT * FROM sis_alunosmatfases WHERE amf_id = (select min(amf_id) from sis_alunosmatfases where amf_alunoid = '$ID_ALUNO' and amf_matid = '$ID_MATRICULA' and amf_concluido is null and amf_cancelado is null  and amf_id > '$ID_FASE' )";
        $NEXT = $FASE->ExecQuery($NEXT);

        
        //SE O RESULTADO FOR 0, É O ULTIMO PROCESSO
        //CONCLUI O PROCESSA E REALIZA O COMMIT, SEM INICIAR PRPXIMO PROCESSO!!!
        if($NEXT->num_rows == 0) {
            $FASE->commit();
            $FASE->autocommit(true);
            $ret['erro'] = '';
            $ret['mensagem'] = 'Fase Concluída!';

            //CONTINUA PROCESSO EM LOTE
            if($CONTINUA) {
                return;
            } else {
                $ID_MATRICULA;
                //marcando matricula como concluida
                $st = "CONCLUIDO";
                if($CANCELADO) {
                    $st = 'CANCELADO';
                }

                $UP = "UPDATE sis_alunosmat SET amat_situacaodata = current_timestamp(), amat_situacao = '$st' where amat_id = '$ID_MATRICULA' ";
                $UP = $FASE->ExecNonQuery($UP);
                echo json_encode($ret);
                exit(); 
            }   
        }


        #####################################################################

        $NEXT = $NEXT->fetch_object();
        
        //SELECIONANDO serviten_cod iguais, todos os que tiverem o mesmo código que a próxima fase do processo, terão
        //a data de inicio gravada no update!
        $funcoes = new FuncoesHelper();
        $NEXT_ID = $NEXT->amf_servitenid;
        $NEXT_COD = $funcoes->fRetCampo('sis_servicositens', 'serviten_cod', "serviten_id = $NEXT_ID");        
         
       
        if($NEXT_COD != '') {

            //SELECIONANDO IDS COM MESMO CODIGO
            $IDSq = "SELECT * FROM sis_servicositens WHERE serviten_cod = '$NEXT_COD' ";
            $IDSq = $FASE->ExecQuery($IDSq);
            //$IDS = $IDSq->fetch_array();

            $IDS_TO_UPDATE = "";
            while($IDx = $IDSq->fetch_object()) {
                $IDS_TO_UPDATE .= $IDx->serviten_id . ",";
            }

        }

        if($IDS_TO_UPDATE != '') {
            $IDS_TO_UPDATE = substr( $IDS_TO_UPDATE,0,-1);
            $xCOND = "amf_servitenid in ($IDS_TO_UPDATE)";
        } else {
            $NEXTID = $NEXT->amf_id;
            $xCOND = "amf_id = '$NEXTID'";
        } 

        //INICIA PROXIMO PROCESSO
        if($NEXT) {
            #TESTE:
            #$NEXTID = $NEXT->amf_id;
            $UP = "UPDATE sis_alunosmatfases SET amf_iniciado = current_date(), amf_pendente = '' WHERE $xCOND AND amf_alunoid = '$ID_ALUNO' and amf_matid = '$ID_MATRICULA' and amf_cancelado IS NULL";// or amf_cancelado = '') ";//and amf_iniciado IS NULL ";
            $UP02 = $FASE->ExecNonQuery($UP);
            if($UP02->error) {
                $FASE->roolback();
                $ret['erro'] = '1';
                $ret['mensagem'] = 'Erro ao atualizar registros de fases - 01 ! ProcessosController Linha => ' . __LINE__;
                echo json_encode($ret);
                exit();                
            }
        } 
        
        $FASE->commit();
        $FASE->autocommit(true);
        $ret['erro'] = '';
        $ret['mensagem'] = 'Fase Concluída!';

        

        if($CONTINUA) {
            return $ret;
        } else {
            echo json_encode($ret);
            exit(); 
        }                       
        
    }
    
    
    /**
     * 
     */
    public function GetDadosFase() {
        $aluno    = $_GET['aluno'];
        $fase     = $_GET['fase'];
        $processo = $_GET['processo'];
        
        $processos = new FasesModel();
        $processo = $processos->Seleciona(
                                            " datediff(current_date, amf_dataprocesso) as DATA_DIF, date_format(amf_dataprocesso, '%d/%m/%Y') AS data_processo, amf_dataprocesso as data_resultado ",
                                            " AND amf_alunoid = '$aluno' AND amf_matid = '$processo' AND amf_id = '$fase' ",
                                            " LEFT OUTER JOIN sis_alunosmat ON amat_id = amf_matid ");
        
        
        $processo = $processo->fetch_assoc();
        
        echo json_encode($processo);
        
    }

    public function GetFasesLogs($idfase) {
        $logsModel = new FasesModel();
        $logs = $logsModel->GetFaseLogs($idfase);
        return $logs;
    }


    function Cancelamento() {
        $this->ValidaNivel(40);
        
        if(!isset($_GET['motivo'])) {
            exit("Informe o motivo para cancelamento!!!");
        }
        
        if(!isset($_GET['aluno'])) {
            exit("ERRO A");
        }

        if(!isset($_GET['processo'])) {
            exit("ERRO B");
        }

        $ID_ALUNO    = (int) $_GET['aluno'];
        $ID_PROCESSO = (int) $_GET['processo'];
        $MOTIVO      = $_GET['motivo'];
        
        $Processo = new ProcessosModel();
        $ret = $Processo->Cancelamento($ID_PROCESSO,$ID_ALUNO,$MOTIVO);
        $datas = array('valor' => $ret );
        //nenhum lancamento no financeiro
        
        $this->RenderView("financeiro/cancelamento",$datas);
        return;
    }
    
    public function VoltaFase() {
        if(!isset($_GET['fase'])) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "FASE NÃO INFORMADA!!! <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;        
        }

        if(!isset($_GET['aluno'])) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "ALUNO NÃO INFORMADO!!! <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;        
        }
        
        if(!isset($_GET['matricula'])) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "MATRICULA NÃO INFORMADO!!! <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;        
        }

        $ID_FASE      = $_GET['fase'];
        $ID_ALUNO     = $_GET['aluno'];
        $ID_MATRICULA = $_GET['matricula'];
        
        if(!$this->ValidaNivel2(40)) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "SEM PERMISSÃO <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;
        }
        
        
        
        /**/
        $db =  new DB();
        $db->CnCliente();
        
        ##############################################################################
        ///UPDATE PARA VOLTAR FASE
        $selec01 = "UPDATE sis_alunosmatfases SET "
                . "amf_concluido = NULL, "
                . "amf_datalista = NULL, "
                . "amf_pendente = '*' "
                . "WHERE amf_id = '$ID_FASE' ";
        $db->ExecNonQuery($selec01);

        $select02 = "SELECT amf_id FROM sis_alunosmatfases WHERE amf_alunoid = '$ID_ALUNO' AND amf_matid = '$ID_MATRICULA' AND amf_id != '$ID_FASE' AND NOT amf_iniciado IS NULL and NOT amf_concluido IS NULL ORDER BY 1 desc LIMIT 1";
        $db->ExecQuery($select02);
        $resultado = $db->result_array();

        $ID_ANT = $resultado[0]['amf_id'];
        $update = "UPDATE sis_alunosmatfases SET amf_concluido = NULL, amf_usuconclui = NULL WHERE amf_id = '$ID_ANT' ";
        $db->ExecNonQuery($update);

        $ret['mensagem'] = "SUCESSO";
        echo json_encode($ret);
    }
    
    
    public function CancelaFase() {
        if(!isset($_GET['fase'])) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "FASE NÃO INFORMADA!!! <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;        
        }
        $ID_FASE = (isset($_GET['fase']) ? $_GET['fase'] : "");
        $ID_MATRICULA = (isset($_GET['matricula']) ? $_GET['matricula'] : "");
        $ID_ALUNO = (isset($_GET['aluno']) ? $_GET['aluno'] : "");
        
        if(!$this->ValidaNivel2(40)) {
            $ret['erro'] = "1";
            $ret['mensagem'] = "SEM PERMISSÃO <br/> ERRO: CTRL_PROC ". __LINE__;
            echo json_encode($ret);
            return;
        }
        
        $ret['mensagem'] = "ERRO DESCONHECIDO!! ER: 1503-17 - 1336";
        
        /**/
        $db =  new DB();
        $db->CnCliente();
        
        ##############################################################################
        ///UPDATE PARA CANCELAR FASE

        $selec1 = "UPDATE sis_alunosmat SET amat_situacao = 'CANCELADO', amat_situacaodata = current_timestamp() WHERE amat_id = '$ID_MATRICULA' and  amat_alunoid = '$ID_ALUNO' ";
        $selec1 = $db->ExecNonQuery($selec1);
        
        $selec = "UPDATE sis_alunosmatfases SET amf_cancelado = current_timestamp() WHERE amf_alunoid = '$ID_ALUNO' AND amf_matid = '$ID_MATRICULA' AND amf_id = '$ID_FASE'";
        $selec = $db->ExecNonQuery($selec);
        
       $this->ConcluirFase($ID_MATRICULA,$ID_FASE,$ID_ALUNO,TRUE);
        
       $ret['mensagem'] = "SUCESSO";
        echo json_encode($ret);
    }    
}

 