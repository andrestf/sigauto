<?php



class FinanceiroController extends Controllers {
    
    /**
     * 
     * \lista os tipos de documentos
     */
    public function index() {
        $this->ValidaNivel(40);
        $parcelas = "";
        $FinanceiroModel = new FinanceiroModel();
//        $parcelas = $FinanceiroModel->ListaParcelas("",1);   
        $parcelas = array();

        if(isset($_POST['lista']) && $_POST['lista'] == 'listagem') {
               $dados = null;
               foreach ($_POST as $key => $value) {
                   $dados[$key] = $value;
                   
                   $limite = 500;
                   if($key == 'limit' && $value != '*') {
                       $limite = $dados[$key];
                   }
               }

               $parcelas = $FinanceiroModel->ListaParcelas($dados, $limite);
        }

        ###################################
        ## CURSOS - 27/03/2018 - EMERSON
           ## $Cursos = new CursosModel();
           ## $Cursos = $Cursos->Listar();


          ## oque mudou era que vc chamava o CursosModel o certo é CrudModel
            $Cursos = new CrudModel();
            $Cursos->table      = "sis_cursos";
            $Cursos->campoid    = "cur_id";
            $Cursos->campolocal = "cur_localid";
            $Cursos->campoorder = "cur_ativo";

            $Cursos = $Cursos->Listar();                   

        ###################################
        ## TIPO DE DOCUMENTOS
           #$TpDocs = new TipoDocumentosModel();
           #$TpDocs = $TpDocs->Listar();


            ## aqui tb mudou
            $TpDocs = new CrudModel();
            $TpDocs->table      = "sis_tpdocum";
            $TpDocs->campoid    = "tpd_id";
            $TpDocs->campolocal = "tpd_localid";
            $TpDocs->campoorder = "tpd_ordem";
            
            $TpDocs = $TpDocs->Listar();            
           
        ####################################
        ## CENTRO DE CUSTO
           $CEC = New CentrodecustoModel();
           $CEC = $CEC->Listar();           

        ####################################
        ## PLANO DE CONTAS
           $PLC = New PlanodecontasModel();
           $PLC = $PLC->Listar();            
        
        ####################################
        ## SELECIONANDO OS LOGS PARCELA 
        /**  */
        $_parcelas = $parcelas;
        $parcelas = array();
        $i = 0;
        if(count($_parcelas) > 1 ) :
        foreach ($_parcelas as $key => $value) {
            $parcelas[$i]['parcela'] = $value;
            $logs = $FinanceiroModel->ListaLogs($value['finmov_id']);
            if($logs)
                $parcelas[$i]['log'] = $logs;
            $i++;
        }
        endif;




        $dados = array(
            "TpDocs" => $TpDocs,
            "CentroDeCustos" => $CEC,
            "PlanoDeContas" => $PLC,
            "parcelas" => $parcelas
        );

        $this->RenderView('financeiro/financeiro', $dados);
       
    }

    /**
    * Carrega lista do financeiro após Action Index e usuário enviar o form do filtro!
    */
    public function CarregaLista() {

    }
    

    /**
    * Função de Baixar Parcelas
    */
    public function BaixaParcela() {  
        $this->ValidaNivel(40);
        $baixa = new FinanceiroModel();

        
        $id_aluno   = $_POST['alu-id'];
        $id_parcela = $_POST['parc-id'];
        $valorbx    = $_POST['baixa-valor'];
        $data_baixa = $_POST['baixa-data'];
        
        
        $usaCredito = false;
        if(isset($_POST['chkUtilizaCreditos'])) {
            $usaCredito = true;
        }


        ## UTILIZANDO CRÉDITOS
        $ValorCredito = 0;
        if($usaCredito) {

            // creditos
            $CreditosModel = new CreditosModel();
            $ValorCredito = $CreditosModel->SomaCreditoDisponivel($id_aluno);
            $ValorCredito = number_format($ValorCredito,2,".","");

            if($ValorCredito > 0) {
                //valor da parcela
                $ValorOri = $baixa->SelecionaParcela($id_parcela);
                $ValorOri = $ValorOri['finmov_valor'];

                //sevalor do credito foi maior que parcela
                if($ValorCredito > $ValorOri ) {
                    $ValorCredito = $ValorOri;
                }
            }

           #$resto = number_format($ValorOri-$ValorCredito,2,",","");
           $resto = $ValorOri-$ValorCredito;

           $valorbx = $ValorOri - $resto;
           $valorbx = $resto = number_format($valorbx,2,",","");


        }

        $valorbx = str_replace(".", "", $valorbx);
        $valorbx = str_replace(",", ".", $valorbx);

        if(!$usaCredito) {
            if($valorbx == "" || $valorbx <= 0 ){
                echo "Valor dever ser maior que zero!!!";
                return;
            }
        }

        
        if($data_baixa == '') {
            echo "Necessário preencher a data de baixa!!!";
            return;
        }
     
        $data_baixa = DataDB($data_baixa);
        $usuariobx  = $_SESSION['APP_USUID'];
        if(!isset($_POST['tipo-documento'])) {
            echo "Informe tipo de documento";
            return;
        }
        $tpdocbx        = $_POST['tipo-documento'];
            
        if(!isset($_POST['observacoes'])) {
            $observacoes    = "";
        } else {
            $observacoes    = $_POST['observacoes'];
        }
        
        if($usaCredito) {
            $observacoes = $observacoes . "[UTILIZAÇÃO DE CRÉDITOS VL CREDITO: $ValorCredito ]";
        }
        
        $tprecpag   = 'CAIXA';

        $baixa->SetAluno($id_aluno);
        $baixa->BaixaParcela($id_parcela, $valorbx, $data_baixa, $usuariobx, $tprecpag, $tpdocbx,$observacoes,$ValorCredito);
    }


    public function EstornaParcela() {  
        if(!$this->ValidaNivel2(40)) 
        {
            echo "sem permissao";
            exit();
        }
        $baixa = new FinanceiroModel();
        $id_parcela = $_POST['parc-id'];
        $usuariobx  = $_SESSION['APP_USUID'];
        $tprecpag   = 'CAIXA';
        

        $baixa->EstornaParcela($id_parcela,$usuariobx);
    }


    public function CriaMovimento() {  
        $this->ValidaNivel(50);
        $Mov = new FinanceiroModel();

        
        $valor = $_POST['valor'];
        $vencimento = $_POST['vencimento'];
        $tipo = $_POST['tipo'];
        $descricao  = $_POST['descri'];
        $tprecpaga = $_POST['tprecpaga'];
        $idaluno = $_POST['aluno'];
        
        $ccusto = $_POST['centrocusto'];
        $pconta= $_POST['planoconta'];
        
        $valor = str_replace(".","", $valor);
        $valor = str_replace(",",".", $valor);


        $nMesesRepetir = $_POST['repetir'];

        if($valor <= 0 || $valor == "") {
            echo "Valor inválido";
            return;
        }

        if($tprecpaga == "") {
            echo "Informe o local da movimentação.";
            return;
        }        

        if($vencimento == "") {
            echo "Informe o vencimento";
            return;
        }

        if($tipo == "") {
            echo "Informe o tipo";
            return;
        }

        if($descricao == "") {
            echo "Informe a descrição";
            return;
        }
        
        if($ccusto== "") {
            echo "Informe o Centro de Custo";
            return;
        }

        if($pconta == "") {
            echo "Informe o Plano de Contas";
            return;
        }




        $movtipo  = "FINANCEIRO";
        $deonde   = "FINAN";
        $tprecpag   = $tprecpaga;
        $deondeid = NULL;
        $idaluno  = $idaluno;
        $retx = $Mov->CriaMovimento($idaluno,$valor,$vencimento,$tipo,$descricao,$movtipo,$deonde,$deondeid,TRUE,$tprecpag,$ccusto,$pconta,$nMesesRepetir);

        $LASTID = $retx;

        $Mov->SalvaLog($LASTID,"INS","Criação de Movimento. </br> Tipo:$tipo Valor:$valor ");        
    }
    
    public function BaixaParcelaLote($id_parcela) {
        
    }



    //Carrega dados da parcela, para edição
    public function CarregaDadosParcela() {
        $id_parcela = (isset($_GET['n']) ) ? $_GET['n'] : '0';
        $from = (isset($_GET['from']) ) ? $_GET['from'] : 'nulo';

        if($id_parcela == '' || $id_parcela == "0") {
            exit('parcela invalida');
        }

        $FinanceiroModel = new FinanceiroModel();
        $parcela = $FinanceiroModel->SelecionaParcela($id_parcela);

        $TpDocs = new TipoDocumentosModel();
        $TpDocs = $TpDocs->Listar();

        $cont = "";

        $cont .= '<input type="hidden" name="parc-id" value="'.$parcela['finmov_id'].'" id="edt-parc-id"/>';
        $cont .= '<input type="hidden" name="loc-id"  value="'.$parcela['finmov_localid'].'" id="edt-mat-id"/>';
        $cont .= '<input type="hidden" name="from"  value="'.$from.'" id="edt-from"/>';
        
        if($from == 'TelaFinan' ) {
            $cont .= "<div class='row'>";
                $cont .= "<div class='col-sm-4'>";
                $cont .= "<label>Vencimento</label>";
                $cont .= "<input type='text' class='form-control ' onkeyup='formataData(this)' name='dtVenc' value='".DataBR($parcela['finmov_dtvenc'])."' /> ";
                $cont .= "</div>";

                $cont .= "<div class='col-sm-4'>";
                $cont .= "<label>Valor</label>";
                $cont .= "<input type='text' class='form-control vlr' name='vlr' value='".$parcela['finmov_valor']."' onkeyup='formataValor(this)'/> ";
                $cont .= "</div>";

                $cont .= "<div class='col-sm-4'>";
                $cont .= "<label>Tipo Documento</label>";
                $cont .= "<select class='form-control' name='tpDoc'>";
                foreach ($TpDocs as $docum) {
                    $selected = "";
                    if($docum['tpd_cd'] == $parcela['finmov_tpdocumbx'] ) {
                        $selected = "selected='selected'";
                    }
                    $cont .= "<option value='".$docum['tpd_cd']."' $selected >".$docum['tpd_descricao']."</option>";
                }
                //$cont .= "<input type='text' class='form-control' value='".$parcela['finmov_tpdocumbx']."' /> ";
                $cont .= "</select>";
                $cont .= "</div>";
            $cont .= "</div>";

            $cont .= "<div class='row'>";
                $cont .= "<div class='col-xs-12'>";
                $cont .= "<label>Descrição</label>";
                $cont .= "<input type='text' name='descri' class='form-control' value='".$parcela['finmov_descricao']."' />";
                $cont .= "</div>";
            $cont .= "</div>";

                $cont .= "</select>";
                $cont .= "</div>";
            $cont .= "</div>";
        
        }// If TelaCobranca

        $cont .= "<div class='row'>";
            $cont .= "<div class='col-xs-12'>";
            $cont .= "<label>Observação de Cobrança</label>";
            $cont .= "<input type='text' name='obscobr' class='form-control' value='".$parcela['finmov_obscobranca']."' />";
            $cont .= "</div>";
        $cont .= "</div>";

        echo $cont;
    }
    /**
     * Editar Parcela
     **/
    public function EditarParcela() {
        


        if(!$this->ValidaNivel2(40)) 
        {
            echo "SEM PERMISSÃO [50]";
            exit();
        }
        

        $Mov = new FinanceiroModel();
        $from = (isset($_GET['from']) ) ? $_GET['from'] : 'nulo';


        $IDPARC     = $_POST['parc-id'];

        if($from == "TelaFinan") {
            if(!isset($_POST['parc-id'])) { exit("PARCELA não informada </ br>ERROR 1711-170323"); }
            if(!isset($_POST['loc-id']))  { exit("LOCAL não informada </ br>ERROR 1712-170323"); }
            if(!isset($_POST['dtVenc']))  { exit("VENCIMENTO não informada </ br>ERROR 1713-170323"); }
            if(!isset($_POST['vlr']))  { exit("VALOR não informada </ br>ERROR 1714-170323"); }
            if(!isset($_POST['tpDoc']))  { exit("TIPO_DOC não informada </ br>ERROR 1715-170323"); }
            if(!isset($_POST['descri']))  { exit("DESCRIÇÃO não informada </ br>ERROR 1715-170323"); }

                
                $DTVENC     = DataDB($_POST['dtVenc']);
                $VALOR      = $_POST['vlr'];
                $TPDOC      = $_POST['tpDoc'];
                $DESCR      = $_POST['descri'];
        }

        if($from == "tabAlunos") {
                $PARCELA = $Mov->SelecionaParcela($IDPARC);
                
                $DTVENC     = $PARCELA['finmov_dtvenc'];
                $VALOR      = $PARCELA['finmov_valor'];
                $TPDOC      = $PARCELA['finmov_tpdocumbx'];
                $DESCR      = $PARCELA['finmov_descricao'];            

                $VALOR = str_replace(".",",",$VALOR);

        }

        if(!isset($_POST['obscobr']))  { exit("OBSERVAÇÃO não informada </ br>ERROR 1715-170323"); }
        $OBSCOBS    = $_POST['obscobr'];


        
        $Editar = $Mov->EditarMovimento($IDPARC,$DTVENC,$VALOR,$TPDOC,$DESCR,$OBSCOBS);
        if($Editar) {
            $ret['mensagem'] = "SUCESSO!";
        } else {
            $ret['erro'] = "1";
            $ret['mensagem'] = $Editar['mensagem'];
        }

        echo json_encode($ret);


    }

    /**
     * Cancelamento de parcelas
     */
    public function CancelarParcela() {
        if(!$this->ValidaNivel2(40)) 
        {
            echo "SEM PERMISSÃO [40]";
            exit();
        }

        $PARCELA = isset($_GET['n']) ? $_GET['n'] : "" ;  

        if($PARCELA <= 0 || $PARCELA == "") {
            $ret['erro'] = "1";
            $ret['erro'] = "ERRO, parcela invalida para cancelamento/recuperação!";
            //echo "ERRO, parcela invalida para cancelamento/recuperação!";
            exit();
        }

        $Mov = new FinanceiroModel();
        $Cancelamento = $Mov->MarcaCancelado($PARCELA);
        if($Cancelamento) {
            $ret['mensagem'] = "SUCESSO!";
        } else {
            $ret['erro'] = "1";
            $ret['mensagem'] = $Cancelamento['mensagem'];
        }

        echo json_encode($ret);
    }

    public function CancelarParcelalote() {
        if(!$this->ValidaNivel2(40)) 
        {
            echo "SEM PERMISSÃO [40]";
            exit();
        }

        $PARCELAS = (!isset($_POST['n']) ? "0" : $_POST['n']);

        if($PARCELAS <= 0 || $PARCELAS == "") {
            echo "ERRO, parcela invalida para cancelamento/recuperação!";
            exit();
        }

        $MOV =  new FinanceiroModel();
        $Cancelamento = $MOV->MarcaCanceladoLote($PARCELAS);
    }

    ## CURSOS - 27/03/2018
    public function Cursos() {
        $dados = array();

        $Cursos = new CrudModel();
        $Cursos->table      = "sis_cursos";
        $Cursos->campoid    = "cur_id";
        $Cursos->campolocal = "cur_localid";
        $Cursos->campoorder = "cur_ativo";

        $listaCursos = $Cursos->Listar();
        $dados['listaCursos'] = $listaCursos;
        
        $this->RenderView('financeiro/cursos',$dados);
    }

    public function CursosEditar() {
        $dados = array();


        $Cursos = new CrudModel();
        $Cursos->table      = "sis_cursos";
        $Cursos->campoid    = "cur_id";
        $Cursos->campolocal = "cur_localid";
        $Cursos->campoorder = "cur_ativo";
        
        if(!isset($_GET['editar'])) { exit('erro'); }
        $id = $_GET['editar'];

        ##############################################
        if(isset($_POST['idEdit'])) {
           
            $campos = array();
            $camposObrigatorios = array(
                "cur_descricao" => "Descrição",
                "cur_dias"      => "Dias"
            );

            $camposIgnorar = array("idEdit");

            foreach ($_POST as $NomeCampo => $Valor) {
                if(!in_array($NomeCampo, $camposIgnorar) )
                    $campos['cur_'.$NomeCampo] = $Valor;
            }

            foreach ($campos as $NomeCampo => $Valor) {
                
                foreach ($camposObrigatorios as $obg => $descri) {
                    if($NomeCampo == $obg) {
                        if($Valor == "") {
                            $this->SetErro('Informe ' . $descri);
                            $this->GetErro();                            
                        }
                    }
                }            
            }
            $id  = $_POST['idEdit'];
            
            if(!$Cursos->Update($id,$campos)) {
                exit("Falha no update... tente novamente ou entre em contato com o responsável.");
            } else {
                $redir = new RedirectHelper();
                $redir->controller_redir_to = "Financeiro/Cursos/";
                $redir->Redirecionar();                
            }
            
            return;
        }
        ##############################################
        
        
        $curso = $Cursos->Seleciona($id);
        $dados['cur'] = $curso;
        $dados['acao'] = "Editar";
        $dados['viewDatas']['PageTitle'] = "Editar Curso";

        $this->RenderView('financeiro/cursos_editar',$dados);
    }     

    
    public function CursosAdicionar() {
        $dados = array();
        $campos = array(
            "cur_id" => "0",
            "cur_descricao" => "",
            "cur_dias" => "",
            "cur_hora" => "",
            "cur_hralmoco" => "",
            "cur_hrtermino" => "",
            "cur_turma" => "",
            "cur_sala" => "",
            "cur_formacao" => "",
            "cur_atualizacao" => "",
            "cur_ativo" => "",
            "cur_contato1" => "",
            "cur_contato2" => "",
            "cur_contato3" => ""
        );
        
        $Cursos = new CrudModel();
        $Cursos->table      = "sis_cursos";
        $Cursos->campoid    = "cur_id";
        $Cursos->campolocal = "cur_localid";
        $Cursos->campoorder = "cur_ativo";
        
        ##############################################
        if(isset($_POST['idEdit'])) {

            $camposObrigatorios = array(
                "cur_descricao" => "Descrição",
                "cur_dias"      => "Dias"
            );

            $camposIgnorar = array("idEdit");


            foreach ($_POST as $NomeCampo => $Valor) {
                if(!in_array($NomeCampo, $camposIgnorar) )
                    $campos['cur_'.$NomeCampo] = $Valor;
            }

            foreach ($campos as $NomeCampo => $Valor) {
                
                foreach ($camposObrigatorios as $obg => $descri) {
                    if($NomeCampo == $obg) {
                        if($Valor == "") {
                            $this->SetErro('Informe ' . $descri);
                            $this->GetErro();                            
                        }
                    }
                }            
            }

            /*
            $cur_descricao  = $_POST['descricao'];
            $cur_dias       = $_POST['dias'];
            $cur_hora       = $_POST['hora'];
            $cur_hralmoco   = $_POST['almoco'];

            if( $cur_descricao  == '' || 
                $cur_dias       == '' || 
                $cur_hora       == '' ) {
                exit("Todos os campos são obrigatórios.");
            }

            $campos['cur_descricao']    = $cur_descricao;
            $campos['cur_dias']         = $cur_dias;
            $campos['cur_hora']         = $cur_hora;
            $campos['cur_hralmoco']     = $cur_hralmoco;
            $campos['cur_hrtermino']     = $cur_hralmoco;
            */

            $campos['cur_localid']   = $_SESSION['APP_LOCALID'];
            $campos['cad_usua']      = $_SESSION['APP_USUID'];
            $campos['cad_data']      = date("Y-m-d G:i:s");

            if(!$Cursos->inserir($campos)) {
                exit("Falha no update... tente novamente ou entre em contato com o responsável.");
            } else {
                $redir = new RedirectHelper();
                $redir->controller_redir_to = "Financeiro/Cursos";
                $redir->Redirecionar();                
            }
            
            return;
        }
        ##############################################
        
    

        $dados['viewDatas']['PageTitle'] = "Adicionar Cursos";
        $dados['cur'] = $campos;
        $dados['acao'] = "Adicionar";
        

        $this->RenderView('financeiro/cursos_editar',$dados);
    } 
    ## CURSOS - FIM 

    #####################################################
    public function TipoDocumentos() {
        $dados = array();
        #$TpDocumentos = new TipoDocumentosModel();
        #$listaDocumentos = $TpDocumentos->Listar();

        $TpDocumentos = new CrudModel();
        
        $TpDocumentos->table      = "sis_tpdocum";
        $TpDocumentos->campoid    = "tpd_id";
        $TpDocumentos->campolocal = "tpd_localid";
        $TpDocumentos->campoorder = "tpd_ordem";

        $listaDocumentos = $TpDocumentos->Listar();


        $dados['listaDocumentos'] = $listaDocumentos;
        
        $this->RenderView('financeiro/tipodocumentos',$dados);
    }
    
    
    public function TipoDocumentosEditar() {
        $dados = array();
        
        if(!isset($_GET['editar'])) { exit('erro'); }
        $id = $_GET['editar'];
       
        $TpDocumentos = new CrudModel();
        $TpDocumentos->table      = "sis_tpdocum";
        $TpDocumentos->campoid    = "tpd_id";
        $TpDocumentos->campolocal = "tpd_localid";
        $TpDocumentos->campoorder = "tpd_ordem";


        ##############################################
        if(isset($_POST['idEdit'])) {
            $tpd_descricao = $_POST['descricao'];
            $tpd_codigo    = $_POST['codigo'];
            $tpd_destino   = $_POST['destino'];
            

            if( $tpd_descricao == '' || 
                $tpd_codigo    == '' || 
                $tpd_destino  == '' ) {
                exit("Todos os campos são obrigatórios.");
            }
            

            $campos['tpd_descricao'] = $tpd_descricao;
            $campos['tpd_cd']    = $tpd_codigo;
            $campos['tpd_destino']   = $tpd_destino;
            
            $id  = $_POST['idEdit'];
            
            if(!$TpDocumentos->Update($id,$campos)) {
                exit("Falha no update... tente novamente ou entre em contato com o responsável.");
            } else {
                $redir = new RedirectHelper();
                $redir->controller_redir_to = "Financeiro/TipoDocumentos/";
                $redir->Redirecionar();                
            }
            
            return;
        }
        ##############################################
        
        
        $documento = $TpDocumentos->Seleciona($id);
        $dados['doc'] = $documento;
        $dados['acao'] = "Editar";
        $dados['viewDatas']['PageTitle'] = "Editar Tipo de Documento";

        $this->RenderView('financeiro/tipodocumentos_editar',$dados);
    }  
    
    
    
    public function TipoDocumentosAdicionar() {
        $dados = array();
        
        $TpDocumentos = new CrudModel();
        $TpDocumentos->table      = "sis_tpdocum";
        $TpDocumentos->campoid    = "tpd_id";
        $TpDocumentos->campolocal = "tpd_localid";
        $TpDocumentos->campoorder = "tpd_ordem";
        
        ##############################################
        if(isset($_POST['idEdit'])) {
            $tpd_descricao = $_POST['descricao'];
            $tpd_codigo    = $_POST['codigo'];
            $tpd_destino   = $_POST['destino'];
            

            if( $tpd_descricao == '' || 
                $tpd_codigo    == '' || 
                $tpd_destino  == '' ) {
                exit("Todos os campos são obrigatórios.");
            }

            $campos['tpd_descricao'] = $tpd_descricao;
            $campos['tpd_cd']        = $tpd_codigo;
            $campos['tpd_destino']   = $tpd_destino;
            $campos['tpd_localid']   = $_SESSION['APP_LOCALID'];

            if(!$TpDocumentos->inserir($campos)) { 
                exit("Falha no update... tente novamente ou entre em contato com o responsável.");
            } else {
                $redir = new RedirectHelper();
                $redir->controller_redir_to = "Financeiro/TipoDocumentos";
                $redir->Redirecionar();                
            }
            
            return;
        }
        ##############################################
        
        
        $documento['tpd_id'] = "";
        $documento['tpd_cd'] = "";
        $documento['tpd_descricao'] = "";
        $documento['tpd_destino'] = "";

        $dados['viewDatas']['PageTitle'] = "Adicionar tipo de documentos";
        $dados['doc'] = $documento;
        $dados['acao'] = "Adicionar";
        

        $this->RenderView('financeiro/tipodocumentos_editar',$dados);
    }      
    
    public function Reports() {
       // $this->ValidaNivel(50);
        
        ###################################
        ## TIPO DE DOCUMENTOS
           $TpDocs = new TipoDocumentosModel();
           $TpDocs = $TpDocs->Listar();
        
        ####################################
        ## USUARIOS
           $Usuarios = New ColaboradoresModel();
           $Usuarios = $Usuarios->Listar( "AND usu_sitacesso = 'A' ");

        ####################################
        ## CENTRO DE CUSTO
           $CEC = New CentrodecustoModel();
           $CEC = $CEC->Listar();           

        ####################################
        ## PLANO DE CONTAS
           $PLC = New PlanodecontasModel();
           $PLC = $PLC->Listar(); 

        $dados = array(
            "TipoDocumentos" => $TpDocs,
            "Usuarios"       => $Usuarios,
            "CentroDeCustos" => $CEC,
            "PlanoDeContas" => $PLC
        );
        
        $this->RenderView('financeiro/filtroreports',$dados);
    }
    
    public function ReportsGerar() {
        //$this->ValidaNivel(50);
        $report = new FinanceiroRelatoriosModel(); 
        $report->SetReport($_POST['report']);
        $r = $report->GetReport();
        
        //$dados_report = $report->Gerar();
        
        $dados = array(
            "dados" => ''
        );
        
        $this->RenderView('financeiro/reports/'.$r,$dados,'public/header','',false);
    }
}
