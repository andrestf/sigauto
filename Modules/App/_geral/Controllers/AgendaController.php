<?php


class AgendaController extends Controllers{
    
    public function index() {
    	$h = new RedirectHelper();
    	$h->controller_redir_to = 'agenda';
    	$h->action_redir_to = 'agenda/&cod=';
    	$h->Redirecionar();
    }
    public function agenda() {
    	
    	$dados = array();   	
    	
    	//**************//
    	$AlunosModel = new AlunosModel();
        $id  = "";
        if(isset($_GET['cod'])) {
    	   $id = $_GET['cod'];
        }

    	$aluno = $AlunosModel->seleciona($id," and usu_localid = '".$_SESSION['APP_LOCALID']."' ");
    	$dados['aluno'] = $aluno;

    	//**************//
    	$ColaboradoresModel = new ColaboradoresModel();
    	$instrutores = $ColaboradoresModel->Listar(" and usu_tipo = 'INSTR' ");
    	$dados['instrutores'] = $instrutores;

    	//**************//
    	$VeiculosModel = new VeiculosModel();
    	$veiculos = $VeiculosModel->Listar("TRUE",'',' order by veic_descricao asc');
    	$dados['veiculos'] = $veiculos;

    	//**************//
    	$AgendaModel = new AgendaModel();
    	//$eventos	 = $AgendaModel->Listar(" au_ini >= current_timestamp() ");
    	$eventos	 = $AgendaModel->Listar();
    	//
    	$dados['eventos'] = "[]";
    	if($eventos) {
	    	$evts = '';
	    	foreach ($eventos as $evt) {
	    		$evts .= "{";
	    		$evts .= "id    : ".$evt['au_id']." , ";
	    		$evts .= "title : '".$evt['au_descricao']."', ";
	    		$evts .= "start : '".str_replace(" ","T",$evt['au_ini'])."', ";
	    		$evts .= "end 	: '".str_replace(" ","T",$evt['au_fim'])."' ";
	    		$evts .= "},";
	    	}

	    	$evts = substr($evts, 0,-1);
	    	$dados['eventos'] = "[".$evts."]";
	    }
    	

    	

        $this->RenderView("agenda/agenda",$dados);
    }


    public function NovoEvento() {
		$alunoid          = (isset($_POST['alunoid']) ? $_POST['alunoid'] : '');
		$cboInstrutorPrin = (isset($_POST['cboInstrutorPrin']) ? $_POST['cboInstrutorPrin'] : '');
		$cboVeicPrin      = (isset($_POST['cboVeicPrin']) ? $_POST['cboVeicPrin'] : '');
		$dini             = (isset($_POST['dini']) ? $_POST['dini'] : '');
		$dfim             = (isset($_POST['dfim']) ? $_POST['dfim'] : '');

		$ret['erro'] = 0;
		$ret['mensagem'] = '';

		if($alunoid == '') {
			$ret['erro'] = 1;
			$ret['mensagem'] = 'Nescessário informar um aluno para continuar!'; }

		if($cboInstrutorPrin == '') {
			$ret['erro'] = 1;
			$ret['mensagem'] = 'Nescessário informar um Instrutor para continuar!'; }

		if($cboVeicPrin == '') {
			$ret['erro'] = 1;
			$ret['mensagem'] = 'Nescessário informar um Veículo para continuar!'; }

		if($dini == '') {
			$ret['erro'] = 1;
			$ret['mensagem'] = 'Nescessário informar uma data de Início para continuar!'; }

		if($dfim == '') {
			$ret['erro'] = 1;
			$ret['mensagem'] = 'Nescessário informar uma data de Término para continuar!'; }


		if($ret['erro'] != '0') {
			echo json_encode($ret);
			return;
		}

		$dini = $this->ConvertDate($dini);
		$dfim = $this->ConvertDate($dfim);

		//$evento['alunoid']      = ;
		$evento['au_seqlanc']   = 0;
		$evento['au_idaluno']   = $alunoid;
		$evento['au_idinstrut'] = $cboInstrutorPrin;
		$evento['au_idlocal'] = $_SESSION['APP_LOCALID'];
		$evento['au_idcarro']   = $cboVeicPrin;
		$evento['au_descricao'] = $_POST['aluno'];
		$evento['au_status']    = 'INICIADO';
		$evento['au_ini']       = $dini;
		$evento['au_fim']       = $dfim;
		$evento['cad_data']     = date("Y-m-d G:i:s");
		$evento['cad_usua']     = $_SESSION['APP_USUID'];

		$AgendaModel = new AgendaModel();

		if(isset($_POST['eventoid']) && $_POST['eventoid'] != '') {
			$id = $_POST['eventoid'];
			$x = $AgendaModel->Update($id,$evento);
		} else {
			$x = $AgendaModel->NovaAula($evento);	
		}
		

		echo json_encode($x);


    }

    public Function ConvertDate($date) {
    	

    	$dia = substr($date, 0,2);
    	$mes = substr($date, 3,2);
    	$ano = substr($date, 6,4);
    	$hora = substr($date, 11,2);
    	$minu = substr($date, 14,2);

    	return $ano ."-". $mes ."-". $dia ." ".$hora.":".$minu.":00";


    }


    public Function ConvertDateTimerPicker($date) {	
    	// 2018-08-29 06:30:00

    	$dia = substr($date, 8,2);
    	$mes = substr($date, 5,2);
    	$ano = substr($date, 0,4);
    	$hora = substr($date, 11,2);
    	$minu = substr($date, 14,2);

    	return $dia ."/". $mes ."/". $ano ." ".$hora.":".$minu.":00";


    }    


    public function update() {
		$AgendaModel = new AgendaModel();
		//$x = $AgendaModel->Update($id,$evento);

		//echo json_encode($x);		    	
    }

    public function updatedates() {

		$evento['au_ini'] = $this->ConvertDate($_POST['dataini']);
		$evento['au_fim'] = $this->ConvertDate($_POST['datafim']);
		
		
		$id = $_POST['id'];

		$AgendaModel = new AgendaModel();
		$x = $AgendaModel->Update($id,$evento);

		echo json_encode($x);		
    	
    }

    public function NovoEventoxxx() {
    	
    	$dados = array();

    	//**************//
    	$ColaboradoresModel = new ColaboradoresModel();
    	$instrutores = $ColaboradoresModel->Listar(" and usu_tipo = 'INSTR' ");
    	$dados['instrutores'] = $instrutores;

    	//**************//
    	$VeiculosModel = new VeiculosModel();
    	$veiculos = $VeiculosModel->Listar("TRUE",'',' order by veic_descricao asc');
    	$dados['veiculos'] = $veiculos;

    	$this->RenderView("agenda/novoevento",$dados);
    }


    public function edtEvt() {
    	$dados = array();

    	//**************//
    	$idEvt = $_GET['codee'];
    	$AgendaModel = new AgendaModel();
    	$Evento = $AgendaModel->Listar(" au_id = '$idEvt' ");
    	$dados['evento'] = $Evento[0];
    	$dados['evento']['au_ini'] = $this->ConvertDateTimerPicker($dados['evento']['au_ini']);
    	$dados['evento']['au_fim'] = $this->ConvertDateTimerPicker($dados['evento']['au_fim']);
    	$idAluno = $dados['evento']['au_idaluno'];
    	//**************//
    	$ColaboradoresModel = new ColaboradoresModel();
    	$instrutores = $ColaboradoresModel->Listar(" and usu_tipo = 'INSTR' ");
    	$dados['instrutores'] = $instrutores;

    	//**************//
    	$VeiculosModel = new VeiculosModel();
    	$veiculos = $VeiculosModel->Listar("TRUE",'',' order by veic_descricao asc');
    	$dados['veiculos'] = $veiculos;    	

    	//**************//
    	$AlunosModel = new AlunosModel();
    	
    	$aluno = $AlunosModel->seleciona($idAluno," and usu_localid = '".$_SESSION['APP_LOCALID']."' ");
    	$dados['aluno'] = $aluno;    	
    	
    	

        $this->RenderView("agenda/editevt",$dados,false,false,false);
    	//$this->RenderPartialView("agenda/editevt",$dados);
    }


    public function RemoverAula() {
        $id  = "0";
        if(isset($_POST['ial'])) {
           $id = $_POST['ial'];
        }

        $idAula = "0";
        if(isset($_POST['iau'])) {
            $idAula = $_POST['iau'];
        }
        $ret = array();
        if($idAula == 0 OR $id == 0) {
            $ret['erro'] = "Ação inválida!";
        }

        $idAluno = $id;
        $AgendaModel = new AgendaModel();
        $ret = $AgendaModel->Remover($idAula,$idAluno);
        echo json_encode($ret);
    }

    public function Relatorios() {
        $dados = array();

        //**************//
        $AlunosModel = new AlunosModel();

        //**************//
        $ColaboradoresModel = new ColaboradoresModel();
        $instrutores = $ColaboradoresModel->Listar(" and usu_tipo = 'INSTR' ");
        $dados['instrutores'] = $instrutores;

        //**************//
        $VeiculosModel = new VeiculosModel();
        $veiculos = $VeiculosModel->Listar("TRUE",'',' order by veic_descricao asc');
        $dados['veiculos'] = $veiculos;

        $this->RenderView("agenda/reports",$dados);

    }


    public function RelatorioGerar() {
        $dados = array(
            "dados" => ''
        );
        $ReportModel = new AgendaRelatoriosModel();


        $report = isset($_POST['report']) ? $_POST['report'] : $_GET['report'];
        $ReportModel->SetReport($report);
        $r = $ReportModel->GetReport();

        //$this->RenderView('agenda/reports/' . $r, $dados, 'public/header', '', false);              

        //PARA GERAR EM PDF, USAR O MODELO ABAIXO
        /* */ 
            ob_start();
            $txt = $this->RenderView('agenda/reports/' . $r, $dados, 'public/header', '', false);      
            $txt = ob_get_contents();
            ob_clean();

            $mpdf = new mPDF("BLANK","A4","12px","",5,5,5,5);
            $mpdf->WriteHTML($txt);
            $mpdf->SetTitle($r . " - Report");
            $file =  $r . ".pdf";           
            $mpdf->Output($file,"I");
        /** */        
    }
}
