<?php

class VeiculosController extends Controllers {
    public function __construct() {
        parent::__construct();

        $cfg = new ConfigHelper();
        $x = $cfg->getVars();
        eval($x);
        
    }
    public function index() {
        //$this->listar();
        $dados = array();

        $TabModel = new TabelasAuxModel("veic_marcas");
        $TabModel->tabela_prefixo = "veicmarc";
        $marcas = $TabModel->Listar();

        $TabTipos = new TabelasAuxModel("veic_tipos");
        $TabTipos->tabela_prefixo = "veictipo";
        $tipos = $TabTipos->Listar();
        
        $dados['marcas'] = $marcas;
        $dados['tipos']  = $tipos;
        $dados['locais']  = $locais;


        $this->RenderView('veiculos/listar',$dados);
    }

    public function listar() {
        $this->ValidaNivel(40);
        $VEICULOS = new VeiculosModel();
        $dados = array();
        


        ############################################################################################
            $condicao = " TRUE ";

            if(isset($_POST['filtro'])) {

                $placa      = isset($_POST['placa'])     ? $_POST['placa']      : '';
                $descricao  = isset($_POST['descricao']) ? $_POST['descricao']  : '';
                $marca      = isset($_POST['marca'])     ? $_POST['marca']      : '';
                $tipo       = isset($_POST['tipo'])      ? $_POST['tipo']       : '';
                $limite     = isset($_POST['limite'])    ? $_POST['limite']     : '';

                if($placa != '') {
                    $condicao .= " AND veic_placa = '$placa' "; }

                if($descricao != '') {
                    $condicao .= " AND veic_descricao = '$descricao' "; }
                if($marca != '') {
                    $condicao .= " AND veic_marca = '$marca' "; }

                if($tipo != '') {
                    $condicao .= " AND veic_tipo = '$tipo' "; }

                #if($limite != '') {
                #    $condicao .= " AND veic_limite = '$limite' "; }                

            }

            $TODOS_VEICULOS = $VEICULOS->Listar($condicao, " LIMIT " . $limite);
            $dados["lista_dos_veiculos"] = $TODOS_VEICULOS;
        ############################################################################################
        
        
        $this->RenderView('veiculos/lista-json',$dados,false,false,false);
    }

    public function ficha() {
        $this->ValidaNivel(40);
        $this->cadastrar();    
    }

    public function cadastrar() {
        $this->ValidaNivel(50);
        $dados = array();
        $dados['ficha'] = false;

        /*****************************************************************************************
         * INSERT OU UPDATE
        /***************************************************************************************** */
        if($_POST) {

            $id = $_POST['id'];

            $CamposIgnora = array("id");

            $CamposObrigatorios = array(
                "marca"     => "marca do veiculo!",
                "modelo"    => "",
                "descricao" => "",
                "tipo"      => "",
                "anofab"    => "",
                "anomod"    => "",
                "comb"      => "",
                "mesipva"   => "",
                "meslicenc" => ""
            );            


            foreach ($_POST as $campo => $valor) {

                //VALIDANDO CAMPOS obrigatorios
                foreach ($CamposObrigatorios as $obg => $descri) {
                    if($campo == $obg && $valor == "") {
                        if($descri == "") {
                            $descri = $obg;
                        }
                        $this->SetErro('Informe ' . $descri);
                        $this->GetErro();                        
                    }
                }
                    
                //ignorando campos
                if(in_array($campo, $CamposIgnora) ) {
                    unset($_POST[$campo]);
                }

            }


            if($id == "") {
                $this->NovoVeiculo($_POST);
            } else {
                $this->EditarVeiculo($id, $_POST);
            }
            
            return;
        }


        /*****************************************************************************************
         * ABRE FICHA PARA CADASTRO OU EDICAO
        /***************************************************************************************** */

        $dados['veiculo'] = true;

        $TabModel = new TabelasAuxModel("veic_marcas");
        $TabModel->tabela_prefixo = "veicmarc";
        $marcas = $TabModel->Listar();

        $TabTipos = new TabelasAuxModel("veic_tipos");
        $TabTipos->tabela_prefixo = "veictipo";
        $tipos = $TabTipos->Listar();

        $TabComb = new TabelasAuxModel("veic_comb");
        $TabComb->tabela_prefixo = "veiccomb";
        $combs = $TabComb->Listar();

        $ResponsModel = new ColaboradoresModel();
        $Respons = $ResponsModel->Listar(" AND USU_TIPO = 'INSTR' ");
        $dados['respons']  = $Respons;        

        //SE FOR UMA EDIÇÃO SELECIONA O VEICULO
        if(isset($_GET['cod'])) {
            $VeiculosModel = new VeiculosModel();            
            $veiculo      = $VeiculosModel->Seleciona($_GET['cod']);

            $dados['veiculo'] = $veiculo;
            $dados['ficha'] = true;
        }


        $dados['marcas'] = $marcas;
        $dados['combs']  = $combs;
        $dados['tipos']  = $tipos;
        


        $this->RenderView('veiculos/cadastrar',$dados);
    }


    public function EditarVeiculo($id, $campos) {
        $this->ValidaNivel(50);
        $dados = array();

        $VeiculosModel = new VeiculosModel();
        $update        = $VeiculosModel->Update($id, $campos);

        if($update) {
            $ret['erro'] = "";
            $ret['mensagem'] = "Veículo editado com Sucesso!";

            echo json_encode($ret);
            return;
        }
    }

    public function NovoVeiculo($campos) {
        $this->ValidaNivel(50);
        $dados = array();

        $VeiculosModel = new VeiculosModel();
        $cadastro = $VeiculosModel->Cadastrar($campos);

        if($cadastro) {
            $ret['erro'] = "";
            $ret['id'] = $cadastro->insert_id;
            $ret['mensagem'] = "Veículo Cadastrado com Sucesso!";

            echo json_encode($ret);
            return;
        }
    }


    public function Despesas() {
        $this->ValidaNivel(50);
        $id = $_GET['cod'];
        if($id == "") { exit('ERRO. LINHA ' . __LINE__); }

        $dados = array();

        $TabTipos = new TabelasAuxModel("veic_despesas");
        $TabTipos->tabela_prefixo = "veicdesp";
        $tipos = $TabTipos->Listar();
        $dados["tiposDesp"] = $tipos;


        $VeiculosModel = new VeiculosModel();     
        $VEICULO = $VeiculosModel->Seleciona($id);
        $dados['veiculo'] = $VEICULO;


        $ResponsModel = new ColaboradoresModel();
        $Respons = $ResponsModel->Listar(" AND USU_TIPO = 'INSTR' ");
        $dados['respons']  = $Respons;        
        
        
        ####################################
        ## CENTRO DE CUSTO
           $CEC = New CentrodecustoModel();
           $CEC = $CEC->Listar();
           $dados['CentroDeCustos']  = $CEC;  

        ####################################
        ## PLANO DE CONTAS
           $PLC = New PlanodecontasModel();
           $PLC = $PLC->Listar();
           $dados['PlanoDeContas']  = $PLC;  


        $teste = new VeiculosCtrModel();

        $this->RenderView('veiculos/despesas',$dados);
    }


    public function DespesasLista() {
        $this->ValidaNivel(50);

        $DESPESAS = new TabelasAuxModel("veic_veicxdespesas");
        $DESPESAS->tabela_prefixo ="veicxdesp";

        $condicao = " ";
        $limite   = "10";

        if(isset($_POST['filtro'])) {

            $dataIni = $_POST['dataIni'];
            $dataFim = $_POST['dataFim'];
            $tipo    = $_POST['tipo'];
            $limite  = $_POST['limite'];
            $veiculo = $_POST['veiculo'];


            if($dataIni != '') {
                $condicao .= " AND date(veicxdesp_data) >= '".DataDB($dataIni)."'"; }

            if($dataFim != '') {
                $condicao .= " AND date(veicxdesp_data) <= '".DataDB($dataFim)."'"; }

            if($tipo != '') {
                $condicao .= " AND veicxdesp_despcod = '$tipo' "; }

            if($veiculo != '') {
                $condicao .= " AND veicxdesp_veicid= '$veiculo' "; }

        }
        $DESPESAS->join("veic_despesas","veicdesp_cod = veicxdesp_despcod");
        $DespesasLista = $DESPESAS->Listar($condicao, " LIMIT $limite ");
        $dados['DespesasLista'] = $DespesasLista;




        $this->RenderView('veiculos/despesas-lista-json',$dados,false,false,false);

    }


    public function DespesasAdicionar() {
        $id = $_POST['cod'];
        if($id == "") {exit("ERRO LINHA: INFORME CODIGO " . __LINE__);}

        $this->ValidaNivel(50);

        $valores['veicxdesp_veicid']   = $id;
        $valores['veicxdesp_respon']   = $_POST['respon'];

        $valores['veicxdesp_pconta']   = $_POST['pconta'];
        $valores['veicxdesp_ccusto']   = $_POST['ccusto'];
        if(isset($_POST['lancafinan'])) {
            $valores['lancafinan']         = $_POST['lancafinan'];
        } else {
            $valores['lancafinan']         = false;    
        }
        

        $valores['veicxdesp_km']       = $_POST['kmatual'];
        $valores['veicxdesp_despcod']  = $_POST['tipo'];
        $valores['veicxdesp_valor']    = formataMoedaDB($_POST['valor']);
        $valores['veicxdesp_data']     = date("Y-m-d");
        $valores['veicxdesp_numrec']   = $_POST['recibo'];
        if(isset($_POST['data'])) {
            if($_POST['data'] != '') {
                $valores['veicxdesp_data'] = DataDB($_POST['data']);
            }
        }
        
        $valores['veicxdesp_descri'] = $_POST['descri'];


        $CamposObrigatorios = array(
            "veicxdesp_veicid"     => "Código do Veículo",
            "veicxdesp_despcod"    => "Tipo de despesa ",
            "veicxdesp_valor"      => "Valor",
            "veicxdesp_data"       => "Data",
            "veicxdesp_km"         => "KM atual",
            "veicxdesp_respon"     => "Responsável",
        );            

        foreach ($valores as $campo => $valor) {
            foreach ($CamposObrigatorios as $obg => $descri) {
                if($campo == $obg && $valor == "") {
                    if($descri == "") {
                        $descri = $obg;
                    }
                    $this->SetErro('Informe ' . $descri);
                    $this->GetErro();                        
                }
            }            
        }

        

        $VeiculosModel = new VeiculosModel();
        $cadastro = $VeiculosModel->InsertDespesas($valores);
        
        if($cadastro) {
            $ret['erro'] = "";
            $ret['id'] = $cadastro->insert_id;
            $ret['mensagem'] = "Concluído";

            echo json_encode($ret);
            return;
        }        

    }


    public function Relatorio() {
        $this->ValidaNivel(40);
        $dados = array();


        ## REPORTS
        $reports = new Reports();
        $reports = $reports->g("VEICULOS");      
        $dados['reports'] = $reports;

        ##MARCAS
        $TabModel = new TabelasAuxModel("veic_marcas");
        $TabModel->tabela_prefixo = "veicmarc";
        $marcas = $TabModel->Listar();
        $dados['marcas'] = $marcas;

        ##TIPOS
        $TabTipos = new TabelasAuxModel("veic_tipos");
        $TabTipos->tabela_prefixo = "veictipo";
        $tipos = $TabTipos->Listar();
        $dados['tipos'] = $tipos;

        ##COMBUSTIVEL
        $TabComb = new TabelasAuxModel("veic_comb");
        $TabComb->tabela_prefixo = "veiccomb";
        $combs = $TabComb->Listar();
        $dados['combs'] = $combs;

        ##VEICULOS
        $veiculos = new VeiculosModel();
        $veiculos = $veiculos->Listar("TRUE");
        $dados['veiculos'] = $veiculos;

        ##RESPONS
        $ResponsModel = new ColaboradoresModel();
        $Respons = $ResponsModel->Listar(" AND USU_TIPO = 'INSTR' ");
        $dados['respons']  = $Respons;   


        ####################################
        ## CENTRO DE CUSTO
           $CEC = New CentrodecustoModel();
           $CEC = $CEC->Listar();
           $dados['CentroDeCustos']  = $CEC;         

        ####################################
        ## PLANO DE CONTAS
           $PLC = New PlanodecontasModel();
           $PLC = $PLC->Listar();
           $dados['PlanoDeContas']  = $PLC; 

        $this->RenderView('veiculos/reports', $dados);
    }

    public function RelatorioGerar() {
            
            $report = new Reports();
            $report->SetReport($_POST['report']);
            $report->SetModel("VeiculosRelatoriosModel");
            $report->SetDados($_POST);
            
            $dados['dados'] = $report->Gerar();
            //$this->RenderView($report->GetReport(), $dados, "public/header", "public/footer", false);
            $report->Render();
            /*
            ob_start();
            $txt = $this->RenderView('alunos/reports/' . $r, $dados, 'public/header', '', false);      
            $txt = ob_get_contents();
            ob_clean();

            $mpdf = new mPDF("BLANK","A4","12px","",5,5,5,5);
            $mpdf->WriteHTML($txt);
            $mpdf->Output();        
            */

    }    



    public function Marcas() {
        require_once "Core/GridPHP.php";

            //$g = new jqgrid($db_conf); 

        

            $grid["rowNum"] = 10; // by default 20 
            $grid["sortname"] = 'veicmarc_id'; // by default sort grid by this field 
            $grid["autowidth"] = true; // expand grid to screen width 
            $grid["multiselect"] = true; // allow you to multi-select through checkboxes 
            $grid["form"]["position"] = "center"; 
            //$grid["add_options"]["jqModal"] = true;
            $grid["caption"] = "Veiculos - Marcas"; 
            //$grid["height"] = "100%";
            $g->set_options($grid); 


            // you can provide custom SQL query to display data 
            #$g->select_command = "SELECT i.id, invdate , c.name, 
            #                        i.note, i.total, i.closed FROM invheader i 
            #                        INNER JOIN clients c ON c.client_id = i.client_id"; 

            #// you can provide custom SQL count query to display data 
            #$g->select_count = "SELECT count(*) as c FROM invheader i 
            #                        INNER JOIN clients c ON c.client_id = i.client_id"; 

            $g->table = "veic_marcas"; 

            $col = array(); 
            $col["title"] = "Id"; // caption of column 
            $col["name"] = "veicmarc_id"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["width"] = "40"; 
            $col["editable"] = false; 
            $col["hidden"] = false; 
            $cols[] = $col; 

            $col = array(); 
            $col["title"] = "Código"; // caption of column 
            $col["name"] = "veicmarc_cod"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["width"] = "80"; 
            $col["editable"] = true; 
            $col["hidden"] = false; 
            $cols[] = $col;  

            $col = array(); 
            $col["title"] = "Descrição"; // caption of column 
            $col["name"] = "veicmarc_descri"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["width"] = "200"; 
            $col["editable"] = true; 
            $col["hidden"] = false; 
            $cols[] = $col;            


            $col = array(); 
            $col["title"] = "cancelado"; // caption of column 
            $col["name"] = "cancelado"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["width"] = "20"; 
            $col["editable"] = true; 
            $col["hidden"] = false; 
            $col["edittype"] = "checkbox"; 
            $col["default"] = "<div style='background-color:navy; height:14px'>12</div>";
            $col["editoptions"] = array("value"=>"*:");             
            $cols[] = $col;                            

            $f = array();
            $f["column"] = "cancelado";
            $f["op"] = "eq";
            $f["value"] = "*";
            $f["css"] = "'background-color':'red', 'color':'black'";
            $f_conditions[] = $f;
            //$g->set_conditional_css($f_conditions);



            $col = array(); 
            $col["title"] = "Local"; // caption of column 
            $col["name"] = "veicmarc_localid"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["editable"] = true; 
            $col["hidden"] = true;
            $col["editoptions"] = array("defaultValue"=>"99","style"=>"border:0");            
            $col["width"] = "0"; 
            #$col["formatter"] = "date";
            #$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y');
            $cols[] = $col;       
  





            $g->set_columns($cols); 

            $out = $g->render("list1"); 
            $dados['out'] = $out;

        $this->RenderView('grid_generica2', $dados);
        
    }
}
