<?php

class FinanceiroController extends Controllers {
    
    /**
     * 
     * \lista os tipos de documentos
     */
    public function index() {
        $this->ValidaNivel(40);
        $parcelas = "";
        $FinanceiroModel = new FinanceiroModel();
//        $parcelas = $FinanceiroModel->ListaParcelas("",1);   
        $parcelas = array();

        if(isset($_POST['lista']) && $_POST['lista'] == 'listagem') {
               $dados = null;

               foreach ($_POST as $key => $value) {
                   $dados[$key] = $value;
               }

               $parcelas = $FinanceiroModel->ListaParcelas($dados);
        }

        ###################################
        ## TIPO DE DOCUMENTOS
           $TpDocs = new TipoDocumentosModel();
           $TpDocs = $TpDocs->Listar();

        

        $dados = array(
            "TpDocs" => $TpDocs,
            "parcelas" => $parcelas
        );

        $this->RenderView('financeiro/financeiro', $dados);
       
    }

    /**
    * Carrega lista do financeiro após Action Index e usuário enviar o form do filtro!
    */
    public function CarregaLista() {

    }
    

    /**
    * Função de Baixar Parcelas
    */
    public function BaixaParcela() {  
        $this->ValidaNivel(40);
        $baixa = new FinanceiroModel();

        
        
            $id_parcela = $_POST['parc-id'];
            $valorbx    = $_POST['baixa-valor'];
            $data_baixa = $_POST['baixa-data'];
        
        
        $valorbx = str_replace(".", "", $valorbx);
        $valorbx = str_replace(",", ".", $valorbx);
        if($valorbx == "" || $valorbx <= 0 ){
            echo "Valor dever ser maior que zero!!!";
            return;
        }
        
        
        if($data_baixa == '') {
            echo "Necessário preencher a data de baixa!!!";
            return;
        }
        
        $data_baixa = DataDB($data_baixa);
        $usuariobx  = $_SESSION['APP_USUID'];
        if(!isset($_POST['tipo-documento'])) {
            echo "Informe tipo de documento";
            return;
        }
            $tpdocbx        = $_POST['tipo-documento'];
            
        if(!isset($_POST['observacoes'])) {
            $observacoes    = "";
        } else {
            $observacoes    = $_POST['observacoes'];
        }
        
        $tprecpag   = 'CAIXA';


        $baixa->BaixaParcela($id_parcela, $valorbx, $data_baixa, $usuariobx, $tprecpag, $tpdocbx,$observacoes);
    }


    public function EstornaParcela() {  
        if(!$this->ValidaNivel2(40)) 
        {
            echo "sem permissao";
            exit();
        }
        $baixa = new FinanceiroModel();
        $id_parcela = $_POST['parc-id'];
        $usuariobx  = $_SESSION['APP_USUID'];
        $tprecpag   = 'CAIXA';
        

        $baixa->EstornaParcela($id_parcela,$usuariobx);
    }


    public function CriaMovimento() {  
        $this->ValidaNivel(50);
        $Mov = new FinanceiroModel();
        
        $valor = $_POST['valor'];
        $vencimento = $_POST['vencimento'];
        $tipo = $_POST['tipo'];
        $descricao  = $_POST['descri'];
        $tprecpaga = $_POST['tprecpaga'];
        $idaluno = $_POST['aluno'];

        if($valor <= 0 || $valor == "") {
            echo "Valor inválido";
            return;
        }

        if($tprecpaga == "") {
            echo "Informe o local da movimentação.";
            return;
        }        

        if($vencimento == "") {
            echo "Informe o vencimento";
            return;
        }

        if($tipo == "") {
            echo "Informe o tipo";
            return;
        }

        if($descricao == "") {
            echo "Informe a descrição";
            return;
        }
        $movtipo  = "FINANCEIRO";
        $deonde   = "FINAN";
        $tprecpag   = $tprecpaga;
        $deondeid = NULL;
        $idaluno  = $idaluno;
        $retx = $Mov->CriaMovimento($idaluno,$valor,$vencimento,$tipo,$descricao,$movtipo,$deonde,$deondeid,TRUE,$tprecpag);

        $LASTID = $retx;

        $Mov->SalvaLog($LASTID,"INS","Criação de Movimento. </br> Tipo:$tipo Valor:$valor ");        
    }
    
    public function BaixaParcelaLote($id_parcela) {
        
    }



    //Carrega dados da parcela, para edição
    public function CarregaDadosParcela() {
        $id_parcela = (isset($_GET['n']) ) ? $_GET['n'] : '0';

        if($id_parcela == '' || $id_parcela == "0") {
            exit('parcela invalida');
        }

        $FinanceiroModel = new FinanceiroModel();
        $parcela = $FinanceiroModel->SelecionaParcela($id_parcela);

        $TpDocs = new TipoDocumentosModel();
        $TpDocs = $TpDocs->Listar();

        $cont = "";

        $cont .= '<input type="hidden" name="parc-id" value="'.$parcela['finmov_id'].'" id="edt-parc-id"/>';
        $cont .= '<input type="hidden" name="loc-id"  value="'.$parcela['finmov_localid'].'" id="edt-mat-id"/>';
        

        $cont .= "<div class='row'>";

            $cont .= "<div class='col-sm-4'>";
            $cont .= "<label>Vencimento</label>";
            $cont .= "<input type='text' class='form-control ' onkeyup='formataData(this)' name='dtVenc' value='".DataBR($parcela['finmov_dtvenc'])."' /> ";
            $cont .= "</div>";

            $cont .= "<div class='col-sm-4'>";
            $cont .= "<label>Valor</label>";
            $cont .= "<input type='text' class='form-control vlr' name='vlr' value='".$parcela['finmov_valor']."' onkeyup='formataValor(this)'/> ";
            $cont .= "</div>";

            $cont .= "<div class='col-sm-4'>";
            $cont .= "<label>Tipo Doc</label>";
            $cont .= "<select class='form-control' name='tpDoc'>";
            foreach ($TpDocs as $docum) {
                $selected = "";
                if($docum['tpd_cd'] == $parcela['finmov_tpdocumbx'] ) {
                    $selected = "selected='selected'";
                }
                $cont .= "<option value='".$docum['tpd_cd']."' $selected >".$docum['tpd_descricao']."</option>";
            }
            //$cont .= "<input type='text' class='form-control' value='".$parcela['finmov_tpdocumbx']."' /> ";
            $cont .= "</select>";
            $cont .= "</div>";
        $cont .= "</div>";

        $cont .= "<div class='row'>";
            $cont .= "<div class='col-xs-12'>";
            $cont .= "<label>Descricao</label>";
            $cont .= "<input type='text' name='descri' class='form-control' value='".$parcela['finmov_descricao']."' />";
            $cont .= "</div>";
        $cont .= "</div>";

        echo $cont;
    }
    /**
     * Editar Parcela
     **/
    public function EditarParcela() {
        
        if(!$this->ValidaNivel2(40)) 
        {
            echo "SEM PERMISSÃO [50]";
            exit();
        }
        
        if(!isset($_POST['parc-id'])) { exit("PARCELA não informada </ br>ERROR 1711-170323"); }
        if(!isset($_POST['loc-id']))  { exit("LOCAL não informada </ br>ERROR 1712-170323"); }
        if(!isset($_POST['dtVenc']))  { exit("VENCIMENTO não informada </ br>ERROR 1713-170323"); }
        if(!isset($_POST['vlr']))  { exit("VALOR não informada </ br>ERROR 1714-170323"); }
        if(!isset($_POST['tpDoc']))  { exit("TIPO_DOC não informada </ br>ERROR 1715-170323"); }
        if(!isset($_POST['descri']))  { exit("DESCRIÇÃO não informada </ br>ERROR 1715-170323"); }

        $IDPARC = $_POST['parc-id'];
        $DTVENC = DataDB($_POST['dtVenc']);
        $VALOR  = $_POST['vlr'];
        $TPDOC  = $_POST['tpDoc'];
        $DESCR  = $_POST['descri'];


        $Mov = new FinanceiroModel();
        $Editar = $Mov->EditarMovimento($IDPARC,$DTVENC,$VALOR,$TPDOC,$DESCR);
        if($Editar) {
            $ret['mensagem'] = "SUCESSO!";
        } else {
            $ret['erro'] = "1";
            $ret['mensagem'] = $Editar['mensagem'];
        }

        echo json_encode($ret);


    }

    /**
     * Cancelamento de parcelas
     */
    public function CancelarParcela() {
        if(!$this->ValidaNivel2(40)) 
        {
            echo "SEM PERMISSÃO [50]";
            exit();
        }

        $PARCELA = (!isset($_GET['n']) ? "0" : $_GET['n']);

        if($PARCELA <= 0 || $PARCELA == "") {
            echo "ERRO, parcela invalida para cancelamento/recuperação!";
            exit();
        }

        $Mov = new FinanceiroModel();
        $Cancelamento = $Mov->MarcaCancelado($PARCELA);
        if($Cancelamento) {
            $ret['mensagem'] = "SUCESSO!";
        } else {
            $ret['erro'] = "1";
            $ret['mensagem'] = $Cancelamento['mensagem'];
        }

        echo json_encode($ret);
    }

    public function Reports() {
        $this->ValidaNivel(50);
        
        ###################################
        ## TIPO DE DOCUMENTOS
           $TpDocs = new TipoDocumentosModel();
           $TpDocs = $TpDocs->Listar();
        
        ####################################
        ## USUARIOS
           $Usuarios = New ColaboradoresModel();
           $Usuarios = $Usuarios->Listar( "AND usu_sitacesso = 'A' ");


        $dados = array(
            "TipoDocumentos" => $TpDocs,
            "Usuarios"       => $Usuarios
        );
        
        $this->RenderView('financeiro/filtroreports',$dados);
    }
    
    public function ReportsGerar() {
        $this->ValidaNivel(50);
        $report = new FinanceiroRelatoriosModel(); 
        $report->SetReport($_POST['report']);
        $r = $report->GetReport();
        
        //$dados_report = $report->Gerar();
        
        $dados = array(
            "dados" => ''
        );
        
        $this->RenderView('financeiro/reports/'.$r,$dados,'public/header','',false);
    }
}
