<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author andrestolf
 */
class LoginController extends Controllers {
    
    public function index() {

        $datas = array(
            "sidebar"    => false,
            "header"     => false,
            "footer"     => false,
            "viewDatas"  => array(
                "id" => 1
            )
        );

        $this->RenderView("login",$datas,false,false,false);
    }
    
    public function Acessar() {
        global $currentEmpresaCod;



        $currentEmpresaCod;
        
        $_Empresa = new EmpresaHelper();
        $EMPRESA = $_Empresa->ValidaCodigo($currentEmpresaCod);

        #echo "abx";
        #exit();
        if(!$EMPRESA){
            exit("EMPRESA INVÁLIDA!");
        }


        if($EMPRESA->CONT_VALIDADE != '' || $EMPRESA->CONT_VALIDADE  != NULL) {
            if($EMPRESA->CONT_VALIDADE < date("Y-m-d G:i:s") ){
                $_SESSION['APP_USUNOME'] = ""; 
                $this->RenderView("validade","","public/header",false,false);
                exit();
            }
        }
        if($EMPRESA->CONT_BLOQUEADO != NULL || $EMPRESA->CONT_BLOQUEADO != ""){
            $_SESSION['APP_USUNOME'] = ""; 
            $this->RenderView("bloqueio","","public/header",false,false);
            exit();
        }
        
        $this->SetIDContrato($EMPRESA->CONT_LOCALID);
    
        $USUARIO = "789**()((Lç";
        if(isset($_POST['login']))
            $USUARIO = $_POST['login'];


        $SENHA = "789***@123##";
        if(isset($_POST['password']))
            $SENHA   = $_POST['password'];
        
        $login = new Login();
        $login->LogarUsuario($USUARIO,$SENHA,$this->GetIDContrato());
    }
    
    public function end() {
        session_destroy();
        $redir = new RedirectHelper();
        $redir->Redir("alunos");
    }    
    
    
}
