<?php

class DebitosController extends Controllers {

	public function index()
	{
		$dados = array();

		$ServicosModel = new ServicosModel();

		$Servicos = $ServicosModel->SelecionaItens();
		$dados['fases'] = $Servicos;

		            ## aqui tb mudou....
            $TpDocs = new CrudModel();
            $TpDocs->table      = "sis_tpdocum";
            $TpDocs->campoid    = "tpd_id";
            $TpDocs->campolocal = "tpd_localid";
            $TpDocs->campoorder = "tpd_ordem";
            
            $TpDocs = $TpDocs->Listar(); 
            $dados['TpDocs'] = $TpDocs;


		$this->RenderView('debitos/filtro', $dados);
	}


	public function post($name) {
		if(isset($_POST[$name])) {
			return $_POST[$name];
		}	

		return false;
	}

	public function Analise()
	{
		$matricula 		= $this->post('matricula');
		$cpf 	   		= $this->post('cpf');

		$matIni 		= $this->post('matIni');
		$matFim 		= $this->post('matFim');
		$VencIni 		= $this->post('VencIni');
		$VencFim 		= $this->post('VencFim');
		
		$fase 			= $this->post('fase');
		if($fase)
			$fase = implode(",",$fase);

		$faseCond 		= $this->post('faseCond');
		$situacao 		= $this->post('situacao');
		$situacaoCond 	= $this->post('situacaoCond');
		$numdebitos 	= $this->post('numdebitos');
		$numdebitosCond = $this->post('numdebitosCond');
		
		$tpDocs 		= $this->post('tpDocs');
		if($tpDocs)
			$tpDocs = "'".implode("','",$tpDocs)."'";



		echo $tpDocs;

	}

}