<?php

class GridController extends Controllers {

	public function index()
	{

		$grid = new AsGrid();
		$grid->init();
		$grid->_set_table("veic_marcas");
		$grid->_set_primary_key("veicmarc_id");
		$grid->where = " (veicmarc_localid = 1 or veicmarc_localid = 0)";
		$grid->_onDelete("UPDATE veic_marcas SET can_data = current_timestamp() where veicmarc_id = @idDelete  and veicmarc_localid = 0 ");


		$grid->actions = array("editar","add","remover");
		$o['rowNum'] = 10; //20 por padrão
		$o['caption'] = "Grid Teste";
		$grid->setOptions($o);




		$col = array();
		$col['title'] = "ID";
		$col['name'] = "veicmarc_id";
		$col['width'] = "100";
		$col['formatter'] = "date";
		$col['hidden'] = false;
		$col["editable"] = false;
		$col['class'] = "col-md-1";
		
		$cols[] = $col;

		$col = array();
		$col['title'] = "Código";
		$col['name'] = "veicmarc_cod";
		$col['width'] = "120";
		$col['formatter'] = "text";
		$col['class'] = "col-md-3";
		$col['hidden'] = false;
		$cols[] = $col;



		$col = array();
		$col['title'] = "Descrição";
		$col['name'] = "veicmarc_descri";
		$col['formatter'] = "text";
		$col['width'] = "350";
		$col['style'] = "";
		$col['hidden'] = false;
		#$col['formatterOptions'] = array("srcformat" => 'Y-m-d');
		$cols[] = $col;



		$col = array();
		$col['title'] = "Cadastro";
		$col['name'] = "cad_data";
        $col["editable"] = true; 
        $col['formatterOptions'] = array("srcformat" => 'Y-m-d');
        $col['editOptions'] = array("value"=>99,"readonly"=>"readonly");
        $col["hidden"] = false;
        $cols[] = $col;


		$grid->setCols($cols);
		$out = $grid->render("grid1");

		$dados['out'] = $out;
		
		/*
		echo '<link rel="stylesheet" href="/Public/plugins/alertify/css/themes/bootstrap.css">
			  <script src="/Public/plugins/jQuery/jquery-2.2.3.min.js"></script>
			  <link rel="stylesheet" href="/Public/plugins/bootstrap/css/bootstrap.min.css">
			  <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script> 
		';
		echo $out; */

		$this->RenderView('grid_generica',$dados);
	}




}

