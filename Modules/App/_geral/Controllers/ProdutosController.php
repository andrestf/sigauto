<?php

class ProdutosController extends Controllers {

    public function index() {
        $this->listar();
    }
    
    public function listar() {
        $this->ValidaNivel(50);
        $PRODUTOS = new ProdutosModel();
        
        $TODOS_PRODUTOS = $PRODUTOS->Listar();
        
        $dados = array(
            "lista_dos_produtos" => $TODOS_PRODUTOS,
        );
        
        $this->RenderView('produtos/listar',$dados);
        
        
    }
    
    public function cadastrar() {
        
    }
}
