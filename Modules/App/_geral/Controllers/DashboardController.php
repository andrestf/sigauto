<?php

class DashboardController extends Controllers {

    public $oLOCAIS;
    public function index() {
    	/*
    	$model = new CrudModel();

    	############################################
    	$model->table 	   = "sis_tpmatriculas";
    	$model->campolocal = "tpmat_localid";
    	$model->campoid    = "tpmat_id";
    	$model->campoorder = "tpmat_descricao";
    	#$matriculas        = $model->Listar();

    	############################################
    	$model->table 	   = "sis_usuarios";
    	$model->campolocal = "usu_localid";
    	$model->campoid    = "usu_id";
    	$model->campoorder = "usu_nomecompleto";
    	#$model->Join(" sis_alunosmat " , "amat_alunoid = usu_id ");
    	#$model->Join(" sis_alunosmatfases " , "amf_alunoid = usu_id ");
    	$alunos = $model->Listar();

    	*/
        #############################################
        $dados = array();
        $grafs = new DashboardModel();



        if($this->ValidaNivel2(100)) {
            $oLOCAIS = array( 
                "1" => "Alvorada",
                "2" => "Alternativa",
                "3" => "Central",
                "4" => "Major Matheus",
                "5" => "Futura",
                "6" => "Fórmula 1",
                "7" => "Matriz",
                "13" => "BotuTransito"
            );

        } else {
            $oLOCAIS = array(
                $_SESSION['APP_LOCALID'] => $_SESSION['APP_LOCALNOME']
            );

        }
        $this->oLOCAIS = $oLOCAIS;
        $dados['oLOCAIS'] = $oLOCAIS;
        #############################################
        #############################################

        $oSERVICOS = $grafs->GetServicos();
        $dados['oSERVICOS'] = $oSERVICOS;
        
        $aSERVICOS = "";
        $servicos  = "";
        if(isset($_POST['tpservicos'])) {
            $servicos  = $_POST['tpservicos'];
            $aSERVICOS = $_POST['tpservicos'];
        }
        $dados['aSERVICOS'] = ($aSERVICOS == '') ? array() : $aSERVICOS;        


        #############################################
        #############################################

        $aLocais = "";
        $locais  = "";
        if(isset($_POST['local'])) {
            $locais = $_POST['local'];
            $aLocais = $_POST['local'];
        }
        $dados['aLocais'] = ($aLocais == '') ? array() : $aLocais;
        $locais = $grafs->SetLocais($aLocais,"usu");

        $dIni = $grafs->SetdIni("01/".date("m/Y"));
        if(isset($_POST['dIni']))
            $dIni = $grafs->SetdIni($_POST['dIni']);
        $dados['dIni'] = DataBR($dIni);

        $dFim = $grafs->SetdFim(date("d/m/Y"));
        if(isset($_POST['dFim']))
            $dFim = $grafs->SetdFim($_POST['dFim']); 
        $dados['dFim'] = DataBR($dFim);


        ####################################
        $dados['alunosTotal'] = $grafs->Cadastros();
        ####################################

        $grafs->SetLocais($aLocais,"amat");
        $grafs->SetTpServicos($aSERVICOS,'amat_servico');

        $dados['alunosTotalMatriculaC'] = $grafs->Matriculas(" and amat_situacao = 'CONCLUIDO' ");
        $dados['alunosTotalMatriculaI'] = $grafs->Matriculas(" and amat_situacao = 'INICIADA' ");
        
        $MatsIni = $grafs->Servicos(" and amat_situacao = 'INICIADA' ");
        $MatsCon = $grafs->Servicos(" and amat_situacao = 'CONCLUIDO' ");


        ###################################################################################
        ## Matriculas Iniciadas
        ###################################################################################
            $servicos = array();
            foreach ($MatsIni as $key => $value) {
                $servicos[$value['serv_id']] = $value['serv_descricao'];
            }
            $servicos = array_unique($servicos);
            @$dados['CategoriasInic'] = $servicos;
            @$dados['DadosInic']      = $MatsIni;


            $graf = "";
            foreach ($servicos as $key => $desc) {
                $tot = 0;
                foreach ($MatsIni as $mats => $mat) {
                    if($mat['serv_id'] == $key)  {
                        $tot = $tot+1;
                    }

                }
                //$graf .= "['$desc - Cód($key)'  , ".$tot."], \n";
                $graf .= "['$desc'  , ".$tot."], \n";
            }

            $dados['grafMatInic'] = $graf;

        ###################################################################################
        ## Matriculas Concluidas
        ###################################################################################
            $servicos = array();
            foreach ($MatsCon as $key => $value) {
                $servicos[$value['serv_id']] = $value['serv_descricao']; }

            $servicos = array_unique($servicos);
            @$dados['CategoriasConc'] = $servicos;
            @$dados['DadosConc']      = $MatsCon;        


            $graf = "";
            foreach ($servicos as $key => $desc) {
                $tot = 0;
                foreach ($MatsCon as $mats => $mat) {
                    if($mat['serv_id'] == $key)  {
                        $tot = $tot+1;
                    }
                }
                //$graf .= "['$desc - Cód($key)'  , ".$tot."], \n";
                $graf .= "['$desc'  , ".$tot."], \n";
            }

            $dados['grafMatConc'] = $graf;        

        

        



        $this->RenderView("dashboard/dashboard",$dados);
    }
}
