
<?php

$report = new FinanceiroRelatoriosModel();
$report->Campos        = " finmov_descricao, finmov_tipo  ";
$report->CondicaoExtra = " and finmov_databaixareal is null ";
$report->OrderBy = " order by finmov_dtvenc, nome_aluno ";
$report->GroupBy = "";

$report->Debug = false;

$dados  = $report->Gerar();

$Funcoes = new UsuariosHelper();

if($dados->num_rows < 1) {
    echo "sem resultado";
    return;
} ?>

<div class='content'>
    <div class='formx'>
        <h3>
            <center>
                <u><?php echo strtoupper($_SESSION['APP_LOCALNOME']);?></u><br/>
                Contas a Receber [Por Vencimento - Analítico ]
            </center>
        </h3>
        <h6>
            <div class="row">
                <div class="col-sm-12">
                    <b>Lçto Inicial: </b><?php echo $_POST['datamov_ini']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Lçto Final: </b><?php echo $_POST['datamov_fim']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Baixa Inicial: </b><?php echo $_POST['databxmov_ini']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Baixa Final: </b><?php echo $_POST['databxmov_fim']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Baixa Real Inicial: </b><?php echo $_POST['databxreal_ini']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Baixa Real Final: </b><?php echo $_POST['databxreal_fim']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Vcto Inicial: </b><?php echo $_POST['venc_ini']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Vcto Final: </b><?php echo $_POST['venc_fim']?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Usuário:</b> <?php
                    if (isset($_POST['usubaixa'])) {
                        foreach ($_POST['usubaixa'] as $usuXX) {
                            echo $Funcoes->fRetCampo('sis_usuarios', 'usu_apelido', "usu_id = '$usuXX'") . ", ";
                        };
                    } ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Documento:</b>
                    <?php
                    if (isset($_POST['tipodocumento'])) {
                        foreach ($_POST['tipodocumento'] as $tdXX) {
                            echo $Funcoes->fRetCampo('sis_tpdocum', 'tpd_descricao', "tpd_cd = '$tdXX'") . ", ";
                        };
                    } ?>
                </div>
            </div>
            <hr/>

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th width='100'>Vencimento</th>
                    <th width="70">Código</th>
                    <th width="350">Aluno</th>
                    <th width="50">Renach</th>
                    <th width="150">Celular</th>
                    <th width="200">Descrição</th>
                    <th width='110'>Valor</th>
                </tr>
                </thead>

                <tbody>
                <?php $valor = 0;
                $i=0;?>
                <?php while ($report = $dados->fetch_assoc()) {
                    $valor = $valor + $report['finmov_valor'];
                    ?>
                    <tr>
                        <td><?php echo DataBR($report['finmov_dtvenc']);?></td>
                        <td><?php echo $report['finmov_alunoid'];?> <br/></td>
                        <td><?php echo $report['nome_aluno'];?> <br/></td>
                        <td><?php echo DataBR($report['usu_procvenc']);?></td>
                        <td><?php echo $report['usu_telcelular'];?></td>
                        <td><?php echo substr($report['finmov_descricao'],0,15);?></td>
                        <td>R$ <?php echo number_format($report['finmov_valor'],2,",",".");?></td>
                    </tr>
                    <?php $i++;} ?>
                </tbody>

                <tfoot>
                <tr>
                    <td colspan="6" align="right">
                        <hr/>
                        <b>
                            TOTAL DE REGISTRO = <?php echo $i; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            VALOR TOTAL A RECEBER: R$ <?php echo number_format($valor,2,",","."); ?>
                        </b>
                    </td>
                </tr>
                </tfoot>

            </table>
        </h6>
    </div>
</div>	