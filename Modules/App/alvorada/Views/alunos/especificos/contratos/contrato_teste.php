<div class="content" style="padding: 0px;">
    <div class='box box-primary' style="border: none; padding: 0px;" >
        <div class='box-header'>
            <center>
                <div style="font-size: 24px;">
                    <b>
                    CONTRATO DE PRESTAÇÃO DE SERVIÇOS
                    </b>
                </div>
            </center>
        </div>

        <div class='box-body' style="line-height: 16px; padding: 0px; text-align: justify">
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo $_SESSION['APP_LOCALNOME'] ?></b>, com o nome Fantasia <b><?php echo $_SESSION['APP_NOMEFANTASIA'] ?></b>, CNPJ: <b><?php echo $_SESSION['APP_CPFCNPJ'] ?></b>, com sede na <b><?php echo $_SESSION['APP_LOGRADOURO'] ?></b> – <b><?php echo $_SESSION['APP_CIDADE'] ?></b>/<b><?php echo $_SESSION['APP_ESTADO'] ?></b>, aqui denominada Contratante, tem entre si justo e contratado o que se segue:
                </p>


                <p>
                <center><b><u>OBJETO DO CONTRATO</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 1 – È objeto do presente contrato a prestação de serviço de: Aulas Práticas de Direção em automóvel, motocicleta, microônibus, carreta e o uso de veículos para exames e marcações de exames junto a 6ª Ciretran da Cidade/JJ. Em anexo o formulário dos valores dos cursos e uma nota promissória no valor da matrícula, emitida pelo aluno(a).
                </p>


                <p>
                    <center><b><u>OBRIGAÇÕES DO CONTRATANTE</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Clàusula 2 – O Contratante deverá fornecer a contratada comprovante de residência recente, contratada comprovante de residência recente contratada comprovante de residência recenteempo 6ª Ciretran da Cidade/JJ.
                </p>

            
                <p>
                    <center><b><u>OBRIGAÇÕES DA CONTRATADA</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 3 – A contratada deverá fornecer ao contratante os recibos de pagamentos, bla bla bla bla bla bla bla bla bla bla bla bla.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 4 – A contratada se compromete bla bla bla bla bla bla bla bla bla bla bla Código de Trânsito Brasileiro.
                </p>

            
                <p>
                    <center><b><u>DOS CURSOS DE CARRO, MOTO, ADIÇÃO E MUDANÇA DE CATEGORIAS</u></b></center>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 5 – Os cursos mencionados no caput desta cláusula terão validade de 12 (doze) meses a contar da data do exame médico, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla   bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla     pelo Detran/SP após o prazo deste. Ficando o Contratante obrigado a quitar os débitos pendentes com a Contratada e resgatar a Nota Promissória emitida no inicio do curso.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Parágrafo 1 – Havendo desistência ou transferência do contratante durante os cursos, os valores serão devolvidos pela contratada, deduzidos o equivalente ao valor da matrícula integral que é de R$ 2,00 (Dois Reais) e as aulas práticas de direção que por ventura o aluno tenha assistido, acrescidos das taxas recolhidas para os exames.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Parágrafo 2 – No caso do aluno estar matriculado nas categorias A/B e por ventura vir a desistir de uma dasbla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla, ou seja R$ 1,20 (Um e Vinte Centavos) aulas práticas que o aluno tenha assistido.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 6 – A marcação de aulas será de acordo com a disponibilidade das agendas da Auto Escola, sendo no máximo de 3 (três) aulas por dia, até atingir o número de aulas exigidos pelo Detran/SP, após serão cobradas as aulas extras no valor de R$ 0,60 (Sessenta Centis) cada aula, cancelamento das aulas só será possível salvo se o aluno comunicar a Contratada com o prazo de 72 horas (Setenta e duas Horas) antes da aula marcada. O não comparecimento do aluno no horário marcado, acarretará no custo da remarcação da aula que é de R$ 60,00 (Sessenta reais) e a bla bla bla bla bla bla bla bla bla bla bla bla bla do sistema Biométrico do Cidade/JJ.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; O que possibilitará desta forma a Contratada emitir o Certificado bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla para então agendar o Exame Prático de Direção.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 7 – Mesmo em dias Chuvosos o aluno deverá comparecer as aulas, sendo que a contratada disponibiliza aos seus alunos Capa de Chuva para a realização da mesma, ebla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla blame normalmente.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 8 – Em casos em que haja reprovação nos exames o Contratante deverá agendar um novo exame dentro dos prazos estabelecidos pelo Detran/SP, sendo que em se tratando do Exame teórico o Contratante bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla blao Exame Prático deverá ser agendado junto a Contratada a um custo de R$ 1,10 (Um Real de Reais) com o prazo de 10 (Dez) bla bla bla bla bla bla bla bla bla bla bla.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 9 – O aluno só poderá agendar o exame dbla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 10 – Ao final dos cursos, o Contratante, para retirar a C.N.H, terá que acertarbla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla.<br>
                    &nbsp;&nbsp;&nbsp;&nbsp; Cláusula 11 – Ao Final e sem mais, as partes debla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ble ble ble a qualquer outro por mais privilegiado que seja.
                </p>

        </div>

        <div style="width: 90%; margin: 0 auto; text-align: right">
            <b>BOTUCATU, <?php echo date('d'); ?> DE <?php echo strtoupper(MesExtenso(date('m')))?> DE <?php echo date('Y')?>.</b>
        </div>

        <br><br>
        
        <div class="row">
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    CONTRATANTE <br>
                    <?php echo $Aluno->usu_nomecompleto; ?><br>
                    <?php echo $Aluno->usu_cpf; ?>
                </center>
            </div>
            <div class="col-xs-2"></div>
            <div class="col-xs-5" style="border-top: solid 1px;">
                <center>
                    CONTRATADA <br>
                    <?php echo $_SESSION['APP_LOCALNOME'] ?> - 05.677.097/0001-95
                </center>
            </div>
        </div>
    </div>
</div>
        


<script>
    
    $(function() {
        $(".main-footer").hide();
    });
    
</script>