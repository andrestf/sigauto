
<?php

$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$report->CondicaoExtra = "AND amf_resultado = 'AGENDADO'";
$report->OrderBy = "usu_nomecompleto ASC, serv_descricao ASC";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<table class='table table-bordered'>
	<thead>
		<tr>
			<th>Aluno</th>
			<th>Matricula</th>

			<th>Fase</th>
                        <th>Agendado Para</th>
		</tr>
	</thead>
	<tbody>
		<?php if($dados->num_rows >= 1) { ?>
                    <?php while($report = $dados->fetch_assoc() ) {?>
                    <tr>
                            <td><?php echo $report['usu_nomecompleto']?> <br/></td>
                            <td><?php echo $report['tpmat_descricao']?> <br/></td>

                            <td><?php echo $report['serviten_descricao']?> <br/></td>
                            <td><?php echo $report['DT_FASE']?> <br/></td>
                    </tr>
                    <?php }//while ?>
                <?php } else {//if ?>
                    <tr>
                        <td  colspan="5" style="font-size: 28px">
                            <center>
                            Sem resultados
                            </center>
                        </td>
                    </tr>
                    
                <?php } ?>
	</tbody>
</table>

