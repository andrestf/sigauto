
<?php

$report = new AlunosRelatoriosModel();
$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE, usu_cpf, usu_renach";
$data = DataDB($_POST['data_fase']);
$report->CondicaoExtra = " AND amf_dataprocesso = '$data' ";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";

$dados  = $report->Gerar();
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-titlez" style="width: 100%">
                <center>
                    
                    DEPARTAMENTO ESTADUAL DE TRÂNSITO<br>
                    UNIDADE DE TRÂNSITO DE LENÇÓIS PAULISTA
                    <br><br>

                    <u><b> CANDIDATOS A EXAME PRÁTICO </b></u>

                </center>
                </h3><br><b>
                <?php echo strtoupper($_SESSION['APP_LOCALNOME']);?><br>
                DATA: <?php echo $_POST['data_fase']; ?>
                </b>

        </div>
        <div class="box-body">
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th with="50">Nº.</th>                        
                        <th with="*">NOME DO EXAMINADO</th>
                        <th width="110">CPF</th>
                        <th width="50">CAT.</th>
                        <th width="80">RESULTADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($dados->num_rows >= 1) {
                        $i = 1;
                        ?>
                        <?php while ($report = $dados->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $i; ?> <br/></td>
                                <td><?php echo $report['usu_nomecompleto']; ?> <br/></td>
                                <td><?php echo $report['usu_cpf']; ?> <br/></td>
                                <td><?php echo str_replace("Primeira Hab.","",$report['serv_descapelido'])?> <br/></td>
                                <td></td>
                            </tr>
                        <?php $i++;}//while ?>
                    <?php } else {//if ?>
                        <tr>
                            <td  colspan="7" style="font-size: 28px">
                    <center>
                        Sem resultados
                    </center>
                    </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
        <br/>
        <b>CARIMBO : </b>
        </div>
    </div>
</section>