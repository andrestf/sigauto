<?php

$report = new AlunosRelatoriosModel();
#$report->CondicaoExtra = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL  ";
$report->OrderBy = "usu_nomecompleto";
#$report->GroupBy = "usu_id";
#$report->Campos = "date_format(amf_dataprocesso, '%d/%m/%Y') AS DT_FASE";
$dados  = $report->Gerar();

//numero de colunas
$nColunasPagina = 6;
$nColunasStart = 0;
?>

<?php if($dados->num_rows >= 1) { ?>
    <center>

        <div style="font-size: 22px"> <b> EXAME TEÓRICO DIA <?php echo $_POST['data_fase']; ?> </b> </div>
    </center>
    <div class="" style="margin-top:-5px">

        <?php $i=0; while($report = $dados->fetch_assoc() ) {?>

            <?php if($i % 6==0){ ?>
            <?php  } $i++;?>

            <div class="col-sm-4 col-xs-4" style="height: 495px; border: solid 1px; ">
                <b><?php echo $i; ?>-
                    <?php echo $report['usu_nomecompleto']; ?> &nbsp
                    <?php echo $report['serv_descapelido']; ?> &nbsp

                    <?php
                    $dataVenc  = $report['usu_procvenc']; // 12/12/2017
                    $nTotParc  = $report['amat_nparcelas'];
                    $nVlEntrada= $report['amat_vlrentrada'];

                    $nTotParc2 = "";
                    if($nVlEntrada > 0) {
                        $nTotParc2 = $nTotParc;
                    } else {
                        $nTotParc2 = $nTotParc - 1;
                    }

                    $dtIniParc = $report['amat_venc'];
                    $dtLimite  = date('Y-m-d', strtotime('+120 days', strtotime(date('Y-m-d'))));
                    $dtVenc    = date('Y-m-d', strtotime('+'.$nTotParc2.' months', strtotime($dtIniParc)));
                    $dataHoje  = date('Y-m-d');
                    $nvlFinal  = $report['amat_vlrfinal'];
                    $nvlParc   = 0;
                    if($nTotParc > 0) {
                        $nvlParc   = ceil(($nvlFinal-$nVlEntrada) / $nTotParc2);
                    }

                    if($dataVenc <= $dtLimite) {
                        echo DataBR($dataVenc);
                    }
                    ?>;<br/>
                    FONE:<br/>
                    INSTRUTOR: </b><br/>
                Durante a Semana:<br/><br/>
                Segunda-feira:<br/><br/>
                Terça-feira:<br/><br/>
                Quarta-feira:<br/><br/>
                Quinta-feira:<br/><br/>
                Sexta-feira:<br/><br/>
                Aos Sábados:<br/>
                Já dirige:<br/>
                FERIADO:<br/>
                AULAS NOT. APÓS 18Hs<br/><br/><br/>
                <?php if( $nvlParc > 0) {
                    echo "1a R$". $nVlEntrada ." + ". $report['amat_nparcelas'] ." X R$ ". $nvlParc. " ". $report['tpd_descricao'] ." ". DataBR($dtVenc);
                } else  {
                    echo "A VISTA R$". $nVlEntrada ." ". $report['tpd_descricao'] ;
                }?><br/>
                <br/>
            </div>

            <?php $nColunasStart++; ?>
            <?php if($nColunasStart == $nColunasPagina) {
                echo "&nbsp;<br>";
                $nColunasStart = 0;
                echo "<div style='page-break-after: always;'></div>";
            } else {
                $break = "";
            }?>

        <?php } //while?>


        <?php

        $mod = ceil(($i / 6));
        $mod = ($mod*6) - $i;

        $n = (int)$mod+6;
        for($nn = 1; $nn <= $n; $nn++) { $i++;?>


            <div class="col-sm-4 col-xs-4" style="height: 495px; border: solid 1px; ">
                <b><?php echo $i; ?> -
                    <?php echo $report['usu_nomecompleto']; ?> &nbsp
                    <?php echo $report['serv_descapelido']; ?> &nbsp;<br/>

                    FONE:<br/>
                    INSTRUTOR: </b><br/>
                Durante a Semana:<br/><br/>
                Segunda-feira:<br/><br/>
                Terça-feira:<br/><br/>
                Quarta-feira:<br/><br/>
                Quinta-feira:<br/><br/>
                Sexta-feira:<br/><br/>
                Aos Sábados:<br/>
                Já dirige:<br/>
                AULAS NOT. APÓS 18Hs<br/><br/>
                <br/>
            </div>

            <?php $nColunasStart++; ?>
            <?php if($nColunasStart == $nColunasPagina) {
                echo "&nbsp;<br>";
                $nColunasStart = 0;
                echo "<div style='page-break-after: always;'></div>";
            } else {
                $break = "";
            }?>


        <?php } ?>
    </div>

<?php } else { echo "sem resultados"; } ?>
