<?php

if(is_file('../config.php')) {
	require_once '../config.php';
};

class AsGridDB {


	private $cn, $resultado, $result;
	

	public function __construct()
	{
		$this->Conexao();
	}

	public function Conexao()
	{
		$this->cn = new mysqli(AsGRID_DBHOST, AsGRID_DBUSER, AsGRID_DBPASS, AsGRID_DBNAME);

        if ($this->cn->connect_errno) {
            //echo $this->cn->connect_error;
            exit("Erro ao conectar banco de dados. Host: " . PRIN_DBHOST . " Tabela: ". PRIN_DBNAME  );
        }

        return $this->cn;

	}
	public function infos() {
		return $this->cn;
	}

	private function Query($query) {
		
		$this->result = $this->cn->query($query);
		if($this->cn->error) {
			return "ERROR => " . $this->cn->error;
		}

	}

	public function Exec($query) {
		return $this->Query($query);
	}

	public function get($query) {
		$this->Query($query);
		return $this->cn;
	}

	public function num_rows() {
		return $this->result->num_rows;
	}

	public function result_array() {
		$ret = array();
		while ($res = $this->result->fetch_assoc()) {
			$ret[] = $res;
		}

		return $ret;
	}



    public function __destruct() {
        @$this->cn->close();
    }

}
