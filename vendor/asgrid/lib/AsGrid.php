<?php

require_once 'AsGridDB.php';

class AsGrid {

	private $db;

	## Colunas da tabela
	private $columns;

	## Campos da tabela
	public $table, $where, $primary_key;
	private $fields;
	public $onDelete;


	## Campos da tabela a editar
	private $fieldsEdit, $fieldsHidden;


	## Opções da tabela
	private $options;
	public $gridActions;

	public function __construct()
	{



	}

	public function init() {
		//parent::__construct();
		if(!$this->gridActions) {
			$this->gridActions = array("edit","add","remove");
		}


		$this->db = new AsGridDB();

		$_SESSION['AsGridHASH'] = md5(sha1(md5(time()*1.3))) . sha1(md5(time()*date('ss')));
		$_SESSION['Asgrid_onDelete'] = NULL;
		$_SESSION['Asgrid_onUpdate'] = NULL;
		$_SESSION['Asgrid_onInsert'] = NULL;
		$_SESSION['Asgrid_primary_key'] = NULL;
		$_SESSION['Asgrid_table'] = NULL;		
	}

	public function _set_table($table) {
		$_SESSION['Asgrid_table'] = $table;
		$this->table = $table;
	}

	public function _set_primary_key($p) {
		$_SESSION['Asgrid_primary_key'] = $p;
		$this->primary_key = $p;
	}

	public function _onDelete($query) {
		$_SESSION['Asgrid_onDelete'] = $query;
	}

	public function _onUpdate($query) {
		$_SESSION['Asgrid_onUpdate'] = $query;
	}	

	/**
	*	Definindo as opções da tabela;
	*/
	public function setOptions($options) {
		$this->options = $options;
	}	


	/**
	* definindo as colunas
	*/
	public function setCols($cols) {
		$this->columns[] = $cols;
	}


	/**
	* Select 
	*/
	private function getDados() {
		$tabela = $this->table;
		$campos = implode("`,`",$this->fields);
		
		$where = '';
		if($this->where != '') {
			$where = " WHERE $this->where ";
		}

		$select = " SELECT `$campos` FROM `$tabela`  $where ";
		$this->db->get($select);

		if($this->db->num_rows() > 0) {
			$x = $this->db->result_array();
		}

		return $x;

		
	}


	/**
	*	Renderizando a tabela
	*/
	public function render($idGrid = "") {

		

		$cols = $this->columns;
		$cols = $cols[0];
		$table = '';


		## caption da tabela;
		########################################################
		$output = "";

		$output .= "<h3>".$this->options['caption']."</h3>";
		
		if(in_array("add", $this->gridActions)) {
			$output .= "NEW";
		}


		$output .= "<hr/>";
		$output .="<table class='table table-bordered table-condensed table-asgrid table-stripped table-hover '>";


		## montar header da tabela
		########################################################
		$output .= "<thead><tr>";
		foreach ($cols as $col_key => $col_value) {
			
			## definindo os campos da tabela (sql)
			$this->fields[] = $col_value['name'];

			## campos que podem ser editados
			if(!isset($col_value['editable']) || $col_value['editable'] == true) {
				$this->fieldsEdit[] = $col_value['name'];
			}


			## se o campo é editavel
			if (!isset($col_value['editable'])) {
				$col_value['editable'] = true;	
			}

			

			## se o campo no for oculto
			if (!isset($col_value['hidden'])) {
				$col_value['hidden'] = false;	
			}


			if(!$col_value['hidden']) {
				$width = "";
				if(isset($col_value['width'])) {
					$width = "width='".$col_value['width']."'";
				}

				$style = "";
				if(isset($col_value['style'])) {
					$style = "style='".$col_value['style']."'";
				}

				$class = "class";
				if(isset($col_value['class'])) {
					$class = $col_value['class'];
				}


				$output .= "<th $width $style class='$class' >".$col_value['title'] . "</th>";
			} else {
				$this->fieldsHidden[] = $col_value['name'];
			}
			



		}
		$output .= "<th class='col-xs-1 text-right'>Ações</th>";
		$output .= "</tr></thead>";

		## monta o corpo fa tabela
		########################################################
		$output .= "<tbody>";
			$body = $this->getDados();

			$iTR = 0;
			foreach ($body as $key) {
				$output .= "<tr id='asgrid_tr_$iTR' class='asgrid_tr '>";
				$output .= "<form id='asgrid_form_$iTR' class='asgrid_form'>";
				foreach ($key as $k => $v) {

					if($k == $this->primary_key){
						$id_registro = $v;	
					}
					
					$class = "class=' $k '";

					$td    = "<td class='asgrid_td_view'>$v</td>";

					$tdEdt = "<td class='asgrid_td_edt' style='display:none;'>";
					if(in_array($k, $this->fieldsEdit)) {
						$tdEdt .= "<input type='text' class='form-control' value='$v' name='$k' />";
					} else {
						$tdEdt .= "$v";
					}
					$tdEdt .= "</td>";

					// se o campo no esta definido como oculto
					if($this->fieldsHidden) {
						if(!in_array($k, $this->fieldsHidden)) {
							$output .= $td . $tdEdt;
						}

					//se nao houver campos para ocultar na grid
					} else {
						$output .= $td . $tdEdt;
					}

				}
					$output .= "<td class='text-right'>";
					$output .= "<i onclick='AsGridCancel($id_registro,$iTR)' class='fa btn btn-xs btn-default fa-history btnCan  asgrid_btnG1'></i> &nbsp;";
					$output .= "<i onclick='AsGridSave($id_registro,$iTR)'   class='fa btn btn-xs btn-default fa-save    btnSave asgrid_btnG1'></i> &nbsp;";
					$output .= "<i onclick='AsGridEdt($id_registro,$iTR)'    class='fa btn btn-xs btn-default fa-edit    btnEdt  asgrid_btnG0'></i> &nbsp;";
					$output .= "<i onclick='AsGridDel($id_registro,$iTR)'    class='fa btn btn-xs btn-default fa-trash   btnDel  asgrid_btnG0'></i> &nbsp;";

					$output .= "</td>";	
				$output .= "</form>";
				$output .= "</tr>";
				$iTR++;
			}


		$output .= "</tbody>";


		## final da tabela
		########################################################
		$output .=  "</table>";

		$output .= "
				<script>
					var __AsGRID_PHPLIB = '".AsGRID_PHPLIB."';
					AsGrid('".$idGrid."');
				</script>
		";




		$render = $output;


		return $render;
	}

		

}