<?php

define("AsGRID_DBTYPE","mysqli"); // mysql,oci8(for oracle),mssql,postgres,sybase
define("AsGRID_DBHOST",$_SESSION['CLI_HOST']);
define("AsGRID_DBUSER",$_SESSION['CLI_DBUSER']);
define("AsGRID_DBPASS",$_SESSION['CLI_DBPASS']);
define("AsGRID_DBNAME",$_SESSION['CLI_DBNAME']);

// Basepath for lib
define("AsGRID_PATH",dirname(__FILE__).DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR);
define("AsGRID_PHPLIB","vendor/asgrid/lib/");