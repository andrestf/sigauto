<?php

// PHP Grid database connection settings, Only need to update these in new project

define("PHPGRID_DBTYPE","mysqli"); // mysql,oci8(for oracle),mssql,postgres,sybase
define("PHPGRID_DBHOST",$_SESSION['CLI_HOST']);
define("PHPGRID_DBUSER",$_SESSION['CLI_DBUSER']);
define("PHPGRID_DBPASS",$_SESSION['CLI_DBPASS']);
define("PHPGRID_DBNAME",$_SESSION['CLI_DBNAME']);

// Basepath for lib
define("PHPGRID_LIBPATH",dirname(__FILE__).DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR);

