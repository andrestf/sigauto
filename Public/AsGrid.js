
function AsGrid() {

}


function AsGridEdt(idPK, idRow) {

	$(".asgrid_tr .asgrid_td_view" ).show();
	$(".asgrid_tr .asgrid_td_edt" ).hide();
	$(".asgrid_tr .asgrid_btnG0").show();
	$(".asgrid_tr .asgrid_btnG1").hide();


	$("#asgrid_tr_"+idRow+ " .asgrid_btnG0").hide();
	$("#asgrid_tr_"+idRow+ " .asgrid_btnG1").show();



	$("#asgrid_tr_"+idRow+ " .asgrid_td_view" ).hide();
	$("#asgrid_tr_"+idRow+ " .asgrid_td_edt" ).show();
}


function AsGridDel(idReg,rowID) {
	del = confirm("Confirmar exclusão de registro?");

	if(del) {
		$.ajax({
			type: 'POST',
			data: { idDelete: idReg, a : 'DELETE'},
			url : __AsGRID_PHPLIB + 'AsGridActions.php'
		})
	}
}


function AsGridSave(idReg,rowID) {

	xdatas = $("#asgrid_form_" + rowID).serializeArray();
	$.ajax({
		type: 'POST',
		data: xdatas,
		url : __AsGRID_PHPLIB + 'AsGridActions.php?idSavex='+idReg+"&a=UPDATE" 
	})

}

function AsGridCancel(idPK, idRow) {
	$("#asgrid_form_"+idRow)[0].reset();
	$("#asgrid_tr_"+idRow+ " .asgrid_btnG0").show();
	$("#asgrid_tr_"+idRow+ " .asgrid_btnG1").hide();

	$("#asgrid_tr_"+idRow+ " .asgrid_td_view" ).show();
	$("#asgrid_tr_"+idRow+ " .asgrid_td_edt" ).hide();	

}