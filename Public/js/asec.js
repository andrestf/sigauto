// Formata o campo CEP
function formataCEP(campo){
	campo.value = filtraCampo(campo);
	vr = campo.value;
	tam = vr.length;

	if ( tam <= 3 )
		campo.value = vr;
	if ( tam > 3 ) 
		campo.value = vr.substr(0, tam-3 ) + '-' + vr.substr(tam-3, tam);
}

// Formata data no padr?o DDMMAAAA
function formataData(campo){
	campo.value = filtraCampo(campo);
	var vr = LimparMoeda(campo.value,"0123456789");  
	tam = vr.length; 
	if ( tam <= 1 ) 
		campo.value = vr;
	if ( tam > 2 && tam < 5 )
		campo.value = vr.substr( 0, tam - 2  ) + '/' + vr.substr( tam - 2, tam );
	if ( tam >= 5 && tam <= 10 )
		campo.value = vr.substr( 0, 2 ) + '/' + vr.substr( 2, 2 ) + '/' + vr.substr( 4, 4 ); 

}

// Formata hora no padrao HH:MM
function formataHora(campo,teclapres) {
	var tecla = teclapres.keyCode;
	campo.value = filtraCampo(campo);
	vr = campo.value;
	vr = vr.replace( ".", "" );
	vr = vr.replace( ":", "" );
	vr = vr.replace( ":", "" );
	tam = vr.length + 1;

	if ( tecla != 9 && tecla != 8 ){
		if ( tam > 2 && tam < 5 )
			campo.value = vr.substr( 0, tam - 2  ) + ':' + vr.substr( tam - 2, tam );
	}
}

// Formata o campo valor
function formataValor(campo) {
	campo.value = filtraCampoValor(campo); 
	vr = campo.value;
	tam = vr.length;

	if ( tam <= 2 ){ 
 		campo.value = vr ; }
 	if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
 		
}

function formataValorSemInput(campo) {
	vr = campo;
	tam = vr.length;

	if ( tam <= 2 ){ 
 		campo = vr ; }
 	if ( (tam > 2) && (tam <= 5) ){
 		campo = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
 	
 	return campo;
 		
}

function formataValorNovoSemDecimais(campo) {
	campo.value = formataNumerico(campo);
	vr = tiraZeros(campo.value);
	tam = vr.length;

 	if ( tam <= 3 ){
 		campo.value = vr.substr( 0, tam); 
 	}
 	if ( (tam > 3) && (tam <= 6) ){
 		campo.value = vr.substr( 0, tam - 3 ) + '.' + vr.substr( tam - 3, tam ) ;
 	}
 	if ( (tam > 6) && (tam <= 9) ){
 		campo.value = vr.substr( 0, tam - 6 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 3, 3 );
 	}
 	if ( (tam > 9) && (tam <= 12) ){
 		campo.value = vr.substr( 0, tam - 9 ) + '.' + vr.substr( tam - 9, 3 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 6, 3 );
 	}
 	if ( (tam > 12) && (tam <= 15) ){
 		campo.value = vr.substr( 0, tam - 12 ) + '.' + vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 3, 3 );
 	}
 	if ( (tam > 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 15 ) + '.' + vr.substr( tam - 15, 3 ) + '.' + vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '.' + vr.substr( tam - 3, 3 );
 	} 	
 	
}

function formataValorNovo(campo) {
	campo.value = formataNumerico(campo);
	vr = tiraZeros(campo.value);
	tam = vr.length;
	if ((tam == 1) && (vr == 0)){
 		campo.value = ""; 
 	} else {
 		campo.value = "0,0" + vr ; 
 	}
 	if ( tam == 2){
 		campo.value = "0," + vr ; 
 	}		
 	if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
}

function formataValorNovo(campo) {
	campo.value = formataNumerico(campo);
	vr = tiraZeros(campo.value);
	tam = vr.length;
	if ((tam == 1) && (vr == 0)){
 		campo.value = ""; 
 	} else {
 		campo.value = "0,0" + vr ; 
 	}
 	if ( tam == 2){
 		campo.value = "0," + vr ; 
 	}		
 	if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;} 	
}

//Formata o campo valor
function formataValorLabel(campo) {
	var vr = campo;
	var tam = campo.length;

	if ( tam <= 2 ){ 
 		campo = vr ; }
 	if ( (tam > 2) && (tam <= 5) ){
 		campo = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
 	return campo;
 		
}

//Tira zeros a esquerda
function tiraZeros(valor){
    var tam=valor.length;
    var cont=0;
    var comzero = new String;
    for(var i=0;i<tam;i++){
        if (valor.substring(i,i+1) == 0) {
            comzero = comzero;
            if (cont == 1){
                comzero = comzero + valor.substring(i,i+1);
            }
        } else {
            comzero = comzero + valor.substring(i,i+1);
            cont = 1;        
        }
   	}
   	return (comzero=="")?"0":comzero;
   }


//Formata o campo valor apenas na exibi��o
function formataValorExibicao(campo) {
	campo.value = filtraCampo(campo);
	vr = campo.value;
	tam = vr.length;

	if ( tam == 1 ){
		campo.value = '0,0' + vr; }
	if ( tam == 2 ){
		campo.value = '0,' + vr; }
 	if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
}

// Formata o campo valor
function formataNumerico(campo) {

	campo.value = filtraCampo(campo);
	vr = campo.value;
	tam = vr.length;
}

// limpa todos os caracteres especiais do campo solicitado
function filtraCampo(campo){
	var s = "";
	var cp = "";
	vr = campo.value;
	tam = vr.length;
	for (i = 0; i < tam ; i++) {  
		if (vr.substring(i,i + 1) != "/" && vr.substring(i,i + 1) != "-"  && vr.substring(i,i + 1) != "," && vr.substring(i,i + 1) != "'" && vr.substring(i,i + 1) != '"' && vr.substring(i,i + 1) != "´" && vr.substring(i,i + 1) != "`" && vr.substring(i,i + 1) != "*" ){
		 	s = s + vr.substring(i,i + 1);}
	}
	campo.value = s;
	return cp = campo.value;
}
//limpa todos os caracteres especiais do campo solicitado
function filtraCampoValor(campo){
	var s = "";
	var cp = "";
	vr = campo.value;
	tam = vr.length;
	for (i = 0; i < tam ; i++) {  
		if (vr.substring(i,i + 1) >= "0" && vr.substring(i,i + 1) <= "9"){
		 	s = s + vr.substring(i,i + 1);}
	} 
	campo.value = s;
	return cp = campo.value;
}



function formataMesAno(campo){
	campo.value = filtraCampo(campo);
	vr = campo.value;
	vr = LimparMoeda(campo.value,"0123456789");
	tam = vr.length;
	if ( tam <= 2 ){ 
 		campo.value = vr ; }
	if ( tam > 2 && tam < 5 )
		campo.value = vr.substr( 0, tam - 2  ) + '/' + vr.substr( tam - 2, tam );
	if ( tam >= 5 && tam <= 10 )
		campo.value = vr.substr( 0, 2 ) + '/' + vr.substr( 2, 4 ); 
}

function formataCgc(campo) {
	campo.value = filtraCampo(campo);
	vr = LimparMoeda(campo.value,"0123456789");    
	tam = vr.length;

	if ( tam <= 2 ){ 
 		campo.value = vr ; }
 	if ( (tam > 2) && (tam <= 6) ){
 		campo.value = vr.substr( 0, tam - 2 ) + '-' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 7) && (tam <= 9) ){
 		campo.value = vr.substr( 0, tam - 6 ) + '/' + vr.substr( tam - 6, 4 ) + '-' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 10) && (tam <= 12) ){
 		campo.value = vr.substr( 0, tam - 9 ) + '.' + vr.substr( tam - 9, 3 ) + '/' + vr.substr( tam - 6, 4 ) + '-' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 13) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 12 ) + '.' + vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '/' + vr.substr( tam - 6, 4 ) + '-' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 17) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + '-' + vr.substr( tam - 2, tam ) ;}	
}


function formataCPF(campo){
	campo.value = filtraCampo(campo);
	vr = LimparMoeda(campo.value,"0123456789"); 
	tam = vr.length ;
	if ( tam <= 2 ){
 		campo.value = vr ;}
	if ( tam > 2 && tam <= 5 ){
		campo.value = vr.substr( 0, tam - 2 ) + '-' + vr.substr( tam - 2, tam );}
	if ( tam >= 6 && tam <= 8 ){
		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr(tam - 5, 3 ) + '-' + vr.substr( tam - 2, tam );}
	if ( tam >= 9 && tam <= 11 ){
		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr(tam - 5, 3 ) + '-' + vr.substr( tam - 2, tam );}

}

function formataPercentual(campo) {
	campo.value = filtraCampo(campo);
	vr = campo.value;
	tam = vr.length;

	if ( tam <= 3 ){ 
 		campo.value = vr ; }
 	if ( (tam > 3) && (tam <= 6) ){
 		campo.value = vr.substr( 0, tam - 3 ) + ',' + vr.substr( tam - 3, tam ) ; }	
}

function formataTelefone(campo) {
	campo.value = filtraCampo(campo);
	vr = campo.value;
	tam = vr.length;


	if ( tam <= 4 )
		campo.value = vr;
	if ( tam > 4 ) 
		campo.value = vr.substr(0, tam-4 ) + '-' + vr.substr(tam-4, tam);
                
                
}
function contacampo(campo, tamtxt) {
	document.forms.aapf[tamtxt].value =  1540-document.forms.aapf[campo].value.length;
}

function limita(campo){
	var tamanho = document.forms.aapf[campo].value.length;
	var tex=document.forms.aapf[campo].value;
	if (tamanho>=1539) {
		document.forms.aapf[campo].value=tex.substring(0,1539); 
	}
	return true;
}

function mudaFoco(campofoco)
{
	focaCampo(campofoco);
}

function focaCampo(campofoco) { 
	if(campofoco == '')
		campofoco = 'botaoConfirma.x';
	var num = parseInt(campofoco);
	if ( num || num == 0 )
	{
		if ( document.forms.aapf[num] ) 
		{
			try 
			{
				document.forms.aapf[num].focus();
			} catch (err) {}
		}
	}
	else 
	{
		if ( (campofoco == "senhaConta" || campofoco == "senhaAtual") && document.applets["tclJava"] ) {
			try 
			{
				document.applets["tclJava"].setFocus();
			} catch (err) {}
			
		} else if ( document.forms.aapf[campofoco] )
		{
			var campo = document.forms.aapf[campofoco];
			if(campo.length)
			{
				for (i = 0; i < campo.length; i++) {
					if (campo[i].type == 'submit'){
						try 
						{
							campo[i].focus();
						} catch (err) {}
						break;
					}
				}
			}
			else
			{
				try
				{
					campo.focus();
				} catch (err) {}
			}
		}
	}
}

function formataCartaoCredito(campo, teclapres) {
	
    var tammax = 16;
	var tecla = teclapres.keyCode;
	vr = document.forms.aapf[campo].value;

	if ( tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105) ) {
		
		vr = vr.replace( "/", "" );
		vr = vr.replace( "/", "" );
		vr = vr.replace( ",", "" );
		vr = vr.replace( ".", "" );
		vr = vr.replace( ".", "" );
		vr = vr.replace( ".", "" );
		vr = vr.replace( ".", "" );
		vr = vr.replace( "-", "" );
		vr = vr.replace( "-", "" );
		vr = vr.replace( "-", "" );
		vr = vr.replace( "-", "" );
		vr = vr.replace( "-", "" );
		tam = vr.length;

		if (tam < tammax && tecla != 8) {
		   tam = vr.length + 1 ;
		}

		if (tecla == 8 ) {
			tam = tam - 1 ;
		}
		
		if ( tam > 1 ) {
			
	        if ( vr.substr(0,1) != "3" ) {
	        	
				if ( tam < 5 ) {
					document.forms.aapf[campo].value = vr ;
				} else if ( ( tam >  4 ) && ( tam < 9 ) ) {
				   document.forms.aapf[campo].value = vr.substr( 0, 4 ) + '.' + vr.substr( 4, tam-4 ) ;
				} else if ( ( tam >  8 ) && ( tam < 13 ) ) {
				   document.forms.aapf[campo].value = vr.substr( 0, 4 ) + '.' + vr.substr( 4, 4 ) + '.' + vr.substr( 8, tam-4 ) ;
				} else if ( tam > 12 ) {
				   document.forms.aapf[campo].value = vr.substr( 0, 4 ) + '.' + vr.substr( 4, 4 ) + '.' + vr.substr( 8, 4 ) + '.' + vr.substr( 12, tam-4 );
				}
			 	
			} else {
				
				if ( tam < 5 ) {
			   	   document.forms.aapf[campo].value = vr ;
			   	} else if ( ( tam >  4 ) && ( tam < 10 ) ) {
				   document.forms.aapf[campo].value = vr.substr( 0, 4 ) + '.' + vr.substr( 4, tam-4 ) ;
				} else if ( tam >  9 ) {
				   document.forms.aapf[campo].value = vr.substr( 0, 4 ) + '.' + vr.substr( 4, 5 ) + '.' + vr.substr( 9, tam-4 ) ;
				}
			 	
			}
		
		}
		
	}	
	
}

// Fun��o utilizada pela 852/952
function escondeDiv(objeto){
	document.getElementById(objeto).style.visibility = "hidden";
}

// Controle de Abertura e Fechamento dos Boxes
function abrirDiv(idDiv){ 
	divStyle_ = document.getElementById(idDiv).style;
	btaStyle_ = document.getElementById(idDiv + '_a').style;
	btfStyle_ = document.getElementById(idDiv + '_f').style;
	divStyle_.display = ''; 
	btaStyle_.display = 'none'; 
	btfStyle_.display = ''; 
	return false; 
}

function fecharDiv(idDiv){
	divStyle_ = document.getElementById(idDiv).style;
	btaStyle_ = document.getElementById(idDiv + '_a').style;
	btfStyle_ = document.getElementById(idDiv + '_f').style;
	divStyle_.display = 'none'; 
	btaStyle_.display = ''; 
	btfStyle_.display = 'none'; 
	return false; 
}


function formataNumerico(campo) {
    var result = "";
    var vr = filtraCampo(campo);
    var tam = vr.length;
    var validChars = "0123456789";
    for (var n = 0; n < tam; n++) {
        if (validChars.indexOf(vr.substring(n, n+1)) != -1)
            result += vr.substring(n, n+1);
    }
    campo.value = result;
    return result;
}

	function verificaSessao() { 
		if(jQuery('#cronometro_div').length > 0){
			var sessaoExpirou = jQuery('#cronometro_div').cronometroSessao('isExpirouSessao');
			if (sessaoExpirou) {
				window.location = "/aapf/principal.jsp";
				return false;
			}
		}
		return true;
		
	}	
	
	function imprimirElemento(elemento){ 
		
		var html = "<html><head>";
		jQuery('link[rel=stylesheet]').each( function(index, value) {
			html+="<link rel='stylesheet' href='" + jQuery(this).attr("href") + "'/>";
		});
		
		var htmlCorpo = jQuery(elemento).html();
		
		
		var nomeElementoCorpo = $("#corpoSRP").length === 0 ? ".corpo" : "#corpoSRP";
		
		console.log(htmlCorpo);
		
		html+="</head><body><div id='"+nomeElementoCorpo+"'/>" + htmlCorpo+ "</div></body></html>";
		
		var windowUrl = 'about:blank';
		var uniqueName = new Date();
		var windowName = 'Print' + uniqueName.getTime();
		var printWindow = window.open(windowUrl, windowName, 'left=0,top=0'); 
		printWindow.document.write(html);
		printWindow.document.close();
		printWindow.focus();
		printWindow.print();
		printWindow.close();
	}

	
	/*
	 * formata valor com a quantidade de casas decimais desejada.
	 */
	function formataValorCasaDecimal(campo,qtdCasaDec) {
		try{
			if(qtdCasaDec == undefined)
				qtdCasaDec = 2;
			
			campo.value = formataNumerico(campo);
			vr = tiraZeros(campo.value);
			tam = vr.length;
			
			if ((tam == 1) && (vr == 0)){
		 		campo.value = ""; 
		 	} else if(tam <= qtdCasaDec){
		 		aux = "0,";
		 		tamCasaDecimal = qtdCasaDec-tam;
		 		for (i = 0; i < tamCasaDecimal; i++) {
		 			aux += "0";
		 		}
		 		campo.value = aux + vr;
		 	}else{
		 		inte = vr.substr( 0, tam-qtdCasaDec); //parte inteira do valor
		 		dec = vr.substr( tam-qtdCasaDec, tam);//parte decimal do valor
		 		tamInte = inte.length;
		 		aux = "";
		 		cont = 0; 
		 		for (i = tamInte-1; i >= 0; i--) {
		 			if(cont == 3){
		 				aux = "."+aux;
		 				cont = 0;
		 			}
		 			aux = inte.charAt(i) + aux;	 			
		 			cont++;
		 		}
		 		
		 		campo.value = aux + (qtdCasaDec != 0 ? "," : "") + dec;
		 	}
			
		}catch(e){}
	 	
	}	

function LimparMoeda(valor, validos) {
  // retira caracteres invalidos da string
  var result = "";
  var aux;
  for (var i=0; i < valor.length; i++) {
  aux = validos.indexOf(valor.substring(i, i+1));
  if (aux>=0) {
  result += aux;
  }
  }
  return result;
  }
	