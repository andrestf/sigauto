<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();


require_once 'Core/functions/functions.php';

require_once 'config.php';

date_default_timezone_set("America/Sao_Paulo");

global $currentModule, $CODIGO_EMPRESA,  $currentEmpresaCod;

spl_autoload_register( function($class){
   global $currentModule;
  
    //Vefiricando se é controller
    if(strpos($class,'Controller') > -1){
        $controllerName = $class;
        $controller     = $currentModule.'/Controllers/'.$class.".php";
        
        if(!file_exists($controller)) {
            $controller     = 'Modules/App/_geral/Controllers/'.$class.".php";    
        }   
        require_once $controller;
    
        
    } else if(strpos($class,'Model') > -1){
        $model =  $currentModule.'/Models/'.$class.".php";
        if(!file_exists($model)) {
            $model = 'Modules/App/_geral/Models/'.$class.".php";
        }
        require_once $model;
            
    } else if(strpos($class,'Helper') > -1){
        $helper = "Core/Helpers/".$class.".php";
        require_once $helper;
        
    } else {
        require_once "Core/".$class.".php";
    }
});